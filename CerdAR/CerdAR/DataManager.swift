//
//  DataManager.swift
//  CerdAR
//
//  Copyright (c) 2018 APPLIED TECHNOLOGY CO.,LTD., 2016 BRILLIANTSERVICE CO.,LTD., CERD (Osaka City University)
//

import Foundation
import UIKit
import MapKit
import AVFoundation
import SceneKit

/* タグに持たせるデータ群 */
class TagData {
    
    // 共通タグ
    var commonId: String!     // 共通ID(必要なのか？)
    var name: String!         // タグの名前
    var inforType: String!    // 種別(info or warn)
    var icon: String!         // 使用する画像
    var descript: String!     // 内容の解説文
    var lat: Double!          // 緯度
    var lon: Double!          // 経度
    
    var direction: Double!     // 各目的地への方角
    
    var pinNum: Int!          // ピン番号
    var pinImage: UIImage!    // タグ画像
    var expandImage: UIImage! // サイズ調節用画像
    var elevation:Double?
    
    var distance = 0     // 現在地から目的地までの距離
    
    // 情報の独自タグ
    var picType: String!      // 写真か動画か
    var photo: String!        // 写真のURL
    var movie: String!        // 動画のURL
    
    // 災害の独自タグ
    var range: Int!           // 災害の範囲
    var start: Date!        // 災害の開始時間
    var stop: Date!         // 災害の終了時間
    var message1: String!     // 警告範囲に近づいてきた時のメッセージ
    var message2: String!     // 警告範囲に侵入した時のメッセージ
    var riskType: Int!        // 災害の種類(0:火災,1:浸水,2:落橋,3:土砂崩れ)
    var isFullRange:Bool = false // 災害の範囲拡大フラグ
    deinit {
        print("\(String(describing: name)) is being deinitialized")
    }
}


class jsonDataManager: NSObject {
    // テストメッセージ出力用
    var infoBox = [TagData]() // 情報タグ用
    var warnBox = [TagData]() // 警告タグ用
    
    func storeData(json: JSON, callback: (String) -> Void) -> Void {
        
        var iN = 0 // 情報タグの番号
        var wN = 0 // 警告タグの番号
        
        for i in 0 ..< json["features"].count {
            // 情報タグ
            if json["features"][i]["properties"]["info_type"].string == kInfo {
                
                infoBox.append(TagData())
                infoBox[iN].pinNum = iN //ピン番号
                
                if let id = json["features"][i]["properties"]["id"].string { // ID
                    infoBox[iN].commonId = id
                } else {
                    infoBox.removeLast()
                    continue
                }
                
                if let name = json["features"][i]["properties"]["Name"].string { // 目的地の名前
                    infoBox[iN].name = name
                    
                } else if let name = json["features"][i]["properties"]["name"].string { // 目的地の名前
                    infoBox[iN].name = name
                    
                } else {
                    infoBox.removeLast()
                    continue
                }
                
                if let iType = json["features"][i]["properties"]["info_type"].string { // タグの種類
                    infoBox[iN].inforType = iType
                } else {
                    infoBox.removeLast()
                    continue
                }
                
                if let icon = json["features"][i]["properties"]["icon"].string, let _ = UIImage(named: icon) { // タグの画像
                    infoBox[iN].icon = icon
                } else {
                    infoBox.removeLast()
                    continue
                }
                
                if let descript = json["features"][i]["properties"]["description"].string { // 解説文
                    infoBox[iN].descript = descript
                } else {
                    if json["features"][i]["geometry"]["coordinates"][2].double != 0 { // 標高
                        infoBox.removeLast()
                        continue
                    }
                }
                
                if let lon = json["features"][i]["geometry"]["coordinates"][0].double { // 緯度
                    infoBox[iN].lon = lon
                } else {
                    infoBox.removeLast()
                    continue
                }
                
                if let lat = json["features"][i]["geometry"]["coordinates"][1].double { // 経度
                    infoBox[iN].lat = lat
                } else {
                    infoBox.removeLast()
                    continue
                }
                
                if let pType = json["features"][i]["properties"]["pic_type"].string { // 写真か動画か
                    if pType == kPhoto {
                        infoBox[iN].picType = kPhoto
                        if let pm = json["features"][i]["properties"][kPhoto].string { // 写真のURL
                            infoBox[iN].photo = pm
                        } else {
                            infoBox[iN].photo = ""
                        }
                        
                    } else if pType == kMovie {
                        infoBox[iN].picType = kMovie
                        if let pm = json["features"][i]["properties"][kMovie].string { // 動画のURL
                            infoBox[iN].movie = pm
                        } else {
                            infoBox[iN].movie = ""
                        }
                        
                    } else {
                        infoBox[iN].picType = ""
                        infoBox[iN].photo = ""
                    }
                    
                } else {
                    infoBox[iN].picType = ""
                    infoBox[iN].photo = ""
                }
                
                
                if infoBox[iN].icon == "icon_infoTag.png" {
                    
                    // 情報タグ・警告タグ
                    let labelImg = makeLabel(infoBox[iN].pinNum, inforType: infoBox[iN].inforType) // UILabelをUIImageに変換する
                    infoBox[iN].pinImage = getPinImage(labelImg, inforType: infoBox[iN].inforType)
                    infoBox[iN].expandImage = getPinImage(labelImg, inforType: infoBox[iN].inforType)
                    
                } else { // iconがicon_infoTags.png以外のとき
                    
                    var iconstr: String!
                    
                    //アイコンを増やした場合、コードを書き換えなくてもいいように修正
                    iconstr = infoBox[iN].icon
                    iconstr = (iconstr as NSString).substring(to: iconstr.count - 4)
                    iconstr = iconstr + "Map.png"
                    
                    infoBox[iN].pinImage = getResizeImage(UIImage(named: iconstr)!, newHeight: 40)
                }
               

                
                iN += 1
                
                // 警告タグ
            } else if json["features"][i]["properties"]["info_type"].string == kWarn {
                
                warnBox.append(TagData())
                warnBox[wN].pinNum = wN //ピン番号
                warnBox[wN].commonId = json["features"][i]["properties"]["id"].string // id
                
                
                if let id = json["features"][i]["properties"]["id"].string { // 目的地の名前
                    warnBox[wN].commonId = id
                } else {
                    warnBox.removeLast()
                    continue
                }
                if let icon = json["features"][i]["properties"]["icon"].string { // 目的地の名前
                    warnBox[wN].icon = icon
                }
                
                if let name = json["features"][i]["properties"]["Name"].string { // 目的地の名前
                    warnBox[wN].name = name
                    
                } else if let name = json["features"][i]["properties"]["name"].string { // 目的地の名前
                    warnBox[wN].name = name
                    
                } else {
                    warnBox.removeLast()
                    continue
                }
                
                if let iType = json["features"][i]["properties"]["info_type"].string { // タグの種類
                    warnBox[wN].inforType = iType
                } else {
                    warnBox.removeLast()
                    continue
                }
                
                if let descript = json["features"][i]["properties"]["description"].string { // 解説文
                    warnBox[wN].descript = descript
                } else {
                    warnBox.removeLast()
                    continue
                }
                
                if let lon = json["features"][i]["geometry"]["coordinates"][0].double { // 緯度
                    warnBox[wN].lon = lon
                } else {
                    warnBox.removeLast()
                    continue
                }
                
                if let lat = json["features"][i]["geometry"]["coordinates"][1].double { // 経度
                    warnBox[wN].lat = lat
                } else {
                    warnBox.removeLast()
                    continue
                }
                
                if let range = json["features"][i]["properties"]["range"].int { // 災害範囲
                    warnBox[wN].range = range
                } else {
                    warnBox.removeLast()
                    continue
                }
                
                
                if let start = json["features"][i]["properties"]["start"].string { // 災害範囲
                    warnBox[wN].start = dateFromString(start, format: "yyyy/MM/dd HH:mm", num: wN)
                    
                } else {
                    warnBox.removeLast()
                    continue
                }
                
                
                if let stop = json["features"][i]["properties"]["stop"].string { // 災害範囲
                    warnBox[wN].stop = dateFromString(stop, format: "yyyy/MM/dd HH:mm", num: wN)
                    
                } else {
                    warnBox.removeLast()
                    continue
                }
                
                
                if let message1 = json["features"][i]["properties"]["message1"].string { // 警告範囲に近づいた時のメッセージ
                    warnBox[wN].message1 = message1
                } else {
                    warnBox.removeLast()
                    continue
                }
                
                if let message2 = json["features"][i]["properties"]["message2"].string { // 警告範囲に侵入した時のメッセージ
                    warnBox[wN].message2 = message2
                } else {
                    warnBox.removeLast()
                    continue
                }
                
                
                if let rType = json["features"][i]["properties"]["risk_type"].int { // 災害の種類
                    warnBox[wN].riskType = rType
                } else {
                    warnBox.removeLast()
                    continue
                }
                
                if let pType = json["features"][i]["properties"]["pic_type"].string { // 写真か動画か
                    if pType == kPhoto {
                        warnBox[wN].picType = kPhoto
                        if let pm = json["features"][i]["properties"][kPhoto].string { // 写真のURL
                            warnBox[wN].photo = pm
                        } else {
                            warnBox[wN].photo = ""
                        }
                        
                    } else if pType == kMovie {
                        warnBox[wN].picType = kMovie
                        if let pm = json["features"][i]["properties"][kMovie].string { // 動画のURL
                            warnBox[wN].movie = pm
                        } else {
                            warnBox[wN].movie = ""
                        }
                        
                    } else {
                        warnBox[wN].picType = ""
                        warnBox[wN].photo = ""
                    }
                    
                } else {
                    warnBox[wN].picType = ""
                    warnBox[wN].photo = ""
                }
                
                circleRadius.append(0.0)
                
                wN += 1
                
            } else {
                print("info_typeの設定を間違えています") /****後でこのときの対策を考える****/
            }
        }
        
        callback("finished")
    }
    
    
    /*
     * String型で書かれた時間をNSData型に変換する
     * @param string 時間 (format通りに書く)
     * @param format "yyyy/mm/dd HH:mm"
     */
    func dateFromString(_ string: String, format: String, num: Int) -> Date {
        let formatter: DateFormatter = DateFormatter()
        formatter.locale = NSLocale.system
        formatter.timeZone = NSTimeZone.system
        formatter.dateFormat = format
        
//        formatter.locale = NSLocale(localeIdentifier: "jp_JP") as Locale!
        
        
        if let warnDate: Date = formatter.date(from: string) { // 災害時間を正しいフォーマットで書いているとき
            return warnDate
            
        } else { // // 災害時間を誤ったフォーマットで書いているとき
            warnBox[num].start = formatter.date(from: "2100/01/01 00:00")!
            return formatter.date(from: "2100/01/01 00:00")!
        }
    }
    
    /** シングルトンでインスタンスを返す */
    class var sharedInstance: jsonDataManager {
        struct Static {
            static let instance: jsonDataManager = jsonDataManager()
        }
        return Static.instance
    }
    
    // initのプライベート化。インスタンスの作成・取得はsharedInstanceを利用する。
    override private init() {
        super.init()
    }
}



// 定数
let kInfo = "info" // 種別(情報)
let kWarn = "warn" // 種別(警告(今災害が起こっている))
let kPhoto = "photo" // 写真
let kMovie = "movie" // 動画

let screenWidth = UIScreen.main.bounds.size.width   // 実機の画面の横の長さ
let screenHeight = UIScreen.main.bounds.size.height // 実機の画面の縦の長さ

let realWidth = [screenWidth,screenHeight].min()
let realHeight = [screenWidth,screenHeight].max()

let dWid = screenWidth * 0.8
let dHei = screenHeight * 0.8

var infoImageBox: [UIImageView] = [] // 画面上での情報タグ画像の表示を管理する
var warnImageBox: [UIImageView] = [] // 画面上での警告タグ画像の表示を管理する

var userLat: CLLocationDegrees = 0   // 緯度
var userLon: CLLocationDegrees = 0 // 経度

var wmsUrl: String? = "http://gisws.media.osaka-cu.ac.jp/cgi-bin/sakai_landcover_all" // WMSサーバー

let butSize: CGFloat = 70.0 // ボタンサイズ

var audioPlayerNear: AVAudioPlayer! // 通知音(付近)
var audioPlayerIntr: AVAudioPlayer! // 通知音(侵入)

/* 現在開いているページは地図画面か、ARカメラ画面か */
enum mode: Int {
    case applemap = 0
    case osm = 1
    case cam = 2
    case osmsat = 3
}

// 現在の画面が、地図かカメラかを保持する変数
var displayMode = mode.osm.rawValue
var mbStyle = "mapbox://styles/mapbox/streets-v11"

var warnImage = UIImage(named: "icon_infoTagAR.png") // 情報タグの画像

var circleRadius = [CLLocationDistance]() // 災害範囲の円の半径

// 現在地から一番近い災害の状況
enum warningState: String {
    case inst = "侵入"
    case near = "付近"
    case safe = "安全"
}

let kTagXY: CGFloat = 0 // タグのx,y座標
let kTagW: CGFloat = 0 // タグの横幅
let kTagH: CGFloat = 30 // タグの縦幅

var pinData: TagData! // タップされたタグの情報を保持

var backgroundView = UIImageView(frame: CGRect.init(x: 0, y: 0, width: screenWidth, height: screenHeight)) // 詳細画面の後ろのビュー

//let changeMapBut = UIButton(frame: CGRect.init(x: 0, y: 0, width: screenWidth / 4, height: screenHeight / 8))
let changeMapBut2 = UIButton(frame: CGRect.init(x: 0, y: 0, width: screenWidth / 3, height: screenHeight / 6))
//let changeMapBut3 = UIButton(frame: CGRect.init(x: 0, y: 0, width: screenWidth / 4, height: screenHeight / 8))
let gisInfoBut = UIButton(frame: CGRect.init(x: 0, y: 0, width: screenWidth / 3, height: screenHeight / 6))
let activeBut = UIButton(frame: CGRect.init(x: 0, y: 0, width: screenWidth / 3, height: screenHeight / 6))

/* GIS表示モード */
enum gisMode: Int {
    case none = 0
    case gis = 1
}
var gisDisplayMode = gisMode.none // GISモード表示中かどうか
let GISALPHA : CGFloat = 0.4 // 画像の透過

let subMapSizeW: CGFloat = 126.0 // AR画面の地図の横幅
let subMapSizeH: CGFloat = 84.0// AR画面の地図の横幅


let cannotTouchView = UIView(frame: CGRect.init(x: 0.0, y: 0.0, width: CGFloat(screenWidth), height: CGFloat(screenHeight))) // 画面に触れられないようにするためのビュー

let particleFire = SCNParticleSystem(named: "fire.scnp", inDirectory: "SceneKit.scnassets")

let particleRain = SCNParticleSystem(named: "rain.scnp", inDirectory: "SceneKit.scnassets")

let particleRock = SCNParticleSystem(named: "rock.scnp", inDirectory: "SceneKit.scnassets")

let particleSmoke = SCNParticleSystem(named: "smoke.scnp", inDirectory: "")

class GisData : NSObject {
    var name:String!
    var server:String!
    var glStyle:JSON!
    var legend:String!
}

class GisList : NSObject {
    var list = [GisData]()
    var selectedGis:GisData?
    func getListFromJson(json : JSON){
        for i in 0 ..< json.count{
            let item = GisData();
            item.name = json[i]["name"].string
            item.server = json[i]["server"].string
            item.glStyle = json[i]["gl_style"]
            item.legend = json[i]["legend"].string
            list.append(item)
        }
    }
    class var sharedGis: GisList {
        struct Static {
            static let instance: GisList = GisList()
        }
        return Static.instance
    }
}

﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void System.Device.Location.CivicAddress::.ctor()
extern void CivicAddress__ctor_m7F59ABD0BBD00DA425E9005597A21B6278CDB6D1 (void);
// 0x00000002 System.Void System.Device.Location.CivicAddress::set_AddressLine1(System.String)
extern void CivicAddress_set_AddressLine1_m7332B7407FE83DDEAF587C582F543D3939BD052C (void);
// 0x00000003 System.Void System.Device.Location.CivicAddress::set_AddressLine2(System.String)
extern void CivicAddress_set_AddressLine2_m78C8EA756514DF24B11385575A65C6270E5CDF37 (void);
// 0x00000004 System.Void System.Device.Location.CivicAddress::set_Building(System.String)
extern void CivicAddress_set_Building_m0CFF68430E65DBDD6EA41E2CE13775AF120D59CC (void);
// 0x00000005 System.Void System.Device.Location.CivicAddress::set_City(System.String)
extern void CivicAddress_set_City_m56F2D9F77FFE8779858128DC13144C0EFA307227 (void);
// 0x00000006 System.Void System.Device.Location.CivicAddress::set_CountryRegion(System.String)
extern void CivicAddress_set_CountryRegion_m599882DD6A8BB8F1BFA8DB1F2B1E6E413CAA0248 (void);
// 0x00000007 System.Void System.Device.Location.CivicAddress::set_FloorLevel(System.String)
extern void CivicAddress_set_FloorLevel_m2763C9EEA07862C276A52B72CC3F154D35115F76 (void);
// 0x00000008 System.Void System.Device.Location.CivicAddress::set_PostalCode(System.String)
extern void CivicAddress_set_PostalCode_m96F6E5FB7575AD8EC32BFC3FAE070217BC8D3D9F (void);
// 0x00000009 System.Void System.Device.Location.CivicAddress::set_StateProvince(System.String)
extern void CivicAddress_set_StateProvince_m4616D216C2DC3CFC119662B46F799E642E6CF2CF (void);
// 0x0000000A System.Void System.Device.Location.CivicAddress::.cctor()
extern void CivicAddress__cctor_m875CE2561270EC9FCBAEFA674A0240A962F30199 (void);
// 0x0000000B System.Void System.Device.Location.GeoCoordinate::.ctor()
extern void GeoCoordinate__ctor_mFF7956F6041D18D6CF94986C4501E12A40DF7CDB (void);
// 0x0000000C System.Void System.Device.Location.GeoCoordinate::.ctor(System.Double,System.Double)
extern void GeoCoordinate__ctor_mF5ECBAD1850E5C9468C77126EDD3B45F93526E14 (void);
// 0x0000000D System.Void System.Device.Location.GeoCoordinate::.ctor(System.Double,System.Double,System.Double)
extern void GeoCoordinate__ctor_mCE00B122E0E2CEE90C1238BC9798EC215BACA029 (void);
// 0x0000000E System.Void System.Device.Location.GeoCoordinate::.ctor(System.Double,System.Double,System.Double,System.Double,System.Double,System.Double,System.Double)
extern void GeoCoordinate__ctor_mA1FC781769EC725D1314778E2BC45D0E3A237453 (void);
// 0x0000000F System.Boolean System.Device.Location.GeoCoordinate::Equals(System.Device.Location.GeoCoordinate)
extern void GeoCoordinate_Equals_m4A5B248447B89BD204A4A7777DE0137657B29A50 (void);
// 0x00000010 System.Boolean System.Device.Location.GeoCoordinate::Equals(System.Object)
extern void GeoCoordinate_Equals_m51F02CE615151AF310DD004118B2D0487048B94B (void);
// 0x00000011 System.Double System.Device.Location.GeoCoordinate::GetDistanceTo(System.Device.Location.GeoCoordinate)
extern void GeoCoordinate_GetDistanceTo_m08A40DE4071016346FC25592DC09337FB16348D7 (void);
// 0x00000012 System.Int32 System.Device.Location.GeoCoordinate::GetHashCode()
extern void GeoCoordinate_GetHashCode_mE14FE09570E70DD73C816F1E0D0C33ABB5075AF5 (void);
// 0x00000013 System.Boolean System.Device.Location.GeoCoordinate::op_Equality(System.Device.Location.GeoCoordinate,System.Device.Location.GeoCoordinate)
extern void GeoCoordinate_op_Equality_m96E1FD033870A064072EF0AFC6BCF7C8DF0AAA53 (void);
// 0x00000014 System.String System.Device.Location.GeoCoordinate::ToString()
extern void GeoCoordinate_ToString_m675154123B5EF9382BBDBC70A31954ADE2E155B8 (void);
// 0x00000015 System.Void System.Device.Location.GeoCoordinate::set_Altitude(System.Double)
extern void GeoCoordinate_set_Altitude_m19DDB5D204D693C21DF883A59A6223AE8DB14738 (void);
// 0x00000016 System.Void System.Device.Location.GeoCoordinate::set_Course(System.Double)
extern void GeoCoordinate_set_Course_mFBEF26FB5685F62D306A039A19215BDA720A5491 (void);
// 0x00000017 System.Void System.Device.Location.GeoCoordinate::set_HorizontalAccuracy(System.Double)
extern void GeoCoordinate_set_HorizontalAccuracy_m463649A918284904B8B464B36A805A0C41DD2037 (void);
// 0x00000018 System.Double System.Device.Location.GeoCoordinate::get_Latitude()
extern void GeoCoordinate_get_Latitude_mD487CAE6C0383D9C4D0CCE2BE85B38C0E68B1888 (void);
// 0x00000019 System.Void System.Device.Location.GeoCoordinate::set_Latitude(System.Double)
extern void GeoCoordinate_set_Latitude_m6C75A7DEA0B9C1AB51EC5E592B8649F2DA63D385 (void);
// 0x0000001A System.Double System.Device.Location.GeoCoordinate::get_Longitude()
extern void GeoCoordinate_get_Longitude_m2EC0079450A74DB09F7F2B68A49D80390716711A (void);
// 0x0000001B System.Void System.Device.Location.GeoCoordinate::set_Longitude(System.Double)
extern void GeoCoordinate_set_Longitude_m78F2D97804483F51BD85805EDD5DB5C2575A7BE2 (void);
// 0x0000001C System.Void System.Device.Location.GeoCoordinate::set_Speed(System.Double)
extern void GeoCoordinate_set_Speed_m4BC694CFDFF6339660846EC8C2205A21D69F1D19 (void);
// 0x0000001D System.Void System.Device.Location.GeoCoordinate::set_VerticalAccuracy(System.Double)
extern void GeoCoordinate_set_VerticalAccuracy_m8D9AFF4893323688B2247834CD0680E5D751EFB1 (void);
// 0x0000001E System.Void System.Device.Location.GeoCoordinate::.cctor()
extern void GeoCoordinate__cctor_mB452811D6D6E8F903C2E8EEBE5B79E3676D198E8 (void);
// 0x0000001F System.Resources.ResourceManager System.Device.Properties.Resources::get_ResourceManager()
extern void Resources_get_ResourceManager_m4392F8971FB189F0151B830C4E8641CBD6B63B1F (void);
// 0x00000020 System.String System.Device.Properties.Resources::get_Argument_LatitudeOrLongitudeIsNotANumber()
extern void Resources_get_Argument_LatitudeOrLongitudeIsNotANumber_mEF075CA186C22FBFB71141EC851E0958EDC41214 (void);
// 0x00000021 System.String System.Device.Properties.Resources::get_Argument_MustBeInRangeNegative180To180()
extern void Resources_get_Argument_MustBeInRangeNegative180To180_m6FE557A9143F65CA218C0E2500F29B2444D0C654 (void);
// 0x00000022 System.String System.Device.Properties.Resources::get_Argument_MustBeInRangeNegative90to90()
extern void Resources_get_Argument_MustBeInRangeNegative90to90_m62EE743532EF0BDC91D4676912FBF9CC82880CF7 (void);
// 0x00000023 System.String System.Device.Properties.Resources::get_Argument_MustBeInRangeZeroTo360()
extern void Resources_get_Argument_MustBeInRangeZeroTo360_m921B2506D02D201C26D9D02990B26312464F687C (void);
// 0x00000024 System.String System.Device.Properties.Resources::get_Argument_MustBeNonNegative()
extern void Resources_get_Argument_MustBeNonNegative_m3ECFCC0A8407832F903B12FC0C891338C8D6E7FD (void);
static Il2CppMethodPointer s_methodPointers[36] = 
{
	CivicAddress__ctor_m7F59ABD0BBD00DA425E9005597A21B6278CDB6D1,
	CivicAddress_set_AddressLine1_m7332B7407FE83DDEAF587C582F543D3939BD052C,
	CivicAddress_set_AddressLine2_m78C8EA756514DF24B11385575A65C6270E5CDF37,
	CivicAddress_set_Building_m0CFF68430E65DBDD6EA41E2CE13775AF120D59CC,
	CivicAddress_set_City_m56F2D9F77FFE8779858128DC13144C0EFA307227,
	CivicAddress_set_CountryRegion_m599882DD6A8BB8F1BFA8DB1F2B1E6E413CAA0248,
	CivicAddress_set_FloorLevel_m2763C9EEA07862C276A52B72CC3F154D35115F76,
	CivicAddress_set_PostalCode_m96F6E5FB7575AD8EC32BFC3FAE070217BC8D3D9F,
	CivicAddress_set_StateProvince_m4616D216C2DC3CFC119662B46F799E642E6CF2CF,
	CivicAddress__cctor_m875CE2561270EC9FCBAEFA674A0240A962F30199,
	GeoCoordinate__ctor_mFF7956F6041D18D6CF94986C4501E12A40DF7CDB,
	GeoCoordinate__ctor_mF5ECBAD1850E5C9468C77126EDD3B45F93526E14,
	GeoCoordinate__ctor_mCE00B122E0E2CEE90C1238BC9798EC215BACA029,
	GeoCoordinate__ctor_mA1FC781769EC725D1314778E2BC45D0E3A237453,
	GeoCoordinate_Equals_m4A5B248447B89BD204A4A7777DE0137657B29A50,
	GeoCoordinate_Equals_m51F02CE615151AF310DD004118B2D0487048B94B,
	GeoCoordinate_GetDistanceTo_m08A40DE4071016346FC25592DC09337FB16348D7,
	GeoCoordinate_GetHashCode_mE14FE09570E70DD73C816F1E0D0C33ABB5075AF5,
	GeoCoordinate_op_Equality_m96E1FD033870A064072EF0AFC6BCF7C8DF0AAA53,
	GeoCoordinate_ToString_m675154123B5EF9382BBDBC70A31954ADE2E155B8,
	GeoCoordinate_set_Altitude_m19DDB5D204D693C21DF883A59A6223AE8DB14738,
	GeoCoordinate_set_Course_mFBEF26FB5685F62D306A039A19215BDA720A5491,
	GeoCoordinate_set_HorizontalAccuracy_m463649A918284904B8B464B36A805A0C41DD2037,
	GeoCoordinate_get_Latitude_mD487CAE6C0383D9C4D0CCE2BE85B38C0E68B1888,
	GeoCoordinate_set_Latitude_m6C75A7DEA0B9C1AB51EC5E592B8649F2DA63D385,
	GeoCoordinate_get_Longitude_m2EC0079450A74DB09F7F2B68A49D80390716711A,
	GeoCoordinate_set_Longitude_m78F2D97804483F51BD85805EDD5DB5C2575A7BE2,
	GeoCoordinate_set_Speed_m4BC694CFDFF6339660846EC8C2205A21D69F1D19,
	GeoCoordinate_set_VerticalAccuracy_m8D9AFF4893323688B2247834CD0680E5D751EFB1,
	GeoCoordinate__cctor_mB452811D6D6E8F903C2E8EEBE5B79E3676D198E8,
	Resources_get_ResourceManager_m4392F8971FB189F0151B830C4E8641CBD6B63B1F,
	Resources_get_Argument_LatitudeOrLongitudeIsNotANumber_mEF075CA186C22FBFB71141EC851E0958EDC41214,
	Resources_get_Argument_MustBeInRangeNegative180To180_m6FE557A9143F65CA218C0E2500F29B2444D0C654,
	Resources_get_Argument_MustBeInRangeNegative90to90_m62EE743532EF0BDC91D4676912FBF9CC82880CF7,
	Resources_get_Argument_MustBeInRangeZeroTo360_m921B2506D02D201C26D9D02990B26312464F687C,
	Resources_get_Argument_MustBeNonNegative_m3ECFCC0A8407832F903B12FC0C891338C8D6E7FD,
};
static const int32_t s_InvokerIndices[36] = 
{
	5515,
	4418,
	4418,
	4418,
	4418,
	4418,
	4418,
	4418,
	4418,
	8766,
	5515,
	2095,
	1362,
	120,
	3171,
	3171,
	3449,
	5392,
	7358,
	5428,
	4349,
	4349,
	4349,
	5354,
	4349,
	5354,
	4349,
	4349,
	4349,
	8766,
	8728,
	8728,
	8728,
	8728,
	8728,
	8728,
};
extern const CustomAttributesCacheGenerator g_System_Device_Portable_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_System_Device_Portable_CodeGenModule;
const Il2CppCodeGenModule g_System_Device_Portable_CodeGenModule = 
{
	"System.Device.Portable.dll",
	36,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_System_Device_Portable_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

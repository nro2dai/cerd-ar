﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
extern void EmbeddedAttribute__ctor_m90D0B6DEB625101355554D49B2EE2FB67C875860 (void);
// 0x00000002 System.Void System.Runtime.CompilerServices.IsReadOnlyAttribute::.ctor()
extern void IsReadOnlyAttribute__ctor_mF1843241F60B2240CFAE651F1FD8A7AE17E32ECD (void);
// 0x00000003 System.Void System.Runtime.CompilerServices.NullableAttribute::.ctor(System.Byte)
extern void NullableAttribute__ctor_m68337A4A4913B9D45F4B7249895084D57F47B445 (void);
// 0x00000004 System.Void System.Runtime.CompilerServices.NullableAttribute::.ctor(System.Byte[])
extern void NullableAttribute__ctor_mDD67D1F687CC4C4A36E4E580AAC3AA2EB559CFAC (void);
// 0x00000005 System.Void System.Runtime.CompilerServices.NullableContextAttribute::.ctor(System.Byte)
extern void NullableContextAttribute__ctor_mC9D42ADAC30424D8311831B59B21E77EB522453F (void);
// 0x00000006 System.Void Float::Start()
extern void Float_Start_mBBF047E2FB9FE4F6EBA52C211264418CA98D016F (void);
// 0x00000007 System.Void Float::Awake()
extern void Float_Awake_m1CB97E0F778177E7DE93F4ACB562E4FC4E57D5BC (void);
// 0x00000008 System.Void Float::Update()
extern void Float_Update_m7D3509C96925953822E1CB021F022E51B215A02B (void);
// 0x00000009 System.Void Float::OnCollisionEnter(UnityEngine.Collision)
extern void Float_OnCollisionEnter_mFD17E2825109F960D197BE896E20C6288B1248F3 (void);
// 0x0000000A System.Collections.IEnumerator Float::Delay()
extern void Float_Delay_mDE41CDCC1490C8860ED800C5C5D24D1DA9AF5712 (void);
// 0x0000000B System.Single Float::getRandomX()
extern void Float_getRandomX_mF3329C27EC7CC92D4C374905524715A39C669AE2 (void);
// 0x0000000C System.Single Float::getRandomZ()
extern void Float_getRandomZ_m5AA6E840A5340091D490D6ECA12148238B4F465F (void);
// 0x0000000D System.Void Float::.ctor()
extern void Float__ctor_mBB0E290A40E91A1DCEFA692420EA6462129EEB2F (void);
// 0x0000000E System.Void Float/<Delay>d__14::.ctor(System.Int32)
extern void U3CDelayU3Ed__14__ctor_mE0FD63AD080B1982DC52C019DAE167D1ADB61767 (void);
// 0x0000000F System.Void Float/<Delay>d__14::System.IDisposable.Dispose()
extern void U3CDelayU3Ed__14_System_IDisposable_Dispose_m145D2FBA5FB9BE406EFF8871C58A121D461F9362 (void);
// 0x00000010 System.Boolean Float/<Delay>d__14::MoveNext()
extern void U3CDelayU3Ed__14_MoveNext_m4A8BC31DA9498D20A159D8CE3DCBC6B732BC1735 (void);
// 0x00000011 System.Object Float/<Delay>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE03431CB00A455A57F2E7880170F7795EB7222AD (void);
// 0x00000012 System.Void Float/<Delay>d__14::System.Collections.IEnumerator.Reset()
extern void U3CDelayU3Ed__14_System_Collections_IEnumerator_Reset_mF0B06F2F67C1E03396DA14F60C9CA648275A2A51 (void);
// 0x00000013 System.Object Float/<Delay>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CDelayU3Ed__14_System_Collections_IEnumerator_get_Current_mFB2D6497B262DFB2D77CE7842FFE8C2DEDF21551 (void);
// 0x00000014 System.Void QuitBehavior::Start()
extern void QuitBehavior_Start_mC35A9690C2F0E124557EAE26CBC3A85BF344D84F (void);
// 0x00000015 System.Void QuitBehavior::Update()
extern void QuitBehavior_Update_m6691DE68363B980C048BCADEDB2BEF2FF1757111 (void);
// 0x00000016 System.Void QuitBehavior::OnButtonPressed()
extern void QuitBehavior_OnButtonPressed_m065E996ECA28CD5B3D867BB5EFD4A9C71AC45BD2 (void);
// 0x00000017 System.Void QuitBehavior::OnQuit()
extern void QuitBehavior_OnQuit_m17EE985A514F9CAC4E09E2F608996097571C8B1D (void);
// 0x00000018 System.Void QuitBehavior::.ctor()
extern void QuitBehavior__ctor_mF05257F242D1BFC8A92FAA85B7A599DE8F772F47 (void);
// 0x00000019 System.Void cardbroadMove::Start()
extern void cardbroadMove_Start_m7BDC779FD9C4A3AAD16B794F926E0DA2237E15C2 (void);
// 0x0000001A System.Void cardbroadMove::Update()
extern void cardbroadMove_Update_m3F92421E4C755951A9AC62EB89C7CA70BDFF7457 (void);
// 0x0000001B System.Void cardbroadMove::.ctor()
extern void cardbroadMove__ctor_m95444D018C4EE2448F6242C6744AA3D121A71C0D (void);
// 0x0000001C System.Void ARAnimationViewDemoController::Start()
extern void ARAnimationViewDemoController_Start_m4213BD7A3099E11418B19A84B28C0D750497FD80 (void);
// 0x0000001D System.Void ARAnimationViewDemoController::Update()
extern void ARAnimationViewDemoController_Update_m4D45C43D0BD5243405EFD753F66FA889DF9D688A (void);
// 0x0000001E System.Void ARAnimationViewDemoController::OnPressedDemo()
extern void ARAnimationViewDemoController_OnPressedDemo_m18E9695A5F5B3D739F522FA8D505E2AE85FE1BAC (void);
// 0x0000001F System.Void ARAnimationViewDemoController::.ctor()
extern void ARAnimationViewDemoController__ctor_m754A317CA6A6ED007C7B3502401F4DCBA42A0A88 (void);
// 0x00000020 System.Void BoxVolumeController::Start()
extern void BoxVolumeController_Start_mA6C14215598AE6024EC3900E898CDC6017404D67 (void);
// 0x00000021 System.Void BoxVolumeController::Awake()
extern void BoxVolumeController_Awake_mD68C435A89E87CC1BB87D2A10FC5C4BD3600B717 (void);
// 0x00000022 System.Void BoxVolumeController::OnEnable()
extern void BoxVolumeController_OnEnable_mDE5417DA910925843682321D44E6FD7486C18FFE (void);
// 0x00000023 System.Void BoxVolumeController::Update()
extern void BoxVolumeController_Update_m14584C19E1FA367941C6116DF3914D15703F2395 (void);
// 0x00000024 System.Void BoxVolumeController::MoveUp()
extern void BoxVolumeController_MoveUp_mD83F65DCFDFEFC2F0C5524262754C1CDB35FDB68 (void);
// 0x00000025 System.Void BoxVolumeController::MoveDown()
extern void BoxVolumeController_MoveDown_mD65D30D899D6C8DD7692AF812D5E8A3A1FC519CD (void);
// 0x00000026 System.Void BoxVolumeController::.ctor()
extern void BoxVolumeController__ctor_mE9784FF4C4605AC9152138B71716A164802543D7 (void);
// 0x00000027 System.Void DialogInfoController::Start()
extern void DialogInfoController_Start_mBA5C319A39AF33FAFA775B83C80D5A082A059867 (void);
// 0x00000028 System.Void DialogInfoController::Awake()
extern void DialogInfoController_Awake_mDE1203FC0FF993CBAC65D301B5234A15236F09F9 (void);
// 0x00000029 System.Void DialogInfoController::OnEnable()
extern void DialogInfoController_OnEnable_mA36C88B51F771D63B8BF6E0842D7F78131CA78E3 (void);
// 0x0000002A System.Void DialogInfoController::OnDisable()
extern void DialogInfoController_OnDisable_m12159A8182EB8B8C554C3454492D8B09FE94A4C6 (void);
// 0x0000002B System.Void DialogInfoController::Update()
extern void DialogInfoController_Update_m71135D349B8B4507F042FC2807343F0323A8D0F7 (void);
// 0x0000002C System.Void DialogInfoController::setFeature(JSONModel.Feature)
extern void DialogInfoController_setFeature_m31FF1490089C972E9C9F526A72F459D127EB003E (void);
// 0x0000002D System.Void DialogInfoController::close()
extern void DialogInfoController_close_m443CCB6E6C96A6DE81C9688109B04FA69A8A9113 (void);
// 0x0000002E System.Void DialogInfoController::openYoutube()
extern void DialogInfoController_openYoutube_m06F37B1CCFE340E42F0F2877875E46A95CBD1474 (void);
// 0x0000002F System.Collections.IEnumerator DialogInfoController::StartDowload()
extern void DialogInfoController_StartDowload_mD3681469F049F32970F9519352530DEC0421FA46 (void);
// 0x00000030 System.Void DialogInfoController::.ctor()
extern void DialogInfoController__ctor_mD932D72E9353CCFF2EC18A6A43F23D642CE59FDF (void);
// 0x00000031 System.Void DialogInfoController/<StartDowload>d__14::.ctor(System.Int32)
extern void U3CStartDowloadU3Ed__14__ctor_m80B41F9CF060CFD89A05F2E91FBED2D1EA3B3F43 (void);
// 0x00000032 System.Void DialogInfoController/<StartDowload>d__14::System.IDisposable.Dispose()
extern void U3CStartDowloadU3Ed__14_System_IDisposable_Dispose_m284ABF2B32514C4DDEBA66218ACC68396A8A3CEB (void);
// 0x00000033 System.Boolean DialogInfoController/<StartDowload>d__14::MoveNext()
extern void U3CStartDowloadU3Ed__14_MoveNext_mAAA46EF715B80FBA81006B6399BD4D1616607185 (void);
// 0x00000034 System.Object DialogInfoController/<StartDowload>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartDowloadU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC03C823F452769B40F191150891AA938CA9650CB (void);
// 0x00000035 System.Void DialogInfoController/<StartDowload>d__14::System.Collections.IEnumerator.Reset()
extern void U3CStartDowloadU3Ed__14_System_Collections_IEnumerator_Reset_m4ED15222CE8F23D9920B6F260B33AB1B749AF1B0 (void);
// 0x00000036 System.Object DialogInfoController/<StartDowload>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CStartDowloadU3Ed__14_System_Collections_IEnumerator_get_Current_mF63595EB73AE5D67E186F7C3ED4C5D34D19DED9B (void);
// 0x00000037 System.Void FloodController::Start()
extern void FloodController_Start_mB8C1F0229CCD844B0BF4098DEB75280B57ADEA31 (void);
// 0x00000038 System.Void FloodController::Awake()
extern void FloodController_Awake_m85B5A456E74D945B74E4DB9CBF503C208CFD89DC (void);
// 0x00000039 System.Void FloodController::OnEnable()
extern void FloodController_OnEnable_m95B061C63FBE540FA0CFE0D02BFECEB094CACF77 (void);
// 0x0000003A System.Single FloodController::GetHeight()
extern void FloodController_GetHeight_m415C73C1E28304276300AACA44EBA3219E63366D (void);
// 0x0000003B System.Void FloodController::Update()
extern void FloodController_Update_mEB7BB8DA95473A472F2EFE34CB03931A38CB5A35 (void);
// 0x0000003C System.Void FloodController::MoveUp()
extern void FloodController_MoveUp_m3F649A7A56FB27828B70A503F2066877E42F7E26 (void);
// 0x0000003D System.Void FloodController::MoveDown()
extern void FloodController_MoveDown_m9884FD11028E14B33EE66410CC569EC8AF03087A (void);
// 0x0000003E System.Void FloodController::.ctor()
extern void FloodController__ctor_m30A7E401C5404E234E5DE1FB68811A1A3ADB679B (void);
// 0x0000003F System.Void GISInfoView::Start()
extern void GISInfoView_Start_mBCC9310B1C4CFFCAB6AE5934DA2E9882F1ED6C88 (void);
// 0x00000040 System.Void GISInfoView::Update()
extern void GISInfoView_Update_m96ED216FBDCCA46B9764B976F5238EFA53880A0A (void);
// 0x00000041 System.Void GISInfoView::.ctor()
extern void GISInfoView__ctor_m667FE268E35A74CC745BEDDCC075EA0722330A53 (void);
// 0x00000042 System.Void InfoModelController::Start()
extern void InfoModelController_Start_m8B84566E08CB3C55ADAF5CA8F4FAE884410E32CC (void);
// 0x00000043 System.Void InfoModelController::OnEnable()
extern void InfoModelController_OnEnable_mD4660535FD6E2A96FD90600F1F66480428C991EE (void);
// 0x00000044 System.Void InfoModelController::Update()
extern void InfoModelController_Update_mD2DC8928610DB03758682BED61DC5C3E75403767 (void);
// 0x00000045 UnityEngine.Vector3 InfoModelController::ConvertGPS2ARCoordinate(JSONModel.Feature)
extern void InfoModelController_ConvertGPS2ARCoordinate_m3327B00A30D5BAF2D81F3128D5A23508283E4916 (void);
// 0x00000046 System.Void InfoModelController::UnityEngine.EventSystems.IPointerClickHandler.OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void InfoModelController_UnityEngine_EventSystems_IPointerClickHandler_OnPointerClick_mA61EA2FD416B0003F7C4018AF826D73C28E7A53C (void);
// 0x00000047 System.Void InfoModelController::onClick()
extern void InfoModelController_onClick_m377AC7F52F4299D9BB5F4567FBC02EBC3B686A1D (void);
// 0x00000048 System.Void InfoModelController::OnDisable()
extern void InfoModelController_OnDisable_mAA7390A9FE38714DE21C4D47856377F96A841A9C (void);
// 0x00000049 System.Void InfoModelController::OnTriggerEnter(UnityEngine.Collider)
extern void InfoModelController_OnTriggerEnter_mDBCD8FFA8851AC294923656F37393FDEEB22C27A (void);
// 0x0000004A System.Void InfoModelController::.ctor()
extern void InfoModelController__ctor_m91EF1FBA4A02CED0964CE4DCC733A6BDB7A2C7F2 (void);
// 0x0000004B System.Boolean InfoModelController::<Update>b__6_1(JSONModel.Feature)
extern void InfoModelController_U3CUpdateU3Eb__6_1_m1CED5E4B8B01A36C5F792398219AD24602738CF4 (void);
// 0x0000004C System.Boolean InfoModelController::<Update>b__6_2(JSONModel.Feature)
extern void InfoModelController_U3CUpdateU3Eb__6_2_m99975B8537464354FAABF3FE0A5317D82486E275 (void);
// 0x0000004D System.Boolean InfoModelController::<Update>b__6_3(JSONModel.Feature)
extern void InfoModelController_U3CUpdateU3Eb__6_3_mEC3CDFC97870E662B38216A94A1CEBB692A50676 (void);
// 0x0000004E System.Boolean InfoModelController::<Update>b__6_0(JSONModel.Feature)
extern void InfoModelController_U3CUpdateU3Eb__6_0_mA9E8D2A85D4F47300922D435D8DBAD311F7B6A62 (void);
// 0x0000004F System.Void MainController::Start()
extern void MainController_Start_mCB939AEB64460DA322E086C461603DE0196A3941 (void);
// 0x00000050 System.Void MainController::Awake()
extern void MainController_Awake_m8B49758963B70D3FDD86A546103BAD85FA4B9182 (void);
// 0x00000051 System.Void MainController::OnEnable()
extern void MainController_OnEnable_m3C165CC055595B26F0A65500C3CD309654DC2732 (void);
// 0x00000052 System.Void MainController::Update()
extern void MainController_Update_m4067F63F85150EEB4BDC438739554B5A10261792 (void);
// 0x00000053 System.Void MainController::updateNotice()
extern void MainController_updateNotice_m0B6D35869A8793C6BDD191BB500B8C0365391BE0 (void);
// 0x00000054 System.Collections.IEnumerator MainController::GetText()
extern void MainController_GetText_m879A1CA0C7905ECE6C2EDE59E1690F8CA79D9AB6 (void);
// 0x00000055 System.String MainController::GetDisasterLink()
extern void MainController_GetDisasterLink_mDA35761724E6563FFC18D6EAE3E7FCBA21C6CA8C (void);
// 0x00000056 System.Void MainController::Info3DView(System.Collections.Generic.List`1<JSONModel.Feature>)
extern void MainController_Info3DView_mD6F4ECB4D37559669E2AA61926694A800B2232A4 (void);
// 0x00000057 System.Void MainController::update3DView(System.Collections.Generic.List`1<JSONModel.Feature>)
extern void MainController_update3DView_mAA2A85DBE9A1C76486945E2FA1101754CB5B673B (void);
// 0x00000058 System.Void MainController::hillAnimation()
extern void MainController_hillAnimation_m6C71068D728E793DB45A94D46E42C3BA4F89F67F (void);
// 0x00000059 System.Void MainController::hosueAnimation()
extern void MainController_hosueAnimation_m87F529AE2D5BC609C76B485FC79FD6F89AF5F34F (void);
// 0x0000005A System.Void MainController::addGameObjectRandom(System.String)
extern void MainController_addGameObjectRandom_mD99984AEE2AFCD9D7429A7C8883906A1C0A7B9CB (void);
// 0x0000005B UnityEngine.Vector3 MainController::ConvertGPS2ARCoordinate(JSONModel.Feature)
extern void MainController_ConvertGPS2ARCoordinate_m545451898C96772C46D25B33FD1BBFE6BEF77359 (void);
// 0x0000005C System.Void MainController::printXYZ(UnityEngine.Vector3)
extern void MainController_printXYZ_m5F8A5F5EB53EE4A5F37B0CBD9C5E8A1EB7792C71 (void);
// 0x0000005D System.Void MainController::printXYZW(UnityEngine.Quaternion)
extern void MainController_printXYZW_mA1D25DAF76CCB0E9E87F4A51C87EF28DBF62FBCA (void);
// 0x0000005E System.Void MainController::addLiquefaction()
extern void MainController_addLiquefaction_mF61A607516E9533FB6D90DB0DC7F149DE7C04B98 (void);
// 0x0000005F System.Void MainController::resetAR()
extern void MainController_resetAR_m1CAD659AC8BD4014EF66DE9E957FB24CD32F3B6A (void);
// 0x00000060 System.Void MainController::.ctor()
extern void MainController__ctor_mD54970CDDB17F015F7D2362DB2074CA80121E22E (void);
// 0x00000061 System.Void MainController/<GetText>d__31::.ctor(System.Int32)
extern void U3CGetTextU3Ed__31__ctor_m2A4D0BB0C315CC4BA35FBC7E9506872CB3E077F2 (void);
// 0x00000062 System.Void MainController/<GetText>d__31::System.IDisposable.Dispose()
extern void U3CGetTextU3Ed__31_System_IDisposable_Dispose_m8E47247269F22FB892CF0724EE346E48D3B51CC4 (void);
// 0x00000063 System.Boolean MainController/<GetText>d__31::MoveNext()
extern void U3CGetTextU3Ed__31_MoveNext_m49E2BFCB08DBD075579926A1D6BC3303DE2370C9 (void);
// 0x00000064 System.Object MainController/<GetText>d__31::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetTextU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC720DA1060114168DCB8EC0603AE70A1F2C23E6E (void);
// 0x00000065 System.Void MainController/<GetText>d__31::System.Collections.IEnumerator.Reset()
extern void U3CGetTextU3Ed__31_System_Collections_IEnumerator_Reset_mB6449E798D29538501031A99E916B6DCBA85EDE4 (void);
// 0x00000066 System.Object MainController/<GetText>d__31::System.Collections.IEnumerator.get_Current()
extern void U3CGetTextU3Ed__31_System_Collections_IEnumerator_get_Current_m8DA2DB73D34847049C754A42FA0C6DDD9ACA9F38 (void);
// 0x00000067 System.Void ModelDestroyController::Start()
extern void ModelDestroyController_Start_m4BD1DCC65E36B93D03E48B99B70030393D17A0DD (void);
// 0x00000068 System.Void ModelDestroyController::Update()
extern void ModelDestroyController_Update_mF3855670D2EAD497005A9F80D98183970BD2453A (void);
// 0x00000069 System.Void ModelDestroyController::.ctor()
extern void ModelDestroyController__ctor_mBBED7F06188965ACD246CC9FC53D4D6B49B4B5C5 (void);
// 0x0000006A System.Void RuleWaterController::Start()
extern void RuleWaterController_Start_mD576FCF36B4F8AB4F24CAD1B2019641BAFEF92C4 (void);
// 0x0000006B System.Void RuleWaterController::Awake()
extern void RuleWaterController_Awake_m72BA9907B8F81932C53116277FF025230E5791DF (void);
// 0x0000006C System.Void RuleWaterController::OnEnable()
extern void RuleWaterController_OnEnable_m13C3621FFF9412B9084029C2146165097CDA9E13 (void);
// 0x0000006D System.Void RuleWaterController::Update()
extern void RuleWaterController_Update_mD1BCE4C85F86617F395880FB362CD19725643E9E (void);
// 0x0000006E System.Void RuleWaterController::.ctor()
extern void RuleWaterController__ctor_mF93D8C950CB3BDC3E475CBBF0C477C339CB472B1 (void);
// 0x0000006F System.Void Swing::Start()
extern void Swing_Start_m936F5AC4B1240998F29DD8DA4DB58E73931876ED (void);
// 0x00000070 System.Void Swing::Update()
extern void Swing_Update_m58743793B1E7E8653EC25FEDD3D68F0DD81216AF (void);
// 0x00000071 System.Void Swing::.ctor()
extern void Swing__ctor_mD5A004559F1511B3BC8C0F38908AED76E7C0D999 (void);
// 0x00000072 System.Void WarnAnimationController::Start()
extern void WarnAnimationController_Start_m3781D2707C18E69932A1ECC59885F61E489F8E50 (void);
// 0x00000073 System.Void WarnAnimationController::Awake()
extern void WarnAnimationController_Awake_m95D8C647B2B22D54EE97F1CD61CC3B082377B6B3 (void);
// 0x00000074 System.Void WarnAnimationController::OnEnable()
extern void WarnAnimationController_OnEnable_mD5B8C99EDEC371633DA21A6990E31A888B616124 (void);
// 0x00000075 System.Void WarnAnimationController::Update()
extern void WarnAnimationController_Update_mEAF6AE80CD51D76DC078C01CF5903F13D5A172F4 (void);
// 0x00000076 System.Void WarnAnimationController::.ctor()
extern void WarnAnimationController__ctor_m164188F58B8749DF99566B8EE878B8289C40F4F3 (void);
// 0x00000077 System.Void WarnModelController::Start()
extern void WarnModelController_Start_m27B305A22773B4E81F5405F0F56157EF93B37F51 (void);
// 0x00000078 System.Void WarnModelController::Update()
extern void WarnModelController_Update_m4B5C6144B09A32C9D0BAEB420E4F3F657D385AD4 (void);
// 0x00000079 System.Void WarnModelController::.ctor()
extern void WarnModelController__ctor_m20DAFE8F2A37308C9FB59AB85B3E53B2DA2E8EF8 (void);
// 0x0000007A System.Void liquefaction::Start()
extern void liquefaction_Start_mD2CCC8A278DD5AE0055C0988301A437FF192057D (void);
// 0x0000007B System.Void liquefaction::Awake()
extern void liquefaction_Awake_m52C53B0011B0E26693D636164933B750F313D685 (void);
// 0x0000007C System.Void liquefaction::Update()
extern void liquefaction_Update_m3585E22E6FA222EE5173513C9C3E3B99D153B2C1 (void);
// 0x0000007D System.Void liquefaction::MoveUp()
extern void liquefaction_MoveUp_mF0D05708E263953D765249E7AECB89AA59C7DE59 (void);
// 0x0000007E System.Void liquefaction::MoveDown()
extern void liquefaction_MoveDown_mF007EC3E05C5C9F41357E70FB9BB674256B3507A (void);
// 0x0000007F System.Void liquefaction::.ctor()
extern void liquefaction__ctor_m448706C8E04C937C210686CC26AFF917F4A9315A (void);
// 0x00000080 System.Void GlobalAR::.cctor()
extern void GlobalAR__cctor_mBEDADBF1C27D13870FF0525AE62061DFA5759A09 (void);
// 0x00000081 System.Void DataManager::storeData(JSONModel.Root)
extern void DataManager_storeData_mCB26B5DAE143064DA9EB8068728B57D3DBD59B37 (void);
// 0x00000082 System.Void DataManager::.ctor()
extern void DataManager__ctor_m8A6DD19CA5399C36D89F0B103A74AB7F0A7BA4DE (void);
// 0x00000083 System.Void Disaster::.ctor()
extern void Disaster__ctor_m18D90E4B89D53B5C22638EC0A067093D8111724B (void);
// 0x00000084 System.String TagAR::get_commonId()
extern void TagAR_get_commonId_m06E0974B23999E02943677D7DB6A241EE72E35DC (void);
// 0x00000085 System.Void TagAR::set_commonId(System.String)
extern void TagAR_set_commonId_m1CC83C1B94478DF1790E18E508F9E0B4C2B8D9B4 (void);
// 0x00000086 System.String TagAR::get_name()
extern void TagAR_get_name_mE6A4711ED39AB9A8FB573635F1B7E47F68821A27 (void);
// 0x00000087 System.Void TagAR::set_name(System.String)
extern void TagAR_set_name_m2868F35D04973D5C4CEFBC58869E79F6D2DF1D8C (void);
// 0x00000088 System.String TagAR::get_inforType()
extern void TagAR_get_inforType_mB82D76AA9B14680FAAF4938714ECD8BAA7DC6638 (void);
// 0x00000089 System.Void TagAR::set_inforType(System.String)
extern void TagAR_set_inforType_m160233DDA5217FB211E2DC34057FCC6C8E17927C (void);
// 0x0000008A System.String TagAR::get_icon()
extern void TagAR_get_icon_m1A9B1EC1B669A0923CC796C1589DFB263066ABAB (void);
// 0x0000008B System.Void TagAR::set_icon(System.String)
extern void TagAR_set_icon_m7433795AF34F3BB311FE37EA3E7265C7FCB8D0A0 (void);
// 0x0000008C System.String TagAR::get_descript()
extern void TagAR_get_descript_mBC86CD4F9FB9E1EE7C7557FC9D278ADF271A6823 (void);
// 0x0000008D System.Void TagAR::set_descript(System.String)
extern void TagAR_set_descript_mD43BF06B9ACE8C2604E114FF8672AA5DA3F22B20 (void);
// 0x0000008E System.Double TagAR::get_lat()
extern void TagAR_get_lat_m34BFA303CEF33D120D8525C971BA739D9731C641 (void);
// 0x0000008F System.Void TagAR::set_lat(System.Double)
extern void TagAR_set_lat_mB52DA0748926995A7EEA2DC1D5322FC47D1F2F77 (void);
// 0x00000090 System.Double TagAR::get_lon()
extern void TagAR_get_lon_mFAB46130CCB2FD8A1D219378AC63A8B0CE04A84B (void);
// 0x00000091 System.Void TagAR::set_lon(System.Double)
extern void TagAR_set_lon_m8FD7853631A0C604AE2343F878CB949908378905 (void);
// 0x00000092 System.Double TagAR::get_direction()
extern void TagAR_get_direction_mFC2DC0A23E6653B71C0BF6410DC0A32C2C9BBFA6 (void);
// 0x00000093 System.Void TagAR::set_direction(System.Double)
extern void TagAR_set_direction_m57DBED00A62F376D2598AE4433933EEA5B53DAC9 (void);
// 0x00000094 System.Int32 TagAR::get_pinNum()
extern void TagAR_get_pinNum_m619B05897744946B08845C5E26C193E1EBF3927D (void);
// 0x00000095 System.Void TagAR::set_pinNum(System.Int32)
extern void TagAR_set_pinNum_mBC606EB69D7AB256EEBE72F7EA88C929C8B501B1 (void);
// 0x00000096 System.Double TagAR::get_elevation()
extern void TagAR_get_elevation_m1F6905EDDB3F6F50FFF961AA7905C0804B1CFD2A (void);
// 0x00000097 System.Void TagAR::set_elevation(System.Double)
extern void TagAR_set_elevation_m1EFE56904E46121CE92C27882AC7BC3A9D8FDC86 (void);
// 0x00000098 System.Double TagAR::get_distance()
extern void TagAR_get_distance_mA119F27092358A14A05E82A0541B871D1C396B58 (void);
// 0x00000099 System.Void TagAR::set_distance(System.Double)
extern void TagAR_set_distance_m94D936020D9D44C041EA684F368B66D1FED6EA79 (void);
// 0x0000009A System.String TagAR::get_picType()
extern void TagAR_get_picType_mEAE09CA8FCA28C6F3462D707015EEA366370E53D (void);
// 0x0000009B System.Void TagAR::set_picType(System.String)
extern void TagAR_set_picType_mB2632221EF235F1A02015D7D9FF51E81A1F6821A (void);
// 0x0000009C System.String TagAR::get_photo()
extern void TagAR_get_photo_mEE6A54EEADBF820F9C743BAF44EB44EDC146C428 (void);
// 0x0000009D System.Void TagAR::set_photo(System.String)
extern void TagAR_set_photo_m51309930F430FE63AC2550F8461FAD0754FF8416 (void);
// 0x0000009E System.String TagAR::get_movie()
extern void TagAR_get_movie_mE050056380C10D63DD7565104EBA36957B117EB0 (void);
// 0x0000009F System.Void TagAR::set_movie(System.String)
extern void TagAR_set_movie_m3EAB144CE79D92075A846035FF69FEDCE06B7031 (void);
// 0x000000A0 System.Int32 TagAR::get_range()
extern void TagAR_get_range_m00FA628599D6E4AC9A366455CCC62DCBDC8CE5A7 (void);
// 0x000000A1 System.Void TagAR::set_range(System.Int32)
extern void TagAR_set_range_mDBF5BBCFFEB6FDC31058F10FC586E08518FA760F (void);
// 0x000000A2 System.DateTime TagAR::get_start()
extern void TagAR_get_start_m03AC44A21B9495E0565367ECF185DA5B96F3E170 (void);
// 0x000000A3 System.Void TagAR::set_start(System.DateTime)
extern void TagAR_set_start_m276BDEFC0867195040FAC35EA823F4BD66773E17 (void);
// 0x000000A4 System.DateTime TagAR::get_stop()
extern void TagAR_get_stop_m06508E9927AADEC8D6A2DDFB4BA2EEAB815C906D (void);
// 0x000000A5 System.Void TagAR::set_stop(System.DateTime)
extern void TagAR_set_stop_mE7F86A5117245DB1AC601514D9F80899C2354A71 (void);
// 0x000000A6 System.String TagAR::get_message1()
extern void TagAR_get_message1_m271B16F7FB4324BE1397A8966770A708754D3C4B (void);
// 0x000000A7 System.Void TagAR::set_message1(System.String)
extern void TagAR_set_message1_m58EE096CDFA4CDA343A6F2C9DB279B3E0B6FDF1A (void);
// 0x000000A8 System.String TagAR::get_message2()
extern void TagAR_get_message2_m3613C10AA51F6B223C243AEECAEA81062994852F (void);
// 0x000000A9 System.Void TagAR::set_message2(System.String)
extern void TagAR_set_message2_m3DAABEBE3FBAB386F46E872199A79DC4BB0164E3 (void);
// 0x000000AA System.Int32 TagAR::get_riskType()
extern void TagAR_get_riskType_m231AE1CFCF9F438FA6904C892F134E786ACA9D4A (void);
// 0x000000AB System.Void TagAR::set_riskType(System.Int32)
extern void TagAR_set_riskType_mC455E5BBB55DDFC740AFC11D1D7B7B857E19DFC2 (void);
// 0x000000AC System.Boolean TagAR::get_isFullRange()
extern void TagAR_get_isFullRange_m0092206E56073B73313F5B081C39E85E068B8F6A (void);
// 0x000000AD System.Void TagAR::set_isFullRange(System.Boolean)
extern void TagAR_set_isFullRange_mF576B581869B3DFCFFBFB000F761FAF50A327EC7 (void);
// 0x000000AE System.Void TagAR::.ctor()
extern void TagAR__ctor_mA2621368011CAD64F03E904B68D424739599C336 (void);
// 0x000000AF UnityEngine.Vector3 GeoTool::ConvertCoordinate(System.Device.Location.GeoCoordinate,System.Device.Location.GeoCoordinate,System.Single,System.Single)
extern void GeoTool_ConvertCoordinate_m13CB410AF2547AADA62D5F33B04BC42E5441D456 (void);
// 0x000000B0 UnityEngine.Vector3 GeoTool::ConvertCoordinateFromCurrentLocation(System.Device.Location.GeoCoordinate,System.Single,System.Single)
extern void GeoTool_ConvertCoordinateFromCurrentLocation_mF2401849DF83AA375D425B1636C0B4136657F400 (void);
// 0x000000B1 UnityEngine.Vector3 GeoTool::ConvertGPS2Position(System.Device.Location.GeoCoordinate,System.Single,UnityEngine.Vector3)
extern void GeoTool_ConvertGPS2Position_m0B0FC2C84414056738F0B2F8499380C909F15DB4 (void);
// 0x000000B2 System.Double GeoTool::CalculateBearing(System.Device.Location.GeoCoordinate,System.Device.Location.GeoCoordinate)
extern void GeoTool_CalculateBearing_m37615AC51758138D68AF1A3FA9C2C78D53C43949 (void);
// 0x000000B3 System.Double GeoTool::ToRadian(System.Double)
extern void GeoTool_ToRadian_mAAC4CCA66C781E803E40F474721FA5599407F25B (void);
// 0x000000B4 System.Double GeoTool::ToDegree(System.Double)
extern void GeoTool_ToDegree_m5FB03044EC34BECC6A8B571A3FBF2FD69ABF25F4 (void);
// 0x000000B5 System.Void GeoTool::.ctor()
extern void GeoTool__ctor_m3CF64CBF32393EA03E2E739B6BA0319CBDAA632E (void);
// 0x000000B6 System.Double LocationData::get_Longitude()
extern void LocationData_get_Longitude_m78A000D2F07C094788276A21D08C517EAC0D3DE6 (void);
// 0x000000B7 System.Void LocationData::set_Longitude(System.Double)
extern void LocationData_set_Longitude_mBBC9954DB21CC6D7AF75C87B52711D034C57B345 (void);
// 0x000000B8 System.Double LocationData::get_Latitude()
extern void LocationData_get_Latitude_m1E7A92D8BD857837E6B585DBBAC9CD00C35C6FB5 (void);
// 0x000000B9 System.Void LocationData::set_Latitude(System.Double)
extern void LocationData_set_Latitude_m97E091743755090A98F015CCF7C71C6544D69A96 (void);
// 0x000000BA System.Boolean LocationData::CanGetLonLat()
extern void LocationData_CanGetLonLat_m6038FA4657A34550114680BA49B5C9DB8A7D03BC (void);
// 0x000000BB System.Void LocationData::Start()
extern void LocationData_Start_m76150EE1CA69833A8B3BD32CCCFA57020036D295 (void);
// 0x000000BC System.Void LocationData::updateGPS()
extern void LocationData_updateGPS_m2E71DF54BB1CBD87FA5ED4928D2C9641E7B7A489 (void);
// 0x000000BD System.Void LocationData::.ctor()
extern void LocationData__ctor_m22C64109BD56D29FEA436011A0FF250D5A6595BF (void);
// 0x000000BE UnityEngine.UI.Button SwitchPointCloudVisualizationMode::get_toggleButton()
extern void SwitchPointCloudVisualizationMode_get_toggleButton_m5FBFEDEBC9B371B76804637D7C8091E26C3C399F (void);
// 0x000000BF System.Void SwitchPointCloudVisualizationMode::set_toggleButton(UnityEngine.UI.Button)
extern void SwitchPointCloudVisualizationMode_set_toggleButton_mD9FC8EA7EAEE800F60DAFBCCAF19636BE29ECE77 (void);
// 0x000000C0 UnityEngine.UI.Text SwitchPointCloudVisualizationMode::get_log()
extern void SwitchPointCloudVisualizationMode_get_log_m6E4EB81C73C81360C40431382CD6EB0AC2C59763 (void);
// 0x000000C1 System.Void SwitchPointCloudVisualizationMode::set_log(UnityEngine.UI.Text)
extern void SwitchPointCloudVisualizationMode_set_log_m564736BD24430DB94CCF144DABD0B6036777832C (void);
// 0x000000C2 UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer/Mode SwitchPointCloudVisualizationMode::get_mode()
extern void SwitchPointCloudVisualizationMode_get_mode_m20AA95EDFB2FFB095D258B2C38987A3BC1227616 (void);
// 0x000000C3 System.Void SwitchPointCloudVisualizationMode::set_mode(UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer/Mode)
extern void SwitchPointCloudVisualizationMode_set_mode_m280F90EBF742145758E7D52D0C84CA5AC16F0075 (void);
// 0x000000C4 System.Void SwitchPointCloudVisualizationMode::SwitchVisualizationMode()
extern void SwitchPointCloudVisualizationMode_SwitchVisualizationMode_m7CA339DEB4B8A40DFDFB8C6D79FC535048BAC278 (void);
// 0x000000C5 System.Void SwitchPointCloudVisualizationMode::OnEnable()
extern void SwitchPointCloudVisualizationMode_OnEnable_m0AE60A40E7545E590E1ACDF4FFEA32912DC1C11B (void);
// 0x000000C6 System.Void SwitchPointCloudVisualizationMode::OnPointCloudsChanged(UnityEngine.XR.ARFoundation.ARPointCloudChangedEventArgs)
extern void SwitchPointCloudVisualizationMode_OnPointCloudsChanged_mDA9644178AA492F76221956ED387EF7BC7C5EE55 (void);
// 0x000000C7 System.Void SwitchPointCloudVisualizationMode::SetMode(UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer/Mode)
extern void SwitchPointCloudVisualizationMode_SetMode_m4BC28FF39AF7600381B725286D7BCFBFE46F85AC (void);
// 0x000000C8 System.Void SwitchPointCloudVisualizationMode::.ctor()
extern void SwitchPointCloudVisualizationMode__ctor_m02EAEB06B7F42B232A6280B8E7872F6596BF35B0 (void);
// 0x000000C9 System.Void RampAsset::.ctor()
extern void RampAsset__ctor_m378D66B2BA42684D9071D8832D6DAC33ABA5E85A (void);
// 0x000000CA System.Void AdjustTimeScale::Start()
extern void AdjustTimeScale_Start_mD1396DCA527CACE33308FF8FC9C0277F2AA2B411 (void);
// 0x000000CB System.Void AdjustTimeScale::Update()
extern void AdjustTimeScale_Update_mC708147D1D143ADCA9CF17252F3CF63ADFE489EA (void);
// 0x000000CC System.Void AdjustTimeScale::OnApplicationQuit()
extern void AdjustTimeScale_OnApplicationQuit_m951AC9B09C81B79661AABC25DDCC2D7C7B5DB634 (void);
// 0x000000CD System.Void AdjustTimeScale::.ctor()
extern void AdjustTimeScale__ctor_m37475124000B20EAD78D8880212E148C74FF0552 (void);
// 0x000000CE System.Void ProximityActivate::Start()
extern void ProximityActivate_Start_m6A50614FF255868F3BA744E24F571DEE27BA4EDE (void);
// 0x000000CF System.Boolean ProximityActivate::IsTargetNear()
extern void ProximityActivate_IsTargetNear_m4703CF6E0EAD767910D51F76C2D2C55C7A90B44F (void);
// 0x000000D0 System.Void ProximityActivate::Update()
extern void ProximityActivate_Update_mE464C3E72F19E9855ACB76FE88B143E50F9C2BC5 (void);
// 0x000000D1 System.Void ProximityActivate::.ctor()
extern void ProximityActivate__ctor_m71A7C0718ECD404F98FB3B476C74DD835757E6B5 (void);
// 0x000000D2 System.Void SimpleCharacterMotor::Awake()
extern void SimpleCharacterMotor_Awake_m16349F4AB176D983169E7D079DC8844E942C49CC (void);
// 0x000000D3 System.Void SimpleCharacterMotor::Update()
extern void SimpleCharacterMotor_Update_m61B6F55DACBBFF2ECBCD5290E0EBAA926F213684 (void);
// 0x000000D4 System.Void SimpleCharacterMotor::UpdateLookRotation()
extern void SimpleCharacterMotor_UpdateLookRotation_m0239C81730CE51AA90C40883BEC2591C7325CA76 (void);
// 0x000000D5 System.Void SimpleCharacterMotor::UpdateTranslation()
extern void SimpleCharacterMotor_UpdateTranslation_mC1E99B9C99D7CFE931538E9150A0634F0D596CF6 (void);
// 0x000000D6 System.Void SimpleCharacterMotor::.ctor()
extern void SimpleCharacterMotor__ctor_mB2A4A69A8CA6E0C101E36ADEEFB4617CBAE5801B (void);
// 0x000000D7 System.Void Readme::.ctor()
extern void Readme__ctor_mF465410C5B2E598F2685E82CFCE1F42186AFF448 (void);
// 0x000000D8 System.Void Readme/Section::.ctor()
extern void Section__ctor_mBAD5262A353BC071C61B8DB462A3D4D5AB5C7C4E (void);
// 0x000000D9 System.Void UnityTemplateProjectss.SimpleCameraController::OnEnable()
extern void SimpleCameraController_OnEnable_m82F47D85DB724F0678DBEB9FF63D15C130E8F7CA (void);
// 0x000000DA UnityEngine.Vector3 UnityTemplateProjectss.SimpleCameraController::GetInputTranslationDirection()
extern void SimpleCameraController_GetInputTranslationDirection_mCD9E5C0B432565D425C5EDDED2BD2B6E7CA04D5F (void);
// 0x000000DB System.Void UnityTemplateProjectss.SimpleCameraController::Update()
extern void SimpleCameraController_Update_mCA01CFEBB977AFFB6A180BAB2DE814E9C30247BB (void);
// 0x000000DC System.Void UnityTemplateProjectss.SimpleCameraController::.ctor()
extern void SimpleCameraController__ctor_mF3F586030598332C5EA58D34699E418DA152B40A (void);
// 0x000000DD System.Void UnityTemplateProjectss.SimpleCameraController/CameraState::SetFromTransform(UnityEngine.Transform)
extern void CameraState_SetFromTransform_mC1D96C5F6D37762A9E1CF205089FB8504D42FEF1 (void);
// 0x000000DE System.Void UnityTemplateProjectss.SimpleCameraController/CameraState::Translate(UnityEngine.Vector3)
extern void CameraState_Translate_m5C6224BD5A0D3F038E2E4C85FBA25CCADD3CAFBA (void);
// 0x000000DF System.Void UnityTemplateProjectss.SimpleCameraController/CameraState::LerpTowards(UnityTemplateProjectss.SimpleCameraController/CameraState,System.Single,System.Single)
extern void CameraState_LerpTowards_mBD869D1D801FD704FE95A14557307216E3C8ACE6 (void);
// 0x000000E0 System.Void UnityTemplateProjectss.SimpleCameraController/CameraState::UpdateTransform(UnityEngine.Transform)
extern void CameraState_UpdateTransform_mFBBF4FF52A569D8C377EE45A8BB49668B897CDEB (void);
// 0x000000E1 System.Void UnityTemplateProjectss.SimpleCameraController/CameraState::.ctor()
extern void CameraState__ctor_mE07091752EB54CE0EA1103B37E34FE0AAC97BA75 (void);
// 0x000000E2 System.Void UnityTemplateProjects.SimpleCameraController::OnEnable()
extern void SimpleCameraController_OnEnable_m2BA1F31BEDE84695933E86CF731059D6FAC2111C (void);
// 0x000000E3 UnityEngine.Vector3 UnityTemplateProjects.SimpleCameraController::GetInputTranslationDirection()
extern void SimpleCameraController_GetInputTranslationDirection_m924AB4CEA66ADC4F1C63EAC21B660E0495765F4A (void);
// 0x000000E4 System.Void UnityTemplateProjects.SimpleCameraController::Update()
extern void SimpleCameraController_Update_m0E587F33074BE72C54BA835B2217B0E8EC94F24A (void);
// 0x000000E5 System.Void UnityTemplateProjects.SimpleCameraController::.ctor()
extern void SimpleCameraController__ctor_mC6AE2509DDF461856450EC7DE0058A1687AB5C87 (void);
// 0x000000E6 System.Void UnityTemplateProjects.SimpleCameraController/CameraState::SetFromTransform(UnityEngine.Transform)
extern void CameraState_SetFromTransform_mAF13C515CFB1085295C01A870D93375E98F16647 (void);
// 0x000000E7 System.Void UnityTemplateProjects.SimpleCameraController/CameraState::Translate(UnityEngine.Vector3)
extern void CameraState_Translate_mB8F7239BD9DB70190E59D47D75DD125AD9AF3A96 (void);
// 0x000000E8 System.Void UnityTemplateProjects.SimpleCameraController/CameraState::LerpTowards(UnityTemplateProjects.SimpleCameraController/CameraState,System.Single,System.Single)
extern void CameraState_LerpTowards_mF2D4B962A677B281ED2F539A2FFF8A693FB9A326 (void);
// 0x000000E9 System.Void UnityTemplateProjects.SimpleCameraController/CameraState::UpdateTransform(UnityEngine.Transform)
extern void CameraState_UpdateTransform_mE653356FD34828D19ECB6793439A14C38F372410 (void);
// 0x000000EA System.Void UnityTemplateProjects.SimpleCameraController/CameraState::.ctor()
extern void CameraState__ctor_m9C5338CABE70B8C73F8A4A08C1AFA1B33417DE9D (void);
// 0x000000EB UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer/Mode UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer::get_mode()
extern void ARAllPointCloudPointsParticleVisualizer_get_mode_mE2961E81075235D3BD24C54D522050A6A16ADF78 (void);
// 0x000000EC System.Void UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer::set_mode(UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer/Mode)
extern void ARAllPointCloudPointsParticleVisualizer_set_mode_m93C09216743B5544F7EDF9C47FCDD9CD4E6E47C9 (void);
// 0x000000ED System.Int32 UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer::get_totalPointCount()
extern void ARAllPointCloudPointsParticleVisualizer_get_totalPointCount_m82C8E588FD5467B4879E96DE8C1D8699A9FFC178 (void);
// 0x000000EE System.Void UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer::OnPointCloudChanged(UnityEngine.XR.ARFoundation.ARPointCloudUpdatedEventArgs)
extern void ARAllPointCloudPointsParticleVisualizer_OnPointCloudChanged_mAC37187B41FAA79B6084C2AE0DBE329AAA84E01E (void);
// 0x000000EF System.Void UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer::SetParticlePosition(System.Int32,UnityEngine.Vector3)
extern void ARAllPointCloudPointsParticleVisualizer_SetParticlePosition_m296F1C3C6B0EA3C9D00B67776EB3A8E64B45B5AD (void);
// 0x000000F0 System.Void UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer::RenderPoints()
extern void ARAllPointCloudPointsParticleVisualizer_RenderPoints_m9308B52D42102E948AF53BE9D349175BE43E18CB (void);
// 0x000000F1 System.Void UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer::Awake()
extern void ARAllPointCloudPointsParticleVisualizer_Awake_m2039867B6B044168F1A26C1F2EF6D0F4E3354D2D (void);
// 0x000000F2 System.Void UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer::OnEnable()
extern void ARAllPointCloudPointsParticleVisualizer_OnEnable_m1F8B642B5E8F23CADAEDC9317FA62A1BE65ACB04 (void);
// 0x000000F3 System.Void UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer::OnDisable()
extern void ARAllPointCloudPointsParticleVisualizer_OnDisable_m482462736CDA9BAF6FF73CD40214E22B251FF77F (void);
// 0x000000F4 System.Void UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer::Update()
extern void ARAllPointCloudPointsParticleVisualizer_Update_m03E104CCAE3B121C955F9C228E16EDD7758896D8 (void);
// 0x000000F5 System.Void UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer::UpdateVisibility()
extern void ARAllPointCloudPointsParticleVisualizer_UpdateVisibility_m4C296E8EC0365D27C9C0BA909417BFB3A70F3F52 (void);
// 0x000000F6 System.Void UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer::SetVisible(System.Boolean)
extern void ARAllPointCloudPointsParticleVisualizer_SetVisible_mF075BA639C161AC2F4B4457D76FBA7392664B233 (void);
// 0x000000F7 System.Void UnityEngine.XR.ARFoundation.ARAllPointCloudPointsParticleVisualizer::.ctor()
extern void ARAllPointCloudPointsParticleVisualizer__ctor_m21F6F93A91B2CDD0C2FE8B7BE9BD38B6213B4088 (void);
// 0x000000F8 System.Int32 UnityEngine.XR.ARFoundation.Samples.LogUtil::get_maxCount()
extern void LogUtil_get_maxCount_m33F0A8C7EF9E563BF520CFC49E2EBD6CF10D6BC8 (void);
// 0x000000F9 System.Void UnityEngine.XR.ARFoundation.Samples.LogUtil::set_maxCount(System.Int32)
extern void LogUtil_set_maxCount_mE6A3A0C8D138CA6B6A19CF288350BFC5D369544A (void);
// 0x000000FA System.Void UnityEngine.XR.ARFoundation.Samples.LogUtil::Log(System.String)
extern void LogUtil_Log_mF7066D27EB01A727F3211BFFE5E90AC6E4FD059C (void);
// 0x000000FB System.String UnityEngine.XR.ARFoundation.Samples.LogUtil::ToString()
extern void LogUtil_ToString_m40240CAE34215070EC176166F0224EEBED32C0EB (void);
// 0x000000FC System.Void UnityEngine.XR.ARFoundation.Samples.LogUtil::.ctor()
extern void LogUtil__ctor_mB249EAB024A57DFB2B586ADDA3DA9E6ADC0B0EA1 (void);
// 0x000000FD System.Void UnityEngine.XR.ARFoundation.Samples.ARCoreSessionRecorder::Awake()
extern void ARCoreSessionRecorder_Awake_mC95DBD06789D376B0F07208043B4FEF622261AE0 (void);
// 0x000000FE System.Int32 UnityEngine.XR.ARFoundation.Samples.ARCoreSessionRecorder::GetRotation()
extern void ARCoreSessionRecorder_GetRotation_m31EEC60D9C8C9B6F5DA2B5D68BFEB771472EEEE1 (void);
// 0x000000FF System.Void UnityEngine.XR.ARFoundation.Samples.ARCoreSessionRecorder::Log(System.String)
extern void ARCoreSessionRecorder_Log_m6EE0D73943DFC78EA266FF39FD1933908A0DD91E (void);
// 0x00000100 System.String UnityEngine.XR.ARFoundation.Samples.ARCoreSessionRecorder::GetFileSize(System.String)
extern void ARCoreSessionRecorder_GetFileSize_mA5ABB9D6E9DB1C4756D7C5E9EB70D6180255CC4B (void);
// 0x00000101 System.Void UnityEngine.XR.ARFoundation.Samples.ARCoreSessionRecorder::OnGUI()
extern void ARCoreSessionRecorder_OnGUI_mAC04B59369514266723AE8DD5FC03F4352DCCC12 (void);
// 0x00000102 System.Void UnityEngine.XR.ARFoundation.Samples.ARCoreSessionRecorder::.ctor()
extern void ARCoreSessionRecorder__ctor_m8B85D2D10CB4C35B49C1D60AA14487AA7D3988CF (void);
// 0x00000103 UnityEngine.XR.ARFoundation.ARSession UnityEngine.XR.ARFoundation.Samples.AnchorInfoManager::get_session()
extern void AnchorInfoManager_get_session_mAC00395D65559ED8101D67D6CE222A214566D78B (void);
// 0x00000104 System.Void UnityEngine.XR.ARFoundation.Samples.AnchorInfoManager::set_session(UnityEngine.XR.ARFoundation.ARSession)
extern void AnchorInfoManager_set_session_m9BB7E6023BCB8DB90737C3C648D170F88A68BA0A (void);
// 0x00000105 System.Void UnityEngine.XR.ARFoundation.Samples.AnchorInfoManager::OnEnable()
extern void AnchorInfoManager_OnEnable_mD5089C508DB2F1E3D09925B7B38D0A6D92326CAE (void);
// 0x00000106 System.Void UnityEngine.XR.ARFoundation.Samples.AnchorInfoManager::OnDisable()
extern void AnchorInfoManager_OnDisable_m31CA73A4A636067C504066970B5D788DB92E8DF0 (void);
// 0x00000107 System.Void UnityEngine.XR.ARFoundation.Samples.AnchorInfoManager::OnAnchorsChanged(UnityEngine.XR.ARFoundation.ARAnchorsChangedEventArgs)
extern void AnchorInfoManager_OnAnchorsChanged_m12C2BF7C72249689C2133443CC8546D8E8A2F85C (void);
// 0x00000108 System.Void UnityEngine.XR.ARFoundation.Samples.AnchorInfoManager::UpdateAnchor(UnityEngine.XR.ARFoundation.ARAnchor)
extern void AnchorInfoManager_UpdateAnchor_mE6350A484AA98FA4D54A575FD7D16F3CC3DAEB62 (void);
// 0x00000109 System.Void UnityEngine.XR.ARFoundation.Samples.AnchorInfoManager::.ctor()
extern void AnchorInfoManager__ctor_mD5EF181F62BFCEA9B0C9FC8DAA81762E22AF745D (void);
// 0x0000010A UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.CanvasTextManager::get_textElement()
extern void CanvasTextManager_get_textElement_m98F4CA56E78F18E0CB17C63257B6E7EED8259C9E (void);
// 0x0000010B System.Void UnityEngine.XR.ARFoundation.Samples.CanvasTextManager::set_textElement(UnityEngine.UI.Text)
extern void CanvasTextManager_set_textElement_m06DD92E78365A312FBB0E149E47F251F7EA739E1 (void);
// 0x0000010C System.String UnityEngine.XR.ARFoundation.Samples.CanvasTextManager::get_text()
extern void CanvasTextManager_get_text_m2062E9DEF643F69580D8C21EB9527F18FD6DEFA7 (void);
// 0x0000010D System.Void UnityEngine.XR.ARFoundation.Samples.CanvasTextManager::set_text(System.String)
extern void CanvasTextManager_set_text_m90560F6D945ADC53C553707EFE863405E9AE04B5 (void);
// 0x0000010E System.Void UnityEngine.XR.ARFoundation.Samples.CanvasTextManager::OnEnable()
extern void CanvasTextManager_OnEnable_m885DAF025B493B55F45C4661AF93436B6F49642C (void);
// 0x0000010F System.Void UnityEngine.XR.ARFoundation.Samples.CanvasTextManager::.ctor()
extern void CanvasTextManager__ctor_mB0F17BFDFBE6674CC915BDE2AE3E4571B8831C7C (void);
// 0x00000110 UnityEngine.UI.Image UnityEngine.XR.ARFoundation.Samples.CollaborationNetworkingIndicator::get_incomingDataImage()
extern void CollaborationNetworkingIndicator_get_incomingDataImage_m942C06B8ADB20888373B15421E68144DAC0D4294 (void);
// 0x00000111 System.Void UnityEngine.XR.ARFoundation.Samples.CollaborationNetworkingIndicator::set_incomingDataImage(UnityEngine.UI.Image)
extern void CollaborationNetworkingIndicator_set_incomingDataImage_m842DB89ED792D5FC7EEA6F043DD87195B2AFDEAD (void);
// 0x00000112 UnityEngine.UI.Image UnityEngine.XR.ARFoundation.Samples.CollaborationNetworkingIndicator::get_outgoingDataImage()
extern void CollaborationNetworkingIndicator_get_outgoingDataImage_m157F3CA2885E6469C4ED98B14A93F19BE0124E72 (void);
// 0x00000113 System.Void UnityEngine.XR.ARFoundation.Samples.CollaborationNetworkingIndicator::set_outgoingDataImage(UnityEngine.UI.Image)
extern void CollaborationNetworkingIndicator_set_outgoingDataImage_m222AB63E9A994D30CCD23885FDFC1DF0EFFFF1F2 (void);
// 0x00000114 UnityEngine.UI.Image UnityEngine.XR.ARFoundation.Samples.CollaborationNetworkingIndicator::get_hasCollaborationDataImage()
extern void CollaborationNetworkingIndicator_get_hasCollaborationDataImage_m492DDEA4332AF1BFCE9B4DCCA2D40B76817D2666 (void);
// 0x00000115 System.Void UnityEngine.XR.ARFoundation.Samples.CollaborationNetworkingIndicator::set_hasCollaborationDataImage(UnityEngine.UI.Image)
extern void CollaborationNetworkingIndicator_set_hasCollaborationDataImage_m4C745566341F8C521AFFFF64DDD88CC2671B4871 (void);
// 0x00000116 System.Void UnityEngine.XR.ARFoundation.Samples.CollaborationNetworkingIndicator::Update()
extern void CollaborationNetworkingIndicator_Update_mAD05EF0458D7F3ED4A4046F2E201317F6E6B33B0 (void);
// 0x00000117 System.Void UnityEngine.XR.ARFoundation.Samples.CollaborationNetworkingIndicator::NotifyIncomingDataReceived()
extern void CollaborationNetworkingIndicator_NotifyIncomingDataReceived_m77BADBABA4085D002A3AA28D992D7DFBF5A329C0 (void);
// 0x00000118 System.Void UnityEngine.XR.ARFoundation.Samples.CollaborationNetworkingIndicator::NotifyOutgoingDataSent()
extern void CollaborationNetworkingIndicator_NotifyOutgoingDataSent_mBC9D3784E222B83F373FE24AF2EF78411090078F (void);
// 0x00000119 System.Void UnityEngine.XR.ARFoundation.Samples.CollaborationNetworkingIndicator::NotifyHasCollaborationData()
extern void CollaborationNetworkingIndicator_NotifyHasCollaborationData_m63D7F2486BB6F32787FB6465AAE437CC2F99A188 (void);
// 0x0000011A System.Void UnityEngine.XR.ARFoundation.Samples.CollaborationNetworkingIndicator::.ctor()
extern void CollaborationNetworkingIndicator__ctor_m647BD9C91E20E2B6D539FB776E689B42C26BE82B (void);
// 0x0000011B System.String UnityEngine.XR.ARFoundation.Samples.CollaborativeSession::get_serviceType()
extern void CollaborativeSession_get_serviceType_mF84DA1EFB55084B2E3CAC5F77E4DB93FC78B17E7 (void);
// 0x0000011C System.Void UnityEngine.XR.ARFoundation.Samples.CollaborativeSession::set_serviceType(System.String)
extern void CollaborativeSession_set_serviceType_m9775C370AC63988FB2517CFAEAA2AEB4D8257885 (void);
// 0x0000011D System.Void UnityEngine.XR.ARFoundation.Samples.CollaborativeSession::DisableNotSupported(System.String)
extern void CollaborativeSession_DisableNotSupported_m3BC41057A7A69B9F833EA8F5459C2BADA62F5B86 (void);
// 0x0000011E System.Void UnityEngine.XR.ARFoundation.Samples.CollaborativeSession::OnEnable()
extern void CollaborativeSession_OnEnable_m67480DBEC60C38429FD55F5C1B4D258E7369521E (void);
// 0x0000011F UnityEngine.XR.ARKit.ARKitSessionSubsystem UnityEngine.XR.ARFoundation.Samples.CollaborativeSession::GetSubsystem()
extern void CollaborativeSession_GetSubsystem_m62F423DC23DAAA7F3C150DEB24F69FD4888CB771 (void);
// 0x00000120 System.Void UnityEngine.XR.ARFoundation.Samples.CollaborativeSession::Awake()
extern void CollaborativeSession_Awake_mB6E2CF54D1A3CC7F098D84F8FB4C083088287A88 (void);
// 0x00000121 System.Void UnityEngine.XR.ARFoundation.Samples.CollaborativeSession::OnDisable()
extern void CollaborativeSession_OnDisable_mD059673F02C82D6647D0D91A9ABDAAD61C0CAFE2 (void);
// 0x00000122 System.Void UnityEngine.XR.ARFoundation.Samples.CollaborativeSession::Update()
extern void CollaborativeSession_Update_m496BA748AEAB4FDC538A34C725E0557D947B5175 (void);
// 0x00000123 System.Void UnityEngine.XR.ARFoundation.Samples.CollaborativeSession::OnDestroy()
extern void CollaborativeSession_OnDestroy_m70B718104DF2538158E6621D83B47301E721D165 (void);
// 0x00000124 System.Void UnityEngine.XR.ARFoundation.Samples.CollaborativeSession::.ctor()
extern void CollaborativeSession__ctor_m7AEE8EDB08E4AD2B0CF4A01D7CCBDADFE1953073 (void);
// 0x00000125 UnityEngine.Renderer UnityEngine.XR.ARFoundation.Samples.Colorizer::get_renderer()
extern void Colorizer_get_renderer_m2E8570623271238C7FF0B0461F9D6949A0B5CF97 (void);
// 0x00000126 System.Void UnityEngine.XR.ARFoundation.Samples.Colorizer::set_renderer(UnityEngine.Renderer)
extern void Colorizer_set_renderer_m27B49F452058BDE0634E43B34B7D553F1D666200 (void);
// 0x00000127 UnityEngine.Color UnityEngine.XR.ARFoundation.Samples.Colorizer::get_color()
extern void Colorizer_get_color_mEE8933C6B7187BA468D6932F9A8901C2D55E1997 (void);
// 0x00000128 System.Void UnityEngine.XR.ARFoundation.Samples.Colorizer::set_color(UnityEngine.Color)
extern void Colorizer_set_color_m7D34E1242EDCF575B74F8513BEDC7165C47661C3 (void);
// 0x00000129 System.Void UnityEngine.XR.ARFoundation.Samples.Colorizer::.ctor()
extern void Colorizer__ctor_m804D41AA438D72BC09ABB83C2F30D03153947630 (void);
// 0x0000012A UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.DisplayTrackingState::get_text()
extern void DisplayTrackingState_get_text_mA66AC86FDB870AEF791116C7C3977FCA2BEB50CC (void);
// 0x0000012B System.Void UnityEngine.XR.ARFoundation.Samples.DisplayTrackingState::set_text(UnityEngine.UI.Text)
extern void DisplayTrackingState_set_text_mF51BBEB1FE7400F31014BFF58D91C03959C29392 (void);
// 0x0000012C System.Void UnityEngine.XR.ARFoundation.Samples.DisplayTrackingState::Start()
extern void DisplayTrackingState_Start_mB0355E56569B21B02F68F1E68F623E2D5C9BAFD8 (void);
// 0x0000012D System.Void UnityEngine.XR.ARFoundation.Samples.DisplayTrackingState::Update()
extern void DisplayTrackingState_Update_mC426E9A68C4D297C68BCFDDCA87D6F8F76BDF7B3 (void);
// 0x0000012E System.Void UnityEngine.XR.ARFoundation.Samples.DisplayTrackingState::.ctor()
extern void DisplayTrackingState__ctor_mDBEDD0EEB9F3660BFC6514C85EE063DB6EDB46CA (void);
// 0x0000012F UnityEngine.XR.ARKit.ARCoachingGoal UnityEngine.XR.ARFoundation.Samples.ARKitCoachingOverlay::get_goal()
extern void ARKitCoachingOverlay_get_goal_m2C6FA956DF340A2C4B2185A6BD12C903BB66A7C5 (void);
// 0x00000130 System.Void UnityEngine.XR.ARFoundation.Samples.ARKitCoachingOverlay::set_goal(UnityEngine.XR.ARKit.ARCoachingGoal)
extern void ARKitCoachingOverlay_set_goal_m47E45877012A2DFAC383C9C9A1B03644D7B0AD01 (void);
// 0x00000131 System.Boolean UnityEngine.XR.ARFoundation.Samples.ARKitCoachingOverlay::get_activatesAutomatically()
extern void ARKitCoachingOverlay_get_activatesAutomatically_mC0C9FDB4BE723C19A55BE8B4E6F80AA7A9460AE5 (void);
// 0x00000132 System.Void UnityEngine.XR.ARFoundation.Samples.ARKitCoachingOverlay::set_activatesAutomatically(System.Boolean)
extern void ARKitCoachingOverlay_set_activatesAutomatically_mEC8D8B30A1B30E8E0B4CFEA835CC9B8746B0B675 (void);
// 0x00000133 System.Boolean UnityEngine.XR.ARFoundation.Samples.ARKitCoachingOverlay::get_supported()
extern void ARKitCoachingOverlay_get_supported_m52B7E0992610E6CD4CEA565BC952F23078B8B87E (void);
// 0x00000134 System.Void UnityEngine.XR.ARFoundation.Samples.ARKitCoachingOverlay::OnEnable()
extern void ARKitCoachingOverlay_OnEnable_m02BF954ED169B30E098223A8EA083057CA80B7DF (void);
// 0x00000135 System.Void UnityEngine.XR.ARFoundation.Samples.ARKitCoachingOverlay::ActivateCoaching(System.Boolean)
extern void ARKitCoachingOverlay_ActivateCoaching_m9E8AB9C2B5864679DB2CF8E2FF286E4493687A0B (void);
// 0x00000136 System.Void UnityEngine.XR.ARFoundation.Samples.ARKitCoachingOverlay::DisableCoaching(System.Boolean)
extern void ARKitCoachingOverlay_DisableCoaching_m2FA5EBF353B1497919DCCE3AECDFC6605CFE3990 (void);
// 0x00000137 System.Void UnityEngine.XR.ARFoundation.Samples.ARKitCoachingOverlay::.ctor()
extern void ARKitCoachingOverlay__ctor_mA4F82369D31535E99798D8BB9C7869B00196F914 (void);
// 0x00000138 System.Void UnityEngine.XR.ARFoundation.Samples.CustomSessionDelegate::OnCoachingOverlayViewWillActivate(UnityEngine.XR.ARKit.ARKitSessionSubsystem)
extern void CustomSessionDelegate_OnCoachingOverlayViewWillActivate_m9D19F21C445FCBAB5F4F519344A483829297652C (void);
// 0x00000139 System.Void UnityEngine.XR.ARFoundation.Samples.CustomSessionDelegate::OnCoachingOverlayViewDidDeactivate(UnityEngine.XR.ARKit.ARKitSessionSubsystem)
extern void CustomSessionDelegate_OnCoachingOverlayViewDidDeactivate_m589A5E8ABFEA7121E31B51B2C1FA556B4BC0E813 (void);
// 0x0000013A System.Void UnityEngine.XR.ARFoundation.Samples.CustomSessionDelegate::.ctor()
extern void CustomSessionDelegate__ctor_m90330193C0F48FF70F11878743F7F11C4BF6FF0A (void);
// 0x0000013B UnityEngine.XR.ARSubsystems.Configuration UnityEngine.XR.ARFoundation.Samples.ARGeoAnchorConfigurationChooser::ChooseConfiguration(Unity.Collections.NativeSlice`1<UnityEngine.XR.ARSubsystems.ConfigurationDescriptor>,UnityEngine.XR.ARSubsystems.Feature)
extern void ARGeoAnchorConfigurationChooser_ChooseConfiguration_m79192D193AD56D7EB80879E2ABF16552CC77951D (void);
// 0x0000013C System.IntPtr UnityEngine.XR.ARFoundation.Samples.ARGeoAnchorConfigurationChooser::get_ARGeoTrackingConfigurationClass()
extern void ARGeoAnchorConfigurationChooser_get_ARGeoTrackingConfigurationClass_m420210848A1233F8B1DCE0790EC31AA5195EE852 (void);
// 0x0000013D System.Void UnityEngine.XR.ARFoundation.Samples.ARGeoAnchorConfigurationChooser::.ctor()
extern void ARGeoAnchorConfigurationChooser__ctor_m7986AF96B515ADAA712557715978F57F7893FE5E (void);
// 0x0000013E System.Void UnityEngine.XR.ARFoundation.Samples.ARGeoAnchorConfigurationChooser::.cctor()
extern void ARGeoAnchorConfigurationChooser__cctor_m1AA0D4C69A3F6512094C2CD15C451450773306CE (void);
// 0x0000013F System.Boolean UnityEngine.XR.ARFoundation.Samples.EnableGeoAnchors::get_IsSupported()
extern void EnableGeoAnchors_get_IsSupported_mD246E8D893BB4D80C1B2FF568F5C0C6F5022420C (void);
// 0x00000140 System.Void UnityEngine.XR.ARFoundation.Samples.EnableGeoAnchors::Start()
extern void EnableGeoAnchors_Start_m2C034CD73FDC07D5D2832536D9B917262E3065B0 (void);
// 0x00000141 System.Void UnityEngine.XR.ARFoundation.Samples.EnableGeoAnchors::OnGUI()
extern void EnableGeoAnchors_OnGUI_mD0BF971AA812D5798864D9B189F3B627D8710C1B (void);
// 0x00000142 System.Void UnityEngine.XR.ARFoundation.Samples.EnableGeoAnchors::Update()
extern void EnableGeoAnchors_Update_mB8118AA641BFE7E13449A1DB8E74289461527CF1 (void);
// 0x00000143 System.Void UnityEngine.XR.ARFoundation.Samples.EnableGeoAnchors::DoSomethingWithSession(System.IntPtr)
extern void EnableGeoAnchors_DoSomethingWithSession_m71D6C728E936D39A91C4B423CF254EA525D35981 (void);
// 0x00000144 System.Void UnityEngine.XR.ARFoundation.Samples.EnableGeoAnchors::AddGeoAnchor(System.IntPtr,UnityEngine.XR.ARFoundation.Samples.EnableGeoAnchors/CLLocationCoordinate2D,System.Double)
extern void EnableGeoAnchors_AddGeoAnchor_mF01DD65F28D0F2CD638CEEC0847198C3F6794F5E (void);
// 0x00000145 System.Void UnityEngine.XR.ARFoundation.Samples.EnableGeoAnchors::.ctor()
extern void EnableGeoAnchors__ctor_mEC8F33255E4A714FCA782B3D8A3950A644C6A672 (void);
// 0x00000146 UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.FeaturesReporting::get_autoFocusText()
extern void FeaturesReporting_get_autoFocusText_m4DB17DF766A36ADB9C36A27B8E6BD43AC48FAB6F (void);
// 0x00000147 System.Void UnityEngine.XR.ARFoundation.Samples.FeaturesReporting::set_autoFocusText(UnityEngine.UI.Text)
extern void FeaturesReporting_set_autoFocusText_mA3488E9E5E19CE38B07C23FF014341BCE3B7B0F6 (void);
// 0x00000148 UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.FeaturesReporting::get_environmentDepthText()
extern void FeaturesReporting_get_environmentDepthText_m6E388C6A68EEF58399CEF4F36377AC6D39398ABA (void);
// 0x00000149 System.Void UnityEngine.XR.ARFoundation.Samples.FeaturesReporting::set_environmentDepthText(UnityEngine.UI.Text)
extern void FeaturesReporting_set_environmentDepthText_m40D52051439A0D77DD0BC2DDCAEF2929BFF5F2D4 (void);
// 0x0000014A UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.FeaturesReporting::get_environmentProbesText()
extern void FeaturesReporting_get_environmentProbesText_mEF1E4C02FC503935D8D1A581F924877E235D3DEF (void);
// 0x0000014B System.Void UnityEngine.XR.ARFoundation.Samples.FeaturesReporting::set_environmentProbesText(UnityEngine.UI.Text)
extern void FeaturesReporting_set_environmentProbesText_mEA7FAB36D89AF22C87A44C382179A7802E92141B (void);
// 0x0000014C UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.FeaturesReporting::get_faceTrackingText()
extern void FeaturesReporting_get_faceTrackingText_mDA057BE918392E4EC7CB2AD27DED7479C5A733B8 (void);
// 0x0000014D System.Void UnityEngine.XR.ARFoundation.Samples.FeaturesReporting::set_faceTrackingText(UnityEngine.UI.Text)
extern void FeaturesReporting_set_faceTrackingText_m45D932C2D869912DA652538A7DAB0BBA5E961A45 (void);
// 0x0000014E UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.FeaturesReporting::get_humanDepthText()
extern void FeaturesReporting_get_humanDepthText_m934A7312EC1F07298E149D58CF12C382CEBDC765 (void);
// 0x0000014F System.Void UnityEngine.XR.ARFoundation.Samples.FeaturesReporting::set_humanDepthText(UnityEngine.UI.Text)
extern void FeaturesReporting_set_humanDepthText_m8718AB65C5EF4157CBE40D4FF81AC97176EEBC4B (void);
// 0x00000150 UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.FeaturesReporting::get_humanStencilText()
extern void FeaturesReporting_get_humanStencilText_m6A226E47D1B26EC9C9FCCDC890E52CB4DA96BA95 (void);
// 0x00000151 System.Void UnityEngine.XR.ARFoundation.Samples.FeaturesReporting::set_humanStencilText(UnityEngine.UI.Text)
extern void FeaturesReporting_set_humanStencilText_m462C444AF832BEA115EE30C79EB07C7F0D16B6C2 (void);
// 0x00000152 UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.FeaturesReporting::get_lightEstimationColorText()
extern void FeaturesReporting_get_lightEstimationColorText_m710DED0B274D6FE9A061D951819A41F810A338D0 (void);
// 0x00000153 System.Void UnityEngine.XR.ARFoundation.Samples.FeaturesReporting::set_lightEstimationColorText(UnityEngine.UI.Text)
extern void FeaturesReporting_set_lightEstimationColorText_mEAE2E0FE2A135D25A2588271B03CEDF565B9ADD7 (void);
// 0x00000154 UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.FeaturesReporting::get_lightEstimationIntensityText()
extern void FeaturesReporting_get_lightEstimationIntensityText_m7FFF77A585791D3FF72A7EF08AC0643F73D8E13E (void);
// 0x00000155 System.Void UnityEngine.XR.ARFoundation.Samples.FeaturesReporting::set_lightEstimationIntensityText(UnityEngine.UI.Text)
extern void FeaturesReporting_set_lightEstimationIntensityText_mBC3A90538496C03899C9ACC51F1EC06278ACA64D (void);
// 0x00000156 UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.FeaturesReporting::get_meshingText()
extern void FeaturesReporting_get_meshingText_m6EF32DA1D93DBA0C671D6B44D326149DAEC7B00D (void);
// 0x00000157 System.Void UnityEngine.XR.ARFoundation.Samples.FeaturesReporting::set_meshingText(UnityEngine.UI.Text)
extern void FeaturesReporting_set_meshingText_mF7D0F28BE9859D73CA1BCF900D7FD5BEB3A8597A (void);
// 0x00000158 UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.FeaturesReporting::get_meshingClassificationText()
extern void FeaturesReporting_get_meshingClassificationText_m06B683EA5DF9D3344704AFEA9FD449343EE58EF6 (void);
// 0x00000159 System.Void UnityEngine.XR.ARFoundation.Samples.FeaturesReporting::set_meshingClassificationText(UnityEngine.UI.Text)
extern void FeaturesReporting_set_meshingClassificationText_m98DA0A404C990264E25370C447D06828B37FDC77 (void);
// 0x0000015A UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.FeaturesReporting::get_planeTrackingText()
extern void FeaturesReporting_get_planeTrackingText_m8DBC17D99798CB2BB999FA1A32A253D1B3AB446D (void);
// 0x0000015B System.Void UnityEngine.XR.ARFoundation.Samples.FeaturesReporting::set_planeTrackingText(UnityEngine.UI.Text)
extern void FeaturesReporting_set_planeTrackingText_mBE255188ABDCCFD0AC3A0281B0AC1B758F2E56EE (void);
// 0x0000015C UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.FeaturesReporting::get_raycastingText()
extern void FeaturesReporting_get_raycastingText_m5C5EED8E315A1742BDFAFCF7F1332975793F1FBF (void);
// 0x0000015D System.Void UnityEngine.XR.ARFoundation.Samples.FeaturesReporting::set_raycastingText(UnityEngine.UI.Text)
extern void FeaturesReporting_set_raycastingText_mE840DB465CEA3381AEF23C8AAA7FE71ECAE5C0EB (void);
// 0x0000015E UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.FeaturesReporting::get_rotationAndOrientationText()
extern void FeaturesReporting_get_rotationAndOrientationText_mED0576EB820393DA87E019FFD0D49A5E8EFE85C5 (void);
// 0x0000015F System.Void UnityEngine.XR.ARFoundation.Samples.FeaturesReporting::set_rotationAndOrientationText(UnityEngine.UI.Text)
extern void FeaturesReporting_set_rotationAndOrientationText_mE3E278D9EAF5AD2411A519D1B4CFD1924BC7803A (void);
// 0x00000160 UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.FeaturesReporting::get_worldFacingCameraText()
extern void FeaturesReporting_get_worldFacingCameraText_m7D7CC909FF7FDF2D4ABB4701E995E02BEDE9241E (void);
// 0x00000161 System.Void UnityEngine.XR.ARFoundation.Samples.FeaturesReporting::set_worldFacingCameraText(UnityEngine.UI.Text)
extern void FeaturesReporting_set_worldFacingCameraText_mDCFA0473F21196D68C29017771F19956D7A09C32 (void);
// 0x00000162 UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.FeaturesReporting::get_sessionFpsText()
extern void FeaturesReporting_get_sessionFpsText_m1EF17B2A8AF961F812D167D791C7840F5A61349B (void);
// 0x00000163 System.Void UnityEngine.XR.ARFoundation.Samples.FeaturesReporting::set_sessionFpsText(UnityEngine.UI.Text)
extern void FeaturesReporting_set_sessionFpsText_m84702398751D0BF225A35F2E047806929DA96783 (void);
// 0x00000164 System.Void UnityEngine.XR.ARFoundation.Samples.FeaturesReporting::Awake()
extern void FeaturesReporting_Awake_mF10B49BB7BB4DC2092FAC9015CFF8185ECA12D93 (void);
// 0x00000165 System.Void UnityEngine.XR.ARFoundation.Samples.FeaturesReporting::Update()
extern void FeaturesReporting_Update_m90D72FEA5DCD33CD35B54C1FBADC469C20D252B2 (void);
// 0x00000166 System.Void UnityEngine.XR.ARFoundation.Samples.FeaturesReporting::SetFeatureDisplayState(UnityEngine.UI.Text,System.Boolean)
extern void FeaturesReporting_SetFeatureDisplayState_m6FC99AEC79D0E054D6E411298573417EDA0DE08D (void);
// 0x00000167 System.Void UnityEngine.XR.ARFoundation.Samples.FeaturesReporting::.ctor()
extern void FeaturesReporting__ctor_mEA6B02A061DEA341C7CC9C61D41E1C1AB5C3CBB9 (void);
// 0x00000168 UnityEngine.XR.ARFoundation.Samples.ThermalStateForIOS/ThermalState UnityEngine.XR.ARFoundation.Samples.ThermalStateForIOS::get_currentThermalState()
extern void ThermalStateForIOS_get_currentThermalState_m23F11F76FCF55B5E2B3E5F50A293C581893BCA6E (void);
// 0x00000169 System.Void UnityEngine.XR.ARFoundation.Samples.ThermalStateForIOS::OnEnable()
extern void ThermalStateForIOS_OnEnable_m0B57658E184FC211DF0F6E861547CE084D7183DA (void);
// 0x0000016A System.Void UnityEngine.XR.ARFoundation.Samples.ThermalStateForIOS::Update()
extern void ThermalStateForIOS_Update_mF80F0FB2E572A3DBF55329BC54CE0625B77C8CE9 (void);
// 0x0000016B System.Void UnityEngine.XR.ARFoundation.Samples.ThermalStateForIOS::.ctor()
extern void ThermalStateForIOS__ctor_m5F87C1E362BE78CD6BC930E402A95B1929CD9948 (void);
// 0x0000016C UnityEngine.XR.ARFoundation.Samples.ThermalStateForIOS/ThermalState UnityEngine.XR.ARFoundation.Samples.ThermalStateForIOS/ThermalStateChange::get_previousThermalState()
extern void ThermalStateChange_get_previousThermalState_mC8EA5EEB4C6D830DC54841A68E19E8D96D4C3B97 (void);
// 0x0000016D UnityEngine.XR.ARFoundation.Samples.ThermalStateForIOS/ThermalState UnityEngine.XR.ARFoundation.Samples.ThermalStateForIOS/ThermalStateChange::get_currentThermalState()
extern void ThermalStateChange_get_currentThermalState_mEC7E8C8DC348064CE3DB492C6C4207656A7F6FB8 (void);
// 0x0000016E System.Void UnityEngine.XR.ARFoundation.Samples.ThermalStateForIOS/ThermalStateChange::.ctor(UnityEngine.XR.ARFoundation.Samples.ThermalStateForIOS/ThermalState,UnityEngine.XR.ARFoundation.Samples.ThermalStateForIOS/ThermalState)
extern void ThermalStateChange__ctor_m2F03DE32F10B12C5168435245D47607951A49515 (void);
// 0x0000016F UnityEngine.XR.ARFoundation.Samples.ThermalStateForIOS/ThermalState UnityEngine.XR.ARFoundation.Samples.ThermalStateForIOS/NativeApi::GetCurrentThermalState()
extern void NativeApi_GetCurrentThermalState_mC518B4EA3B6AE785F53F3CFC6404F10527A299DB (void);
// 0x00000170 UnityEngine.XR.ARFoundation.Samples.ThermalStateForIOS UnityEngine.XR.ARFoundation.Samples.ThermalStateHandling::get_thermalStateForIOS()
extern void ThermalStateHandling_get_thermalStateForIOS_m56BF7D84309E859D824535215AAFE6B151386551 (void);
// 0x00000171 System.Void UnityEngine.XR.ARFoundation.Samples.ThermalStateHandling::set_thermalStateForIOS(UnityEngine.XR.ARFoundation.Samples.ThermalStateForIOS)
extern void ThermalStateHandling_set_thermalStateForIOS_mB983A715C583F92F6BA193D025893F2BB788A4DC (void);
// 0x00000172 UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.ThermalStateHandling::get_thermalStateText()
extern void ThermalStateHandling_get_thermalStateText_m464CA7C2E4D0FDD2E344C26C4A58D4AE74D85E36 (void);
// 0x00000173 System.Void UnityEngine.XR.ARFoundation.Samples.ThermalStateHandling::set_thermalStateText(UnityEngine.UI.Text)
extern void ThermalStateHandling_set_thermalStateText_m9E28C4A3D86A539D653FDCB5E30F35BE4E933184 (void);
// 0x00000174 System.Void UnityEngine.XR.ARFoundation.Samples.ThermalStateHandling::Awake()
extern void ThermalStateHandling_Awake_mD14B1CB3ED8AF2136ED6F6F7762C1E5CD1E19ACE (void);
// 0x00000175 System.Void UnityEngine.XR.ARFoundation.Samples.ThermalStateHandling::OnEnable()
extern void ThermalStateHandling_OnEnable_mDC00A21BBE48894823240530EF2B9D96D5CD2B00 (void);
// 0x00000176 System.Void UnityEngine.XR.ARFoundation.Samples.ThermalStateHandling::OnDisable()
extern void ThermalStateHandling_OnDisable_m1BFDFF7F12BD46D7433CEA7BDBCF3C0E0C986131 (void);
// 0x00000177 System.Void UnityEngine.XR.ARFoundation.Samples.ThermalStateHandling::OnThermalStateChanged(UnityEngine.XR.ARFoundation.Samples.ThermalStateForIOS/ThermalStateChange)
extern void ThermalStateHandling_OnThermalStateChanged_mDD0EDD3355DFA4467E5F9F7109B5B77A04F44440 (void);
// 0x00000178 System.Void UnityEngine.XR.ARFoundation.Samples.ThermalStateHandling::SetARFeaturesBasedOnThermalState(UnityEngine.XR.ARFoundation.Samples.ThermalStateForIOS/ThermalState)
extern void ThermalStateHandling_SetARFeaturesBasedOnThermalState_m372B8E6523CA0021D189399F0A315003F78ED4A0 (void);
// 0x00000179 System.Void UnityEngine.XR.ARFoundation.Samples.ThermalStateHandling::ToggleEnvironmentProbeFeature(System.Boolean)
extern void ThermalStateHandling_ToggleEnvironmentProbeFeature_mCBDC6915E364D11DC496CE651D4E94C71E66FAF0 (void);
// 0x0000017A System.Void UnityEngine.XR.ARFoundation.Samples.ThermalStateHandling::ToggleFaceTrackingFeature(System.Boolean)
extern void ThermalStateHandling_ToggleFaceTrackingFeature_m1C52C1F4366D44A7C85D6F53FFE5E284E51DE516 (void);
// 0x0000017B System.Void UnityEngine.XR.ARFoundation.Samples.ThermalStateHandling::ToggleMeshingFeature(System.Boolean)
extern void ThermalStateHandling_ToggleMeshingFeature_m4056257217FB5815145D03B3FDF337D9FB33C70B (void);
// 0x0000017C System.Void UnityEngine.XR.ARFoundation.Samples.ThermalStateHandling::ToggleHumanSegmentationFeatures(System.Boolean)
extern void ThermalStateHandling_ToggleHumanSegmentationFeatures_m823844F891187CB253FB376F11D8AD4124FEB9AD (void);
// 0x0000017D System.Void UnityEngine.XR.ARFoundation.Samples.ThermalStateHandling::ToggleEnvironmentDepthFeature(System.Boolean)
extern void ThermalStateHandling_ToggleEnvironmentDepthFeature_mB531F0C1DEA420D6BC4D5F5244BB1AB87AD9F0D2 (void);
// 0x0000017E System.Void UnityEngine.XR.ARFoundation.Samples.ThermalStateHandling::ToggleARSession(System.Boolean)
extern void ThermalStateHandling_ToggleARSession_mE294009F786FAACA2D181242AF0F01407E0341DE (void);
// 0x0000017F System.Void UnityEngine.XR.ARFoundation.Samples.ThermalStateHandling::.ctor()
extern void ThermalStateHandling__ctor_m52AD1B9D887C889CE9D7C70BFDFBAC85C81C809A (void);
// 0x00000180 UnityEngine.XR.ARFoundation.ARCameraManager UnityEngine.XR.ARFoundation.Samples.CameraGrain::get_cameraManager()
extern void CameraGrain_get_cameraManager_mDD228AE643D232E853CABEC4EB66DD16000F5A8F (void);
// 0x00000181 System.Void UnityEngine.XR.ARFoundation.Samples.CameraGrain::set_cameraManager(UnityEngine.XR.ARFoundation.ARCameraManager)
extern void CameraGrain_set_cameraManager_mDB18F54E87C683B3FDF9AC8F11EC7481DD2FCC7C (void);
// 0x00000182 System.Void UnityEngine.XR.ARFoundation.Samples.CameraGrain::Start()
extern void CameraGrain_Start_m2F0CEAB415E3F0508ED2947FF86A818DF1B0180D (void);
// 0x00000183 System.Void UnityEngine.XR.ARFoundation.Samples.CameraGrain::OnDisable()
extern void CameraGrain_OnDisable_m87E99A967FA0A355FBF2E07BEF3DA0854C182BAA (void);
// 0x00000184 System.Void UnityEngine.XR.ARFoundation.Samples.CameraGrain::OnReceivedFrame(UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs)
extern void CameraGrain_OnReceivedFrame_mC96A6191A9E9C723A1F113331943A33547675B85 (void);
// 0x00000185 System.Void UnityEngine.XR.ARFoundation.Samples.CameraGrain::.ctor()
extern void CameraGrain__ctor_mE559C38F99B691A3EF2ACB1D1FBFBF658EECEA95 (void);
// 0x00000186 UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.DisplayFaceInfo::get_faceInfoText()
extern void DisplayFaceInfo_get_faceInfoText_m4A0D6014A92C826FF98335D3D9FE01EAD8C66AFB (void);
// 0x00000187 System.Void UnityEngine.XR.ARFoundation.Samples.DisplayFaceInfo::set_faceInfoText(UnityEngine.UI.Text)
extern void DisplayFaceInfo_set_faceInfoText_m1FE7A372A0D41936BF03C54508CC34AA11E4B476 (void);
// 0x00000188 UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.DisplayFaceInfo::get_instructionsText()
extern void DisplayFaceInfo_get_instructionsText_mFCBF9F211FCB7D1B1A905A00D99CF99ADC40A32D (void);
// 0x00000189 System.Void UnityEngine.XR.ARFoundation.Samples.DisplayFaceInfo::set_instructionsText(UnityEngine.UI.Text)
extern void DisplayFaceInfo_set_instructionsText_m948D04ADABCF371C87F8B37089A151764FED6490 (void);
// 0x0000018A UnityEngine.GameObject UnityEngine.XR.ARFoundation.Samples.DisplayFaceInfo::get_notSupportedElement()
extern void DisplayFaceInfo_get_notSupportedElement_m60DD41707ADDB6B395203CB01DAA8D704A9D3627 (void);
// 0x0000018B System.Void UnityEngine.XR.ARFoundation.Samples.DisplayFaceInfo::set_notSupportedElement(UnityEngine.GameObject)
extern void DisplayFaceInfo_set_notSupportedElement_mF0E8C6B4677700681A29461597648D2B5B7311DB (void);
// 0x0000018C UnityEngine.Transform UnityEngine.XR.ARFoundation.Samples.DisplayFaceInfo::get_faceControlledObject()
extern void DisplayFaceInfo_get_faceControlledObject_mADE583779F21FE539C9441C9D636032FF2E79AF4 (void);
// 0x0000018D System.Void UnityEngine.XR.ARFoundation.Samples.DisplayFaceInfo::set_faceControlledObject(UnityEngine.Transform)
extern void DisplayFaceInfo_set_faceControlledObject_mC148E577866F364C10685D5E5EBD4C62FBA096AD (void);
// 0x0000018E System.Void UnityEngine.XR.ARFoundation.Samples.DisplayFaceInfo::Awake()
extern void DisplayFaceInfo_Awake_mA4FE652199142AF7EDDD83FDBF9184F329F2FD94 (void);
// 0x0000018F System.Void UnityEngine.XR.ARFoundation.Samples.DisplayFaceInfo::OnEnable()
extern void DisplayFaceInfo_OnEnable_m312273878D50E20F17F88E9B6B0DA2BAF53B92AB (void);
// 0x00000190 System.Void UnityEngine.XR.ARFoundation.Samples.DisplayFaceInfo::OnDisable()
extern void DisplayFaceInfo_OnDisable_m7B672804CFBC3BCB6D5172D16C65ED18D1299F38 (void);
// 0x00000191 System.Void UnityEngine.XR.ARFoundation.Samples.DisplayFaceInfo::OnBeforeRender()
extern void DisplayFaceInfo_OnBeforeRender_mF82FD8174EB5456DDB946A311304BC9AD782BE12 (void);
// 0x00000192 System.Void UnityEngine.XR.ARFoundation.Samples.DisplayFaceInfo::Update()
extern void DisplayFaceInfo_Update_m618E108C3B432528CD43E7CBE31BC309DB98EC18 (void);
// 0x00000193 System.Void UnityEngine.XR.ARFoundation.Samples.DisplayFaceInfo::.ctor()
extern void DisplayFaceInfo__ctor_m91106693C325EC3296D0E8D7D3642A7B9D8CD915 (void);
// 0x00000194 UnityEngine.XR.ARFoundation.ARCameraManager UnityEngine.XR.ARFoundation.Samples.ToggleCameraFacingDirection::get_cameraManager()
extern void ToggleCameraFacingDirection_get_cameraManager_mBFC4F1605D41D22AD463C0C440FFB72524B75372 (void);
// 0x00000195 System.Void UnityEngine.XR.ARFoundation.Samples.ToggleCameraFacingDirection::set_cameraManager(UnityEngine.XR.ARFoundation.ARCameraManager)
extern void ToggleCameraFacingDirection_set_cameraManager_m9F2D133BF1F8DF5D066FD98AF3006DFBFA0DF902 (void);
// 0x00000196 UnityEngine.XR.ARFoundation.ARSession UnityEngine.XR.ARFoundation.Samples.ToggleCameraFacingDirection::get_session()
extern void ToggleCameraFacingDirection_get_session_mEEE6D7D90DE67570FE4164F022499035CBD50C21 (void);
// 0x00000197 System.Void UnityEngine.XR.ARFoundation.Samples.ToggleCameraFacingDirection::set_session(UnityEngine.XR.ARFoundation.ARSession)
extern void ToggleCameraFacingDirection_set_session_m0C899E75182B8FFB8238C70441461D42E787DA2C (void);
// 0x00000198 System.Void UnityEngine.XR.ARFoundation.Samples.ToggleCameraFacingDirection::Update()
extern void ToggleCameraFacingDirection_Update_m6B83C93575822D6198046195B528824882202D61 (void);
// 0x00000199 System.Void UnityEngine.XR.ARFoundation.Samples.ToggleCameraFacingDirection::.ctor()
extern void ToggleCameraFacingDirection__ctor_m47B36115BD93E9A1493E75906548D12F87EB0007 (void);
// 0x0000019A UnityEngine.Camera UnityEngine.XR.ARFoundation.Samples.TrackedImageInfoManager::get_worldSpaceCanvasCamera()
extern void TrackedImageInfoManager_get_worldSpaceCanvasCamera_m2009EB2480A1FF118343F604EFE5AC5A9D1EAF98 (void);
// 0x0000019B System.Void UnityEngine.XR.ARFoundation.Samples.TrackedImageInfoManager::set_worldSpaceCanvasCamera(UnityEngine.Camera)
extern void TrackedImageInfoManager_set_worldSpaceCanvasCamera_m425A097A896E7ACFB54A58818F8A9E93012F00D8 (void);
// 0x0000019C UnityEngine.Texture2D UnityEngine.XR.ARFoundation.Samples.TrackedImageInfoManager::get_defaultTexture()
extern void TrackedImageInfoManager_get_defaultTexture_m6A0D413300AF5BAC5E00557530B03CF16CEE7EBF (void);
// 0x0000019D System.Void UnityEngine.XR.ARFoundation.Samples.TrackedImageInfoManager::set_defaultTexture(UnityEngine.Texture2D)
extern void TrackedImageInfoManager_set_defaultTexture_m9FE88BD89D3C1A0E724B56E3DD57598EACBCD647 (void);
// 0x0000019E System.Void UnityEngine.XR.ARFoundation.Samples.TrackedImageInfoManager::Awake()
extern void TrackedImageInfoManager_Awake_m5CF4E3E359D5EE51D9CAC08F04ED16EA949BD609 (void);
// 0x0000019F System.Void UnityEngine.XR.ARFoundation.Samples.TrackedImageInfoManager::OnEnable()
extern void TrackedImageInfoManager_OnEnable_m2F61DD659F81B38E8DA09622DEA0C0B851E35948 (void);
// 0x000001A0 System.Void UnityEngine.XR.ARFoundation.Samples.TrackedImageInfoManager::OnDisable()
extern void TrackedImageInfoManager_OnDisable_m25E3579157C28DC1ABA8A19CAF5572D96587E700 (void);
// 0x000001A1 System.Void UnityEngine.XR.ARFoundation.Samples.TrackedImageInfoManager::UpdateInfo(UnityEngine.XR.ARFoundation.ARTrackedImage)
extern void TrackedImageInfoManager_UpdateInfo_m5B077255A0CF8EB647C37D9C27862B550BA45045 (void);
// 0x000001A2 System.Void UnityEngine.XR.ARFoundation.Samples.TrackedImageInfoManager::OnTrackedImagesChanged(UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs)
extern void TrackedImageInfoManager_OnTrackedImagesChanged_m7D340AF3DC1C788A581C43A4AA47C4D3286D0C01 (void);
// 0x000001A3 System.Void UnityEngine.XR.ARFoundation.Samples.TrackedImageInfoManager::.ctor()
extern void TrackedImageInfoManager__ctor_mF3CC8344AE3000C2C0BD654B6853371F86B0972C (void);
// 0x000001A4 UnityEngine.XR.ARFoundation.Samples.DynamicLibrary/ImageData[] UnityEngine.XR.ARFoundation.Samples.DynamicLibrary::get_images()
extern void DynamicLibrary_get_images_m2FC396F22C83BBD54343040144A935ED3FBF547F (void);
// 0x000001A5 System.Void UnityEngine.XR.ARFoundation.Samples.DynamicLibrary::set_images(UnityEngine.XR.ARFoundation.Samples.DynamicLibrary/ImageData[])
extern void DynamicLibrary_set_images_m6C44825F8CBD0AB08CB033A3220B0937C8AAF035 (void);
// 0x000001A6 System.Void UnityEngine.XR.ARFoundation.Samples.DynamicLibrary::OnGUI()
extern void DynamicLibrary_OnGUI_mEB7711C3A3BD11B1F4B5D168152A051C75E12506 (void);
// 0x000001A7 System.Void UnityEngine.XR.ARFoundation.Samples.DynamicLibrary::SetError(System.String)
extern void DynamicLibrary_SetError_m3C91911DB9F4FF8EAFABD2C9821C936738426973 (void);
// 0x000001A8 System.Void UnityEngine.XR.ARFoundation.Samples.DynamicLibrary::Update()
extern void DynamicLibrary_Update_mD093DD71352B9198CEAD96F5E8D9F43EFC15AC78 (void);
// 0x000001A9 System.Void UnityEngine.XR.ARFoundation.Samples.DynamicLibrary::.ctor()
extern void DynamicLibrary__ctor_mD51A617C9BE65684051DF13361876A44AA57FD4D (void);
// 0x000001AA UnityEngine.Texture2D UnityEngine.XR.ARFoundation.Samples.DynamicLibrary/ImageData::get_texture()
extern void ImageData_get_texture_m808D389F1C640BDDF41FA7BA62D33D7CBF688DDE (void);
// 0x000001AB System.Void UnityEngine.XR.ARFoundation.Samples.DynamicLibrary/ImageData::set_texture(UnityEngine.Texture2D)
extern void ImageData_set_texture_mE9BFF48AB9EF7DC01ABEB4DCACF0E2352C9E506B (void);
// 0x000001AC System.String UnityEngine.XR.ARFoundation.Samples.DynamicLibrary/ImageData::get_name()
extern void ImageData_get_name_mEE3EE88AC17A5486CA757AB8551B54CE7991DB09 (void);
// 0x000001AD System.Void UnityEngine.XR.ARFoundation.Samples.DynamicLibrary/ImageData::set_name(System.String)
extern void ImageData_set_name_m6DF1F29081F51AC73809C880EA6C1FCFDCFA7F9C (void);
// 0x000001AE System.Single UnityEngine.XR.ARFoundation.Samples.DynamicLibrary/ImageData::get_width()
extern void ImageData_get_width_mDA30009E63B828F4D8617FD37DC591989A5AD4C1 (void);
// 0x000001AF System.Void UnityEngine.XR.ARFoundation.Samples.DynamicLibrary/ImageData::set_width(System.Single)
extern void ImageData_set_width_mC1E5E51A65F3D52405F1FBE4246602413A0740E0 (void);
// 0x000001B0 UnityEngine.XR.ARSubsystems.AddReferenceImageJobState UnityEngine.XR.ARFoundation.Samples.DynamicLibrary/ImageData::get_jobState()
extern void ImageData_get_jobState_mFA144AA269EBBE825C57B43F8F4850CB37D4F677 (void);
// 0x000001B1 System.Void UnityEngine.XR.ARFoundation.Samples.DynamicLibrary/ImageData::set_jobState(UnityEngine.XR.ARSubsystems.AddReferenceImageJobState)
extern void ImageData_set_jobState_m9EC3407D1C8FAFECC0A2F25EB924297F7C901A73 (void);
// 0x000001B2 System.Void UnityEngine.XR.ARFoundation.Samples.DynamicLibrary/ImageData::.ctor()
extern void ImageData__ctor_mAD7C5F0696DAAFE916D1B5BBF9EE7A771972B789 (void);
// 0x000001B3 UnityEngine.GameObject UnityEngine.XR.ARFoundation.Samples.DynamicPrefab::get_alternativePrefab()
extern void DynamicPrefab_get_alternativePrefab_m4B48EB6B3CFE5493EDF32163F0A185A3E0B284F6 (void);
// 0x000001B4 System.Void UnityEngine.XR.ARFoundation.Samples.DynamicPrefab::set_alternativePrefab(UnityEngine.GameObject)
extern void DynamicPrefab_set_alternativePrefab_mB496995B03DBC0C9F41FE31C6E09E6D711311204 (void);
// 0x000001B5 System.Void UnityEngine.XR.ARFoundation.Samples.DynamicPrefab::OnGUI()
extern void DynamicPrefab_OnGUI_m7B42974745B7C610AC8BEE42B00461EB4F36FEC9 (void);
// 0x000001B6 System.Void UnityEngine.XR.ARFoundation.Samples.DynamicPrefab::SetError(System.String)
extern void DynamicPrefab_SetError_mC4122FB4973F14E169E99825FA9A01DFA9A6B061 (void);
// 0x000001B7 System.Void UnityEngine.XR.ARFoundation.Samples.DynamicPrefab::Update()
extern void DynamicPrefab_Update_m34D182813AFDCF28D8C664D835F61912273079CF (void);
// 0x000001B8 System.Void UnityEngine.XR.ARFoundation.Samples.DynamicPrefab::.ctor()
extern void DynamicPrefab__ctor_m7B0ABDA21E114912AB522C7DBB5413668AE9D425 (void);
// 0x000001B9 UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager::get_imageLibrary()
extern void PrefabImagePairManager_get_imageLibrary_mF75838258C00F909150CB75F5989903E7AF21286 (void);
// 0x000001BA System.Void UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager::set_imageLibrary(UnityEngine.XR.ARSubsystems.XRReferenceImageLibrary)
extern void PrefabImagePairManager_set_imageLibrary_m5598C2AC4459C6D5135CB5AC153B803874958BC9 (void);
// 0x000001BB System.Void UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager::OnBeforeSerialize()
extern void PrefabImagePairManager_OnBeforeSerialize_m5B178C81BC1C1982D01E7CABB3DA6C8152F5F451 (void);
// 0x000001BC System.Void UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager::OnAfterDeserialize()
extern void PrefabImagePairManager_OnAfterDeserialize_m10CAF6488BD05E15650CCAC2D8DDFDBEBE4C6D53 (void);
// 0x000001BD System.Void UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager::Awake()
extern void PrefabImagePairManager_Awake_m5D1BADF276ACEBE409796C5783389B4DDBC6A076 (void);
// 0x000001BE System.Void UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager::OnEnable()
extern void PrefabImagePairManager_OnEnable_mA5D8272C6A44CC8ED3E26144CC2424F0B3F48133 (void);
// 0x000001BF System.Void UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager::OnDisable()
extern void PrefabImagePairManager_OnDisable_mC339AF0AAEBDB53FEC7E643E1A69D9560A23890C (void);
// 0x000001C0 System.Void UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager::OnTrackedImagesChanged(UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs)
extern void PrefabImagePairManager_OnTrackedImagesChanged_m8950068F3FE83FBE39C0D38F1EA06FA58F82DF5E (void);
// 0x000001C1 System.Void UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager::AssignPrefab(UnityEngine.XR.ARFoundation.ARTrackedImage)
extern void PrefabImagePairManager_AssignPrefab_m3A612DEED64B82D56C3FAACF8BF4E46A7729BA72 (void);
// 0x000001C2 UnityEngine.GameObject UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager::GetPrefabForReferenceImage(UnityEngine.XR.ARSubsystems.XRReferenceImage)
extern void PrefabImagePairManager_GetPrefabForReferenceImage_m37CE15147B141D8ECAF155CDD7BBEAF86C109364 (void);
// 0x000001C3 System.Void UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager::SetPrefabForReferenceImage(UnityEngine.XR.ARSubsystems.XRReferenceImage,UnityEngine.GameObject)
extern void PrefabImagePairManager_SetPrefabForReferenceImage_m660F9185F078D8492942F596D6596986DCF68DC8 (void);
// 0x000001C4 System.Void UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager::.ctor()
extern void PrefabImagePairManager__ctor_m706F23D05C16A9911D7637808AE62800AFFE6760 (void);
// 0x000001C5 System.Void UnityEngine.XR.ARFoundation.Samples.PrefabImagePairManager/NamedPrefab::.ctor(System.Guid,UnityEngine.GameObject)
extern void NamedPrefab__ctor_m4C5A1EAC954851954E10ADF5F4ED646967BAB252 (void);
// 0x000001C6 UnityEngine.GameObject UnityEngine.XR.ARFoundation.Samples.InputSystem_PlaceOnPlane::get_placedPrefab()
extern void InputSystem_PlaceOnPlane_get_placedPrefab_m1294EBD3BD1D9BC33BC68AAF0E3B1C9563D17305 (void);
// 0x000001C7 System.Void UnityEngine.XR.ARFoundation.Samples.InputSystem_PlaceOnPlane::set_placedPrefab(UnityEngine.GameObject)
extern void InputSystem_PlaceOnPlane_set_placedPrefab_mF3CFC1B744337A313EC32DD14754BA086AA47F0F (void);
// 0x000001C8 UnityEngine.GameObject UnityEngine.XR.ARFoundation.Samples.InputSystem_PlaceOnPlane::get_spawnedObject()
extern void InputSystem_PlaceOnPlane_get_spawnedObject_m0AC5BD84B735508ED94FFE77CA3F83F510C07434 (void);
// 0x000001C9 System.Void UnityEngine.XR.ARFoundation.Samples.InputSystem_PlaceOnPlane::set_spawnedObject(UnityEngine.GameObject)
extern void InputSystem_PlaceOnPlane_set_spawnedObject_m44AFB1A6AE9D71955BF4EE29DE4FE4D9AF757E98 (void);
// 0x000001CA System.Void UnityEngine.XR.ARFoundation.Samples.InputSystem_PlaceOnPlane::Awake()
extern void InputSystem_PlaceOnPlane_Awake_mAECA5D93BDE511AB0D0FE41F829888DB0BF6B7C3 (void);
// 0x000001CB System.Void UnityEngine.XR.ARFoundation.Samples.InputSystem_PlaceOnPlane::AddObject(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void InputSystem_PlaceOnPlane_AddObject_m622FA4442A14D844FE4BB5C7C3C881D41D5ECA64 (void);
// 0x000001CC System.Void UnityEngine.XR.ARFoundation.Samples.InputSystem_PlaceOnPlane::.ctor()
extern void InputSystem_PlaceOnPlane__ctor_mDCD75DE0C26E8F3848762A19AD7A90F2C3FCEC46 (void);
// 0x000001CD System.Void UnityEngine.XR.ARFoundation.Samples.InputSystem_PlaceOnPlane::.cctor()
extern void InputSystem_PlaceOnPlane__cctor_mE9C17502BA947C08D1F3C307F0689AC2EBD40AAC (void);
// 0x000001CE UnityEngine.GameObject UnityEngine.XR.ARFoundation.Samples.FacingDirectionManager::get_worldSpaceObject()
extern void FacingDirectionManager_get_worldSpaceObject_m5A8EF128F606CA0B9A5DDEAFE1CEEDC8ADE73195 (void);
// 0x000001CF System.Void UnityEngine.XR.ARFoundation.Samples.FacingDirectionManager::set_worldSpaceObject(UnityEngine.GameObject)
extern void FacingDirectionManager_set_worldSpaceObject_mD7EB5526AC76B928B920368BAF7B0C0359AFC9ED (void);
// 0x000001D0 System.Void UnityEngine.XR.ARFoundation.Samples.FacingDirectionManager::OnEnable()
extern void FacingDirectionManager_OnEnable_mCB4CA374C5FF2EA7E77C941D31A875D064923FE7 (void);
// 0x000001D1 System.Void UnityEngine.XR.ARFoundation.Samples.FacingDirectionManager::Update()
extern void FacingDirectionManager_Update_mE07AC3E3A7F7D8359AD22A3272F1C0616357A84A (void);
// 0x000001D2 System.Void UnityEngine.XR.ARFoundation.Samples.FacingDirectionManager::OnDisable()
extern void FacingDirectionManager_OnDisable_mE41E3232C5B4F735D43C861067AADFC3DD637B6B (void);
// 0x000001D3 System.Void UnityEngine.XR.ARFoundation.Samples.FacingDirectionManager::OnBeforeRender()
extern void FacingDirectionManager_OnBeforeRender_mE9B6589A255D251879736DB577EFCF56FE883418 (void);
// 0x000001D4 System.Void UnityEngine.XR.ARFoundation.Samples.FacingDirectionManager::.ctor()
extern void FacingDirectionManager__ctor_m8E25BA21D4A091FA374E1B8971585ED06C4691D1 (void);
// 0x000001D5 System.Void UnityEngine.XR.ARFoundation.Samples.Rotator::Update()
extern void Rotator_Update_m4CC324BFC9A5FEA628BD67053EED1DC1BF2D5E14 (void);
// 0x000001D6 System.Void UnityEngine.XR.ARFoundation.Samples.Rotator::.ctor()
extern void Rotator__ctor_m5919872FB104ECD80FB34A18D4B8C16169B9523C (void);
// 0x000001D7 System.Void UnityEngine.XR.ARFoundation.Samples.MeshClassificationFracking::Awake()
extern void MeshClassificationFracking_Awake_mD86D06C56B8667774DA34D843F2C0E0A7A89BDA4 (void);
// 0x000001D8 System.Void UnityEngine.XR.ARFoundation.Samples.MeshClassificationFracking::OnEnable()
extern void MeshClassificationFracking_OnEnable_m4B836A9A43C928520870E4F33730233CCD4D3B77 (void);
// 0x000001D9 System.Void UnityEngine.XR.ARFoundation.Samples.MeshClassificationFracking::OnDisable()
extern void MeshClassificationFracking_OnDisable_mC71ECB3C6A8915C35BD05E5B21FDCFCE7236AAAA (void);
// 0x000001DA System.Void UnityEngine.XR.ARFoundation.Samples.MeshClassificationFracking::OnMeshesChanged(UnityEngine.XR.ARFoundation.ARMeshesChangedEventArgs)
extern void MeshClassificationFracking_OnMeshesChanged_m0B02E7A7108C195CF1479FC64AF7FA0994484A5A (void);
// 0x000001DB UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARFoundation.Samples.MeshClassificationFracking::ExtractTrackableId(System.String)
extern void MeshClassificationFracking_ExtractTrackableId_m4007E88EC94D38E3E16626F1CC4AC074AB78D62D (void);
// 0x000001DC System.Void UnityEngine.XR.ARFoundation.Samples.MeshClassificationFracking::ExtractClassifiedMesh(UnityEngine.Mesh,Unity.Collections.NativeArray`1<UnityEngine.XR.ARKit.ARMeshClassification>,UnityEngine.XR.ARKit.ARMeshClassification,UnityEngine.Mesh)
extern void MeshClassificationFracking_ExtractClassifiedMesh_mCEB94D57DEAF921272F7E66547622DF3773D5166 (void);
// 0x000001DD System.Void UnityEngine.XR.ARFoundation.Samples.MeshClassificationFracking::BreakupMesh(UnityEngine.MeshFilter)
extern void MeshClassificationFracking_BreakupMesh_mD722493A6C5C55C1135823F0405163D0AD3B5DBB (void);
// 0x000001DE System.Void UnityEngine.XR.ARFoundation.Samples.MeshClassificationFracking::UpdateMesh(UnityEngine.MeshFilter)
extern void MeshClassificationFracking_UpdateMesh_m34745767B9BA1F79C3F98153E02A7EC8C8D286A9 (void);
// 0x000001DF System.Void UnityEngine.XR.ARFoundation.Samples.MeshClassificationFracking::RemoveMesh(UnityEngine.MeshFilter)
extern void MeshClassificationFracking_RemoveMesh_m27A3A7197CCC6ABBD72A31F6A09ED2BB030E4E03 (void);
// 0x000001E0 System.Void UnityEngine.XR.ARFoundation.Samples.MeshClassificationFracking::.ctor()
extern void MeshClassificationFracking__ctor_mA95E462A2CFBBF9E0AFD5CFDB3A57884C445459B (void);
// 0x000001E1 UnityEngine.Rigidbody UnityEngine.XR.ARFoundation.Samples.ProjectileLauncher::get_projectilePrefab()
extern void ProjectileLauncher_get_projectilePrefab_m4F05A1B02168D3215721555025FF4A4AEC06F6A8 (void);
// 0x000001E2 System.Void UnityEngine.XR.ARFoundation.Samples.ProjectileLauncher::set_projectilePrefab(UnityEngine.Rigidbody)
extern void ProjectileLauncher_set_projectilePrefab_m81569C053340C089D73241BDD1841989F52C55CE (void);
// 0x000001E3 System.Single UnityEngine.XR.ARFoundation.Samples.ProjectileLauncher::get_initialSpeed()
extern void ProjectileLauncher_get_initialSpeed_mCE0943F2C6E14E623A450DFE1A6D719A371000FD (void);
// 0x000001E4 System.Void UnityEngine.XR.ARFoundation.Samples.ProjectileLauncher::set_initialSpeed(System.Single)
extern void ProjectileLauncher_set_initialSpeed_m6830FD039243F187A00399F6F88BEC0F75804453 (void);
// 0x000001E5 System.Void UnityEngine.XR.ARFoundation.Samples.ProjectileLauncher::Update()
extern void ProjectileLauncher_Update_mFE282128D30F7581A4C8E6EFEBCDB55F43FC3516 (void);
// 0x000001E6 System.Void UnityEngine.XR.ARFoundation.Samples.ProjectileLauncher::.ctor()
extern void ProjectileLauncher__ctor_m4B3DF202914A639E1F642243322722281046F85C (void);
// 0x000001E7 UnityEngine.XR.ARFoundation.ARMeshManager UnityEngine.XR.ARFoundation.Samples.ToggleMeshClassification::get_meshManager()
extern void ToggleMeshClassification_get_meshManager_mBDA4125E3DFB9D601810935F3CC90E9171C9A18D (void);
// 0x000001E8 System.Void UnityEngine.XR.ARFoundation.Samples.ToggleMeshClassification::set_meshManager(UnityEngine.XR.ARFoundation.ARMeshManager)
extern void ToggleMeshClassification_set_meshManager_m7283B677E39AD9CDD22815514D9D17216B7EDE68 (void);
// 0x000001E9 System.Boolean UnityEngine.XR.ARFoundation.Samples.ToggleMeshClassification::get_classificationEnabled()
extern void ToggleMeshClassification_get_classificationEnabled_mF4C6A5C1FABE3522D314EAF8607DF73B5886D5E9 (void);
// 0x000001EA System.Void UnityEngine.XR.ARFoundation.Samples.ToggleMeshClassification::set_classificationEnabled(System.Boolean)
extern void ToggleMeshClassification_set_classificationEnabled_mF2725278262931FB74542064BE14F5D9DC45FBA2 (void);
// 0x000001EB System.Void UnityEngine.XR.ARFoundation.Samples.ToggleMeshClassification::OnEnable()
extern void ToggleMeshClassification_OnEnable_mBABEDFE0342AA7E7827329B7C69A5DC1A5A76357 (void);
// 0x000001EC System.Void UnityEngine.XR.ARFoundation.Samples.ToggleMeshClassification::UpdateMeshSubsystem()
extern void ToggleMeshClassification_UpdateMeshSubsystem_m8370947D0DA98CBB6493161CF656B832121D7F0B (void);
// 0x000001ED System.Void UnityEngine.XR.ARFoundation.Samples.ToggleMeshClassification::.ctor()
extern void ToggleMeshClassification__ctor_m32F40C59A6530B5CC8A3751AFBC47F3257FEEFDD (void);
// 0x000001EE System.Void UnityEngine.XR.ARFoundation.Samples.PlaneClassificationLabeler::Awake()
extern void PlaneClassificationLabeler_Awake_mA8E8CE7C74787E4C897E8A8C392A7E60B3D0E447 (void);
// 0x000001EF System.Void UnityEngine.XR.ARFoundation.Samples.PlaneClassificationLabeler::Update()
extern void PlaneClassificationLabeler_Update_m4E8FC7D0E31D2BF7BBD8AE664B04111D62338883 (void);
// 0x000001F0 System.Void UnityEngine.XR.ARFoundation.Samples.PlaneClassificationLabeler::UpdateLabel()
extern void PlaneClassificationLabeler_UpdateLabel_m9704258C3912DB6B8383AACF7D8155F7353D8A09 (void);
// 0x000001F1 System.Void UnityEngine.XR.ARFoundation.Samples.PlaneClassificationLabeler::UpdatePlaneColor()
extern void PlaneClassificationLabeler_UpdatePlaneColor_m337F617E6EED7737A9970797ABECFE6ADEEFF9F4 (void);
// 0x000001F2 System.Void UnityEngine.XR.ARFoundation.Samples.PlaneClassificationLabeler::OnDestroy()
extern void PlaneClassificationLabeler_OnDestroy_mBAFB13C3696BE286FAFC07A32F0322E435D238E2 (void);
// 0x000001F3 System.Void UnityEngine.XR.ARFoundation.Samples.PlaneClassificationLabeler::.ctor()
extern void PlaneClassificationLabeler__ctor_m863EB2A0ED6190ACFC36EC5B1159C9E409B9FC94 (void);
// 0x000001F4 UnityEngine.GameObject UnityEngine.XR.ARFoundation.Samples.ARCoreFaceRegionManager::get_regionPrefab()
extern void ARCoreFaceRegionManager_get_regionPrefab_m7884407D2CC7FC144F9B11CB140FA6416F03B5A5 (void);
// 0x000001F5 System.Void UnityEngine.XR.ARFoundation.Samples.ARCoreFaceRegionManager::set_regionPrefab(UnityEngine.GameObject)
extern void ARCoreFaceRegionManager_set_regionPrefab_mA42D42267CE88E735EFDCB80316EF95EF3CF083E (void);
// 0x000001F6 System.Void UnityEngine.XR.ARFoundation.Samples.ARCoreFaceRegionManager::Start()
extern void ARCoreFaceRegionManager_Start_mDB7040A56177E80A0B0A73D988A21EDF81FE5A9E (void);
// 0x000001F7 System.Void UnityEngine.XR.ARFoundation.Samples.ARCoreFaceRegionManager::Update()
extern void ARCoreFaceRegionManager_Update_m0CBA5BD373C03DB42FCC6BBDD299786E42C8B79C (void);
// 0x000001F8 System.Void UnityEngine.XR.ARFoundation.Samples.ARCoreFaceRegionManager::OnDestroy()
extern void ARCoreFaceRegionManager_OnDestroy_mE4EBCB2E365AD7C91056D218C2BF65CE3701E6B2 (void);
// 0x000001F9 System.Void UnityEngine.XR.ARFoundation.Samples.ARCoreFaceRegionManager::.ctor()
extern void ARCoreFaceRegionManager__ctor_m7201895468F37B815262B66BEC6EB6E56E066B2B (void);
// 0x000001FA System.Single UnityEngine.XR.ARFoundation.Samples.ARFeatheredPlaneMeshVisualizer::get_featheringWidth()
extern void ARFeatheredPlaneMeshVisualizer_get_featheringWidth_mEC5F0FFB6CC81194431BA3A90EDFA779EC2C53E9 (void);
// 0x000001FB System.Void UnityEngine.XR.ARFoundation.Samples.ARFeatheredPlaneMeshVisualizer::set_featheringWidth(System.Single)
extern void ARFeatheredPlaneMeshVisualizer_set_featheringWidth_mBCC1C2C9FC3E4A5499438F0E558C8CA8D38B2749 (void);
// 0x000001FC System.Void UnityEngine.XR.ARFoundation.Samples.ARFeatheredPlaneMeshVisualizer::Awake()
extern void ARFeatheredPlaneMeshVisualizer_Awake_mACD2BF5B879D5F0B815711EDE6233060192B45F9 (void);
// 0x000001FD System.Void UnityEngine.XR.ARFoundation.Samples.ARFeatheredPlaneMeshVisualizer::OnEnable()
extern void ARFeatheredPlaneMeshVisualizer_OnEnable_m1398A828B101DF022DD23DCB3EC52F1501DE49F4 (void);
// 0x000001FE System.Void UnityEngine.XR.ARFoundation.Samples.ARFeatheredPlaneMeshVisualizer::OnDisable()
extern void ARFeatheredPlaneMeshVisualizer_OnDisable_m26C8AF9A5BFCBB372D8B2B7495182D1092A1FE63 (void);
// 0x000001FF System.Void UnityEngine.XR.ARFoundation.Samples.ARFeatheredPlaneMeshVisualizer::ARPlane_boundaryUpdated(UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs)
extern void ARFeatheredPlaneMeshVisualizer_ARPlane_boundaryUpdated_m2673DCA745FD934E5A2B961B4FAAC239A505B046 (void);
// 0x00000200 System.Void UnityEngine.XR.ARFoundation.Samples.ARFeatheredPlaneMeshVisualizer::GenerateBoundaryUVs(UnityEngine.Mesh)
extern void ARFeatheredPlaneMeshVisualizer_GenerateBoundaryUVs_mE1328246098580D09EC14266F0DB8A4AA1B1980F (void);
// 0x00000201 System.Void UnityEngine.XR.ARFoundation.Samples.ARFeatheredPlaneMeshVisualizer::.ctor()
extern void ARFeatheredPlaneMeshVisualizer__ctor_mF21C6CBF25B44AD20AC601D9447BE849B9DAB207 (void);
// 0x00000202 System.Void UnityEngine.XR.ARFoundation.Samples.ARFeatheredPlaneMeshVisualizer::.cctor()
extern void ARFeatheredPlaneMeshVisualizer__cctor_m47BC2F741A3AC0DE3272F7E0291955102CBA9AA7 (void);
// 0x00000203 UnityEngine.XR.ARFoundation.ARSession UnityEngine.XR.ARFoundation.Samples.ARWorldMapController::get_arSession()
extern void ARWorldMapController_get_arSession_m69026D4C29EF8DB2CD86257E251F84542725F52A (void);
// 0x00000204 System.Void UnityEngine.XR.ARFoundation.Samples.ARWorldMapController::set_arSession(UnityEngine.XR.ARFoundation.ARSession)
extern void ARWorldMapController_set_arSession_mFFCCA36CC053F88636141338B7711D09BCF0F21F (void);
// 0x00000205 UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.ARWorldMapController::get_errorText()
extern void ARWorldMapController_get_errorText_m4DADD78C2F6CBC20688B750C9DC1F23F560E75A4 (void);
// 0x00000206 System.Void UnityEngine.XR.ARFoundation.Samples.ARWorldMapController::set_errorText(UnityEngine.UI.Text)
extern void ARWorldMapController_set_errorText_m6810095C64B844B2A1A0A79BB7762A1B98047DBA (void);
// 0x00000207 UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.ARWorldMapController::get_logText()
extern void ARWorldMapController_get_logText_m443EDE6108DC259B188BC877D8D1276D4C4C210E (void);
// 0x00000208 System.Void UnityEngine.XR.ARFoundation.Samples.ARWorldMapController::set_logText(UnityEngine.UI.Text)
extern void ARWorldMapController_set_logText_m417B4D06634A39F0EBC4F4C6E7E6195D68DC69A6 (void);
// 0x00000209 UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.ARWorldMapController::get_mappingStatusText()
extern void ARWorldMapController_get_mappingStatusText_mB66A598071A4D7174A470D3290AFF7E58840256D (void);
// 0x0000020A System.Void UnityEngine.XR.ARFoundation.Samples.ARWorldMapController::set_mappingStatusText(UnityEngine.UI.Text)
extern void ARWorldMapController_set_mappingStatusText_m20F9526980AC71CE836F56BBBAE0FF73B11FEC51 (void);
// 0x0000020B UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.ARWorldMapController::get_saveButton()
extern void ARWorldMapController_get_saveButton_m41771C16AC02935EE89284ED6AB38C681C30864D (void);
// 0x0000020C System.Void UnityEngine.XR.ARFoundation.Samples.ARWorldMapController::set_saveButton(UnityEngine.UI.Button)
extern void ARWorldMapController_set_saveButton_mFDE9A27E0D719BEC85E2FC1FEBAA5A546B3DF28C (void);
// 0x0000020D UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.ARWorldMapController::get_loadButton()
extern void ARWorldMapController_get_loadButton_mCAC816CAB2264E326A28AEBC6A500F825E11A9FB (void);
// 0x0000020E System.Void UnityEngine.XR.ARFoundation.Samples.ARWorldMapController::set_loadButton(UnityEngine.UI.Button)
extern void ARWorldMapController_set_loadButton_m425700EBEA7DCF8AFFF8F385A3837EE4F3F1667C (void);
// 0x0000020F System.Void UnityEngine.XR.ARFoundation.Samples.ARWorldMapController::OnSaveButton()
extern void ARWorldMapController_OnSaveButton_m144B637F1D2B068DB4D9EDBBEB08856254742990 (void);
// 0x00000210 System.Void UnityEngine.XR.ARFoundation.Samples.ARWorldMapController::OnLoadButton()
extern void ARWorldMapController_OnLoadButton_m85CD45BC18EF99CA1ED15496C06A305F8367847C (void);
// 0x00000211 System.Void UnityEngine.XR.ARFoundation.Samples.ARWorldMapController::OnResetButton()
extern void ARWorldMapController_OnResetButton_m649547DFBAF7ED1A2709370AF9D6EBAED498323E (void);
// 0x00000212 System.Void UnityEngine.XR.ARFoundation.Samples.ARWorldMapController::OnDisable()
extern void ARWorldMapController_OnDisable_mC991427317B4ED97FE6CF2452A9CE5770E30E959 (void);
// 0x00000213 System.Void UnityEngine.XR.ARFoundation.Samples.ARWorldMapController::OnEnable()
extern void ARWorldMapController_OnEnable_m65C4AA9EF65E1A01518DC7BB1C97136F375D7F53 (void);
// 0x00000214 System.Collections.IEnumerator UnityEngine.XR.ARFoundation.Samples.ARWorldMapController::Save()
extern void ARWorldMapController_Save_mEBC0CB6E12015C5F906CC072AC3241FF35E953DB (void);
// 0x00000215 System.Collections.IEnumerator UnityEngine.XR.ARFoundation.Samples.ARWorldMapController::Load()
extern void ARWorldMapController_Load_mE20F01573B21065DFC8C79EA84882FFCC0971D56 (void);
// 0x00000216 System.Void UnityEngine.XR.ARFoundation.Samples.ARWorldMapController::SaveAndDisposeWorldMap(UnityEngine.XR.ARKit.ARWorldMap)
extern void ARWorldMapController_SaveAndDisposeWorldMap_mE23C4AC42725CE7B1808BF13CDD8F17D0E58FFB3 (void);
// 0x00000217 System.String UnityEngine.XR.ARFoundation.Samples.ARWorldMapController::get_path()
extern void ARWorldMapController_get_path_mBE58B6C7DBC04BC95F797B1E19AB816937A954C9 (void);
// 0x00000218 System.Boolean UnityEngine.XR.ARFoundation.Samples.ARWorldMapController::get_supported()
extern void ARWorldMapController_get_supported_mCEB0F6C290AA5CB340952F48FAB0DFB41F102FEF (void);
// 0x00000219 System.Void UnityEngine.XR.ARFoundation.Samples.ARWorldMapController::Awake()
extern void ARWorldMapController_Awake_m83374F221A1CE2A456B67575A38F44EB8946AEA8 (void);
// 0x0000021A System.Void UnityEngine.XR.ARFoundation.Samples.ARWorldMapController::Log(System.String)
extern void ARWorldMapController_Log_m34249A60506298E07471366AACD5A3A152BEB590 (void);
// 0x0000021B System.Void UnityEngine.XR.ARFoundation.Samples.ARWorldMapController::SetActive(UnityEngine.UI.Button,System.Boolean)
extern void ARWorldMapController_SetActive_mCF214EBBE83A213CCC9CB107EF410FAA5F82A7A4 (void);
// 0x0000021C System.Void UnityEngine.XR.ARFoundation.Samples.ARWorldMapController::SetActive(UnityEngine.UI.Text,System.Boolean)
extern void ARWorldMapController_SetActive_mB774A9DB91E3F5F2D1640A829199E909EFC2990B (void);
// 0x0000021D System.Void UnityEngine.XR.ARFoundation.Samples.ARWorldMapController::SetText(UnityEngine.UI.Text,System.String)
extern void ARWorldMapController_SetText_m98C4EA68C5C0CDD840AC4586441AD140DE43591A (void);
// 0x0000021E System.Void UnityEngine.XR.ARFoundation.Samples.ARWorldMapController::Update()
extern void ARWorldMapController_Update_mE97C929A2D053A6008711CA0443BB90B94D5EA08 (void);
// 0x0000021F System.Void UnityEngine.XR.ARFoundation.Samples.ARWorldMapController::.ctor()
extern void ARWorldMapController__ctor_mCBB740A0A2ECA65CCD3A2A2E4A737B180730AF5D (void);
// 0x00000220 System.Void UnityEngine.XR.ARFoundation.Samples.ARWorldMapController/<Save>d__29::.ctor(System.Int32)
extern void U3CSaveU3Ed__29__ctor_mDCE4891B2A5738FE78C7E83CEE5D565502AC9BEB (void);
// 0x00000221 System.Void UnityEngine.XR.ARFoundation.Samples.ARWorldMapController/<Save>d__29::System.IDisposable.Dispose()
extern void U3CSaveU3Ed__29_System_IDisposable_Dispose_m8FC5CF2CF627629A7F9A08B461B0995012D4EA3C (void);
// 0x00000222 System.Boolean UnityEngine.XR.ARFoundation.Samples.ARWorldMapController/<Save>d__29::MoveNext()
extern void U3CSaveU3Ed__29_MoveNext_mF72BB112DBC436A2CCB551E53711C854117B16F2 (void);
// 0x00000223 System.Object UnityEngine.XR.ARFoundation.Samples.ARWorldMapController/<Save>d__29::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSaveU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m49BCAC304972E4E88E45CF4FD4466ECE9DC9A77F (void);
// 0x00000224 System.Void UnityEngine.XR.ARFoundation.Samples.ARWorldMapController/<Save>d__29::System.Collections.IEnumerator.Reset()
extern void U3CSaveU3Ed__29_System_Collections_IEnumerator_Reset_mB725B98E0DC2804591774BE458964E3316B09702 (void);
// 0x00000225 System.Object UnityEngine.XR.ARFoundation.Samples.ARWorldMapController/<Save>d__29::System.Collections.IEnumerator.get_Current()
extern void U3CSaveU3Ed__29_System_Collections_IEnumerator_get_Current_mEEF293954D0CA73EBA6E2A8EA21ABD2A904426A1 (void);
// 0x00000226 System.Void UnityEngine.XR.ARFoundation.Samples.ARWorldMapController/<Load>d__30::.ctor(System.Int32)
extern void U3CLoadU3Ed__30__ctor_m7D5110F97548BAE0C00CF1B7B394C4C11270E81F (void);
// 0x00000227 System.Void UnityEngine.XR.ARFoundation.Samples.ARWorldMapController/<Load>d__30::System.IDisposable.Dispose()
extern void U3CLoadU3Ed__30_System_IDisposable_Dispose_m7F12A811CEBA553460C3971DD27A5B0C7A56EB47 (void);
// 0x00000228 System.Boolean UnityEngine.XR.ARFoundation.Samples.ARWorldMapController/<Load>d__30::MoveNext()
extern void U3CLoadU3Ed__30_MoveNext_m49BFE332AD9ED0AA590226B073EA5B783C3BAEA9 (void);
// 0x00000229 System.Object UnityEngine.XR.ARFoundation.Samples.ARWorldMapController/<Load>d__30::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAB3EBB68B37A4E190E7BF896DD1C54C8B0748E54 (void);
// 0x0000022A System.Void UnityEngine.XR.ARFoundation.Samples.ARWorldMapController/<Load>d__30::System.Collections.IEnumerator.Reset()
extern void U3CLoadU3Ed__30_System_Collections_IEnumerator_Reset_mFB86B0FB985083E6A0E6187D072CCB0674199ED5 (void);
// 0x0000022B System.Object UnityEngine.XR.ARFoundation.Samples.ARWorldMapController/<Load>d__30::System.Collections.IEnumerator.get_Current()
extern void U3CLoadU3Ed__30_System_Collections_IEnumerator_get_Current_mB48DD21F3BBDE4B899BE248FE6C979562F4B5FCE (void);
// 0x0000022C UnityEngine.GameObject UnityEngine.XR.ARFoundation.Samples.AnchorCreator::get_prefab()
extern void AnchorCreator_get_prefab_m912AB3AC42E54CE22DB6D9B08E0371C16731EF6D (void);
// 0x0000022D System.Void UnityEngine.XR.ARFoundation.Samples.AnchorCreator::set_prefab(UnityEngine.GameObject)
extern void AnchorCreator_set_prefab_m1F14137F2AA4F3FE1D1887BCCEC706092B8B951A (void);
// 0x0000022E System.Void UnityEngine.XR.ARFoundation.Samples.AnchorCreator::RemoveAllAnchors()
extern void AnchorCreator_RemoveAllAnchors_m703F0AF2F4BB316073A495CFE6C32096FD706158 (void);
// 0x0000022F System.Void UnityEngine.XR.ARFoundation.Samples.AnchorCreator::Awake()
extern void AnchorCreator_Awake_mBADB4F2A6680A1958EE0100C9421FDFE09939583 (void);
// 0x00000230 System.Void UnityEngine.XR.ARFoundation.Samples.AnchorCreator::SetAnchorText(UnityEngine.XR.ARFoundation.ARAnchor,System.String)
extern void AnchorCreator_SetAnchorText_mE41668A55201B063262EBD80B6B955DDAF08F82E (void);
// 0x00000231 UnityEngine.XR.ARFoundation.ARAnchor UnityEngine.XR.ARFoundation.Samples.AnchorCreator::CreateAnchor(UnityEngine.XR.ARFoundation.ARRaycastHit&)
extern void AnchorCreator_CreateAnchor_m9B70ECAACA6B5F5083011C5AF84390DF5B7E9A61 (void);
// 0x00000232 System.Void UnityEngine.XR.ARFoundation.Samples.AnchorCreator::Update()
extern void AnchorCreator_Update_m548D84FC0481BB0E5D1C6A12E79668F69C004770 (void);
// 0x00000233 System.Void UnityEngine.XR.ARFoundation.Samples.AnchorCreator::.ctor()
extern void AnchorCreator__ctor_m7B451467D22D68EC8A0C054F05E6C830C385D299 (void);
// 0x00000234 System.Void UnityEngine.XR.ARFoundation.Samples.AnchorCreator::.cctor()
extern void AnchorCreator__cctor_m4D61D040B4AA4609EF0B31991EF7D95B9467B9E9 (void);
// 0x00000235 UnityEngine.XR.ARFoundation.ARCameraManager UnityEngine.XR.ARFoundation.Samples.BasicLightEstimation::get_cameraManager()
extern void BasicLightEstimation_get_cameraManager_m2FF46C0DCBA1AAA282A75819242E86E461994A63 (void);
// 0x00000236 System.Void UnityEngine.XR.ARFoundation.Samples.BasicLightEstimation::set_cameraManager(UnityEngine.XR.ARFoundation.ARCameraManager)
extern void BasicLightEstimation_set_cameraManager_m9834DEFCF429BC776121EC00C175CC5FC2D83511 (void);
// 0x00000237 System.Nullable`1<System.Single> UnityEngine.XR.ARFoundation.Samples.BasicLightEstimation::get_brightness()
extern void BasicLightEstimation_get_brightness_m47023A76E5A5C0F4031EEFEFDF16992EE6C907ED (void);
// 0x00000238 System.Void UnityEngine.XR.ARFoundation.Samples.BasicLightEstimation::set_brightness(System.Nullable`1<System.Single>)
extern void BasicLightEstimation_set_brightness_m69FE007316403054C8769132F9D9677F2BBCDB85 (void);
// 0x00000239 System.Nullable`1<System.Single> UnityEngine.XR.ARFoundation.Samples.BasicLightEstimation::get_colorTemperature()
extern void BasicLightEstimation_get_colorTemperature_mE1DF5ACF679FFA019ECF36BCC8CEE1ECAF2D627F (void);
// 0x0000023A System.Void UnityEngine.XR.ARFoundation.Samples.BasicLightEstimation::set_colorTemperature(System.Nullable`1<System.Single>)
extern void BasicLightEstimation_set_colorTemperature_mCCA65C52707512E9A69A4436AE21829E9E557690 (void);
// 0x0000023B System.Nullable`1<UnityEngine.Color> UnityEngine.XR.ARFoundation.Samples.BasicLightEstimation::get_colorCorrection()
extern void BasicLightEstimation_get_colorCorrection_m4A7BE7EE9AA132D8BAE721F32AE1F7988D4BFB67 (void);
// 0x0000023C System.Void UnityEngine.XR.ARFoundation.Samples.BasicLightEstimation::set_colorCorrection(System.Nullable`1<UnityEngine.Color>)
extern void BasicLightEstimation_set_colorCorrection_mCA839E2B4EDFCABB3603E399B0266AC734EE03E1 (void);
// 0x0000023D System.Void UnityEngine.XR.ARFoundation.Samples.BasicLightEstimation::Awake()
extern void BasicLightEstimation_Awake_m1082ECB1B7B816098B1CF6E623577B6E4BD97667 (void);
// 0x0000023E System.Void UnityEngine.XR.ARFoundation.Samples.BasicLightEstimation::OnEnable()
extern void BasicLightEstimation_OnEnable_m7420DDF8C4C19430384B021B6CAA709B9669D773 (void);
// 0x0000023F System.Void UnityEngine.XR.ARFoundation.Samples.BasicLightEstimation::OnDisable()
extern void BasicLightEstimation_OnDisable_m3F5488EC84F1541AA24CD6D493285B24591C310E (void);
// 0x00000240 System.Void UnityEngine.XR.ARFoundation.Samples.BasicLightEstimation::FrameChanged(UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs)
extern void BasicLightEstimation_FrameChanged_m5921A950A4A78F1C7B010A1C59D41F5CB3D7E6DF (void);
// 0x00000241 System.Void UnityEngine.XR.ARFoundation.Samples.BasicLightEstimation::.ctor()
extern void BasicLightEstimation__ctor_m4DB7B5CDE38B20A9A78C3FF153387217F055D174 (void);
// 0x00000242 UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.BasicLightEstimationUI::get_ambientIntensityText()
extern void BasicLightEstimationUI_get_ambientIntensityText_mF5DD9EC437B8EEA9CC3D88112C3CB709CA415758 (void);
// 0x00000243 System.Void UnityEngine.XR.ARFoundation.Samples.BasicLightEstimationUI::set_ambientIntensityText(UnityEngine.UI.Text)
extern void BasicLightEstimationUI_set_ambientIntensityText_m73EBAF44DE08F1EF99681575453A6EAE4051BFC3 (void);
// 0x00000244 UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.BasicLightEstimationUI::get_ambientColorText()
extern void BasicLightEstimationUI_get_ambientColorText_m78683D6B9E19CF20CC0CA57EBBB86626DAAE69E2 (void);
// 0x00000245 System.Void UnityEngine.XR.ARFoundation.Samples.BasicLightEstimationUI::set_ambientColorText(UnityEngine.UI.Text)
extern void BasicLightEstimationUI_set_ambientColorText_m65ED5BE1033D384547B5889897B586F990FD93C1 (void);
// 0x00000246 System.Void UnityEngine.XR.ARFoundation.Samples.BasicLightEstimationUI::Awake()
extern void BasicLightEstimationUI_Awake_mD8481D54BD2FD529DBA49FAAAEA8484B72E54D26 (void);
// 0x00000247 System.Void UnityEngine.XR.ARFoundation.Samples.BasicLightEstimationUI::Update()
extern void BasicLightEstimationUI_Update_m7C6A4CB5E40734089F881B261E0BF449624576A0 (void);
// 0x00000248 System.Void UnityEngine.XR.ARFoundation.Samples.BasicLightEstimationUI::SetUIValue(System.Nullable`1<T>,UnityEngine.UI.Text)
// 0x00000249 System.Void UnityEngine.XR.ARFoundation.Samples.BasicLightEstimationUI::.ctor()
extern void BasicLightEstimationUI__ctor_m562829C03EC74BFA75B9510FEF1136DB91E0DDC3 (void);
// 0x0000024A UnityEngine.Transform UnityEngine.XR.ARFoundation.Samples.BoneController::get_skeletonRoot()
extern void BoneController_get_skeletonRoot_m697B5CE6F22C45E1B14D11D825CC1752246593C8 (void);
// 0x0000024B System.Void UnityEngine.XR.ARFoundation.Samples.BoneController::set_skeletonRoot(UnityEngine.Transform)
extern void BoneController_set_skeletonRoot_m55AB83224C341EB9A485028D6B36DA0BAB87AA00 (void);
// 0x0000024C System.Void UnityEngine.XR.ARFoundation.Samples.BoneController::InitializeSkeletonJoints()
extern void BoneController_InitializeSkeletonJoints_m7D3A632559B7682732BC315D41D5773075835908 (void);
// 0x0000024D System.Void UnityEngine.XR.ARFoundation.Samples.BoneController::ApplyBodyPose(UnityEngine.XR.ARFoundation.ARHumanBody)
extern void BoneController_ApplyBodyPose_m56466BFEAEB2AAE2690B972D1D1183282B702B2A (void);
// 0x0000024E System.Void UnityEngine.XR.ARFoundation.Samples.BoneController::ProcessJoint(UnityEngine.Transform)
extern void BoneController_ProcessJoint_m30129C62C2813FDCA741CB988F479972B2B68FF0 (void);
// 0x0000024F System.Int32 UnityEngine.XR.ARFoundation.Samples.BoneController::GetJointIndex(System.String)
extern void BoneController_GetJointIndex_mCC196D3A8A3842E5BA8F9182A50F3A809F58DD9C (void);
// 0x00000250 System.Void UnityEngine.XR.ARFoundation.Samples.BoneController::.ctor()
extern void BoneController__ctor_m3F59B437317CD5417C12D28337377FC2CC39566B (void);
// 0x00000251 UnityEngine.XR.ARFoundation.ARCameraManager UnityEngine.XR.ARFoundation.Samples.CameraConfigController::get_cameraManager()
extern void CameraConfigController_get_cameraManager_m393112C1D6D75EC49E1F208B723ED8D9C4E36945 (void);
// 0x00000252 System.Void UnityEngine.XR.ARFoundation.Samples.CameraConfigController::set_cameraManager(UnityEngine.XR.ARFoundation.ARCameraManager)
extern void CameraConfigController_set_cameraManager_m20E7984E7798F2001B23AA53056C52E0C01BA5E9 (void);
// 0x00000253 System.Void UnityEngine.XR.ARFoundation.Samples.CameraConfigController::OnDropdownValueChanged(UnityEngine.UI.Dropdown)
extern void CameraConfigController_OnDropdownValueChanged_m0D6E37DB8DB0B9D1980842ED425FE70522810A19 (void);
// 0x00000254 System.Void UnityEngine.XR.ARFoundation.Samples.CameraConfigController::Awake()
extern void CameraConfigController_Awake_m0FC893876C79EE026661CA139E15D56E90E5B62B (void);
// 0x00000255 System.Void UnityEngine.XR.ARFoundation.Samples.CameraConfigController::PopulateDropdown()
extern void CameraConfigController_PopulateDropdown_m63B6FD3782AC6F12727DCEF7FBF613978A4779F9 (void);
// 0x00000256 System.Void UnityEngine.XR.ARFoundation.Samples.CameraConfigController::Update()
extern void CameraConfigController_Update_m5B1792D6E2A38AB7B0481AE4774A389629128CEA (void);
// 0x00000257 System.Void UnityEngine.XR.ARFoundation.Samples.CameraConfigController::.ctor()
extern void CameraConfigController__ctor_m8B0D7D7C40BF77774E6D98A531833A4487149AF1 (void);
// 0x00000258 System.Void UnityEngine.XR.ARFoundation.Samples.CameraConfigController::<Awake>b__7_0(System.Int32)
extern void CameraConfigController_U3CAwakeU3Eb__7_0_m7485FB75401600BD48EE6B0D723A604769A2FC84 (void);
// 0x00000259 UnityEngine.XR.ARFoundation.ARCameraManager UnityEngine.XR.ARFoundation.Samples.CameraSwapper::get_cameraManager()
extern void CameraSwapper_get_cameraManager_m45680899758DB6717619A287F5C5F3255AE125A0 (void);
// 0x0000025A System.Void UnityEngine.XR.ARFoundation.Samples.CameraSwapper::set_cameraManager(UnityEngine.XR.ARFoundation.ARCameraManager)
extern void CameraSwapper_set_cameraManager_m697FF15D67E329D79298042416E37B5327F45D63 (void);
// 0x0000025B System.Void UnityEngine.XR.ARFoundation.Samples.CameraSwapper::OnSwapCameraButtonPress()
extern void CameraSwapper_OnSwapCameraButtonPress_mDF7ACDCCDEC3594633234E496908DDF0732E2EF3 (void);
// 0x0000025C System.Void UnityEngine.XR.ARFoundation.Samples.CameraSwapper::.ctor()
extern void CameraSwapper__ctor_m5CAE77BEAF6CA736958740E7816CB34B7A27013A (void);
// 0x0000025D UnityEngine.XR.ARFoundation.ARCameraManager UnityEngine.XR.ARFoundation.Samples.CpuImageSample::get_cameraManager()
extern void CpuImageSample_get_cameraManager_m15DC9F90C267E95F1D327EED1C62AE9A32E5BF24 (void);
// 0x0000025E System.Void UnityEngine.XR.ARFoundation.Samples.CpuImageSample::set_cameraManager(UnityEngine.XR.ARFoundation.ARCameraManager)
extern void CpuImageSample_set_cameraManager_m94232E4E08912AD4D6BAB3093859E0C3CE217EBC (void);
// 0x0000025F UnityEngine.UI.RawImage UnityEngine.XR.ARFoundation.Samples.CpuImageSample::get_rawCameraImage()
extern void CpuImageSample_get_rawCameraImage_m6F2D78AFF0CE51931A6CD679FDCCEE3CBF066CDB (void);
// 0x00000260 System.Void UnityEngine.XR.ARFoundation.Samples.CpuImageSample::set_rawCameraImage(UnityEngine.UI.RawImage)
extern void CpuImageSample_set_rawCameraImage_mF8CD35C518B2D27381ACFB40994C8BFA170611FB (void);
// 0x00000261 UnityEngine.XR.ARFoundation.AROcclusionManager UnityEngine.XR.ARFoundation.Samples.CpuImageSample::get_occlusionManager()
extern void CpuImageSample_get_occlusionManager_m860B08FBDF6109D1EBB6536551BB78607F601C8A (void);
// 0x00000262 System.Void UnityEngine.XR.ARFoundation.Samples.CpuImageSample::set_occlusionManager(UnityEngine.XR.ARFoundation.AROcclusionManager)
extern void CpuImageSample_set_occlusionManager_mDE4BC9B126AA713084D2EED93DADC29128ECE236 (void);
// 0x00000263 UnityEngine.UI.RawImage UnityEngine.XR.ARFoundation.Samples.CpuImageSample::get_rawHumanDepthImage()
extern void CpuImageSample_get_rawHumanDepthImage_m9E8150358C97F4832AB296EEDCAFE1EF2226E8C9 (void);
// 0x00000264 System.Void UnityEngine.XR.ARFoundation.Samples.CpuImageSample::set_rawHumanDepthImage(UnityEngine.UI.RawImage)
extern void CpuImageSample_set_rawHumanDepthImage_mB58AD845F964B70DF7D2D890F4EACC8067F9AC48 (void);
// 0x00000265 UnityEngine.UI.RawImage UnityEngine.XR.ARFoundation.Samples.CpuImageSample::get_rawHumanStencilImage()
extern void CpuImageSample_get_rawHumanStencilImage_m63A967551A83F182E6EE08A22A5F78A1005B2DFF (void);
// 0x00000266 System.Void UnityEngine.XR.ARFoundation.Samples.CpuImageSample::set_rawHumanStencilImage(UnityEngine.UI.RawImage)
extern void CpuImageSample_set_rawHumanStencilImage_m04AE7D841E9BB7D25563B5CA53945CA8BDC8B954 (void);
// 0x00000267 UnityEngine.UI.RawImage UnityEngine.XR.ARFoundation.Samples.CpuImageSample::get_rawEnvironmentDepthImage()
extern void CpuImageSample_get_rawEnvironmentDepthImage_m857278B6770C5DF1282A28CB85DA0C1188BD456B (void);
// 0x00000268 System.Void UnityEngine.XR.ARFoundation.Samples.CpuImageSample::set_rawEnvironmentDepthImage(UnityEngine.UI.RawImage)
extern void CpuImageSample_set_rawEnvironmentDepthImage_m45A4FC88B175348475BE5DD04980153223C047BF (void);
// 0x00000269 UnityEngine.UI.RawImage UnityEngine.XR.ARFoundation.Samples.CpuImageSample::get_rawEnvironmentDepthConfidenceImage()
extern void CpuImageSample_get_rawEnvironmentDepthConfidenceImage_m7991CE32992B2F0A9A7B6B80578E97018F7AEAEE (void);
// 0x0000026A System.Void UnityEngine.XR.ARFoundation.Samples.CpuImageSample::set_rawEnvironmentDepthConfidenceImage(UnityEngine.UI.RawImage)
extern void CpuImageSample_set_rawEnvironmentDepthConfidenceImage_mCAE5C3BE528A1B8F889765604392045EEF06C4EE (void);
// 0x0000026B UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.CpuImageSample::get_imageInfo()
extern void CpuImageSample_get_imageInfo_m25E380710AB1DE2E0B31AF3F2256D8D5948D67F1 (void);
// 0x0000026C System.Void UnityEngine.XR.ARFoundation.Samples.CpuImageSample::set_imageInfo(UnityEngine.UI.Text)
extern void CpuImageSample_set_imageInfo_m33E658759CE129958F3E75E46D0D22AC1D348692 (void);
// 0x0000026D UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.CpuImageSample::get_transformationButton()
extern void CpuImageSample_get_transformationButton_m577D64F70296935B6638D4235F1CE5F0A8CD4236 (void);
// 0x0000026E System.Void UnityEngine.XR.ARFoundation.Samples.CpuImageSample::set_transformationButton(UnityEngine.UI.Button)
extern void CpuImageSample_set_transformationButton_mAEA3EBBA554CA0DEB95094FC072E902977EE084F (void);
// 0x0000026F System.Void UnityEngine.XR.ARFoundation.Samples.CpuImageSample::CycleTransformation()
extern void CpuImageSample_CycleTransformation_m93E95F9CCD059F1BE0EBB4B10F57710B409B9715 (void);
// 0x00000270 System.Void UnityEngine.XR.ARFoundation.Samples.CpuImageSample::OnEnable()
extern void CpuImageSample_OnEnable_m2105BFDB78E13309B81CEA650FDEC727BAAB012A (void);
// 0x00000271 System.Void UnityEngine.XR.ARFoundation.Samples.CpuImageSample::OnDisable()
extern void CpuImageSample_OnDisable_m2FA10B8069C0A3FA86561792AC18251575156F81 (void);
// 0x00000272 System.Void UnityEngine.XR.ARFoundation.Samples.CpuImageSample::UpdateCameraImage()
extern void CpuImageSample_UpdateCameraImage_m3F11902F25F15184BD3A72115CA69E1797968850 (void);
// 0x00000273 System.Void UnityEngine.XR.ARFoundation.Samples.CpuImageSample::UpdateHumanDepthImage()
extern void CpuImageSample_UpdateHumanDepthImage_m58BBCBE881DBF069845FA097C0F73E4786078771 (void);
// 0x00000274 System.Void UnityEngine.XR.ARFoundation.Samples.CpuImageSample::UpdateHumanStencilImage()
extern void CpuImageSample_UpdateHumanStencilImage_mA9F27AAAA22113EA3230435EECC21023D66A6D08 (void);
// 0x00000275 System.Void UnityEngine.XR.ARFoundation.Samples.CpuImageSample::UpdateEnvironmentDepthImage()
extern void CpuImageSample_UpdateEnvironmentDepthImage_mBDFBE3464DCA551FEA4D76683B61F88562C27CEB (void);
// 0x00000276 System.Void UnityEngine.XR.ARFoundation.Samples.CpuImageSample::UpdateEnvironmentDepthConfidenceImage()
extern void CpuImageSample_UpdateEnvironmentDepthConfidenceImage_m11D88113BC0DEE8B92E62D2941174827B45C8827 (void);
// 0x00000277 System.Void UnityEngine.XR.ARFoundation.Samples.CpuImageSample::UpdateRawImage(UnityEngine.UI.RawImage,UnityEngine.XR.ARSubsystems.XRCpuImage,UnityEngine.XR.ARSubsystems.XRCpuImage/Transformation)
extern void CpuImageSample_UpdateRawImage_m0AEEF002454523157ADF5DD753A985C6AE4D2271 (void);
// 0x00000278 System.Void UnityEngine.XR.ARFoundation.Samples.CpuImageSample::OnCameraFrameReceived(UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs)
extern void CpuImageSample_OnCameraFrameReceived_mEEF1868F0CA5A61920465C9990681C8842708DBF (void);
// 0x00000279 System.Void UnityEngine.XR.ARFoundation.Samples.CpuImageSample::.ctor()
extern void CpuImageSample__ctor_m1CFB846E16C673B6723C106A6E4D35C597D3CD1F (void);
// 0x0000027A UnityEngine.XR.ARFoundation.ARSession UnityEngine.XR.ARFoundation.Samples.CustomConfigurationChooser::get_session()
extern void CustomConfigurationChooser_get_session_m02CF779466E363657FF1F81FC83663BD161B9ED6 (void);
// 0x0000027B System.Void UnityEngine.XR.ARFoundation.Samples.CustomConfigurationChooser::set_session(UnityEngine.XR.ARFoundation.ARSession)
extern void CustomConfigurationChooser_set_session_m845D42CB95D87B00DE0EED482E0C919D6FEE44EA (void);
// 0x0000027C System.Void UnityEngine.XR.ARFoundation.Samples.CustomConfigurationChooser::Start()
extern void CustomConfigurationChooser_Start_m6261E4058AD28C3627E444A43E24CD7CDB3CEC4E (void);
// 0x0000027D System.Void UnityEngine.XR.ARFoundation.Samples.CustomConfigurationChooser::OnDepthModeDropdownValueChanged(UnityEngine.UI.Dropdown)
extern void CustomConfigurationChooser_OnDepthModeDropdownValueChanged_m59AF7B6746B2145E512AE8DE3AB3287444E208EF (void);
// 0x0000027E System.Void UnityEngine.XR.ARFoundation.Samples.CustomConfigurationChooser::UpdateConfigurationChooser()
extern void CustomConfigurationChooser_UpdateConfigurationChooser_m12CE92F7124DABFC5081F5C60F7AC2B2A7957315 (void);
// 0x0000027F System.Void UnityEngine.XR.ARFoundation.Samples.CustomConfigurationChooser::.ctor()
extern void CustomConfigurationChooser__ctor_mC49F3D4437FDCE4C96F4513804B55E7D1D2EF399 (void);
// 0x00000280 System.Void UnityEngine.XR.ARFoundation.Samples.CustomConfigurationChooser::.cctor()
extern void CustomConfigurationChooser__cctor_m9AE5CBBCC16D5DF81DE428E2D7A83E443064E4B5 (void);
// 0x00000281 UnityEngine.XR.ARSubsystems.Configuration UnityEngine.XR.ARFoundation.Samples.CustomConfigurationChooser/PreferCameraConfigurationChooser::ChooseConfiguration(Unity.Collections.NativeSlice`1<UnityEngine.XR.ARSubsystems.ConfigurationDescriptor>,UnityEngine.XR.ARSubsystems.Feature)
extern void PreferCameraConfigurationChooser_ChooseConfiguration_m78AF1466A38DC288F00EA299D86DA2BA8135BE65 (void);
// 0x00000282 System.Void UnityEngine.XR.ARFoundation.Samples.CustomConfigurationChooser/PreferCameraConfigurationChooser::.ctor()
extern void PreferCameraConfigurationChooser__ctor_mFF508587CD4779A9B98A50CE1498859BE702DE92 (void);
// 0x00000283 UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.DisableVerticalPlanes::get_logText()
extern void DisableVerticalPlanes_get_logText_m4FC2821AB03D129738B7D97ABB66A8D522829ABB (void);
// 0x00000284 System.Void UnityEngine.XR.ARFoundation.Samples.DisableVerticalPlanes::set_logText(UnityEngine.UI.Text)
extern void DisableVerticalPlanes_set_logText_m2CF8BAF19AEEDEE9966FD2D11204EA217DEFA6C5 (void);
// 0x00000285 System.Void UnityEngine.XR.ARFoundation.Samples.DisableVerticalPlanes::OnEnable()
extern void DisableVerticalPlanes_OnEnable_m6025E522FF3272391926FB389BDC44B8AAD91E23 (void);
// 0x00000286 System.Void UnityEngine.XR.ARFoundation.Samples.DisableVerticalPlanes::OnDisable()
extern void DisableVerticalPlanes_OnDisable_m6B0DF33C6DCF0B12EEBB4C23B6DCF44F4244CFA7 (void);
// 0x00000287 System.Void UnityEngine.XR.ARFoundation.Samples.DisableVerticalPlanes::OnPlaneAdded(UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs)
extern void DisableVerticalPlanes_OnPlaneAdded_m340CA415E7B4742A92E480107555C24570BA8E32 (void);
// 0x00000288 System.Void UnityEngine.XR.ARFoundation.Samples.DisableVerticalPlanes::DisableIfVertical(UnityEngine.XR.ARFoundation.ARPlane)
extern void DisableVerticalPlanes_DisableIfVertical_mD91BD30FE87A833AF7E3E25078A24632BD4DBD49 (void);
// 0x00000289 System.Void UnityEngine.XR.ARFoundation.Samples.DisableVerticalPlanes::.ctor()
extern void DisableVerticalPlanes__ctor_mDF58BA984AAB54DB4C7619A90A777251CC202352 (void);
// 0x0000028A UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.DisplayARSessionInformation::get_infoText()
extern void DisplayARSessionInformation_get_infoText_m288E28D2E54F488632E836623E845C70B480F9E8 (void);
// 0x0000028B System.Void UnityEngine.XR.ARFoundation.Samples.DisplayARSessionInformation::set_infoText(UnityEngine.UI.Text)
extern void DisplayARSessionInformation_set_infoText_mB7DD855E260347EC58B7AED5ABEA1ED3C09B88DC (void);
// 0x0000028C UnityEngine.XR.ARFoundation.ARCameraManager UnityEngine.XR.ARFoundation.Samples.DisplayARSessionInformation::get_cameraManager()
extern void DisplayARSessionInformation_get_cameraManager_mB0F37919CC75A6E945F99FF0039CE42032EAF478 (void);
// 0x0000028D System.Void UnityEngine.XR.ARFoundation.Samples.DisplayARSessionInformation::set_cameraManager(UnityEngine.XR.ARFoundation.ARCameraManager)
extern void DisplayARSessionInformation_set_cameraManager_mE5BAEF3345E95178447A5C5D20166D2E5FF130B8 (void);
// 0x0000028E UnityEngine.XR.ARFoundation.ARPlaneManager UnityEngine.XR.ARFoundation.Samples.DisplayARSessionInformation::get_planeManager()
extern void DisplayARSessionInformation_get_planeManager_mB993D1332A5C7B1ED6098DB3E42E889D3FA1F2D1 (void);
// 0x0000028F System.Void UnityEngine.XR.ARFoundation.Samples.DisplayARSessionInformation::set_planeManager(UnityEngine.XR.ARFoundation.ARPlaneManager)
extern void DisplayARSessionInformation_set_planeManager_m59A893B36F0B903AC74B0964506A54001EB7CE7A (void);
// 0x00000290 UnityEngine.XR.ARFoundation.AROcclusionManager UnityEngine.XR.ARFoundation.Samples.DisplayARSessionInformation::get_occlusionManager()
extern void DisplayARSessionInformation_get_occlusionManager_mA4612967D9F128FA04223D9B15AE24D2A645DD43 (void);
// 0x00000291 System.Void UnityEngine.XR.ARFoundation.Samples.DisplayARSessionInformation::set_occlusionManager(UnityEngine.XR.ARFoundation.AROcclusionManager)
extern void DisplayARSessionInformation_set_occlusionManager_m6ED77883F5AB357BEF8C7432663B5F226B4ED3A1 (void);
// 0x00000292 System.Void UnityEngine.XR.ARFoundation.Samples.DisplayARSessionInformation::Update()
extern void DisplayARSessionInformation_Update_m22C72883BEABD571D84995070F9A8D85CCAC2353 (void);
// 0x00000293 System.Void UnityEngine.XR.ARFoundation.Samples.DisplayARSessionInformation::BuildCameraMangerInfo(System.Text.StringBuilder)
extern void DisplayARSessionInformation_BuildCameraMangerInfo_m373CF5EDA0E015E745E1F54166C347275E46DA79 (void);
// 0x00000294 System.Void UnityEngine.XR.ARFoundation.Samples.DisplayARSessionInformation::BuildPlaneMangerInfo(System.Text.StringBuilder)
extern void DisplayARSessionInformation_BuildPlaneMangerInfo_m03F66BE39C78397C3E5D23E65F726D9FD58DCC6B (void);
// 0x00000295 System.Void UnityEngine.XR.ARFoundation.Samples.DisplayARSessionInformation::BuildOcclusionMangerInfo(System.Text.StringBuilder)
extern void DisplayARSessionInformation_BuildOcclusionMangerInfo_m9EAD0DC2AA7F61824A9B12C156B9F8A7EC10D73C (void);
// 0x00000296 System.Void UnityEngine.XR.ARFoundation.Samples.DisplayARSessionInformation::LogText(System.String)
extern void DisplayARSessionInformation_LogText_mC2717993968CC3B668B0B3737B95FE24A1CD62AF (void);
// 0x00000297 System.Void UnityEngine.XR.ARFoundation.Samples.DisplayARSessionInformation::.ctor()
extern void DisplayARSessionInformation__ctor_m5BF2029E9983E6492786D7215BF1C08F10EE0905 (void);
// 0x00000298 UnityEngine.XR.ARFoundation.AROcclusionManager UnityEngine.XR.ARFoundation.Samples.DisplayDepthImage::get_occlusionManager()
extern void DisplayDepthImage_get_occlusionManager_m4B6F625E54247ECD7DE19B9B89B358CB8AF6A64D (void);
// 0x00000299 System.Void UnityEngine.XR.ARFoundation.Samples.DisplayDepthImage::set_occlusionManager(UnityEngine.XR.ARFoundation.AROcclusionManager)
extern void DisplayDepthImage_set_occlusionManager_m4F84AA719DF0589955056F0561FE7945A1476078 (void);
// 0x0000029A UnityEngine.XR.ARFoundation.ARCameraManager UnityEngine.XR.ARFoundation.Samples.DisplayDepthImage::get_cameraManager()
extern void DisplayDepthImage_get_cameraManager_mA930D2941683660701D8F8C7E59833660C5E9D3F (void);
// 0x0000029B System.Void UnityEngine.XR.ARFoundation.Samples.DisplayDepthImage::set_cameraManager(UnityEngine.XR.ARFoundation.ARCameraManager)
extern void DisplayDepthImage_set_cameraManager_m89283B3ABCC6B111018CF4CA754E655736CB70F1 (void);
// 0x0000029C UnityEngine.UI.RawImage UnityEngine.XR.ARFoundation.Samples.DisplayDepthImage::get_rawImage()
extern void DisplayDepthImage_get_rawImage_m555BA4023EE84A74F3E48317908D29C1E7C89E01 (void);
// 0x0000029D System.Void UnityEngine.XR.ARFoundation.Samples.DisplayDepthImage::set_rawImage(UnityEngine.UI.RawImage)
extern void DisplayDepthImage_set_rawImage_mF1EA809CB7FDD6DCFAF7B8294DC55A2A97D0C83D (void);
// 0x0000029E UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.DisplayDepthImage::get_imageInfo()
extern void DisplayDepthImage_get_imageInfo_m86E8F1F4E9A868043AC9D7331A11406C58082F55 (void);
// 0x0000029F System.Void UnityEngine.XR.ARFoundation.Samples.DisplayDepthImage::set_imageInfo(UnityEngine.UI.Text)
extern void DisplayDepthImage_set_imageInfo_mC979D02106E58FAEA201DB0EDC3BD487B675A4AF (void);
// 0x000002A0 UnityEngine.Material UnityEngine.XR.ARFoundation.Samples.DisplayDepthImage::get_depthMaterial()
extern void DisplayDepthImage_get_depthMaterial_mDE033B005D94CE31A28C2E282C24A75C0A972D3F (void);
// 0x000002A1 System.Void UnityEngine.XR.ARFoundation.Samples.DisplayDepthImage::set_depthMaterial(UnityEngine.Material)
extern void DisplayDepthImage_set_depthMaterial_m49F8B096FC7F05F32265306103BC29C24323E128 (void);
// 0x000002A2 UnityEngine.Material UnityEngine.XR.ARFoundation.Samples.DisplayDepthImage::get_stencilMaterial()
extern void DisplayDepthImage_get_stencilMaterial_m7D87A8A4341CB44DEBF57D525EE76249D8A9ACCC (void);
// 0x000002A3 System.Void UnityEngine.XR.ARFoundation.Samples.DisplayDepthImage::set_stencilMaterial(UnityEngine.Material)
extern void DisplayDepthImage_set_stencilMaterial_m945286FDB49120653054DD26DD7CC39FB4F76D7F (void);
// 0x000002A4 System.Single UnityEngine.XR.ARFoundation.Samples.DisplayDepthImage::get_maxEnvironmentDistance()
extern void DisplayDepthImage_get_maxEnvironmentDistance_m6E539E7BD828F8F5676AA560D9AA019866D738A0 (void);
// 0x000002A5 System.Void UnityEngine.XR.ARFoundation.Samples.DisplayDepthImage::set_maxEnvironmentDistance(System.Single)
extern void DisplayDepthImage_set_maxEnvironmentDistance_mE5AD8BF6610AB450C153A91332D0FAD369E7A3C5 (void);
// 0x000002A6 System.Single UnityEngine.XR.ARFoundation.Samples.DisplayDepthImage::get_maxHumanDistance()
extern void DisplayDepthImage_get_maxHumanDistance_mFADDABF942055F7F0E0722A399104DC1D9FB222C (void);
// 0x000002A7 System.Void UnityEngine.XR.ARFoundation.Samples.DisplayDepthImage::set_maxHumanDistance(System.Single)
extern void DisplayDepthImage_set_maxHumanDistance_m830E41789F47D11B1DE10F2A5202D047AA33D40C (void);
// 0x000002A8 System.Void UnityEngine.XR.ARFoundation.Samples.DisplayDepthImage::Awake()
extern void DisplayDepthImage_Awake_m6FAB11844F77EE3C9E4A5F1B549CA9C7454A9BEB (void);
// 0x000002A9 System.Void UnityEngine.XR.ARFoundation.Samples.DisplayDepthImage::OnEnable()
extern void DisplayDepthImage_OnEnable_m41A7394A88F1D15900B1525FA56633AAE60FF5C8 (void);
// 0x000002AA System.Void UnityEngine.XR.ARFoundation.Samples.DisplayDepthImage::OnDisable()
extern void DisplayDepthImage_OnDisable_mA4C930A96939F5824635241CFF314D239F89FF1D (void);
// 0x000002AB System.Void UnityEngine.XR.ARFoundation.Samples.DisplayDepthImage::Update()
extern void DisplayDepthImage_Update_mA5ADB24067F51144AF22E93BB52BBF5B72072548 (void);
// 0x000002AC System.Void UnityEngine.XR.ARFoundation.Samples.DisplayDepthImage::OnCameraFrameEventReceived(UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs)
extern void DisplayDepthImage_OnCameraFrameEventReceived_mBF6AFD20DD4A4BE08DC22AF62D452961250879C3 (void);
// 0x000002AD System.Void UnityEngine.XR.ARFoundation.Samples.DisplayDepthImage::BuildTextureInfo(System.Text.StringBuilder,System.String,UnityEngine.Texture2D)
extern void DisplayDepthImage_BuildTextureInfo_m1C378251232FD154A1D572697B870F7F0512D950 (void);
// 0x000002AE System.Void UnityEngine.XR.ARFoundation.Samples.DisplayDepthImage::LogText(System.String)
extern void DisplayDepthImage_LogText_mF47707F237D9B84714B240F3AA47921BC74A7A5A (void);
// 0x000002AF System.Void UnityEngine.XR.ARFoundation.Samples.DisplayDepthImage::UpdateRawImage()
extern void DisplayDepthImage_UpdateRawImage_m79B0E475C16EA6C8BA70D8809C594FD86B508763 (void);
// 0x000002B0 System.Void UnityEngine.XR.ARFoundation.Samples.DisplayDepthImage::OnDepthModeDropdownValueChanged(UnityEngine.UI.Dropdown)
extern void DisplayDepthImage_OnDepthModeDropdownValueChanged_mA9702D1DB4784BC61EB08EFFD4BB3A00E26A2E09 (void);
// 0x000002B1 System.Void UnityEngine.XR.ARFoundation.Samples.DisplayDepthImage::.ctor()
extern void DisplayDepthImage__ctor_mCD4ECA7AE1314093C83BB107869971BCDFDB5B51 (void);
// 0x000002B2 System.Void UnityEngine.XR.ARFoundation.Samples.DisplayDepthImage::.cctor()
extern void DisplayDepthImage__cctor_m14CFB48C8FB7B4DB6BDE4A108024844F3DD59736 (void);
// 0x000002B3 UnityEngine.ReflectionProbe UnityEngine.XR.ARFoundation.Samples.EnvironmentProbeVisualizer::get_reflectionProbe()
extern void EnvironmentProbeVisualizer_get_reflectionProbe_m61ADE1ED312E60EADCFACD21D7A0FE125848CB4B (void);
// 0x000002B4 System.Void UnityEngine.XR.ARFoundation.Samples.EnvironmentProbeVisualizer::set_reflectionProbe(UnityEngine.ReflectionProbe)
extern void EnvironmentProbeVisualizer_set_reflectionProbe_mD70DC1DDD225326BDEDF51D1B21A44B9F5456C1E (void);
// 0x000002B5 System.Void UnityEngine.XR.ARFoundation.Samples.EnvironmentProbeVisualizer::Update()
extern void EnvironmentProbeVisualizer_Update_m677D36029326CD0EC1EB87C7FF711645B35AFC97 (void);
// 0x000002B6 System.Void UnityEngine.XR.ARFoundation.Samples.EnvironmentProbeVisualizer::.ctor()
extern void EnvironmentProbeVisualizer__ctor_m5F51424429041401691F83C82FAD8CF4D5BDA636 (void);
// 0x000002B7 UnityEngine.GameObject UnityEngine.XR.ARFoundation.Samples.EyePoseVisualizer::get_eyePrefab()
extern void EyePoseVisualizer_get_eyePrefab_m74D5C1314105BF8202A27531428429E297314B6F (void);
// 0x000002B8 System.Void UnityEngine.XR.ARFoundation.Samples.EyePoseVisualizer::set_eyePrefab(UnityEngine.GameObject)
extern void EyePoseVisualizer_set_eyePrefab_mDC3549A5C860D76CB5F9D3CFB62257A2FA45BCDC (void);
// 0x000002B9 System.Void UnityEngine.XR.ARFoundation.Samples.EyePoseVisualizer::Awake()
extern void EyePoseVisualizer_Awake_m0610E20376FC9A93E122D9943153AF9067A25DF1 (void);
// 0x000002BA System.Void UnityEngine.XR.ARFoundation.Samples.EyePoseVisualizer::CreateEyeGameObjectsIfNecessary()
extern void EyePoseVisualizer_CreateEyeGameObjectsIfNecessary_mDE18B2E9AADB86A108A1C2F2663E19CAC766A7F8 (void);
// 0x000002BB System.Void UnityEngine.XR.ARFoundation.Samples.EyePoseVisualizer::SetVisible(System.Boolean)
extern void EyePoseVisualizer_SetVisible_m9BAA4E2E9FA0F367A1C1BD82548AFD27B70A297F (void);
// 0x000002BC System.Void UnityEngine.XR.ARFoundation.Samples.EyePoseVisualizer::OnEnable()
extern void EyePoseVisualizer_OnEnable_m22DFFE533C432D6C77A65943F63825442A1C0A34 (void);
// 0x000002BD System.Void UnityEngine.XR.ARFoundation.Samples.EyePoseVisualizer::OnDisable()
extern void EyePoseVisualizer_OnDisable_m5E622EA50288E0FDD02CFEEA7A02CAAA1704FD24 (void);
// 0x000002BE System.Void UnityEngine.XR.ARFoundation.Samples.EyePoseVisualizer::OnUpdated(UnityEngine.XR.ARFoundation.ARFaceUpdatedEventArgs)
extern void EyePoseVisualizer_OnUpdated_mC63CD584AB5AB242D3FBF355CCB1D6C113C237B4 (void);
// 0x000002BF System.Void UnityEngine.XR.ARFoundation.Samples.EyePoseVisualizer::.ctor()
extern void EyePoseVisualizer__ctor_m559A9515B10C94B45336F660BA20258BD5AE3AE0 (void);
// 0x000002C0 System.Void UnityEngine.XR.ARFoundation.Samples.EyeTrackingUI::OnEnable()
extern void EyeTrackingUI_OnEnable_m93171CFDBE6A15584E3EAE557D17CAE682C5E787 (void);
// 0x000002C1 System.Void UnityEngine.XR.ARFoundation.Samples.EyeTrackingUI::.ctor()
extern void EyeTrackingUI__ctor_m56E74AD4A03C5A4F72D57C0A1DFFC3473F3C4BA4 (void);
// 0x000002C2 UnityEngine.Material[] UnityEngine.XR.ARFoundation.Samples.FaceMaterialSwitcher::get_faceMaterials()
extern void FaceMaterialSwitcher_get_faceMaterials_mDB48B48C5AA4BE6467FD42BC1B391A16F4C13C69 (void);
// 0x000002C3 System.Void UnityEngine.XR.ARFoundation.Samples.FaceMaterialSwitcher::set_faceMaterials(UnityEngine.Material[])
extern void FaceMaterialSwitcher_set_faceMaterials_m8497777F759682093C8B7C77D6FD063B7C0A3D92 (void);
// 0x000002C4 System.Void UnityEngine.XR.ARFoundation.Samples.FaceMaterialSwitcher::Start()
extern void FaceMaterialSwitcher_Start_mDA3CDC9F077770F4ED3FE67314F3C50B14D25412 (void);
// 0x000002C5 System.Void UnityEngine.XR.ARFoundation.Samples.FaceMaterialSwitcher::.ctor()
extern void FaceMaterialSwitcher__ctor_m3552FA11C9908431E2FCD579CA07A48B05183880 (void);
// 0x000002C6 System.Void UnityEngine.XR.ARFoundation.Samples.FaceMaterialSwitcher::.cctor()
extern void FaceMaterialSwitcher__cctor_mE5A6A6016E634C9FBBE9873E21E69C0348D3FBE2 (void);
// 0x000002C7 UnityEngine.GameObject UnityEngine.XR.ARFoundation.Samples.FixationPoint2DVisualizer::get_fixationReticlePrefab()
extern void FixationPoint2DVisualizer_get_fixationReticlePrefab_m0AC88784C743616CF8BE1DE9E59F416DD01C549E (void);
// 0x000002C8 System.Void UnityEngine.XR.ARFoundation.Samples.FixationPoint2DVisualizer::set_fixationReticlePrefab(UnityEngine.GameObject)
extern void FixationPoint2DVisualizer_set_fixationReticlePrefab_m6612A3DF1FF5FCF7B97D1A50A6F8828F2A3CCD8E (void);
// 0x000002C9 System.Void UnityEngine.XR.ARFoundation.Samples.FixationPoint2DVisualizer::Awake()
extern void FixationPoint2DVisualizer_Awake_mB70B69DDC32EA887DD982D653F3CAA206780C116 (void);
// 0x000002CA System.Void UnityEngine.XR.ARFoundation.Samples.FixationPoint2DVisualizer::CreateEyeGameObjectsIfNecessary()
extern void FixationPoint2DVisualizer_CreateEyeGameObjectsIfNecessary_m0725F95191DDC8E56C034455E3CDB1A6E6470013 (void);
// 0x000002CB System.Void UnityEngine.XR.ARFoundation.Samples.FixationPoint2DVisualizer::SetVisible(System.Boolean)
extern void FixationPoint2DVisualizer_SetVisible_m6E379224EB59A0F4F9AF05CF292CDFEDB7162E8D (void);
// 0x000002CC System.Void UnityEngine.XR.ARFoundation.Samples.FixationPoint2DVisualizer::OnEnable()
extern void FixationPoint2DVisualizer_OnEnable_mC9C9B32F2BC0D095AACAD5CD2D5E971C087308A3 (void);
// 0x000002CD System.Void UnityEngine.XR.ARFoundation.Samples.FixationPoint2DVisualizer::OnDisable()
extern void FixationPoint2DVisualizer_OnDisable_m67E6CED611148BB2099C41794D5BEA61FEDD9B8B (void);
// 0x000002CE System.Void UnityEngine.XR.ARFoundation.Samples.FixationPoint2DVisualizer::OnUpdated(UnityEngine.XR.ARFoundation.ARFaceUpdatedEventArgs)
extern void FixationPoint2DVisualizer_OnUpdated_m4D5CB9273DDBE8709F7EFDB9A7C997183CC83C92 (void);
// 0x000002CF System.Void UnityEngine.XR.ARFoundation.Samples.FixationPoint2DVisualizer::UpdateScreenReticle()
extern void FixationPoint2DVisualizer_UpdateScreenReticle_mC64AFC5718C4D4CB3D4A86E8D000D4177199C2E5 (void);
// 0x000002D0 System.Void UnityEngine.XR.ARFoundation.Samples.FixationPoint2DVisualizer::.ctor()
extern void FixationPoint2DVisualizer__ctor_mE3275F757592205E593A9C7E1894F19B16A0A16F (void);
// 0x000002D1 UnityEngine.GameObject UnityEngine.XR.ARFoundation.Samples.FixationPoint3DVisualizer::get_fixationRayPrefab()
extern void FixationPoint3DVisualizer_get_fixationRayPrefab_mDD47B9E5145B54181E687BA651B079E537A18CA1 (void);
// 0x000002D2 System.Void UnityEngine.XR.ARFoundation.Samples.FixationPoint3DVisualizer::set_fixationRayPrefab(UnityEngine.GameObject)
extern void FixationPoint3DVisualizer_set_fixationRayPrefab_mADB921730CB68A8DC5432D1FF555C76805B23292 (void);
// 0x000002D3 System.Void UnityEngine.XR.ARFoundation.Samples.FixationPoint3DVisualizer::Awake()
extern void FixationPoint3DVisualizer_Awake_m00E7437FA6254A80DE07F60FE5131A30ADB73A73 (void);
// 0x000002D4 System.Void UnityEngine.XR.ARFoundation.Samples.FixationPoint3DVisualizer::CreateEyeGameObjectsIfNecessary()
extern void FixationPoint3DVisualizer_CreateEyeGameObjectsIfNecessary_m4894BCD862920D2E748FF47CEA7B3F82DA2DE1D8 (void);
// 0x000002D5 System.Void UnityEngine.XR.ARFoundation.Samples.FixationPoint3DVisualizer::SetVisible(System.Boolean)
extern void FixationPoint3DVisualizer_SetVisible_m8644E24B74D10D6145E8A719F766661EA9CA110A (void);
// 0x000002D6 System.Void UnityEngine.XR.ARFoundation.Samples.FixationPoint3DVisualizer::OnEnable()
extern void FixationPoint3DVisualizer_OnEnable_m74453225F5848B489BFA3F47FD0DEFDA3F9167D8 (void);
// 0x000002D7 System.Void UnityEngine.XR.ARFoundation.Samples.FixationPoint3DVisualizer::OnDisable()
extern void FixationPoint3DVisualizer_OnDisable_mC5E8C4E168F22EB4E2A9A10113C0DDA44D81C897 (void);
// 0x000002D8 System.Void UnityEngine.XR.ARFoundation.Samples.FixationPoint3DVisualizer::OnUpdated(UnityEngine.XR.ARFoundation.ARFaceUpdatedEventArgs)
extern void FixationPoint3DVisualizer_OnUpdated_m669960CF84D281A810B61D87B2E02A72A0E2FE39 (void);
// 0x000002D9 System.Void UnityEngine.XR.ARFoundation.Samples.FixationPoint3DVisualizer::UpdateFixationPoint()
extern void FixationPoint3DVisualizer_UpdateFixationPoint_m3FAB46BD53F9AD16E78A3F7C4489A34805AE9F65 (void);
// 0x000002DA System.Void UnityEngine.XR.ARFoundation.Samples.FixationPoint3DVisualizer::.ctor()
extern void FixationPoint3DVisualizer__ctor_m0986C5F6392CFED3D66D9F273B75ADE77F817794 (void);
// 0x000002DB UnityEngine.Transform UnityEngine.XR.ARFoundation.Samples.HDRLightEstimation::get_arrow()
extern void HDRLightEstimation_get_arrow_m13863AA62B103000FC1B11480939D91DBFF0F9B1 (void);
// 0x000002DC System.Void UnityEngine.XR.ARFoundation.Samples.HDRLightEstimation::set_arrow(UnityEngine.Transform)
extern void HDRLightEstimation_set_arrow_m2C0873884E747E47551B1504283AD00686A31A0C (void);
// 0x000002DD UnityEngine.XR.ARFoundation.ARCameraManager UnityEngine.XR.ARFoundation.Samples.HDRLightEstimation::get_cameraManager()
extern void HDRLightEstimation_get_cameraManager_m812D48793AC05371A33E1BDDD1B64581353DC6EB (void);
// 0x000002DE System.Void UnityEngine.XR.ARFoundation.Samples.HDRLightEstimation::set_cameraManager(UnityEngine.XR.ARFoundation.ARCameraManager)
extern void HDRLightEstimation_set_cameraManager_mE2F1E770C5E2381A53110D41FFCC0F1A9E01D050 (void);
// 0x000002DF System.Nullable`1<System.Single> UnityEngine.XR.ARFoundation.Samples.HDRLightEstimation::get_brightness()
extern void HDRLightEstimation_get_brightness_mAC9A8A666266491EB7548AE78200B3D078BB27A0 (void);
// 0x000002E0 System.Void UnityEngine.XR.ARFoundation.Samples.HDRLightEstimation::set_brightness(System.Nullable`1<System.Single>)
extern void HDRLightEstimation_set_brightness_m75A6F53B06B1774309C54BCDE08189DEF25B5D7D (void);
// 0x000002E1 System.Nullable`1<System.Single> UnityEngine.XR.ARFoundation.Samples.HDRLightEstimation::get_colorTemperature()
extern void HDRLightEstimation_get_colorTemperature_m37FC4B5A3D359B5543FF27975B84A4160674ACE3 (void);
// 0x000002E2 System.Void UnityEngine.XR.ARFoundation.Samples.HDRLightEstimation::set_colorTemperature(System.Nullable`1<System.Single>)
extern void HDRLightEstimation_set_colorTemperature_mD6CEE6C0EB848772B794870BAD31FA934B38265D (void);
// 0x000002E3 System.Nullable`1<UnityEngine.Color> UnityEngine.XR.ARFoundation.Samples.HDRLightEstimation::get_colorCorrection()
extern void HDRLightEstimation_get_colorCorrection_mABD61C33CF8C51594822CED7C9B32A47595647DC (void);
// 0x000002E4 System.Void UnityEngine.XR.ARFoundation.Samples.HDRLightEstimation::set_colorCorrection(System.Nullable`1<UnityEngine.Color>)
extern void HDRLightEstimation_set_colorCorrection_mEEE316804EECE09BC5D2F7AD1497FCA4300DA6CC (void);
// 0x000002E5 System.Nullable`1<UnityEngine.Vector3> UnityEngine.XR.ARFoundation.Samples.HDRLightEstimation::get_mainLightDirection()
extern void HDRLightEstimation_get_mainLightDirection_m2B3E896A43CFC5AD43A77B8F082BD1E2DA86FAD6 (void);
// 0x000002E6 System.Void UnityEngine.XR.ARFoundation.Samples.HDRLightEstimation::set_mainLightDirection(System.Nullable`1<UnityEngine.Vector3>)
extern void HDRLightEstimation_set_mainLightDirection_m0184B2A629BFA8DC8283D6A7B33D7E19EEE2B191 (void);
// 0x000002E7 System.Nullable`1<UnityEngine.Color> UnityEngine.XR.ARFoundation.Samples.HDRLightEstimation::get_mainLightColor()
extern void HDRLightEstimation_get_mainLightColor_m5305DF10F6F7C734959DA7F90F0BE718D90CACAA (void);
// 0x000002E8 System.Void UnityEngine.XR.ARFoundation.Samples.HDRLightEstimation::set_mainLightColor(System.Nullable`1<UnityEngine.Color>)
extern void HDRLightEstimation_set_mainLightColor_m8B42F93CA5A24B47D95DA30AFDAFE13EAE3410E0 (void);
// 0x000002E9 System.Nullable`1<System.Single> UnityEngine.XR.ARFoundation.Samples.HDRLightEstimation::get_mainLightIntensityLumens()
extern void HDRLightEstimation_get_mainLightIntensityLumens_m9B63B9BD7A8409CFF8E1FBB47F8F93EBF10F6326 (void);
// 0x000002EA System.Void UnityEngine.XR.ARFoundation.Samples.HDRLightEstimation::set_mainLightIntensityLumens(System.Nullable`1<System.Single>)
extern void HDRLightEstimation_set_mainLightIntensityLumens_m24A7EF4F8F32DED0A3D5DBFB2CE623D661B8B853 (void);
// 0x000002EB System.Nullable`1<UnityEngine.Rendering.SphericalHarmonicsL2> UnityEngine.XR.ARFoundation.Samples.HDRLightEstimation::get_sphericalHarmonics()
extern void HDRLightEstimation_get_sphericalHarmonics_m2B3F3AFC87ED7EB1ED81D49506C9B61FF03E3F92 (void);
// 0x000002EC System.Void UnityEngine.XR.ARFoundation.Samples.HDRLightEstimation::set_sphericalHarmonics(System.Nullable`1<UnityEngine.Rendering.SphericalHarmonicsL2>)
extern void HDRLightEstimation_set_sphericalHarmonics_m4F08104D790EF9E052E4EA491E5CA447533FFA9B (void);
// 0x000002ED System.Void UnityEngine.XR.ARFoundation.Samples.HDRLightEstimation::Awake()
extern void HDRLightEstimation_Awake_m9CDD40A1EF21B9172AA39C104793241F098967FB (void);
// 0x000002EE System.Void UnityEngine.XR.ARFoundation.Samples.HDRLightEstimation::OnEnable()
extern void HDRLightEstimation_OnEnable_m383309DF85AA9329CE1D3966851433FB244ED138 (void);
// 0x000002EF System.Void UnityEngine.XR.ARFoundation.Samples.HDRLightEstimation::OnDisable()
extern void HDRLightEstimation_OnDisable_m0A56ABEB300E59FC7F05FA11CA037C8F344AB558 (void);
// 0x000002F0 System.Void UnityEngine.XR.ARFoundation.Samples.HDRLightEstimation::OnBeforeRender()
extern void HDRLightEstimation_OnBeforeRender_m5FF4F08EAFA9F57AA9A0B0E05ED0ACC83E0263CF (void);
// 0x000002F1 System.Void UnityEngine.XR.ARFoundation.Samples.HDRLightEstimation::FrameChanged(UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs)
extern void HDRLightEstimation_FrameChanged_m5227B42F4E8A32FC08AAFF14C23D5849F739E6EA (void);
// 0x000002F2 System.Void UnityEngine.XR.ARFoundation.Samples.HDRLightEstimation::.ctor()
extern void HDRLightEstimation__ctor_mA6FA8FE8A8DE001B4765024E9536912E328EF584 (void);
// 0x000002F3 UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.HDRLightEstimationUI::get_ambientIntensityText()
extern void HDRLightEstimationUI_get_ambientIntensityText_mF860111603CCAF7758DB4D66F6A5A0BF07694C83 (void);
// 0x000002F4 System.Void UnityEngine.XR.ARFoundation.Samples.HDRLightEstimationUI::set_ambientIntensityText(UnityEngine.UI.Text)
extern void HDRLightEstimationUI_set_ambientIntensityText_m225883E2BF39C8D6099321ED35E1DFE3B7A5AEE1 (void);
// 0x000002F5 UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.HDRLightEstimationUI::get_ambientColorText()
extern void HDRLightEstimationUI_get_ambientColorText_mC7E352E37195E0828E601BC1C8A63C5FEB2196E0 (void);
// 0x000002F6 System.Void UnityEngine.XR.ARFoundation.Samples.HDRLightEstimationUI::set_ambientColorText(UnityEngine.UI.Text)
extern void HDRLightEstimationUI_set_ambientColorText_m4769CE4BFA8C7D94223FD6C1496F6FAB1A9040DE (void);
// 0x000002F7 UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.HDRLightEstimationUI::get_mainLightDirectionText()
extern void HDRLightEstimationUI_get_mainLightDirectionText_mD3CEAC0CE3169ACE5BFC029FC6E5C041CFD5F3FD (void);
// 0x000002F8 System.Void UnityEngine.XR.ARFoundation.Samples.HDRLightEstimationUI::set_mainLightDirectionText(UnityEngine.UI.Text)
extern void HDRLightEstimationUI_set_mainLightDirectionText_m1636273FAC859F042B76D0DC3B297A5326F4B0B6 (void);
// 0x000002F9 UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.HDRLightEstimationUI::get_mainLightIntensityLumens()
extern void HDRLightEstimationUI_get_mainLightIntensityLumens_m6198E8087B6594AA1A7C2BC95BB57963F4993454 (void);
// 0x000002FA System.Void UnityEngine.XR.ARFoundation.Samples.HDRLightEstimationUI::set_mainLightIntensityLumens(UnityEngine.UI.Text)
extern void HDRLightEstimationUI_set_mainLightIntensityLumens_mDC48EED111445A44B0DB129B9B28F6E7DD7B5261 (void);
// 0x000002FB UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.HDRLightEstimationUI::get_mainLightColorText()
extern void HDRLightEstimationUI_get_mainLightColorText_m90B05C3D83AF798FA2B2711ACE48488B69540F96 (void);
// 0x000002FC System.Void UnityEngine.XR.ARFoundation.Samples.HDRLightEstimationUI::set_mainLightColorText(UnityEngine.UI.Text)
extern void HDRLightEstimationUI_set_mainLightColorText_mD8B41FDF63A8282658365FD7B43B82F197F4DA93 (void);
// 0x000002FD UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.HDRLightEstimationUI::get_ambientSphericalHarmonicsText()
extern void HDRLightEstimationUI_get_ambientSphericalHarmonicsText_m222ACCC5E72E1B5BB2C2C2601083BD292DF212D2 (void);
// 0x000002FE System.Void UnityEngine.XR.ARFoundation.Samples.HDRLightEstimationUI::set_ambientSphericalHarmonicsText(UnityEngine.UI.Text)
extern void HDRLightEstimationUI_set_ambientSphericalHarmonicsText_mB4D235352CAC99891E9195AA7D01154AEFAB9950 (void);
// 0x000002FF System.Void UnityEngine.XR.ARFoundation.Samples.HDRLightEstimationUI::Awake()
extern void HDRLightEstimationUI_Awake_m643AA0B06E1363BA733147B417D6A86D3711EC79 (void);
// 0x00000300 System.Void UnityEngine.XR.ARFoundation.Samples.HDRLightEstimationUI::Update()
extern void HDRLightEstimationUI_Update_m60C3A05958F002B6430EB6E73CD6FB2F006A03AE (void);
// 0x00000301 System.Void UnityEngine.XR.ARFoundation.Samples.HDRLightEstimationUI::SetSphericalHarmonicsUIValue(System.Nullable`1<UnityEngine.Rendering.SphericalHarmonicsL2>,UnityEngine.UI.Text)
extern void HDRLightEstimationUI_SetSphericalHarmonicsUIValue_m72C7EF297802E4C09D98F0F8F9ECB2ECB7D31AA8 (void);
// 0x00000302 System.Void UnityEngine.XR.ARFoundation.Samples.HDRLightEstimationUI::SetUIValue(System.Nullable`1<T>,UnityEngine.UI.Text)
// 0x00000303 System.Void UnityEngine.XR.ARFoundation.Samples.HDRLightEstimationUI::.ctor()
extern void HDRLightEstimationUI__ctor_mC109515C913718446D5AA703586414B3BA501071 (void);
// 0x00000304 UnityEngine.XR.ARFoundation.ARHumanBodyManager UnityEngine.XR.ARFoundation.Samples.HumanBodyTracker::get_humanBodyManager()
extern void HumanBodyTracker_get_humanBodyManager_mC4B8945E39D5320F9979435792D262AC93C4D073 (void);
// 0x00000305 System.Void UnityEngine.XR.ARFoundation.Samples.HumanBodyTracker::set_humanBodyManager(UnityEngine.XR.ARFoundation.ARHumanBodyManager)
extern void HumanBodyTracker_set_humanBodyManager_mD698CD4F92BECC8322CA17ED80CF62114B690FB3 (void);
// 0x00000306 UnityEngine.GameObject UnityEngine.XR.ARFoundation.Samples.HumanBodyTracker::get_skeletonPrefab()
extern void HumanBodyTracker_get_skeletonPrefab_m682A136691745238FA4425FBBD3B777E7AD42FFB (void);
// 0x00000307 System.Void UnityEngine.XR.ARFoundation.Samples.HumanBodyTracker::set_skeletonPrefab(UnityEngine.GameObject)
extern void HumanBodyTracker_set_skeletonPrefab_m9A137EC0A4552DEA901F4EBFF84851E4EC7EA881 (void);
// 0x00000308 System.Void UnityEngine.XR.ARFoundation.Samples.HumanBodyTracker::OnEnable()
extern void HumanBodyTracker_OnEnable_mB8955E62494A0CDD4402ACFC172A62496CA26394 (void);
// 0x00000309 System.Void UnityEngine.XR.ARFoundation.Samples.HumanBodyTracker::OnDisable()
extern void HumanBodyTracker_OnDisable_m7E3C3D6DEAF82B58346242D91653ECB81487823A (void);
// 0x0000030A System.Void UnityEngine.XR.ARFoundation.Samples.HumanBodyTracker::OnHumanBodiesChanged(UnityEngine.XR.ARFoundation.ARHumanBodiesChangedEventArgs)
extern void HumanBodyTracker_OnHumanBodiesChanged_mCA0BA7F2AAC9D2A12EB37B819B86A2D793EDE86F (void);
// 0x0000030B System.Void UnityEngine.XR.ARFoundation.Samples.HumanBodyTracker::.ctor()
extern void HumanBodyTracker__ctor_m5BE66EF9DEF2A47385D9E29D6C693C97F910392A (void);
// 0x0000030C UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.Logger::get_logText()
extern void Logger_get_logText_m600AC307038B8EC747490883DCF4665FEAFD875F (void);
// 0x0000030D System.Void UnityEngine.XR.ARFoundation.Samples.Logger::set_logText(UnityEngine.UI.Text)
extern void Logger_set_logText_mF8B44C562FEE09DC292F556C9F36A7E9C6993BFA (void);
// 0x0000030E System.Int32 UnityEngine.XR.ARFoundation.Samples.Logger::get_visibleMessageCount()
extern void Logger_get_visibleMessageCount_m285BE29324B7B59765055ADCEDCBC3D2BB5D89B4 (void);
// 0x0000030F System.Void UnityEngine.XR.ARFoundation.Samples.Logger::set_visibleMessageCount(System.Int32)
extern void Logger_set_visibleMessageCount_m1F814B57BBFF8D248C7A5A938D82818CA4123C1C (void);
// 0x00000310 System.Void UnityEngine.XR.ARFoundation.Samples.Logger::Awake()
extern void Logger_Awake_mD1016F1251DB6CA3FBD2ED28DE93A9B4B11DC98C (void);
// 0x00000311 System.Void UnityEngine.XR.ARFoundation.Samples.Logger::Update()
extern void Logger_Update_m4721D90DF76645FF144C8C9AA3C97087BBF174A0 (void);
// 0x00000312 System.Void UnityEngine.XR.ARFoundation.Samples.Logger::Log(System.String)
extern void Logger_Log_m85A7E39D61A1961CEFC7B93FDAE300E26107B3FC (void);
// 0x00000313 System.Void UnityEngine.XR.ARFoundation.Samples.Logger::.ctor()
extern void Logger__ctor_m6E4DB291CACE70BD296CC49008DD47D7C9E660AA (void);
// 0x00000314 System.Void UnityEngine.XR.ARFoundation.Samples.Logger::.cctor()
extern void Logger__cctor_mA9B645798BC042347C3FBC3D1182B442D23C0D6A (void);
// 0x00000315 UnityEngine.Transform UnityEngine.XR.ARFoundation.Samples.MakeAppearOnPlane::get_content()
extern void MakeAppearOnPlane_get_content_m467D1D9468F11D8A7F8384C8F7529D94EDF04493 (void);
// 0x00000316 System.Void UnityEngine.XR.ARFoundation.Samples.MakeAppearOnPlane::set_content(UnityEngine.Transform)
extern void MakeAppearOnPlane_set_content_mF2774D958C87E7A4ADB68771DD2D2A658B3C52E6 (void);
// 0x00000317 UnityEngine.Quaternion UnityEngine.XR.ARFoundation.Samples.MakeAppearOnPlane::get_rotation()
extern void MakeAppearOnPlane_get_rotation_m883F2C51687D1856ED637E74E88C9C8B51237447 (void);
// 0x00000318 System.Void UnityEngine.XR.ARFoundation.Samples.MakeAppearOnPlane::set_rotation(UnityEngine.Quaternion)
extern void MakeAppearOnPlane_set_rotation_m409C3FEC4330F90717156C433B1CD4CAA536843B (void);
// 0x00000319 System.Void UnityEngine.XR.ARFoundation.Samples.MakeAppearOnPlane::Awake()
extern void MakeAppearOnPlane_Awake_mA31F7C1E85C5C25B755CEAF4790AD37A418F3DFF (void);
// 0x0000031A System.Void UnityEngine.XR.ARFoundation.Samples.MakeAppearOnPlane::Update()
extern void MakeAppearOnPlane_Update_m10DF99B497DB464A96AF4BC6ED003736FE8A6E1B (void);
// 0x0000031B System.Void UnityEngine.XR.ARFoundation.Samples.MakeAppearOnPlane::.ctor()
extern void MakeAppearOnPlane__ctor_m525EF5D6F93E1E740946A1E75977CA5341582B49 (void);
// 0x0000031C System.Void UnityEngine.XR.ARFoundation.Samples.MakeAppearOnPlane::.cctor()
extern void MakeAppearOnPlane__cctor_m3DAA4262BF27E0B9BE69E72EDA89EEBFD0375842 (void);
// 0x0000031D UnityEngine.GameObject UnityEngine.XR.ARFoundation.Samples.PlaceMultipleObjectsOnPlane::get_placedPrefab()
extern void PlaceMultipleObjectsOnPlane_get_placedPrefab_mD60B8AC0A8C0C41F8C1E766370FF9F898CCB917A (void);
// 0x0000031E System.Void UnityEngine.XR.ARFoundation.Samples.PlaceMultipleObjectsOnPlane::set_placedPrefab(UnityEngine.GameObject)
extern void PlaceMultipleObjectsOnPlane_set_placedPrefab_mEC44306B88B9D34593B6A3DFE0DCB97ED4F679EE (void);
// 0x0000031F UnityEngine.GameObject UnityEngine.XR.ARFoundation.Samples.PlaceMultipleObjectsOnPlane::get_spawnedObject()
extern void PlaceMultipleObjectsOnPlane_get_spawnedObject_mD86A3A7110223667DA31A226CEC429FB88E24EF7 (void);
// 0x00000320 System.Void UnityEngine.XR.ARFoundation.Samples.PlaceMultipleObjectsOnPlane::set_spawnedObject(UnityEngine.GameObject)
extern void PlaceMultipleObjectsOnPlane_set_spawnedObject_m5407E287877625F501DC82BEA022FF6E8C183014 (void);
// 0x00000321 System.Void UnityEngine.XR.ARFoundation.Samples.PlaceMultipleObjectsOnPlane::add_onPlacedObject(System.Action)
extern void PlaceMultipleObjectsOnPlane_add_onPlacedObject_m28FBD07B2F6D8D6B3431FEE5E754F6BAECBBDFE5 (void);
// 0x00000322 System.Void UnityEngine.XR.ARFoundation.Samples.PlaceMultipleObjectsOnPlane::remove_onPlacedObject(System.Action)
extern void PlaceMultipleObjectsOnPlane_remove_onPlacedObject_mECDF0B3E90B57303F9D2A7D539E4067838C81CFE (void);
// 0x00000323 System.Void UnityEngine.XR.ARFoundation.Samples.PlaceMultipleObjectsOnPlane::Awake()
extern void PlaceMultipleObjectsOnPlane_Awake_mA7E09D89EC480E9D00045922ED93CCACD03CF3C6 (void);
// 0x00000324 System.Void UnityEngine.XR.ARFoundation.Samples.PlaceMultipleObjectsOnPlane::Update()
extern void PlaceMultipleObjectsOnPlane_Update_m42D86D85FD1E2D2C5E3C0AE64ED213608F37FE62 (void);
// 0x00000325 System.Void UnityEngine.XR.ARFoundation.Samples.PlaceMultipleObjectsOnPlane::.ctor()
extern void PlaceMultipleObjectsOnPlane__ctor_m04EA4054A36090BF7C4B3F0813232094873745B8 (void);
// 0x00000326 System.Void UnityEngine.XR.ARFoundation.Samples.PlaceMultipleObjectsOnPlane::.cctor()
extern void PlaceMultipleObjectsOnPlane__cctor_m3A901E424B21C70F413709C9BF0AEF28C7FC68FA (void);
// 0x00000327 UnityEngine.GameObject UnityEngine.XR.ARFoundation.Samples.PlaceOnPlane::get_placedPrefab()
extern void PlaceOnPlane_get_placedPrefab_m763D6F073D3EDF7415E1E32E10AA426967DCDA18 (void);
// 0x00000328 System.Void UnityEngine.XR.ARFoundation.Samples.PlaceOnPlane::set_placedPrefab(UnityEngine.GameObject)
extern void PlaceOnPlane_set_placedPrefab_mB1D74F674553C7DD3E35919E9B3510A2A72300D9 (void);
// 0x00000329 UnityEngine.GameObject UnityEngine.XR.ARFoundation.Samples.PlaceOnPlane::get_spawnedObject()
extern void PlaceOnPlane_get_spawnedObject_m35FA877FA58C9C21BD450CDA01EF4BF5F6189C09 (void);
// 0x0000032A System.Void UnityEngine.XR.ARFoundation.Samples.PlaceOnPlane::set_spawnedObject(UnityEngine.GameObject)
extern void PlaceOnPlane_set_spawnedObject_m4831A436BFFF40945FCE6376CF88870D4E0DFA87 (void);
// 0x0000032B System.Void UnityEngine.XR.ARFoundation.Samples.PlaceOnPlane::Awake()
extern void PlaceOnPlane_Awake_mE68544675C14502921A783F67B2D7B80E00BDBB9 (void);
// 0x0000032C System.Boolean UnityEngine.XR.ARFoundation.Samples.PlaceOnPlane::TryGetTouchPosition(UnityEngine.Vector2&)
extern void PlaceOnPlane_TryGetTouchPosition_mE940E600E9FAB1C3D9F182B04A920CAE6F39C61C (void);
// 0x0000032D System.Void UnityEngine.XR.ARFoundation.Samples.PlaceOnPlane::Update()
extern void PlaceOnPlane_Update_mDAFDB8C7CABA3CFA3E6C422A51585A3159C5B807 (void);
// 0x0000032E System.Void UnityEngine.XR.ARFoundation.Samples.PlaceOnPlane::.ctor()
extern void PlaceOnPlane__ctor_m6DD8D183AC8B0486B5791C6AF332B305E3CAA1AE (void);
// 0x0000032F System.Void UnityEngine.XR.ARFoundation.Samples.PlaceOnPlane::.cctor()
extern void PlaceOnPlane__cctor_mDE3040EFFFB02FA683F04061F2ACC23E26010E76 (void);
// 0x00000330 UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.PlaneDetectionController::get_togglePlaneDetectionText()
extern void PlaneDetectionController_get_togglePlaneDetectionText_m914781015127293A67ABFECF438A1CF2ACBF9E31 (void);
// 0x00000331 System.Void UnityEngine.XR.ARFoundation.Samples.PlaneDetectionController::set_togglePlaneDetectionText(UnityEngine.UI.Text)
extern void PlaneDetectionController_set_togglePlaneDetectionText_mDE2BA925E54F24A9AF128166DDBCF346E0FFCF94 (void);
// 0x00000332 System.Void UnityEngine.XR.ARFoundation.Samples.PlaneDetectionController::TogglePlaneDetection()
extern void PlaneDetectionController_TogglePlaneDetection_mDC082A6B018A93A31F9D3503447FB3B69BF7E17E (void);
// 0x00000333 System.Void UnityEngine.XR.ARFoundation.Samples.PlaneDetectionController::SetAllPlanesActive(System.Boolean)
extern void PlaneDetectionController_SetAllPlanesActive_m30933F95A82C187D3EF7B8122BD1A15180508B7F (void);
// 0x00000334 System.Void UnityEngine.XR.ARFoundation.Samples.PlaneDetectionController::Awake()
extern void PlaneDetectionController_Awake_m0248674C37BEB34AF39227EA8937309089340149 (void);
// 0x00000335 System.Void UnityEngine.XR.ARFoundation.Samples.PlaneDetectionController::.ctor()
extern void PlaneDetectionController__ctor_m70E75C6E3216E94E9600F78E7B2B8CD286D9FD5D (void);
// 0x00000336 UnityEngine.UI.Slider UnityEngine.XR.ARFoundation.Samples.RotationController::get_slider()
extern void RotationController_get_slider_m688A2516939785224E2135387DFBEFCFB521B5E8 (void);
// 0x00000337 System.Void UnityEngine.XR.ARFoundation.Samples.RotationController::set_slider(UnityEngine.UI.Slider)
extern void RotationController_set_slider_m032C8F2D03C60D88BF3C51E9E69BC839A058303A (void);
// 0x00000338 UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.RotationController::get_text()
extern void RotationController_get_text_mAEF0FDDADD3A641257FF3C29D1681E9CD17C8839 (void);
// 0x00000339 System.Void UnityEngine.XR.ARFoundation.Samples.RotationController::set_text(UnityEngine.UI.Text)
extern void RotationController_set_text_mBA49076027FE64EFAA7C45A331B0B19E4DFF27CF (void);
// 0x0000033A System.Single UnityEngine.XR.ARFoundation.Samples.RotationController::get_min()
extern void RotationController_get_min_mB3746DD093A77C530E0CB36F6F673B321F83822C (void);
// 0x0000033B System.Void UnityEngine.XR.ARFoundation.Samples.RotationController::set_min(System.Single)
extern void RotationController_set_min_mCFDF217E94A470520148859597955A42C84020EE (void);
// 0x0000033C System.Single UnityEngine.XR.ARFoundation.Samples.RotationController::get_max()
extern void RotationController_get_max_mDBB0C27A2AF8529AE647BCA1F48ACD2A6D3A1369 (void);
// 0x0000033D System.Void UnityEngine.XR.ARFoundation.Samples.RotationController::set_max(System.Single)
extern void RotationController_set_max_m0184CBFE1E139F74BF2267C5D26F873255C4502F (void);
// 0x0000033E System.Void UnityEngine.XR.ARFoundation.Samples.RotationController::OnSliderValueChanged()
extern void RotationController_OnSliderValueChanged_m60BC86CF728B683B9F5E2DCB25481A021862B3ED (void);
// 0x0000033F System.Single UnityEngine.XR.ARFoundation.Samples.RotationController::get_angle()
extern void RotationController_get_angle_m6E3BB762FFD942D9D0EF2FF6E2BA87B8E26E75EB (void);
// 0x00000340 System.Void UnityEngine.XR.ARFoundation.Samples.RotationController::set_angle(System.Single)
extern void RotationController_set_angle_mFC6803991BF2B19CE3E092E18CD2AECC9C0C28B3 (void);
// 0x00000341 System.Void UnityEngine.XR.ARFoundation.Samples.RotationController::Awake()
extern void RotationController_Awake_mACC2B7EF2D1375C9C0B3953AAC71F00BFEBF4CBA (void);
// 0x00000342 System.Void UnityEngine.XR.ARFoundation.Samples.RotationController::OnEnable()
extern void RotationController_OnEnable_m1652900F3F65365A46C8168FA80A3F90F1BF478F (void);
// 0x00000343 System.Void UnityEngine.XR.ARFoundation.Samples.RotationController::UpdateText()
extern void RotationController_UpdateText_m3786EFAF0145BA714F41DD135E2DE2E7D9B82C88 (void);
// 0x00000344 System.Void UnityEngine.XR.ARFoundation.Samples.RotationController::.ctor()
extern void RotationController__ctor_m26E8F9A4C98DDEF771DF98A4B955A12F6EADD064 (void);
// 0x00000345 UnityEngine.UI.Slider UnityEngine.XR.ARFoundation.Samples.ScaleController::get_slider()
extern void ScaleController_get_slider_m4759BCE9036C2F489DB791862A8F651EC4879762 (void);
// 0x00000346 System.Void UnityEngine.XR.ARFoundation.Samples.ScaleController::set_slider(UnityEngine.UI.Slider)
extern void ScaleController_set_slider_m3EA04E532D3A83F4DDEB30058B9A03265FC9ECEF (void);
// 0x00000347 UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.ScaleController::get_text()
extern void ScaleController_get_text_mEC690CB74411DF88C67C8A5442AF28DF11C29D29 (void);
// 0x00000348 System.Void UnityEngine.XR.ARFoundation.Samples.ScaleController::set_text(UnityEngine.UI.Text)
extern void ScaleController_set_text_mE1EA21CABB6941E297E26B9456E6080124C26465 (void);
// 0x00000349 System.Single UnityEngine.XR.ARFoundation.Samples.ScaleController::get_min()
extern void ScaleController_get_min_m93ACE40734850E95EBC2FEDC1C86CE11F4967D87 (void);
// 0x0000034A System.Void UnityEngine.XR.ARFoundation.Samples.ScaleController::set_min(System.Single)
extern void ScaleController_set_min_m5B00A4A654467E0C9720AA4D9FC05302A5D49729 (void);
// 0x0000034B System.Single UnityEngine.XR.ARFoundation.Samples.ScaleController::get_max()
extern void ScaleController_get_max_mC82966DEACB852B73D101156CDEB1B786037F0D5 (void);
// 0x0000034C System.Void UnityEngine.XR.ARFoundation.Samples.ScaleController::set_max(System.Single)
extern void ScaleController_set_max_mAB7B381C689D4FAF758A264258A93DC788D409B5 (void);
// 0x0000034D System.Void UnityEngine.XR.ARFoundation.Samples.ScaleController::OnSliderValueChanged()
extern void ScaleController_OnSliderValueChanged_m523EF1DF1293E767D9EAFA7359F8C53E0445D918 (void);
// 0x0000034E System.Single UnityEngine.XR.ARFoundation.Samples.ScaleController::get_scale()
extern void ScaleController_get_scale_m70A80D5C1AE10DC47849CDF68A8A349FA98A40BA (void);
// 0x0000034F System.Void UnityEngine.XR.ARFoundation.Samples.ScaleController::set_scale(System.Single)
extern void ScaleController_set_scale_m5C5040F5D3D019542F9A59496300643A7B2DCB11 (void);
// 0x00000350 System.Void UnityEngine.XR.ARFoundation.Samples.ScaleController::Awake()
extern void ScaleController_Awake_m07CE359676D88A307EBB1F6EAE45A28E1C544195 (void);
// 0x00000351 System.Void UnityEngine.XR.ARFoundation.Samples.ScaleController::OnEnable()
extern void ScaleController_OnEnable_m32ADA2F5C752C902FD37EF4E365F3AA0506EAFA5 (void);
// 0x00000352 System.Void UnityEngine.XR.ARFoundation.Samples.ScaleController::UpdateText()
extern void ScaleController_UpdateText_m8BF6016F76B0DD20598237FDC35A8D3F55F944B8 (void);
// 0x00000353 System.Void UnityEngine.XR.ARFoundation.Samples.ScaleController::.ctor()
extern void ScaleController__ctor_mDB262AF1ABE1815460CAEB62A47BC996A62754AA (void);
// 0x00000354 UnityEngine.Camera UnityEngine.XR.ARFoundation.Samples.ScreenSpaceJointVisualizer::get_arCamera()
extern void ScreenSpaceJointVisualizer_get_arCamera_m82AFEE273B58ECD1161F08092482BF4490D2A47A (void);
// 0x00000355 System.Void UnityEngine.XR.ARFoundation.Samples.ScreenSpaceJointVisualizer::set_arCamera(UnityEngine.Camera)
extern void ScreenSpaceJointVisualizer_set_arCamera_m54AC785E901584A0FA22633581C8E6FDADBEF81E (void);
// 0x00000356 UnityEngine.XR.ARFoundation.ARHumanBodyManager UnityEngine.XR.ARFoundation.Samples.ScreenSpaceJointVisualizer::get_humanBodyManager()
extern void ScreenSpaceJointVisualizer_get_humanBodyManager_mC7CD80E44640BD62F49F534E5CA1F319564C63C1 (void);
// 0x00000357 System.Void UnityEngine.XR.ARFoundation.Samples.ScreenSpaceJointVisualizer::set_humanBodyManager(UnityEngine.XR.ARFoundation.ARHumanBodyManager)
extern void ScreenSpaceJointVisualizer_set_humanBodyManager_mB1FAC9EE439BB945CB09F2F9E3F5FEC74EDD8A8E (void);
// 0x00000358 UnityEngine.GameObject UnityEngine.XR.ARFoundation.Samples.ScreenSpaceJointVisualizer::get_lineRendererPrefab()
extern void ScreenSpaceJointVisualizer_get_lineRendererPrefab_m61EEB13EF427976C8DB5E1FB1A9AAE666F1679EE (void);
// 0x00000359 System.Void UnityEngine.XR.ARFoundation.Samples.ScreenSpaceJointVisualizer::set_lineRendererPrefab(UnityEngine.GameObject)
extern void ScreenSpaceJointVisualizer_set_lineRendererPrefab_m85C6B1BBC286B0BEEE8EB729199F26BB6545F4C9 (void);
// 0x0000035A System.Void UnityEngine.XR.ARFoundation.Samples.ScreenSpaceJointVisualizer::Awake()
extern void ScreenSpaceJointVisualizer_Awake_mBA10AB7D06AC6C582459127F631DE8D10F05B5A4 (void);
// 0x0000035B System.Void UnityEngine.XR.ARFoundation.Samples.ScreenSpaceJointVisualizer::UpdateRenderer(Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRHumanBodyPose2DJoint>,System.Int32)
extern void ScreenSpaceJointVisualizer_UpdateRenderer_m7579F800D27BA512ECBDCC5BE3385305FEF8AB1B (void);
// 0x0000035C System.Void UnityEngine.XR.ARFoundation.Samples.ScreenSpaceJointVisualizer::Update()
extern void ScreenSpaceJointVisualizer_Update_m9D138197FF36EA87CAB234FBB728FD42EBC62FBB (void);
// 0x0000035D System.Void UnityEngine.XR.ARFoundation.Samples.ScreenSpaceJointVisualizer::HideJointLines()
extern void ScreenSpaceJointVisualizer_HideJointLines_m38A24B97D80276D5E89A4FA9975C83E43AC2DA65 (void);
// 0x0000035E System.Void UnityEngine.XR.ARFoundation.Samples.ScreenSpaceJointVisualizer::.ctor()
extern void ScreenSpaceJointVisualizer__ctor_m44D5CB68146DDA2E672736563FE99DF773C88B57 (void);
// 0x0000035F System.Void UnityEngine.XR.ARFoundation.Samples.ScreenSpaceJointVisualizer::.cctor()
extern void ScreenSpaceJointVisualizer__cctor_m282B7FF2145B51125F1B4260F8C18182ABC741C1 (void);
// 0x00000360 UnityEngine.XR.ARFoundation.ARSession UnityEngine.XR.ARFoundation.Samples.SupportChecker::get_session()
extern void SupportChecker_get_session_mEE0E9882292D6996B415E9C4C6B6FA34AF0A3E40 (void);
// 0x00000361 System.Void UnityEngine.XR.ARFoundation.Samples.SupportChecker::set_session(UnityEngine.XR.ARFoundation.ARSession)
extern void SupportChecker_set_session_m3C19705CEB93F7E223885891A9F620DAC56DEAB6 (void);
// 0x00000362 UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.SupportChecker::get_logText()
extern void SupportChecker_get_logText_m4218723C52FD34A5C56C915CA8CB1DB7445400DC (void);
// 0x00000363 System.Void UnityEngine.XR.ARFoundation.Samples.SupportChecker::set_logText(UnityEngine.UI.Text)
extern void SupportChecker_set_logText_mCA32B9D279BECACE0B57965D6361E718537B6407 (void);
// 0x00000364 UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.SupportChecker::get_installButton()
extern void SupportChecker_get_installButton_mABEB073536D29DD2141F6B87A02F6F6F1BEEA9F3 (void);
// 0x00000365 System.Void UnityEngine.XR.ARFoundation.Samples.SupportChecker::set_installButton(UnityEngine.UI.Button)
extern void SupportChecker_set_installButton_mA7649BD06BCA3F7E97FF1F2EC579BFD12A2D0A01 (void);
// 0x00000366 System.Void UnityEngine.XR.ARFoundation.Samples.SupportChecker::Log(System.String)
extern void SupportChecker_Log_m46AD9890AA7900830EC90CD845F5EE2375BB7E17 (void);
// 0x00000367 System.Collections.IEnumerator UnityEngine.XR.ARFoundation.Samples.SupportChecker::CheckSupport()
extern void SupportChecker_CheckSupport_m975FB3F2C90FE4E4EFD391E6747162496ABA7C27 (void);
// 0x00000368 System.Void UnityEngine.XR.ARFoundation.Samples.SupportChecker::SetInstallButtonActive(System.Boolean)
extern void SupportChecker_SetInstallButtonActive_m3FAAA2F30D77E269B7C169F6FAB5A0419C16350A (void);
// 0x00000369 System.Collections.IEnumerator UnityEngine.XR.ARFoundation.Samples.SupportChecker::Install()
extern void SupportChecker_Install_mD735E330C220AEC1266E88C73DB1BC8048FECCEF (void);
// 0x0000036A System.Void UnityEngine.XR.ARFoundation.Samples.SupportChecker::OnInstallButtonPressed()
extern void SupportChecker_OnInstallButtonPressed_m36E562592D0D90C35EA48F96B9A4D7F6EAF6B7A8 (void);
// 0x0000036B System.Void UnityEngine.XR.ARFoundation.Samples.SupportChecker::OnEnable()
extern void SupportChecker_OnEnable_m0C7462B91FECE976CC2720F35657DA327C1639D1 (void);
// 0x0000036C System.Void UnityEngine.XR.ARFoundation.Samples.SupportChecker::.ctor()
extern void SupportChecker__ctor_mD2784A992FDF0C6405AD1C43DBB58686F0E81712 (void);
// 0x0000036D System.Void UnityEngine.XR.ARFoundation.Samples.SupportChecker/<CheckSupport>d__13::.ctor(System.Int32)
extern void U3CCheckSupportU3Ed__13__ctor_m6BC6258C4F1ABA808914DD53128A7D30D4A48EBE (void);
// 0x0000036E System.Void UnityEngine.XR.ARFoundation.Samples.SupportChecker/<CheckSupport>d__13::System.IDisposable.Dispose()
extern void U3CCheckSupportU3Ed__13_System_IDisposable_Dispose_m396334D7FCB3267C034E19C3CC2F3CDB5EAA31C1 (void);
// 0x0000036F System.Boolean UnityEngine.XR.ARFoundation.Samples.SupportChecker/<CheckSupport>d__13::MoveNext()
extern void U3CCheckSupportU3Ed__13_MoveNext_mD885F827DD821A2AA2CDC0A64A5581E72BD8093F (void);
// 0x00000370 System.Object UnityEngine.XR.ARFoundation.Samples.SupportChecker/<CheckSupport>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCheckSupportU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA74258C01070CABFB04F4BA5784E6505009EDFB8 (void);
// 0x00000371 System.Void UnityEngine.XR.ARFoundation.Samples.SupportChecker/<CheckSupport>d__13::System.Collections.IEnumerator.Reset()
extern void U3CCheckSupportU3Ed__13_System_Collections_IEnumerator_Reset_m4CE9E2693E35645009E13AEDD9C12B69D7731868 (void);
// 0x00000372 System.Object UnityEngine.XR.ARFoundation.Samples.SupportChecker/<CheckSupport>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CCheckSupportU3Ed__13_System_Collections_IEnumerator_get_Current_m6E12FC445F61FF2A359FD44245513590E756C0E4 (void);
// 0x00000373 System.Void UnityEngine.XR.ARFoundation.Samples.SupportChecker/<Install>d__15::.ctor(System.Int32)
extern void U3CInstallU3Ed__15__ctor_m7779EEDD7771D621B16FCC240A3FB9EB02E8B671 (void);
// 0x00000374 System.Void UnityEngine.XR.ARFoundation.Samples.SupportChecker/<Install>d__15::System.IDisposable.Dispose()
extern void U3CInstallU3Ed__15_System_IDisposable_Dispose_m4C7497608DCC1C85B0A011B6A8944C0FCD27D8BD (void);
// 0x00000375 System.Boolean UnityEngine.XR.ARFoundation.Samples.SupportChecker/<Install>d__15::MoveNext()
extern void U3CInstallU3Ed__15_MoveNext_mA1A230F1CD888AD0A52BD44712194C54A037EEA1 (void);
// 0x00000376 System.Object UnityEngine.XR.ARFoundation.Samples.SupportChecker/<Install>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInstallU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14C586453D364A731DEF5637A847E37B76BCDD6A (void);
// 0x00000377 System.Void UnityEngine.XR.ARFoundation.Samples.SupportChecker/<Install>d__15::System.Collections.IEnumerator.Reset()
extern void U3CInstallU3Ed__15_System_Collections_IEnumerator_Reset_m39BD04A5F9C8864E386EF7AD0FA2FD75CC2F44CA (void);
// 0x00000378 System.Object UnityEngine.XR.ARFoundation.Samples.SupportChecker/<Install>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CInstallU3Ed__15_System_Collections_IEnumerator_get_Current_mA1C54C035E6319C42E5B1B88BF7C71631EB559ED (void);
// 0x00000379 UnityEngine.XR.ARFoundation.ARHumanBodyManager UnityEngine.XR.ARFoundation.Samples.TestBodyAnchorScale::get_humanBodyManager()
extern void TestBodyAnchorScale_get_humanBodyManager_m4B37BF9F3ADF4BC6B89BED199E611E580EBB0526 (void);
// 0x0000037A System.Void UnityEngine.XR.ARFoundation.Samples.TestBodyAnchorScale::set_humanBodyManager(UnityEngine.XR.ARFoundation.ARHumanBodyManager)
extern void TestBodyAnchorScale_set_humanBodyManager_m43FF3C3D743059075AEE92F6C96AA0A066736DC0 (void);
// 0x0000037B UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.TestBodyAnchorScale::get_imageInfo()
extern void TestBodyAnchorScale_get_imageInfo_m1A99F301BC287BB82EA21ECF33E947C1382A1E3F (void);
// 0x0000037C System.Void UnityEngine.XR.ARFoundation.Samples.TestBodyAnchorScale::set_imageInfo(UnityEngine.UI.Text)
extern void TestBodyAnchorScale_set_imageInfo_mCAFB5D7DCF072A4D58D0B975B13B46FB14124F3C (void);
// 0x0000037D System.Void UnityEngine.XR.ARFoundation.Samples.TestBodyAnchorScale::OnEnable()
extern void TestBodyAnchorScale_OnEnable_m468B3225428A1BB05C8467977AD9923EB63C7C98 (void);
// 0x0000037E System.Void UnityEngine.XR.ARFoundation.Samples.TestBodyAnchorScale::OnDisable()
extern void TestBodyAnchorScale_OnDisable_m2EC04A2C49D4DB6174622B949E058B583AB5CC5A (void);
// 0x0000037F System.Void UnityEngine.XR.ARFoundation.Samples.TestBodyAnchorScale::OnHumanBodiesChanged(UnityEngine.XR.ARFoundation.ARHumanBodiesChangedEventArgs)
extern void TestBodyAnchorScale_OnHumanBodiesChanged_m023FA84984B3ED88DBD663D3EC9CC87DD0944628 (void);
// 0x00000380 System.Void UnityEngine.XR.ARFoundation.Samples.TestBodyAnchorScale::.ctor()
extern void TestBodyAnchorScale__ctor_mB4D6913DBC970760892584FBEFA3FC899C3BFD29 (void);
// 0x00000381 UnityEngine.UI.Scrollbar UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::get_horizontalScrollBar()
extern void ARSceneSelectUI_get_horizontalScrollBar_m5FC064E170727EC1341826098767F5E30E55D7A7 (void);
// 0x00000382 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::set_horizontalScrollBar(UnityEngine.UI.Scrollbar)
extern void ARSceneSelectUI_set_horizontalScrollBar_mFCC0AC1D8001AC12A8A802CB24C5ACBD7DED53A9 (void);
// 0x00000383 UnityEngine.UI.Scrollbar UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::get_verticalScrollBar()
extern void ARSceneSelectUI_get_verticalScrollBar_mB7B43395D026639213E2C6C97D00E0317AECA8DA (void);
// 0x00000384 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::set_verticalScrollBar(UnityEngine.UI.Scrollbar)
extern void ARSceneSelectUI_set_verticalScrollBar_m63AC2199A6318700CC9B4422EE6CC451AAE6BF7F (void);
// 0x00000385 UnityEngine.GameObject UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::get_allMenu()
extern void ARSceneSelectUI_get_allMenu_m6B813F9EFA127A805767249833689BED9DAA5B98 (void);
// 0x00000386 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::set_allMenu(UnityEngine.GameObject)
extern void ARSceneSelectUI_set_allMenu_mC13C9AFCC9A6C8967B254542AECB4FC3DE5524F2 (void);
// 0x00000387 UnityEngine.GameObject UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::get_imageTrackingMenu()
extern void ARSceneSelectUI_get_imageTrackingMenu_m618C928E056904FE0305E8A343BC5BE8E62C20E1 (void);
// 0x00000388 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::set_imageTrackingMenu(UnityEngine.GameObject)
extern void ARSceneSelectUI_set_imageTrackingMenu_m1EA3088540273AE638DABBDB4F507ABFA38BAFF8 (void);
// 0x00000389 UnityEngine.GameObject UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::get_faceTrackingMenu()
extern void ARSceneSelectUI_get_faceTrackingMenu_m0FD1C96BCD7122107A90377CD90C9FE3A392A428 (void);
// 0x0000038A System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::set_faceTrackingMenu(UnityEngine.GameObject)
extern void ARSceneSelectUI_set_faceTrackingMenu_mB3A6D2DAEC230A0D4895F8661390B6622E04B759 (void);
// 0x0000038B UnityEngine.GameObject UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::get_bodyTrackingMenu()
extern void ARSceneSelectUI_get_bodyTrackingMenu_m1753C092DB4B2DCB0443871AD98E7F87824E7A43 (void);
// 0x0000038C System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::set_bodyTrackingMenu(UnityEngine.GameObject)
extern void ARSceneSelectUI_set_bodyTrackingMenu_m905D2F689496EFB15DF17A0800B968AE1EFBC39E (void);
// 0x0000038D UnityEngine.GameObject UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::get_planeDetectionMenu()
extern void ARSceneSelectUI_get_planeDetectionMenu_m49174CF9A1CF511271213D2F5DAEF61AC6569549 (void);
// 0x0000038E System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::set_planeDetectionMenu(UnityEngine.GameObject)
extern void ARSceneSelectUI_set_planeDetectionMenu_mF694A86F40ADBA7470108B6983906AD52E25623B (void);
// 0x0000038F UnityEngine.GameObject UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::get_meshingMenu()
extern void ARSceneSelectUI_get_meshingMenu_mCBA6BE45CF69596192BDE7366BBA21C3CD22268A (void);
// 0x00000390 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::set_meshingMenu(UnityEngine.GameObject)
extern void ARSceneSelectUI_set_meshingMenu_m373DF2BE32E1EB0C8256DAF7AA1F91FDCFDB2F0F (void);
// 0x00000391 UnityEngine.GameObject UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::get_depthMenu()
extern void ARSceneSelectUI_get_depthMenu_mECB6444247E9C8365B001FE778BA383345372876 (void);
// 0x00000392 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::set_depthMenu(UnityEngine.GameObject)
extern void ARSceneSelectUI_set_depthMenu_mA1345C822EF8C625C1C96401CCA962CB5A8F813A (void);
// 0x00000393 UnityEngine.GameObject UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::get_lightEstimationMenu()
extern void ARSceneSelectUI_get_lightEstimationMenu_mEFCD70188B0EF109BF3CFE831BBD37B7AB28910D (void);
// 0x00000394 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::set_lightEstimationMenu(UnityEngine.GameObject)
extern void ARSceneSelectUI_set_lightEstimationMenu_m711C48D1E2B77ADF72BBADA4F4CEB9F39512FA03 (void);
// 0x00000395 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::Start()
extern void ARSceneSelectUI_Start_mF4DA86AE4A4205E475EFDDF5E6AE06BC4313F338 (void);
// 0x00000396 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::LoadScene(System.String)
extern void ARSceneSelectUI_LoadScene_m05B38B643DF5DD35A8A6AF039653DC7ED81E4B4F (void);
// 0x00000397 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::SimpleARButtonPressed()
extern void ARSceneSelectUI_SimpleARButtonPressed_mFECE0F3533419FED607D960BE741857AF071C7AF (void);
// 0x00000398 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::ImageTrackableButtonPressed()
extern void ARSceneSelectUI_ImageTrackableButtonPressed_mFF06A0F6174405F9A28DA2AA944BAFAB7CD5B026 (void);
// 0x00000399 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::BasicImageTrackingButtonPressed()
extern void ARSceneSelectUI_BasicImageTrackingButtonPressed_mB69AA1FF3DAAF06030A616B27425D63E797D4816 (void);
// 0x0000039A System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::MultiImagesTrackingButtonPressed()
extern void ARSceneSelectUI_MultiImagesTrackingButtonPressed_mC7B68980F39FC224040BF1DC8927881AA30F9997 (void);
// 0x0000039B System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::AnchorsButtonPressed()
extern void ARSceneSelectUI_AnchorsButtonPressed_m4D74007E998FFE09820E185E8B1B29748A551961 (void);
// 0x0000039C System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::ARCollaborationDataButtonPressed()
extern void ARSceneSelectUI_ARCollaborationDataButtonPressed_mB64483D9440DAF442667BAE3BC3D5506A54EDB6A (void);
// 0x0000039D System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::ARKitCoachingOverlayButtonPressed()
extern void ARSceneSelectUI_ARKitCoachingOverlayButtonPressed_mBC1FBF527FCA56EDCFAFD2C2420A9B30040AFF4C (void);
// 0x0000039E System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::ARWorldMapButtonPressed()
extern void ARSceneSelectUI_ARWorldMapButtonPressed_m685077D2BC15B90CFDCAFF677F72EF5F73C22212 (void);
// 0x0000039F System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::ARKitGeoAnchorsButtonPressed()
extern void ARSceneSelectUI_ARKitGeoAnchorsButtonPressed_mED96251EBE27AED533BBBEC9236FDAE94CB99BF2 (void);
// 0x000003A0 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::CpuImagesButtonPressed()
extern void ARSceneSelectUI_CpuImagesButtonPressed_mC92A3754DCD430A77C0676E9023E1124BECD5A47 (void);
// 0x000003A1 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::CheckSupportButtonPressed()
extern void ARSceneSelectUI_CheckSupportButtonPressed_m7DD5279F6C0B54D8DD6FA197AD0E48673AD29BA7 (void);
// 0x000003A2 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::EnvironmentProbesButtonPressed()
extern void ARSceneSelectUI_EnvironmentProbesButtonPressed_m7EA06FF68E7294F5AC217B24DF35D1343BCDD397 (void);
// 0x000003A3 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::ObjectTrackingButtonPressed()
extern void ARSceneSelectUI_ObjectTrackingButtonPressed_mB58E92CB43E182FF064CE5F54815BA4ED70BA60F (void);
// 0x000003A4 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::PlaneOcclusionButtonPressed()
extern void ARSceneSelectUI_PlaneOcclusionButtonPressed_mB8B43C5B912F32574C3B3A287F2CAE8491DB97E3 (void);
// 0x000003A5 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::PointCloudButtonPressed()
extern void ARSceneSelectUI_PointCloudButtonPressed_m56DC266199DDCB760381C7398626E23863EF72B4 (void);
// 0x000003A6 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::ScaleButtonPressed()
extern void ARSceneSelectUI_ScaleButtonPressed_m0E68CF5BA71FF162E501F57F4661BE819ECAF1E1 (void);
// 0x000003A7 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::ConfigChooserButtonPressed()
extern void ARSceneSelectUI_ConfigChooserButtonPressed_mA72D45CB8BA104BDEBCCEA19ABDA3E08BA44E4E6 (void);
// 0x000003A8 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::FaceTrackingMenuButtonPressed()
extern void ARSceneSelectUI_FaceTrackingMenuButtonPressed_mEB63B5232054262E0F24AA74F21C8842EC0AF3D9 (void);
// 0x000003A9 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::ARCoreFaceRegionsButtonPressed()
extern void ARSceneSelectUI_ARCoreFaceRegionsButtonPressed_m1164E97990A1B64B761E83E17EEEF37F2157A8D1 (void);
// 0x000003AA System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::ARKitFaceBlendShapesButtonPressed()
extern void ARSceneSelectUI_ARKitFaceBlendShapesButtonPressed_m29D1FC848969A13499EEB2A8D1D6BBCFF0A4FCC9 (void);
// 0x000003AB System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::EyeLasersButtonPressed()
extern void ARSceneSelectUI_EyeLasersButtonPressed_mA7157DC0B90604C8AC1D70E5D6E0B74F387B9419 (void);
// 0x000003AC System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::EyePosesButtonPressed()
extern void ARSceneSelectUI_EyePosesButtonPressed_m4FAB1813DC0EF0B8447B5A8965CBB6E23643A7BE (void);
// 0x000003AD System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::FaceMeshButtonPressed()
extern void ARSceneSelectUI_FaceMeshButtonPressed_m38A33707BED022E6462BFC93DD31A3FBF93873DD (void);
// 0x000003AE System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::FacePoseButtonPressed()
extern void ARSceneSelectUI_FacePoseButtonPressed_mE3F19B1E6E6A21D479DC9EDC256E9624218F586D (void);
// 0x000003AF System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::FixationPointButtonPressed()
extern void ARSceneSelectUI_FixationPointButtonPressed_mC3C88F6282B3B1271FE4D4DF05F0BAB965E8A461 (void);
// 0x000003B0 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::RearCameraWithFrontCameraFaceMeshButtonPressed()
extern void ARSceneSelectUI_RearCameraWithFrontCameraFaceMeshButtonPressed_m48454B9187555F258A632F4010CE7B55830AC1FE (void);
// 0x000003B1 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::BodyTrackingMenuButtonPressed()
extern void ARSceneSelectUI_BodyTrackingMenuButtonPressed_m0514F115B07CBF754390067B536F5E37DD5E2D94 (void);
// 0x000003B2 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::BodyTracking2DButtonPressed()
extern void ARSceneSelectUI_BodyTracking2DButtonPressed_m515F3BA1B85207D2E3EB41110159585053F1A476 (void);
// 0x000003B3 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::BodyTracking3DButtonPressed()
extern void ARSceneSelectUI_BodyTracking3DButtonPressed_mED675FBE8C46565FEF6AC19F9A2CB4B263D073DF (void);
// 0x000003B4 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::LightEstimationMenuButtonPressed()
extern void ARSceneSelectUI_LightEstimationMenuButtonPressed_mF43ECC02071AA319B73C919FAE89FB574C7EE41C (void);
// 0x000003B5 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::BasicLightEstimationButtonPressed()
extern void ARSceneSelectUI_BasicLightEstimationButtonPressed_m3CD7677C680229E141F2C081E2BF127A8F72636B (void);
// 0x000003B6 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::HDRLightEstimationButtonPressed()
extern void ARSceneSelectUI_HDRLightEstimationButtonPressed_m449963DD0B0A23925074C37CD9C993F0F1CC6F0B (void);
// 0x000003B7 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::PlaneDetectionMenuButtonPressed()
extern void ARSceneSelectUI_PlaneDetectionMenuButtonPressed_m215F88FAA9FDFD4EB6A9729FCA4AC4F8ACAE09FB (void);
// 0x000003B8 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::FeatheredPlanesButtonPressed()
extern void ARSceneSelectUI_FeatheredPlanesButtonPressed_m42C04E26051E1B85CC1D21D944A41DCD60BA31C0 (void);
// 0x000003B9 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::PlaneClassificationButtonPressed()
extern void ARSceneSelectUI_PlaneClassificationButtonPressed_m745BEAEAECD1CCF65B5BC353754658F6BFE93CA3 (void);
// 0x000003BA System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::TogglePlaneDetectionButtonPressed()
extern void ARSceneSelectUI_TogglePlaneDetectionButtonPressed_m3A675640C489652D7809DB619F05F9C1C4674285 (void);
// 0x000003BB System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::BackButtonPressed()
extern void ARSceneSelectUI_BackButtonPressed_m44C70E5DCA188954A09D863BAD67E75817F0FF89 (void);
// 0x000003BC System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::MeshingMenuButtonPressed()
extern void ARSceneSelectUI_MeshingMenuButtonPressed_m142F06B7B0603065037E91537CE104B2F993486C (void);
// 0x000003BD System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::DepthMenuButtonPressed()
extern void ARSceneSelectUI_DepthMenuButtonPressed_mA3526C518E164667DA0EAB537BF6B1E5582E8CB9 (void);
// 0x000003BE System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::ClassificationMeshesButtonPressed()
extern void ARSceneSelectUI_ClassificationMeshesButtonPressed_mD64FEFCB3EAC9A4C65BD6F11D4E6BBF6450CB83B (void);
// 0x000003BF System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::NormalMeshesButtonPressed()
extern void ARSceneSelectUI_NormalMeshesButtonPressed_m10DB0F01EEF975758AD042279F719AA5BA1FC714 (void);
// 0x000003C0 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::OcclusionMeshesButtonPressed()
extern void ARSceneSelectUI_OcclusionMeshesButtonPressed_m0C13630ABB67F752B851312DADE9D589BE66E53C (void);
// 0x000003C1 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::InteractionButtonPressed()
extern void ARSceneSelectUI_InteractionButtonPressed_mFBFA1BE40D0E00A6192A2D8D5640FD84E8C2CA7D (void);
// 0x000003C2 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::SimpleOcclusionButtonPressed()
extern void ARSceneSelectUI_SimpleOcclusionButtonPressed_mF54C9E9CDD6D84F3002527A4655834D22D66B95E (void);
// 0x000003C3 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::DepthImagesButtonPressed()
extern void ARSceneSelectUI_DepthImagesButtonPressed_m59798E7B3CCCB943E3E4089C395C3ED0D8F9D000 (void);
// 0x000003C4 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::InputSystemButtonPressed()
extern void ARSceneSelectUI_InputSystemButtonPressed_mACF7AF4DF8AD532DB4D57D0DAD8F3ED8AA4147DE (void);
// 0x000003C5 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::CameraGrainButtonPressed()
extern void ARSceneSelectUI_CameraGrainButtonPressed_m725EBFA471DF073EA02007E78AD3783F586004ED (void);
// 0x000003C6 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::ThermalStateButtonPressed()
extern void ARSceneSelectUI_ThermalStateButtonPressed_mA86AD006F0FFDA3CB0B498C02698974B78A7AAB4 (void);
// 0x000003C7 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::ScrollToStartPosition()
extern void ARSceneSelectUI_ScrollToStartPosition_m77D718FFC3D98A3BDD81909557E46948B25B48D4 (void);
// 0x000003C8 System.Void UnityEngine.XR.ARFoundation.Samples.ARSceneSelectUI::.ctor()
extern void ARSceneSelectUI__ctor_m3A439E23B65D0C0802CD21DAD6635627CF9F69F9 (void);
// 0x000003C9 UnityEngine.XR.ARFoundation.Samples.MenuType UnityEngine.XR.ARFoundation.Samples.ActiveMenu::get_currentMenu()
extern void ActiveMenu_get_currentMenu_m57A139E0FF21CEEB6416C25D94832832496EB1F7 (void);
// 0x000003CA System.Void UnityEngine.XR.ARFoundation.Samples.ActiveMenu::set_currentMenu(UnityEngine.XR.ARFoundation.Samples.MenuType)
extern void ActiveMenu_set_currentMenu_m1D5E74394A21ADC83B2694047532CDEE983F35FE (void);
// 0x000003CB UnityEngine.GameObject UnityEngine.XR.ARFoundation.Samples.BackButton::get_backButton()
extern void BackButton_get_backButton_m6C42C5B8C3C5D726A2A638614C3C1004F8FF6B9D (void);
// 0x000003CC System.Void UnityEngine.XR.ARFoundation.Samples.BackButton::set_backButton(UnityEngine.GameObject)
extern void BackButton_set_backButton_mA20F4E8B6A0CAA1FCE697861488007DEB11588E7 (void);
// 0x000003CD System.Void UnityEngine.XR.ARFoundation.Samples.BackButton::Start()
extern void BackButton_Start_m91D83EF34477C02DEDEACF640208D15735EA3526 (void);
// 0x000003CE System.Void UnityEngine.XR.ARFoundation.Samples.BackButton::Update()
extern void BackButton_Update_m59F6AD5F9B15A73F885351A5521812C3E4253B8B (void);
// 0x000003CF System.Void UnityEngine.XR.ARFoundation.Samples.BackButton::BackButtonPressed()
extern void BackButton_BackButtonPressed_m3AE314F7F35880CCCC2D7C1D37075DF582A7CEEB (void);
// 0x000003D0 System.Void UnityEngine.XR.ARFoundation.Samples.BackButton::.ctor()
extern void BackButton__ctor_m99B8ED800DE45430146458136205319B7DE77498 (void);
// 0x000003D1 UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::get_simpleAR()
extern void CheckAvailableFeatures_get_simpleAR_m9BDD412478DBFED9EFAD94FA990B4E4FD87E5D71 (void);
// 0x000003D2 System.Void UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::set_simpleAR(UnityEngine.UI.Button)
extern void CheckAvailableFeatures_set_simpleAR_mA82545C6F82001D54265892D1628D58B44A7FEA2 (void);
// 0x000003D3 UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::get_imageTracking()
extern void CheckAvailableFeatures_get_imageTracking_m8B97E8F482FF872EC425C68B7436BA0750B4757E (void);
// 0x000003D4 System.Void UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::set_imageTracking(UnityEngine.UI.Button)
extern void CheckAvailableFeatures_set_imageTracking_mDDFBF25047147596820CDA7CC2B8C9DB31A70443 (void);
// 0x000003D5 UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::get_anchors()
extern void CheckAvailableFeatures_get_anchors_m0C777536139265A2F5EF3A9AD13A56BC1CEAAF3D (void);
// 0x000003D6 System.Void UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::set_anchors(UnityEngine.UI.Button)
extern void CheckAvailableFeatures_set_anchors_mB69251FB41E9511CA05662BEFB7A096025046414 (void);
// 0x000003D7 UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::get_ARWorldMap()
extern void CheckAvailableFeatures_get_ARWorldMap_m61D2685E3133DA2A1C54FD9524916A460859A12A (void);
// 0x000003D8 System.Void UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::set_ARWorldMap(UnityEngine.UI.Button)
extern void CheckAvailableFeatures_set_ARWorldMap_mE6F68F17EB263A7F83321E701200DBCC22CE3D86 (void);
// 0x000003D9 UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::get_ARKitGeoAnchors()
extern void CheckAvailableFeatures_get_ARKitGeoAnchors_mBD85BAD41538EE9C2DFCAE714A79566A1283E448 (void);
// 0x000003DA System.Void UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::set_ARKitGeoAnchors(UnityEngine.UI.Button)
extern void CheckAvailableFeatures_set_ARKitGeoAnchors_mD56CC44D8BFBC2DF442D109ED41427B079CDCF8F (void);
// 0x000003DB UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::get_cpuImages()
extern void CheckAvailableFeatures_get_cpuImages_m2B9E5E75184DE071E0327E4E7E369B0D7F2200F3 (void);
// 0x000003DC System.Void UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::set_cpuImages(UnityEngine.UI.Button)
extern void CheckAvailableFeatures_set_cpuImages_m834981832D3B834AA1C4E8FE6FDCC6775A0F27C1 (void);
// 0x000003DD UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::get_environmentProbes()
extern void CheckAvailableFeatures_get_environmentProbes_m7AA73DCDAAEE8A037168E746A0CD5F4BDDA43544 (void);
// 0x000003DE System.Void UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::set_environmentProbes(UnityEngine.UI.Button)
extern void CheckAvailableFeatures_set_environmentProbes_m87ABD4E1275CA502BFDDBE2A164CD0E33F6A190D (void);
// 0x000003DF UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::get_ARCollaborationData()
extern void CheckAvailableFeatures_get_ARCollaborationData_m9EA54245F78986923CB90F13844A58FBC426BC8C (void);
// 0x000003E0 System.Void UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::set_ARCollaborationData(UnityEngine.UI.Button)
extern void CheckAvailableFeatures_set_ARCollaborationData_m00F15E38EC6FA7DFC95EFFC1FE2D9ACAEC62D96B (void);
// 0x000003E1 UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::get_ARKitCoachingOverlay()
extern void CheckAvailableFeatures_get_ARKitCoachingOverlay_mE755BA45EB7B7BA09CF48135E6D8BDCED1394EA3 (void);
// 0x000003E2 System.Void UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::set_ARKitCoachingOverlay(UnityEngine.UI.Button)
extern void CheckAvailableFeatures_set_ARKitCoachingOverlay_m5A209F73A11D8948A1A5C7ECFEBA68DC215EECCE (void);
// 0x000003E3 UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::get_scale()
extern void CheckAvailableFeatures_get_scale_m682AAABD71F99CA664528D6D87F6B80B2B091FEC (void);
// 0x000003E4 System.Void UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::set_scale(UnityEngine.UI.Button)
extern void CheckAvailableFeatures_set_scale_m265178438AFB21F54B2B60FA5A4E940FA5828216 (void);
// 0x000003E5 UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::get_objectTracking()
extern void CheckAvailableFeatures_get_objectTracking_mFABD15AA61633BEFDB6C0075FA7796C01D58B39C (void);
// 0x000003E6 System.Void UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::set_objectTracking(UnityEngine.UI.Button)
extern void CheckAvailableFeatures_set_objectTracking_mBE263863F1C9CC3BF722567C0A7F7E895B53A610 (void);
// 0x000003E7 UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::get_planeOcclusion()
extern void CheckAvailableFeatures_get_planeOcclusion_m2567FD5A29E502A7E50428CA306C681D3C134FB3 (void);
// 0x000003E8 System.Void UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::set_planeOcclusion(UnityEngine.UI.Button)
extern void CheckAvailableFeatures_set_planeOcclusion_m12ED16B83C7E453A76AC6E0A20DB41C002343DD4 (void);
// 0x000003E9 UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::get_pointCloud()
extern void CheckAvailableFeatures_get_pointCloud_mAAEFE28828216AE4E808874F59B96C6050091AFD (void);
// 0x000003EA System.Void UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::set_pointCloud(UnityEngine.UI.Button)
extern void CheckAvailableFeatures_set_pointCloud_m74B0561B66D525AD8874D0F9F1BDF0D6F6F5938A (void);
// 0x000003EB UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::get_faceTracking()
extern void CheckAvailableFeatures_get_faceTracking_mD245135F71D1B439B1A39AF487E7FC03FC5CA518 (void);
// 0x000003EC System.Void UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::set_faceTracking(UnityEngine.UI.Button)
extern void CheckAvailableFeatures_set_faceTracking_mDCFD516DC78297DFA6DA2304BAA2D3527AAF24F7 (void);
// 0x000003ED UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::get_faceBlendShapes()
extern void CheckAvailableFeatures_get_faceBlendShapes_m30E405B68EB1C38CB90383883F88F82687E2A1F9 (void);
// 0x000003EE System.Void UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::set_faceBlendShapes(UnityEngine.UI.Button)
extern void CheckAvailableFeatures_set_faceBlendShapes_mFFCC1814463E8EDDCA22BC1A14AD1428C5E6BBB2 (void);
// 0x000003EF UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::get_faceRegions()
extern void CheckAvailableFeatures_get_faceRegions_mAA657FD6E5F354E8B2C61AA5233EB215FD9FF7F7 (void);
// 0x000003F0 System.Void UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::set_faceRegions(UnityEngine.UI.Button)
extern void CheckAvailableFeatures_set_faceRegions_mF34A67C064754AD3901A7BE618A7A97596F1B983 (void);
// 0x000003F1 UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::get_bodyTracking()
extern void CheckAvailableFeatures_get_bodyTracking_m20527AEEA4478E0E7F6BE34A8A5417872CAAC877 (void);
// 0x000003F2 System.Void UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::set_bodyTracking(UnityEngine.UI.Button)
extern void CheckAvailableFeatures_set_bodyTracking_m24DD1C3A24BEE5A90032093CC06F1998C307CCED (void);
// 0x000003F3 UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::get_lightEstimation()
extern void CheckAvailableFeatures_get_lightEstimation_m3F63CBDB379F89CC8236BD15BAA34C548EBD877F (void);
// 0x000003F4 System.Void UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::set_lightEstimation(UnityEngine.UI.Button)
extern void CheckAvailableFeatures_set_lightEstimation_m495A8BDD20A58DF2F205E4F7E9B4B0168A0FCDCA (void);
// 0x000003F5 UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::get_basicLightEstimation()
extern void CheckAvailableFeatures_get_basicLightEstimation_mE275B32E67BAC14411E54CBDD84736B7EB5F63B9 (void);
// 0x000003F6 System.Void UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::set_basicLightEstimation(UnityEngine.UI.Button)
extern void CheckAvailableFeatures_set_basicLightEstimation_mD5000FC7736E58018808A92F5D552D9976316727 (void);
// 0x000003F7 UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::get_HDRLightEstimation()
extern void CheckAvailableFeatures_get_HDRLightEstimation_m8567404E4FB6522CC0BCE7D23BE99021B8003165 (void);
// 0x000003F8 System.Void UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::set_HDRLightEstimation(UnityEngine.UI.Button)
extern void CheckAvailableFeatures_set_HDRLightEstimation_m88D9DFD515201B0E0729F98902913C807C098BFC (void);
// 0x000003F9 UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::get_planeDetection()
extern void CheckAvailableFeatures_get_planeDetection_mA11D23A722E63E4153E3DA9783EBC9DED9BDD102 (void);
// 0x000003FA System.Void UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::set_planeDetection(UnityEngine.UI.Button)
extern void CheckAvailableFeatures_set_planeDetection_m6EDE773076FA2B5908E85D651C80124A9D488E02 (void);
// 0x000003FB UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::get_planeClassification()
extern void CheckAvailableFeatures_get_planeClassification_m01CABC528B33EACD74653427D80DF2AE68ED42B5 (void);
// 0x000003FC System.Void UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::set_planeClassification(UnityEngine.UI.Button)
extern void CheckAvailableFeatures_set_planeClassification_m87C63BC487908C5A824D430E12C717CC59713218 (void);
// 0x000003FD UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::get_meshing()
extern void CheckAvailableFeatures_get_meshing_m920941D5C3AD748F79C5561FB6E3312F14F4C4B3 (void);
// 0x000003FE System.Void UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::set_meshing(UnityEngine.UI.Button)
extern void CheckAvailableFeatures_set_meshing_mD958293C0A82B4765B5148003DFB2DD6D96F5654 (void);
// 0x000003FF UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::get_interaction()
extern void CheckAvailableFeatures_get_interaction_m2E0EE4751E498795586C487D313253AF7C96FE9B (void);
// 0x00000400 System.Void UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::set_interaction(UnityEngine.UI.Button)
extern void CheckAvailableFeatures_set_interaction_m7209AE06A6B3CEC26A8829F6F2773EBECD739A9D (void);
// 0x00000401 UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::get_fixationPoint()
extern void CheckAvailableFeatures_get_fixationPoint_mD048092333181E71778D5FFA9BA026927FB9B74F (void);
// 0x00000402 System.Void UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::set_fixationPoint(UnityEngine.UI.Button)
extern void CheckAvailableFeatures_set_fixationPoint_mF87552591910BF75A0EACAEE6693FC8AE56623CA (void);
// 0x00000403 UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::get_eyePoses()
extern void CheckAvailableFeatures_get_eyePoses_mB28690DF1EE2EFFF4F93B071F2901C7F7CEABBAE (void);
// 0x00000404 System.Void UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::set_eyePoses(UnityEngine.UI.Button)
extern void CheckAvailableFeatures_set_eyePoses_m5D45A4CED8962396FCC83D865C58F4CB5867CE7F (void);
// 0x00000405 UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::get_eyeLasers()
extern void CheckAvailableFeatures_get_eyeLasers_m25F0044D1B80B19D1D6BC4A080F87DFA01535FA8 (void);
// 0x00000406 System.Void UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::set_eyeLasers(UnityEngine.UI.Button)
extern void CheckAvailableFeatures_set_eyeLasers_m5F1030B67FEF1DA79522E30943E1AE628E749281 (void);
// 0x00000407 UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::get_checkSupport()
extern void CheckAvailableFeatures_get_checkSupport_m5063595EAED8A5EBC4C1560DA14565E20C98A9BC (void);
// 0x00000408 System.Void UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::set_checkSupport(UnityEngine.UI.Button)
extern void CheckAvailableFeatures_set_checkSupport_m707494422F72A4D121D91817C2B9AF8CE70A0E47 (void);
// 0x00000409 UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::get_depth()
extern void CheckAvailableFeatures_get_depth_mEF572EA560DE0091A7FC409F0EEEDEBBA03CB910 (void);
// 0x0000040A System.Void UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::set_depth(UnityEngine.UI.Button)
extern void CheckAvailableFeatures_set_depth_mB7C3C8EC50F774273C94700D179BB79997C14B58 (void);
// 0x0000040B UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::get_configChooser()
extern void CheckAvailableFeatures_get_configChooser_m2394190E1FDB13C935403B9AEE77829CBE9CDDEC (void);
// 0x0000040C System.Void UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::set_configChooser(UnityEngine.UI.Button)
extern void CheckAvailableFeatures_set_configChooser_m716AFBFDC49F7E99DCC8C1D7F1A4FAD092A7F380 (void);
// 0x0000040D UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::get_inputSystem()
extern void CheckAvailableFeatures_get_inputSystem_m060A8016F592EEDD9EF0F32E6782EB2866B92AF9 (void);
// 0x0000040E System.Void UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::set_inputSystem(UnityEngine.UI.Button)
extern void CheckAvailableFeatures_set_inputSystem_m37C37232C55AE0DA337CE248820C3C4D72EF094F (void);
// 0x0000040F UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::get_cameraGrain()
extern void CheckAvailableFeatures_get_cameraGrain_m4555FDC4B2C25B1261CEFB1D675C423D247C1E78 (void);
// 0x00000410 System.Void UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::set_cameraGrain(UnityEngine.UI.Button)
extern void CheckAvailableFeatures_set_cameraGrain_m10E9572EAA79933A22B75FDB0B30CAA040B7BA2A (void);
// 0x00000411 UnityEngine.UI.Button UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::get_thermalStateButton()
extern void CheckAvailableFeatures_get_thermalStateButton_m1ADC7BC2AD411824A1BAA8A2F9CC7BA91858CBB1 (void);
// 0x00000412 System.Void UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::set_thermalStateButton(UnityEngine.UI.Button)
extern void CheckAvailableFeatures_set_thermalStateButton_m2485BDDDA6C4C0DB205377D268ABE6F14AD21767 (void);
// 0x00000413 System.Void UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::Start()
extern void CheckAvailableFeatures_Start_m7A61A7DADD22FB81C3C0DD7255A0365900FD0ED3 (void);
// 0x00000414 System.Void UnityEngine.XR.ARFoundation.Samples.CheckAvailableFeatures::.ctor()
extern void CheckAvailableFeatures__ctor_mDE69B473063EE750F6E37D68692EE0C61573AA84 (void);
// 0x00000415 UnityEngine.XR.ARFoundation.AROcclusionManager UnityEngine.XR.ARFoundation.Samples.CheckRuntimeDepth::get_occlusionManager()
extern void CheckRuntimeDepth_get_occlusionManager_m0E6BEAF0FE86814FFC75A505B3E4AEA1C14367A0 (void);
// 0x00000416 System.Void UnityEngine.XR.ARFoundation.Samples.CheckRuntimeDepth::set_occlusionManager(UnityEngine.XR.ARFoundation.AROcclusionManager)
extern void CheckRuntimeDepth_set_occlusionManager_mCF2E7E43C764A54EE8456D1F66AB2C0C567DC166 (void);
// 0x00000417 UnityEngine.UI.Text UnityEngine.XR.ARFoundation.Samples.CheckRuntimeDepth::get_depthAvailabilityInfo()
extern void CheckRuntimeDepth_get_depthAvailabilityInfo_mA5333955942DDFD90E1F32C4598CC8AC8837D42C (void);
// 0x00000418 System.Void UnityEngine.XR.ARFoundation.Samples.CheckRuntimeDepth::set_depthAvailabilityInfo(UnityEngine.UI.Text)
extern void CheckRuntimeDepth_set_depthAvailabilityInfo_mFC2E36C76071C838D12E8747DA106D4EA4B88226 (void);
// 0x00000419 System.Void UnityEngine.XR.ARFoundation.Samples.CheckRuntimeDepth::Update()
extern void CheckRuntimeDepth_Update_m11001470F189AFFD2A327F98C142C630BDDB3CAC (void);
// 0x0000041A System.Void UnityEngine.XR.ARFoundation.Samples.CheckRuntimeDepth::.ctor()
extern void CheckRuntimeDepth__ctor_m5F249FF11545555777E3966567BF019EC8CBDA8A (void);
// 0x0000041B UnityEngine.GameObject UnityEngine.XR.ARFoundation.Samples.Tooltip::get_toolTip()
extern void Tooltip_get_toolTip_mFAEB13D6C6F59968E15515E7562590F2D679538F (void);
// 0x0000041C System.Void UnityEngine.XR.ARFoundation.Samples.Tooltip::set_toolTip(UnityEngine.GameObject)
extern void Tooltip_set_toolTip_m563A8CA229F2D52047B281366C2ED796667923BE (void);
// 0x0000041D System.Void UnityEngine.XR.ARFoundation.Samples.Tooltip::Start()
extern void Tooltip_Start_m09784ADCC06FE4C3A3EC00DA97BA1E37A4C94935 (void);
// 0x0000041E System.Void UnityEngine.XR.ARFoundation.Samples.Tooltip::Update()
extern void Tooltip_Update_mEFEFDB9EF2106B5C65B5F9D6C1EF08506A25EB0B (void);
// 0x0000041F System.Void UnityEngine.XR.ARFoundation.Samples.Tooltip::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void Tooltip_OnPointerEnter_m29F0B4FADC02BE76CEBB83F5903E87A39FB2C6CC (void);
// 0x00000420 System.Void UnityEngine.XR.ARFoundation.Samples.Tooltip::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void Tooltip_OnPointerExit_m542FD03066EABF976D9958F05CC4A6D0B8763FE6 (void);
// 0x00000421 System.Void UnityEngine.XR.ARFoundation.Samples.Tooltip::.ctor()
extern void Tooltip__ctor_mC6DACD76A8238B0F3D879EFB3581E2A4752401D0 (void);
// 0x00000422 System.String JSONModel.Geometry::get_type()
extern void Geometry_get_type_m1887A492B188BFB1F9F83DFA9D858EDC7C0B5CB3 (void);
// 0x00000423 System.Void JSONModel.Geometry::set_type(System.String)
extern void Geometry_set_type_m0AC6BEF9990A280BD29326C644485AAA20338057 (void);
// 0x00000424 System.Collections.Generic.List`1<System.Double> JSONModel.Geometry::get_coordinates()
extern void Geometry_get_coordinates_m65BB75C86E83D46BB5EE1275C85B32A0111AB375 (void);
// 0x00000425 System.Void JSONModel.Geometry::set_coordinates(System.Collections.Generic.List`1<System.Double>)
extern void Geometry_set_coordinates_m71CC1DDAEB1DB65565E80B34B4FD40621C3E4D87 (void);
// 0x00000426 System.Void JSONModel.Geometry::.ctor()
extern void Geometry__ctor_m3FFD17C9CDF524B05385A70CCAECD65CD82A0121 (void);
// 0x00000427 System.String JSONModel.Feature::get_type()
extern void Feature_get_type_mF2E77FE6940C11B184B8DBCC79DBF03C0295D183 (void);
// 0x00000428 System.Void JSONModel.Feature::set_type(System.String)
extern void Feature_set_type_m31AD9B5EBFC2E508969A04CBF14EA639EF0609B9 (void);
// 0x00000429 JSONModel.Properties JSONModel.Feature::get_properties()
extern void Feature_get_properties_m2D29DD047BD28E7FE343E430CA66CDA093D4B72F (void);
// 0x0000042A System.Void JSONModel.Feature::set_properties(JSONModel.Properties)
extern void Feature_set_properties_m9E28FE4C74CD556CA38B1DBF4023661FCB0AA98E (void);
// 0x0000042B JSONModel.Geometry JSONModel.Feature::get_geometry()
extern void Feature_get_geometry_mAF48ABD0CBD61DAC2CB073868EFAE6EAD00D5C2E (void);
// 0x0000042C System.Void JSONModel.Feature::set_geometry(JSONModel.Geometry)
extern void Feature_set_geometry_m00F69594E38D4DBF01CC1A42A53B6ED4D3DD4CDA (void);
// 0x0000042D System.String JSONModel.Feature::get_id()
extern void Feature_get_id_m8F409898D966264752DDBAF62473FA0ACF79EF07 (void);
// 0x0000042E System.Void JSONModel.Feature::set_id(System.String)
extern void Feature_set_id_m7B4E7DAA1B73A0AAAA5DFECCC74514A17FF5FE80 (void);
// 0x0000042F System.Void JSONModel.Feature::.ctor()
extern void Feature__ctor_m0D546A98532F40794E22C4A0E9679B56F6ED0D6E (void);
// 0x00000430 System.String JSONModel.Root::get_type()
extern void Root_get_type_m5F0226A3F03F1CD9A912726008E49735966BA95F (void);
// 0x00000431 System.Void JSONModel.Root::set_type(System.String)
extern void Root_set_type_mECC5E5A23C4B9A4EF1FEC8156C99F5CB07502F88 (void);
// 0x00000432 System.Collections.Generic.List`1<JSONModel.Feature> JSONModel.Root::get_features()
extern void Root_get_features_m8CEF9B138BE459A7414671B9D5E2DC21B91633A3 (void);
// 0x00000433 System.Void JSONModel.Root::set_features(System.Collections.Generic.List`1<JSONModel.Feature>)
extern void Root_set_features_mFEB1BA3E6B7D46DDCA47BEE4B0FA13C78C1A2AE7 (void);
// 0x00000434 System.Void JSONModel.Root::.ctor()
extern void Root__ctor_m0E0B7CED7B239A774BF98F310E5E14899979B087 (void);
// 0x00000435 System.String JSONModel.Properties::get_icon()
extern void Properties_get_icon_m0ED2EFC338132D7F5E1C06E4437A5326C48019A6 (void);
// 0x00000436 System.Void JSONModel.Properties::set_icon(System.String)
extern void Properties_set_icon_mA3422628CD62248FD408232125FBA72239ABD587 (void);
// 0x00000437 System.String JSONModel.Properties::get_id()
extern void Properties_get_id_m38C16F2FFD0D5EE18D3D48F3E4C2BB982ED76326 (void);
// 0x00000438 System.Void JSONModel.Properties::set_id(System.String)
extern void Properties_set_id_m1A7BB9D5936286D8CA094B181D08BA35664698E7 (void);
// 0x00000439 System.String JSONModel.Properties::get_name()
extern void Properties_get_name_mC39F85DF9902CEDD867EF30FDEF4867147776F58 (void);
// 0x0000043A System.Void JSONModel.Properties::set_name(System.String)
extern void Properties_set_name_mCA53F5665A286632EB169B44B8B74A092AB1A27C (void);
// 0x0000043B System.String JSONModel.Properties::get_description()
extern void Properties_get_description_mDC4B9BB91CC94F5693FC36D5FC185B0DC8FF1D0F (void);
// 0x0000043C System.Void JSONModel.Properties::set_description(System.String)
extern void Properties_set_description_m43B6DAB50FEAF05E01FEE48741A09656585E75CC (void);
// 0x0000043D System.String JSONModel.Properties::get_info_type()
extern void Properties_get_info_type_mA5B588015A3CF2B2F721769E1F25C7A0CE634363 (void);
// 0x0000043E System.Void JSONModel.Properties::set_info_type(System.String)
extern void Properties_set_info_type_m3407628F5EE1B0F4069CC7B78A1CCB7EBCE4116F (void);
// 0x0000043F System.String JSONModel.Properties::get_Name()
extern void Properties_get_Name_m8B9089BD4B41621ACE464AA295CDFA34CE531DA0 (void);
// 0x00000440 System.Void JSONModel.Properties::set_Name(System.String)
extern void Properties_set_Name_mEADD5273FD508CD164BABE98C32C0B78241353CA (void);
// 0x00000441 System.Nullable`1<System.Int32> JSONModel.Properties::get_range()
extern void Properties_get_range_mE5184ED0FD15844A94E5496B8190195D79BC7452 (void);
// 0x00000442 System.Void JSONModel.Properties::set_range(System.Nullable`1<System.Int32>)
extern void Properties_set_range_mD71067C1F9A4D8164B90A76110C793BF5895548E (void);
// 0x00000443 System.String JSONModel.Properties::get_start()
extern void Properties_get_start_m84CBDBCC6432A5E278C8CFCC71034D716BE16CBF (void);
// 0x00000444 System.Void JSONModel.Properties::set_start(System.String)
extern void Properties_set_start_mA53934C23868FF9821F6FD05195D41E1EEBDECC6 (void);
// 0x00000445 System.String JSONModel.Properties::get_stop()
extern void Properties_get_stop_m3EF3602B3555AEED1BD8C88B3E84776FCCC6A353 (void);
// 0x00000446 System.Void JSONModel.Properties::set_stop(System.String)
extern void Properties_set_stop_m46BC4581D1A9FAAE92A09ECA677E9DA424836F17 (void);
// 0x00000447 System.String JSONModel.Properties::get_message1()
extern void Properties_get_message1_mC3790FE29009909102057A90496AD8C062E99304 (void);
// 0x00000448 System.Void JSONModel.Properties::set_message1(System.String)
extern void Properties_set_message1_mE539BD051D38F76DDE742EB92FE64BBB648435CF (void);
// 0x00000449 System.String JSONModel.Properties::get_message2()
extern void Properties_get_message2_mD2784E85D92A4B6C2653F47E80B720C5DFCE6842 (void);
// 0x0000044A System.Void JSONModel.Properties::set_message2(System.String)
extern void Properties_set_message2_mE0DB7C92F0D420B92FDABAB8B74AAFED25CF6957 (void);
// 0x0000044B System.Nullable`1<System.Int32> JSONModel.Properties::get_risk_type()
extern void Properties_get_risk_type_mBB5FB85D1A3665AD8A83075050C5964DBA074BC2 (void);
// 0x0000044C System.Void JSONModel.Properties::set_risk_type(System.Nullable`1<System.Int32>)
extern void Properties_set_risk_type_m799A4701C2A79877468BA4D0CEE91D1C9B58A17D (void);
// 0x0000044D System.String JSONModel.Properties::get_movie()
extern void Properties_get_movie_m8A23142B10AECCF9B51E9966F6D8F3880CFF4FFB (void);
// 0x0000044E System.Void JSONModel.Properties::set_movie(System.String)
extern void Properties_set_movie_mF0948C281C93C9D631910AFF2F84CD3B5F32A0E6 (void);
// 0x0000044F System.String JSONModel.Properties::get_pic_type()
extern void Properties_get_pic_type_mF6B0300ABDFB652656BEF2D9399FB8AA1686FCA2 (void);
// 0x00000450 System.Void JSONModel.Properties::set_pic_type(System.String)
extern void Properties_set_pic_type_mEE0CE33FF1BA7CEE91DDDA9BB5CA0EA86809DC60 (void);
// 0x00000451 System.String JSONModel.Properties::get_photo()
extern void Properties_get_photo_mE89456BC3ADFFAD1676EC82ECEDA3E48F2A30972 (void);
// 0x00000452 System.Void JSONModel.Properties::set_photo(System.String)
extern void Properties_set_photo_mAE38814DDAEDF4DFD5C0F9CB3FC59ED8F0339F14 (void);
// 0x00000453 System.Nullable`1<System.Double> JSONModel.Properties::get_water_level()
extern void Properties_get_water_level_mA2DCF204B67965D93B7AACD6BFDD29A50071C08F (void);
// 0x00000454 System.Void JSONModel.Properties::set_water_level(System.Nullable`1<System.Double>)
extern void Properties_set_water_level_mA5543B61DE90A04559CC17E2891543D6112B03C7 (void);
// 0x00000455 System.Void JSONModel.Properties::.ctor()
extern void Properties__ctor_m76BDFD561290BCEF4A9FCC700412328A1BEC3803 (void);
static Il2CppMethodPointer s_methodPointers[1109] = 
{
	EmbeddedAttribute__ctor_m90D0B6DEB625101355554D49B2EE2FB67C875860,
	IsReadOnlyAttribute__ctor_mF1843241F60B2240CFAE651F1FD8A7AE17E32ECD,
	NullableAttribute__ctor_m68337A4A4913B9D45F4B7249895084D57F47B445,
	NullableAttribute__ctor_mDD67D1F687CC4C4A36E4E580AAC3AA2EB559CFAC,
	NullableContextAttribute__ctor_mC9D42ADAC30424D8311831B59B21E77EB522453F,
	Float_Start_mBBF047E2FB9FE4F6EBA52C211264418CA98D016F,
	Float_Awake_m1CB97E0F778177E7DE93F4ACB562E4FC4E57D5BC,
	Float_Update_m7D3509C96925953822E1CB021F022E51B215A02B,
	Float_OnCollisionEnter_mFD17E2825109F960D197BE896E20C6288B1248F3,
	Float_Delay_mDE41CDCC1490C8860ED800C5C5D24D1DA9AF5712,
	Float_getRandomX_mF3329C27EC7CC92D4C374905524715A39C669AE2,
	Float_getRandomZ_m5AA6E840A5340091D490D6ECA12148238B4F465F,
	Float__ctor_mBB0E290A40E91A1DCEFA692420EA6462129EEB2F,
	U3CDelayU3Ed__14__ctor_mE0FD63AD080B1982DC52C019DAE167D1ADB61767,
	U3CDelayU3Ed__14_System_IDisposable_Dispose_m145D2FBA5FB9BE406EFF8871C58A121D461F9362,
	U3CDelayU3Ed__14_MoveNext_m4A8BC31DA9498D20A159D8CE3DCBC6B732BC1735,
	U3CDelayU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE03431CB00A455A57F2E7880170F7795EB7222AD,
	U3CDelayU3Ed__14_System_Collections_IEnumerator_Reset_mF0B06F2F67C1E03396DA14F60C9CA648275A2A51,
	U3CDelayU3Ed__14_System_Collections_IEnumerator_get_Current_mFB2D6497B262DFB2D77CE7842FFE8C2DEDF21551,
	QuitBehavior_Start_mC35A9690C2F0E124557EAE26CBC3A85BF344D84F,
	QuitBehavior_Update_m6691DE68363B980C048BCADEDB2BEF2FF1757111,
	QuitBehavior_OnButtonPressed_m065E996ECA28CD5B3D867BB5EFD4A9C71AC45BD2,
	QuitBehavior_OnQuit_m17EE985A514F9CAC4E09E2F608996097571C8B1D,
	QuitBehavior__ctor_mF05257F242D1BFC8A92FAA85B7A599DE8F772F47,
	cardbroadMove_Start_m7BDC779FD9C4A3AAD16B794F926E0DA2237E15C2,
	cardbroadMove_Update_m3F92421E4C755951A9AC62EB89C7CA70BDFF7457,
	cardbroadMove__ctor_m95444D018C4EE2448F6242C6744AA3D121A71C0D,
	ARAnimationViewDemoController_Start_m4213BD7A3099E11418B19A84B28C0D750497FD80,
	ARAnimationViewDemoController_Update_m4D45C43D0BD5243405EFD753F66FA889DF9D688A,
	ARAnimationViewDemoController_OnPressedDemo_m18E9695A5F5B3D739F522FA8D505E2AE85FE1BAC,
	ARAnimationViewDemoController__ctor_m754A317CA6A6ED007C7B3502401F4DCBA42A0A88,
	BoxVolumeController_Start_mA6C14215598AE6024EC3900E898CDC6017404D67,
	BoxVolumeController_Awake_mD68C435A89E87CC1BB87D2A10FC5C4BD3600B717,
	BoxVolumeController_OnEnable_mDE5417DA910925843682321D44E6FD7486C18FFE,
	BoxVolumeController_Update_m14584C19E1FA367941C6116DF3914D15703F2395,
	BoxVolumeController_MoveUp_mD83F65DCFDFEFC2F0C5524262754C1CDB35FDB68,
	BoxVolumeController_MoveDown_mD65D30D899D6C8DD7692AF812D5E8A3A1FC519CD,
	BoxVolumeController__ctor_mE9784FF4C4605AC9152138B71716A164802543D7,
	DialogInfoController_Start_mBA5C319A39AF33FAFA775B83C80D5A082A059867,
	DialogInfoController_Awake_mDE1203FC0FF993CBAC65D301B5234A15236F09F9,
	DialogInfoController_OnEnable_mA36C88B51F771D63B8BF6E0842D7F78131CA78E3,
	DialogInfoController_OnDisable_m12159A8182EB8B8C554C3454492D8B09FE94A4C6,
	DialogInfoController_Update_m71135D349B8B4507F042FC2807343F0323A8D0F7,
	DialogInfoController_setFeature_m31FF1490089C972E9C9F526A72F459D127EB003E,
	DialogInfoController_close_m443CCB6E6C96A6DE81C9688109B04FA69A8A9113,
	DialogInfoController_openYoutube_m06F37B1CCFE340E42F0F2877875E46A95CBD1474,
	DialogInfoController_StartDowload_mD3681469F049F32970F9519352530DEC0421FA46,
	DialogInfoController__ctor_mD932D72E9353CCFF2EC18A6A43F23D642CE59FDF,
	U3CStartDowloadU3Ed__14__ctor_m80B41F9CF060CFD89A05F2E91FBED2D1EA3B3F43,
	U3CStartDowloadU3Ed__14_System_IDisposable_Dispose_m284ABF2B32514C4DDEBA66218ACC68396A8A3CEB,
	U3CStartDowloadU3Ed__14_MoveNext_mAAA46EF715B80FBA81006B6399BD4D1616607185,
	U3CStartDowloadU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC03C823F452769B40F191150891AA938CA9650CB,
	U3CStartDowloadU3Ed__14_System_Collections_IEnumerator_Reset_m4ED15222CE8F23D9920B6F260B33AB1B749AF1B0,
	U3CStartDowloadU3Ed__14_System_Collections_IEnumerator_get_Current_mF63595EB73AE5D67E186F7C3ED4C5D34D19DED9B,
	FloodController_Start_mB8C1F0229CCD844B0BF4098DEB75280B57ADEA31,
	FloodController_Awake_m85B5A456E74D945B74E4DB9CBF503C208CFD89DC,
	FloodController_OnEnable_m95B061C63FBE540FA0CFE0D02BFECEB094CACF77,
	FloodController_GetHeight_m415C73C1E28304276300AACA44EBA3219E63366D,
	FloodController_Update_mEB7BB8DA95473A472F2EFE34CB03931A38CB5A35,
	FloodController_MoveUp_m3F649A7A56FB27828B70A503F2066877E42F7E26,
	FloodController_MoveDown_m9884FD11028E14B33EE66410CC569EC8AF03087A,
	FloodController__ctor_m30A7E401C5404E234E5DE1FB68811A1A3ADB679B,
	GISInfoView_Start_mBCC9310B1C4CFFCAB6AE5934DA2E9882F1ED6C88,
	GISInfoView_Update_m96ED216FBDCCA46B9764B976F5238EFA53880A0A,
	GISInfoView__ctor_m667FE268E35A74CC745BEDDCC075EA0722330A53,
	InfoModelController_Start_m8B84566E08CB3C55ADAF5CA8F4FAE884410E32CC,
	InfoModelController_OnEnable_mD4660535FD6E2A96FD90600F1F66480428C991EE,
	InfoModelController_Update_mD2DC8928610DB03758682BED61DC5C3E75403767,
	InfoModelController_ConvertGPS2ARCoordinate_m3327B00A30D5BAF2D81F3128D5A23508283E4916,
	InfoModelController_UnityEngine_EventSystems_IPointerClickHandler_OnPointerClick_mA61EA2FD416B0003F7C4018AF826D73C28E7A53C,
	InfoModelController_onClick_m377AC7F52F4299D9BB5F4567FBC02EBC3B686A1D,
	InfoModelController_OnDisable_mAA7390A9FE38714DE21C4D47856377F96A841A9C,
	InfoModelController_OnTriggerEnter_mDBCD8FFA8851AC294923656F37393FDEEB22C27A,
	InfoModelController__ctor_m91EF1FBA4A02CED0964CE4DCC733A6BDB7A2C7F2,
	InfoModelController_U3CUpdateU3Eb__6_1_m1CED5E4B8B01A36C5F792398219AD24602738CF4,
	InfoModelController_U3CUpdateU3Eb__6_2_m99975B8537464354FAABF3FE0A5317D82486E275,
	InfoModelController_U3CUpdateU3Eb__6_3_mEC3CDFC97870E662B38216A94A1CEBB692A50676,
	InfoModelController_U3CUpdateU3Eb__6_0_mA9E8D2A85D4F47300922D435D8DBAD311F7B6A62,
	MainController_Start_mCB939AEB64460DA322E086C461603DE0196A3941,
	MainController_Awake_m8B49758963B70D3FDD86A546103BAD85FA4B9182,
	MainController_OnEnable_m3C165CC055595B26F0A65500C3CD309654DC2732,
	MainController_Update_m4067F63F85150EEB4BDC438739554B5A10261792,
	MainController_updateNotice_m0B6D35869A8793C6BDD191BB500B8C0365391BE0,
	MainController_GetText_m879A1CA0C7905ECE6C2EDE59E1690F8CA79D9AB6,
	MainController_GetDisasterLink_mDA35761724E6563FFC18D6EAE3E7FCBA21C6CA8C,
	MainController_Info3DView_mD6F4ECB4D37559669E2AA61926694A800B2232A4,
	MainController_update3DView_mAA2A85DBE9A1C76486945E2FA1101754CB5B673B,
	MainController_hillAnimation_m6C71068D728E793DB45A94D46E42C3BA4F89F67F,
	MainController_hosueAnimation_m87F529AE2D5BC609C76B485FC79FD6F89AF5F34F,
	MainController_addGameObjectRandom_mD99984AEE2AFCD9D7429A7C8883906A1C0A7B9CB,
	MainController_ConvertGPS2ARCoordinate_m545451898C96772C46D25B33FD1BBFE6BEF77359,
	MainController_printXYZ_m5F8A5F5EB53EE4A5F37B0CBD9C5E8A1EB7792C71,
	MainController_printXYZW_mA1D25DAF76CCB0E9E87F4A51C87EF28DBF62FBCA,
	MainController_addLiquefaction_mF61A607516E9533FB6D90DB0DC7F149DE7C04B98,
	MainController_resetAR_m1CAD659AC8BD4014EF66DE9E957FB24CD32F3B6A,
	MainController__ctor_mD54970CDDB17F015F7D2362DB2074CA80121E22E,
	U3CGetTextU3Ed__31__ctor_m2A4D0BB0C315CC4BA35FBC7E9506872CB3E077F2,
	U3CGetTextU3Ed__31_System_IDisposable_Dispose_m8E47247269F22FB892CF0724EE346E48D3B51CC4,
	U3CGetTextU3Ed__31_MoveNext_m49E2BFCB08DBD075579926A1D6BC3303DE2370C9,
	U3CGetTextU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC720DA1060114168DCB8EC0603AE70A1F2C23E6E,
	U3CGetTextU3Ed__31_System_Collections_IEnumerator_Reset_mB6449E798D29538501031A99E916B6DCBA85EDE4,
	U3CGetTextU3Ed__31_System_Collections_IEnumerator_get_Current_m8DA2DB73D34847049C754A42FA0C6DDD9ACA9F38,
	ModelDestroyController_Start_m4BD1DCC65E36B93D03E48B99B70030393D17A0DD,
	ModelDestroyController_Update_mF3855670D2EAD497005A9F80D98183970BD2453A,
	ModelDestroyController__ctor_mBBED7F06188965ACD246CC9FC53D4D6B49B4B5C5,
	RuleWaterController_Start_mD576FCF36B4F8AB4F24CAD1B2019641BAFEF92C4,
	RuleWaterController_Awake_m72BA9907B8F81932C53116277FF025230E5791DF,
	RuleWaterController_OnEnable_m13C3621FFF9412B9084029C2146165097CDA9E13,
	RuleWaterController_Update_mD1BCE4C85F86617F395880FB362CD19725643E9E,
	RuleWaterController__ctor_mF93D8C950CB3BDC3E475CBBF0C477C339CB472B1,
	Swing_Start_m936F5AC4B1240998F29DD8DA4DB58E73931876ED,
	Swing_Update_m58743793B1E7E8653EC25FEDD3D68F0DD81216AF,
	Swing__ctor_mD5A004559F1511B3BC8C0F38908AED76E7C0D999,
	WarnAnimationController_Start_m3781D2707C18E69932A1ECC59885F61E489F8E50,
	WarnAnimationController_Awake_m95D8C647B2B22D54EE97F1CD61CC3B082377B6B3,
	WarnAnimationController_OnEnable_mD5B8C99EDEC371633DA21A6990E31A888B616124,
	WarnAnimationController_Update_mEAF6AE80CD51D76DC078C01CF5903F13D5A172F4,
	WarnAnimationController__ctor_m164188F58B8749DF99566B8EE878B8289C40F4F3,
	WarnModelController_Start_m27B305A22773B4E81F5405F0F56157EF93B37F51,
	WarnModelController_Update_m4B5C6144B09A32C9D0BAEB420E4F3F657D385AD4,
	WarnModelController__ctor_m20DAFE8F2A37308C9FB59AB85B3E53B2DA2E8EF8,
	liquefaction_Start_mD2CCC8A278DD5AE0055C0988301A437FF192057D,
	liquefaction_Awake_m52C53B0011B0E26693D636164933B750F313D685,
	liquefaction_Update_m3585E22E6FA222EE5173513C9C3E3B99D153B2C1,
	liquefaction_MoveUp_mF0D05708E263953D765249E7AECB89AA59C7DE59,
	liquefaction_MoveDown_mF007EC3E05C5C9F41357E70FB9BB674256B3507A,
	liquefaction__ctor_m448706C8E04C937C210686CC26AFF917F4A9315A,
	GlobalAR__cctor_mBEDADBF1C27D13870FF0525AE62061DFA5759A09,
	DataManager_storeData_mCB26B5DAE143064DA9EB8068728B57D3DBD59B37,
	DataManager__ctor_m8A6DD19CA5399C36D89F0B103A74AB7F0A7BA4DE,
	Disaster__ctor_m18D90E4B89D53B5C22638EC0A067093D8111724B,
	TagAR_get_commonId_m06E0974B23999E02943677D7DB6A241EE72E35DC,
	TagAR_set_commonId_m1CC83C1B94478DF1790E18E508F9E0B4C2B8D9B4,
	TagAR_get_name_mE6A4711ED39AB9A8FB573635F1B7E47F68821A27,
	TagAR_set_name_m2868F35D04973D5C4CEFBC58869E79F6D2DF1D8C,
	TagAR_get_inforType_mB82D76AA9B14680FAAF4938714ECD8BAA7DC6638,
	TagAR_set_inforType_m160233DDA5217FB211E2DC34057FCC6C8E17927C,
	TagAR_get_icon_m1A9B1EC1B669A0923CC796C1589DFB263066ABAB,
	TagAR_set_icon_m7433795AF34F3BB311FE37EA3E7265C7FCB8D0A0,
	TagAR_get_descript_mBC86CD4F9FB9E1EE7C7557FC9D278ADF271A6823,
	TagAR_set_descript_mD43BF06B9ACE8C2604E114FF8672AA5DA3F22B20,
	TagAR_get_lat_m34BFA303CEF33D120D8525C971BA739D9731C641,
	TagAR_set_lat_mB52DA0748926995A7EEA2DC1D5322FC47D1F2F77,
	TagAR_get_lon_mFAB46130CCB2FD8A1D219378AC63A8B0CE04A84B,
	TagAR_set_lon_m8FD7853631A0C604AE2343F878CB949908378905,
	TagAR_get_direction_mFC2DC0A23E6653B71C0BF6410DC0A32C2C9BBFA6,
	TagAR_set_direction_m57DBED00A62F376D2598AE4433933EEA5B53DAC9,
	TagAR_get_pinNum_m619B05897744946B08845C5E26C193E1EBF3927D,
	TagAR_set_pinNum_mBC606EB69D7AB256EEBE72F7EA88C929C8B501B1,
	TagAR_get_elevation_m1F6905EDDB3F6F50FFF961AA7905C0804B1CFD2A,
	TagAR_set_elevation_m1EFE56904E46121CE92C27882AC7BC3A9D8FDC86,
	TagAR_get_distance_mA119F27092358A14A05E82A0541B871D1C396B58,
	TagAR_set_distance_m94D936020D9D44C041EA684F368B66D1FED6EA79,
	TagAR_get_picType_mEAE09CA8FCA28C6F3462D707015EEA366370E53D,
	TagAR_set_picType_mB2632221EF235F1A02015D7D9FF51E81A1F6821A,
	TagAR_get_photo_mEE6A54EEADBF820F9C743BAF44EB44EDC146C428,
	TagAR_set_photo_m51309930F430FE63AC2550F8461FAD0754FF8416,
	TagAR_get_movie_mE050056380C10D63DD7565104EBA36957B117EB0,
	TagAR_set_movie_m3EAB144CE79D92075A846035FF69FEDCE06B7031,
	TagAR_get_range_m00FA628599D6E4AC9A366455CCC62DCBDC8CE5A7,
	TagAR_set_range_mDBF5BBCFFEB6FDC31058F10FC586E08518FA760F,
	TagAR_get_start_m03AC44A21B9495E0565367ECF185DA5B96F3E170,
	TagAR_set_start_m276BDEFC0867195040FAC35EA823F4BD66773E17,
	TagAR_get_stop_m06508E9927AADEC8D6A2DDFB4BA2EEAB815C906D,
	TagAR_set_stop_mE7F86A5117245DB1AC601514D9F80899C2354A71,
	TagAR_get_message1_m271B16F7FB4324BE1397A8966770A708754D3C4B,
	TagAR_set_message1_m58EE096CDFA4CDA343A6F2C9DB279B3E0B6FDF1A,
	TagAR_get_message2_m3613C10AA51F6B223C243AEECAEA81062994852F,
	TagAR_set_message2_m3DAABEBE3FBAB386F46E872199A79DC4BB0164E3,
	TagAR_get_riskType_m231AE1CFCF9F438FA6904C892F134E786ACA9D4A,
	TagAR_set_riskType_mC455E5BBB55DDFC740AFC11D1D7B7B857E19DFC2,
	TagAR_get_isFullRange_m0092206E56073B73313F5B081C39E85E068B8F6A,
	TagAR_set_isFullRange_mF576B581869B3DFCFFBFB000F761FAF50A327EC7,
	TagAR__ctor_mA2621368011CAD64F03E904B68D424739599C336,
	GeoTool_ConvertCoordinate_m13CB410AF2547AADA62D5F33B04BC42E5441D456,
	GeoTool_ConvertCoordinateFromCurrentLocation_mF2401849DF83AA375D425B1636C0B4136657F400,
	GeoTool_ConvertGPS2Position_m0B0FC2C84414056738F0B2F8499380C909F15DB4,
	GeoTool_CalculateBearing_m37615AC51758138D68AF1A3FA9C2C78D53C43949,
	GeoTool_ToRadian_mAAC4CCA66C781E803E40F474721FA5599407F25B,
	GeoTool_ToDegree_m5FB03044EC34BECC6A8B571A3FBF2FD69ABF25F4,
	GeoTool__ctor_m3CF64CBF32393EA03E2E739B6BA0319CBDAA632E,
	LocationData_get_Longitude_m78A000D2F07C094788276A21D08C517EAC0D3DE6,
	LocationData_set_Longitude_mBBC9954DB21CC6D7AF75C87B52711D034C57B345,
	LocationData_get_Latitude_m1E7A92D8BD857837E6B585DBBAC9CD00C35C6FB5,
	LocationData_set_Latitude_m97E091743755090A98F015CCF7C71C6544D69A96,
	LocationData_CanGetLonLat_m6038FA4657A34550114680BA49B5C9DB8A7D03BC,
	LocationData_Start_m76150EE1CA69833A8B3BD32CCCFA57020036D295,
	LocationData_updateGPS_m2E71DF54BB1CBD87FA5ED4928D2C9641E7B7A489,
	LocationData__ctor_m22C64109BD56D29FEA436011A0FF250D5A6595BF,
	SwitchPointCloudVisualizationMode_get_toggleButton_m5FBFEDEBC9B371B76804637D7C8091E26C3C399F,
	SwitchPointCloudVisualizationMode_set_toggleButton_mD9FC8EA7EAEE800F60DAFBCCAF19636BE29ECE77,
	SwitchPointCloudVisualizationMode_get_log_m6E4EB81C73C81360C40431382CD6EB0AC2C59763,
	SwitchPointCloudVisualizationMode_set_log_m564736BD24430DB94CCF144DABD0B6036777832C,
	SwitchPointCloudVisualizationMode_get_mode_m20AA95EDFB2FFB095D258B2C38987A3BC1227616,
	SwitchPointCloudVisualizationMode_set_mode_m280F90EBF742145758E7D52D0C84CA5AC16F0075,
	SwitchPointCloudVisualizationMode_SwitchVisualizationMode_m7CA339DEB4B8A40DFDFB8C6D79FC535048BAC278,
	SwitchPointCloudVisualizationMode_OnEnable_m0AE60A40E7545E590E1ACDF4FFEA32912DC1C11B,
	SwitchPointCloudVisualizationMode_OnPointCloudsChanged_mDA9644178AA492F76221956ED387EF7BC7C5EE55,
	SwitchPointCloudVisualizationMode_SetMode_m4BC28FF39AF7600381B725286D7BCFBFE46F85AC,
	SwitchPointCloudVisualizationMode__ctor_m02EAEB06B7F42B232A6280B8E7872F6596BF35B0,
	RampAsset__ctor_m378D66B2BA42684D9071D8832D6DAC33ABA5E85A,
	AdjustTimeScale_Start_mD1396DCA527CACE33308FF8FC9C0277F2AA2B411,
	AdjustTimeScale_Update_mC708147D1D143ADCA9CF17252F3CF63ADFE489EA,
	AdjustTimeScale_OnApplicationQuit_m951AC9B09C81B79661AABC25DDCC2D7C7B5DB634,
	AdjustTimeScale__ctor_m37475124000B20EAD78D8880212E148C74FF0552,
	ProximityActivate_Start_m6A50614FF255868F3BA744E24F571DEE27BA4EDE,
	ProximityActivate_IsTargetNear_m4703CF6E0EAD767910D51F76C2D2C55C7A90B44F,
	ProximityActivate_Update_mE464C3E72F19E9855ACB76FE88B143E50F9C2BC5,
	ProximityActivate__ctor_m71A7C0718ECD404F98FB3B476C74DD835757E6B5,
	SimpleCharacterMotor_Awake_m16349F4AB176D983169E7D079DC8844E942C49CC,
	SimpleCharacterMotor_Update_m61B6F55DACBBFF2ECBCD5290E0EBAA926F213684,
	SimpleCharacterMotor_UpdateLookRotation_m0239C81730CE51AA90C40883BEC2591C7325CA76,
	SimpleCharacterMotor_UpdateTranslation_mC1E99B9C99D7CFE931538E9150A0634F0D596CF6,
	SimpleCharacterMotor__ctor_mB2A4A69A8CA6E0C101E36ADEEFB4617CBAE5801B,
	Readme__ctor_mF465410C5B2E598F2685E82CFCE1F42186AFF448,
	Section__ctor_mBAD5262A353BC071C61B8DB462A3D4D5AB5C7C4E,
	SimpleCameraController_OnEnable_m82F47D85DB724F0678DBEB9FF63D15C130E8F7CA,
	SimpleCameraController_GetInputTranslationDirection_mCD9E5C0B432565D425C5EDDED2BD2B6E7CA04D5F,
	SimpleCameraController_Update_mCA01CFEBB977AFFB6A180BAB2DE814E9C30247BB,
	SimpleCameraController__ctor_mF3F586030598332C5EA58D34699E418DA152B40A,
	CameraState_SetFromTransform_mC1D96C5F6D37762A9E1CF205089FB8504D42FEF1,
	CameraState_Translate_m5C6224BD5A0D3F038E2E4C85FBA25CCADD3CAFBA,
	CameraState_LerpTowards_mBD869D1D801FD704FE95A14557307216E3C8ACE6,
	CameraState_UpdateTransform_mFBBF4FF52A569D8C377EE45A8BB49668B897CDEB,
	CameraState__ctor_mE07091752EB54CE0EA1103B37E34FE0AAC97BA75,
	SimpleCameraController_OnEnable_m2BA1F31BEDE84695933E86CF731059D6FAC2111C,
	SimpleCameraController_GetInputTranslationDirection_m924AB4CEA66ADC4F1C63EAC21B660E0495765F4A,
	SimpleCameraController_Update_m0E587F33074BE72C54BA835B2217B0E8EC94F24A,
	SimpleCameraController__ctor_mC6AE2509DDF461856450EC7DE0058A1687AB5C87,
	CameraState_SetFromTransform_mAF13C515CFB1085295C01A870D93375E98F16647,
	CameraState_Translate_mB8F7239BD9DB70190E59D47D75DD125AD9AF3A96,
	CameraState_LerpTowards_mF2D4B962A677B281ED2F539A2FFF8A693FB9A326,
	CameraState_UpdateTransform_mE653356FD34828D19ECB6793439A14C38F372410,
	CameraState__ctor_m9C5338CABE70B8C73F8A4A08C1AFA1B33417DE9D,
	ARAllPointCloudPointsParticleVisualizer_get_mode_mE2961E81075235D3BD24C54D522050A6A16ADF78,
	ARAllPointCloudPointsParticleVisualizer_set_mode_m93C09216743B5544F7EDF9C47FCDD9CD4E6E47C9,
	ARAllPointCloudPointsParticleVisualizer_get_totalPointCount_m82C8E588FD5467B4879E96DE8C1D8699A9FFC178,
	ARAllPointCloudPointsParticleVisualizer_OnPointCloudChanged_mAC37187B41FAA79B6084C2AE0DBE329AAA84E01E,
	ARAllPointCloudPointsParticleVisualizer_SetParticlePosition_m296F1C3C6B0EA3C9D00B67776EB3A8E64B45B5AD,
	ARAllPointCloudPointsParticleVisualizer_RenderPoints_m9308B52D42102E948AF53BE9D349175BE43E18CB,
	ARAllPointCloudPointsParticleVisualizer_Awake_m2039867B6B044168F1A26C1F2EF6D0F4E3354D2D,
	ARAllPointCloudPointsParticleVisualizer_OnEnable_m1F8B642B5E8F23CADAEDC9317FA62A1BE65ACB04,
	ARAllPointCloudPointsParticleVisualizer_OnDisable_m482462736CDA9BAF6FF73CD40214E22B251FF77F,
	ARAllPointCloudPointsParticleVisualizer_Update_m03E104CCAE3B121C955F9C228E16EDD7758896D8,
	ARAllPointCloudPointsParticleVisualizer_UpdateVisibility_m4C296E8EC0365D27C9C0BA909417BFB3A70F3F52,
	ARAllPointCloudPointsParticleVisualizer_SetVisible_mF075BA639C161AC2F4B4457D76FBA7392664B233,
	ARAllPointCloudPointsParticleVisualizer__ctor_m21F6F93A91B2CDD0C2FE8B7BE9BD38B6213B4088,
	LogUtil_get_maxCount_m33F0A8C7EF9E563BF520CFC49E2EBD6CF10D6BC8,
	LogUtil_set_maxCount_mE6A3A0C8D138CA6B6A19CF288350BFC5D369544A,
	LogUtil_Log_mF7066D27EB01A727F3211BFFE5E90AC6E4FD059C,
	LogUtil_ToString_m40240CAE34215070EC176166F0224EEBED32C0EB,
	LogUtil__ctor_mB249EAB024A57DFB2B586ADDA3DA9E6ADC0B0EA1,
	ARCoreSessionRecorder_Awake_mC95DBD06789D376B0F07208043B4FEF622261AE0,
	ARCoreSessionRecorder_GetRotation_m31EEC60D9C8C9B6F5DA2B5D68BFEB771472EEEE1,
	ARCoreSessionRecorder_Log_m6EE0D73943DFC78EA266FF39FD1933908A0DD91E,
	ARCoreSessionRecorder_GetFileSize_mA5ABB9D6E9DB1C4756D7C5E9EB70D6180255CC4B,
	ARCoreSessionRecorder_OnGUI_mAC04B59369514266723AE8DD5FC03F4352DCCC12,
	ARCoreSessionRecorder__ctor_m8B85D2D10CB4C35B49C1D60AA14487AA7D3988CF,
	AnchorInfoManager_get_session_mAC00395D65559ED8101D67D6CE222A214566D78B,
	AnchorInfoManager_set_session_m9BB7E6023BCB8DB90737C3C648D170F88A68BA0A,
	AnchorInfoManager_OnEnable_mD5089C508DB2F1E3D09925B7B38D0A6D92326CAE,
	AnchorInfoManager_OnDisable_m31CA73A4A636067C504066970B5D788DB92E8DF0,
	AnchorInfoManager_OnAnchorsChanged_m12C2BF7C72249689C2133443CC8546D8E8A2F85C,
	AnchorInfoManager_UpdateAnchor_mE6350A484AA98FA4D54A575FD7D16F3CC3DAEB62,
	AnchorInfoManager__ctor_mD5EF181F62BFCEA9B0C9FC8DAA81762E22AF745D,
	CanvasTextManager_get_textElement_m98F4CA56E78F18E0CB17C63257B6E7EED8259C9E,
	CanvasTextManager_set_textElement_m06DD92E78365A312FBB0E149E47F251F7EA739E1,
	CanvasTextManager_get_text_m2062E9DEF643F69580D8C21EB9527F18FD6DEFA7,
	CanvasTextManager_set_text_m90560F6D945ADC53C553707EFE863405E9AE04B5,
	CanvasTextManager_OnEnable_m885DAF025B493B55F45C4661AF93436B6F49642C,
	CanvasTextManager__ctor_mB0F17BFDFBE6674CC915BDE2AE3E4571B8831C7C,
	CollaborationNetworkingIndicator_get_incomingDataImage_m942C06B8ADB20888373B15421E68144DAC0D4294,
	CollaborationNetworkingIndicator_set_incomingDataImage_m842DB89ED792D5FC7EEA6F043DD87195B2AFDEAD,
	CollaborationNetworkingIndicator_get_outgoingDataImage_m157F3CA2885E6469C4ED98B14A93F19BE0124E72,
	CollaborationNetworkingIndicator_set_outgoingDataImage_m222AB63E9A994D30CCD23885FDFC1DF0EFFFF1F2,
	CollaborationNetworkingIndicator_get_hasCollaborationDataImage_m492DDEA4332AF1BFCE9B4DCCA2D40B76817D2666,
	CollaborationNetworkingIndicator_set_hasCollaborationDataImage_m4C745566341F8C521AFFFF64DDD88CC2671B4871,
	CollaborationNetworkingIndicator_Update_mAD05EF0458D7F3ED4A4046F2E201317F6E6B33B0,
	CollaborationNetworkingIndicator_NotifyIncomingDataReceived_m77BADBABA4085D002A3AA28D992D7DFBF5A329C0,
	CollaborationNetworkingIndicator_NotifyOutgoingDataSent_mBC9D3784E222B83F373FE24AF2EF78411090078F,
	CollaborationNetworkingIndicator_NotifyHasCollaborationData_m63D7F2486BB6F32787FB6465AAE437CC2F99A188,
	CollaborationNetworkingIndicator__ctor_m647BD9C91E20E2B6D539FB776E689B42C26BE82B,
	CollaborativeSession_get_serviceType_mF84DA1EFB55084B2E3CAC5F77E4DB93FC78B17E7,
	CollaborativeSession_set_serviceType_m9775C370AC63988FB2517CFAEAA2AEB4D8257885,
	CollaborativeSession_DisableNotSupported_m3BC41057A7A69B9F833EA8F5459C2BADA62F5B86,
	CollaborativeSession_OnEnable_m67480DBEC60C38429FD55F5C1B4D258E7369521E,
	CollaborativeSession_GetSubsystem_m62F423DC23DAAA7F3C150DEB24F69FD4888CB771,
	CollaborativeSession_Awake_mB6E2CF54D1A3CC7F098D84F8FB4C083088287A88,
	CollaborativeSession_OnDisable_mD059673F02C82D6647D0D91A9ABDAAD61C0CAFE2,
	CollaborativeSession_Update_m496BA748AEAB4FDC538A34C725E0557D947B5175,
	CollaborativeSession_OnDestroy_m70B718104DF2538158E6621D83B47301E721D165,
	CollaborativeSession__ctor_m7AEE8EDB08E4AD2B0CF4A01D7CCBDADFE1953073,
	Colorizer_get_renderer_m2E8570623271238C7FF0B0461F9D6949A0B5CF97,
	Colorizer_set_renderer_m27B49F452058BDE0634E43B34B7D553F1D666200,
	Colorizer_get_color_mEE8933C6B7187BA468D6932F9A8901C2D55E1997,
	Colorizer_set_color_m7D34E1242EDCF575B74F8513BEDC7165C47661C3,
	Colorizer__ctor_m804D41AA438D72BC09ABB83C2F30D03153947630,
	DisplayTrackingState_get_text_mA66AC86FDB870AEF791116C7C3977FCA2BEB50CC,
	DisplayTrackingState_set_text_mF51BBEB1FE7400F31014BFF58D91C03959C29392,
	DisplayTrackingState_Start_mB0355E56569B21B02F68F1E68F623E2D5C9BAFD8,
	DisplayTrackingState_Update_mC426E9A68C4D297C68BCFDDCA87D6F8F76BDF7B3,
	DisplayTrackingState__ctor_mDBEDD0EEB9F3660BFC6514C85EE063DB6EDB46CA,
	ARKitCoachingOverlay_get_goal_m2C6FA956DF340A2C4B2185A6BD12C903BB66A7C5,
	ARKitCoachingOverlay_set_goal_m47E45877012A2DFAC383C9C9A1B03644D7B0AD01,
	ARKitCoachingOverlay_get_activatesAutomatically_mC0C9FDB4BE723C19A55BE8B4E6F80AA7A9460AE5,
	ARKitCoachingOverlay_set_activatesAutomatically_mEC8D8B30A1B30E8E0B4CFEA835CC9B8746B0B675,
	ARKitCoachingOverlay_get_supported_m52B7E0992610E6CD4CEA565BC952F23078B8B87E,
	ARKitCoachingOverlay_OnEnable_m02BF954ED169B30E098223A8EA083057CA80B7DF,
	ARKitCoachingOverlay_ActivateCoaching_m9E8AB9C2B5864679DB2CF8E2FF286E4493687A0B,
	ARKitCoachingOverlay_DisableCoaching_m2FA5EBF353B1497919DCCE3AECDFC6605CFE3990,
	ARKitCoachingOverlay__ctor_mA4F82369D31535E99798D8BB9C7869B00196F914,
	CustomSessionDelegate_OnCoachingOverlayViewWillActivate_m9D19F21C445FCBAB5F4F519344A483829297652C,
	CustomSessionDelegate_OnCoachingOverlayViewDidDeactivate_m589A5E8ABFEA7121E31B51B2C1FA556B4BC0E813,
	CustomSessionDelegate__ctor_m90330193C0F48FF70F11878743F7F11C4BF6FF0A,
	ARGeoAnchorConfigurationChooser_ChooseConfiguration_m79192D193AD56D7EB80879E2ABF16552CC77951D,
	ARGeoAnchorConfigurationChooser_get_ARGeoTrackingConfigurationClass_m420210848A1233F8B1DCE0790EC31AA5195EE852,
	ARGeoAnchorConfigurationChooser__ctor_m7986AF96B515ADAA712557715978F57F7893FE5E,
	ARGeoAnchorConfigurationChooser__cctor_m1AA0D4C69A3F6512094C2CD15C451450773306CE,
	EnableGeoAnchors_get_IsSupported_mD246E8D893BB4D80C1B2FF568F5C0C6F5022420C,
	EnableGeoAnchors_Start_m2C034CD73FDC07D5D2832536D9B917262E3065B0,
	EnableGeoAnchors_OnGUI_mD0BF971AA812D5798864D9B189F3B627D8710C1B,
	EnableGeoAnchors_Update_mB8118AA641BFE7E13449A1DB8E74289461527CF1,
	EnableGeoAnchors_DoSomethingWithSession_m71D6C728E936D39A91C4B423CF254EA525D35981,
	EnableGeoAnchors_AddGeoAnchor_mF01DD65F28D0F2CD638CEEC0847198C3F6794F5E,
	EnableGeoAnchors__ctor_mEC8F33255E4A714FCA782B3D8A3950A644C6A672,
	FeaturesReporting_get_autoFocusText_m4DB17DF766A36ADB9C36A27B8E6BD43AC48FAB6F,
	FeaturesReporting_set_autoFocusText_mA3488E9E5E19CE38B07C23FF014341BCE3B7B0F6,
	FeaturesReporting_get_environmentDepthText_m6E388C6A68EEF58399CEF4F36377AC6D39398ABA,
	FeaturesReporting_set_environmentDepthText_m40D52051439A0D77DD0BC2DDCAEF2929BFF5F2D4,
	FeaturesReporting_get_environmentProbesText_mEF1E4C02FC503935D8D1A581F924877E235D3DEF,
	FeaturesReporting_set_environmentProbesText_mEA7FAB36D89AF22C87A44C382179A7802E92141B,
	FeaturesReporting_get_faceTrackingText_mDA057BE918392E4EC7CB2AD27DED7479C5A733B8,
	FeaturesReporting_set_faceTrackingText_m45D932C2D869912DA652538A7DAB0BBA5E961A45,
	FeaturesReporting_get_humanDepthText_m934A7312EC1F07298E149D58CF12C382CEBDC765,
	FeaturesReporting_set_humanDepthText_m8718AB65C5EF4157CBE40D4FF81AC97176EEBC4B,
	FeaturesReporting_get_humanStencilText_m6A226E47D1B26EC9C9FCCDC890E52CB4DA96BA95,
	FeaturesReporting_set_humanStencilText_m462C444AF832BEA115EE30C79EB07C7F0D16B6C2,
	FeaturesReporting_get_lightEstimationColorText_m710DED0B274D6FE9A061D951819A41F810A338D0,
	FeaturesReporting_set_lightEstimationColorText_mEAE2E0FE2A135D25A2588271B03CEDF565B9ADD7,
	FeaturesReporting_get_lightEstimationIntensityText_m7FFF77A585791D3FF72A7EF08AC0643F73D8E13E,
	FeaturesReporting_set_lightEstimationIntensityText_mBC3A90538496C03899C9ACC51F1EC06278ACA64D,
	FeaturesReporting_get_meshingText_m6EF32DA1D93DBA0C671D6B44D326149DAEC7B00D,
	FeaturesReporting_set_meshingText_mF7D0F28BE9859D73CA1BCF900D7FD5BEB3A8597A,
	FeaturesReporting_get_meshingClassificationText_m06B683EA5DF9D3344704AFEA9FD449343EE58EF6,
	FeaturesReporting_set_meshingClassificationText_m98DA0A404C990264E25370C447D06828B37FDC77,
	FeaturesReporting_get_planeTrackingText_m8DBC17D99798CB2BB999FA1A32A253D1B3AB446D,
	FeaturesReporting_set_planeTrackingText_mBE255188ABDCCFD0AC3A0281B0AC1B758F2E56EE,
	FeaturesReporting_get_raycastingText_m5C5EED8E315A1742BDFAFCF7F1332975793F1FBF,
	FeaturesReporting_set_raycastingText_mE840DB465CEA3381AEF23C8AAA7FE71ECAE5C0EB,
	FeaturesReporting_get_rotationAndOrientationText_mED0576EB820393DA87E019FFD0D49A5E8EFE85C5,
	FeaturesReporting_set_rotationAndOrientationText_mE3E278D9EAF5AD2411A519D1B4CFD1924BC7803A,
	FeaturesReporting_get_worldFacingCameraText_m7D7CC909FF7FDF2D4ABB4701E995E02BEDE9241E,
	FeaturesReporting_set_worldFacingCameraText_mDCFA0473F21196D68C29017771F19956D7A09C32,
	FeaturesReporting_get_sessionFpsText_m1EF17B2A8AF961F812D167D791C7840F5A61349B,
	FeaturesReporting_set_sessionFpsText_m84702398751D0BF225A35F2E047806929DA96783,
	FeaturesReporting_Awake_mF10B49BB7BB4DC2092FAC9015CFF8185ECA12D93,
	FeaturesReporting_Update_m90D72FEA5DCD33CD35B54C1FBADC469C20D252B2,
	FeaturesReporting_SetFeatureDisplayState_m6FC99AEC79D0E054D6E411298573417EDA0DE08D,
	FeaturesReporting__ctor_mEA6B02A061DEA341C7CC9C61D41E1C1AB5C3CBB9,
	ThermalStateForIOS_get_currentThermalState_m23F11F76FCF55B5E2B3E5F50A293C581893BCA6E,
	ThermalStateForIOS_OnEnable_m0B57658E184FC211DF0F6E861547CE084D7183DA,
	ThermalStateForIOS_Update_mF80F0FB2E572A3DBF55329BC54CE0625B77C8CE9,
	ThermalStateForIOS__ctor_m5F87C1E362BE78CD6BC930E402A95B1929CD9948,
	ThermalStateChange_get_previousThermalState_mC8EA5EEB4C6D830DC54841A68E19E8D96D4C3B97,
	ThermalStateChange_get_currentThermalState_mEC7E8C8DC348064CE3DB492C6C4207656A7F6FB8,
	ThermalStateChange__ctor_m2F03DE32F10B12C5168435245D47607951A49515,
	NativeApi_GetCurrentThermalState_mC518B4EA3B6AE785F53F3CFC6404F10527A299DB,
	ThermalStateHandling_get_thermalStateForIOS_m56BF7D84309E859D824535215AAFE6B151386551,
	ThermalStateHandling_set_thermalStateForIOS_mB983A715C583F92F6BA193D025893F2BB788A4DC,
	ThermalStateHandling_get_thermalStateText_m464CA7C2E4D0FDD2E344C26C4A58D4AE74D85E36,
	ThermalStateHandling_set_thermalStateText_m9E28C4A3D86A539D653FDCB5E30F35BE4E933184,
	ThermalStateHandling_Awake_mD14B1CB3ED8AF2136ED6F6F7762C1E5CD1E19ACE,
	ThermalStateHandling_OnEnable_mDC00A21BBE48894823240530EF2B9D96D5CD2B00,
	ThermalStateHandling_OnDisable_m1BFDFF7F12BD46D7433CEA7BDBCF3C0E0C986131,
	ThermalStateHandling_OnThermalStateChanged_mDD0EDD3355DFA4467E5F9F7109B5B77A04F44440,
	ThermalStateHandling_SetARFeaturesBasedOnThermalState_m372B8E6523CA0021D189399F0A315003F78ED4A0,
	ThermalStateHandling_ToggleEnvironmentProbeFeature_mCBDC6915E364D11DC496CE651D4E94C71E66FAF0,
	ThermalStateHandling_ToggleFaceTrackingFeature_m1C52C1F4366D44A7C85D6F53FFE5E284E51DE516,
	ThermalStateHandling_ToggleMeshingFeature_m4056257217FB5815145D03B3FDF337D9FB33C70B,
	ThermalStateHandling_ToggleHumanSegmentationFeatures_m823844F891187CB253FB376F11D8AD4124FEB9AD,
	ThermalStateHandling_ToggleEnvironmentDepthFeature_mB531F0C1DEA420D6BC4D5F5244BB1AB87AD9F0D2,
	ThermalStateHandling_ToggleARSession_mE294009F786FAACA2D181242AF0F01407E0341DE,
	ThermalStateHandling__ctor_m52AD1B9D887C889CE9D7C70BFDFBAC85C81C809A,
	CameraGrain_get_cameraManager_mDD228AE643D232E853CABEC4EB66DD16000F5A8F,
	CameraGrain_set_cameraManager_mDB18F54E87C683B3FDF9AC8F11EC7481DD2FCC7C,
	CameraGrain_Start_m2F0CEAB415E3F0508ED2947FF86A818DF1B0180D,
	CameraGrain_OnDisable_m87E99A967FA0A355FBF2E07BEF3DA0854C182BAA,
	CameraGrain_OnReceivedFrame_mC96A6191A9E9C723A1F113331943A33547675B85,
	CameraGrain__ctor_mE559C38F99B691A3EF2ACB1D1FBFBF658EECEA95,
	DisplayFaceInfo_get_faceInfoText_m4A0D6014A92C826FF98335D3D9FE01EAD8C66AFB,
	DisplayFaceInfo_set_faceInfoText_m1FE7A372A0D41936BF03C54508CC34AA11E4B476,
	DisplayFaceInfo_get_instructionsText_mFCBF9F211FCB7D1B1A905A00D99CF99ADC40A32D,
	DisplayFaceInfo_set_instructionsText_m948D04ADABCF371C87F8B37089A151764FED6490,
	DisplayFaceInfo_get_notSupportedElement_m60DD41707ADDB6B395203CB01DAA8D704A9D3627,
	DisplayFaceInfo_set_notSupportedElement_mF0E8C6B4677700681A29461597648D2B5B7311DB,
	DisplayFaceInfo_get_faceControlledObject_mADE583779F21FE539C9441C9D636032FF2E79AF4,
	DisplayFaceInfo_set_faceControlledObject_mC148E577866F364C10685D5E5EBD4C62FBA096AD,
	DisplayFaceInfo_Awake_mA4FE652199142AF7EDDD83FDBF9184F329F2FD94,
	DisplayFaceInfo_OnEnable_m312273878D50E20F17F88E9B6B0DA2BAF53B92AB,
	DisplayFaceInfo_OnDisable_m7B672804CFBC3BCB6D5172D16C65ED18D1299F38,
	DisplayFaceInfo_OnBeforeRender_mF82FD8174EB5456DDB946A311304BC9AD782BE12,
	DisplayFaceInfo_Update_m618E108C3B432528CD43E7CBE31BC309DB98EC18,
	DisplayFaceInfo__ctor_m91106693C325EC3296D0E8D7D3642A7B9D8CD915,
	ToggleCameraFacingDirection_get_cameraManager_mBFC4F1605D41D22AD463C0C440FFB72524B75372,
	ToggleCameraFacingDirection_set_cameraManager_m9F2D133BF1F8DF5D066FD98AF3006DFBFA0DF902,
	ToggleCameraFacingDirection_get_session_mEEE6D7D90DE67570FE4164F022499035CBD50C21,
	ToggleCameraFacingDirection_set_session_m0C899E75182B8FFB8238C70441461D42E787DA2C,
	ToggleCameraFacingDirection_Update_m6B83C93575822D6198046195B528824882202D61,
	ToggleCameraFacingDirection__ctor_m47B36115BD93E9A1493E75906548D12F87EB0007,
	TrackedImageInfoManager_get_worldSpaceCanvasCamera_m2009EB2480A1FF118343F604EFE5AC5A9D1EAF98,
	TrackedImageInfoManager_set_worldSpaceCanvasCamera_m425A097A896E7ACFB54A58818F8A9E93012F00D8,
	TrackedImageInfoManager_get_defaultTexture_m6A0D413300AF5BAC5E00557530B03CF16CEE7EBF,
	TrackedImageInfoManager_set_defaultTexture_m9FE88BD89D3C1A0E724B56E3DD57598EACBCD647,
	TrackedImageInfoManager_Awake_m5CF4E3E359D5EE51D9CAC08F04ED16EA949BD609,
	TrackedImageInfoManager_OnEnable_m2F61DD659F81B38E8DA09622DEA0C0B851E35948,
	TrackedImageInfoManager_OnDisable_m25E3579157C28DC1ABA8A19CAF5572D96587E700,
	TrackedImageInfoManager_UpdateInfo_m5B077255A0CF8EB647C37D9C27862B550BA45045,
	TrackedImageInfoManager_OnTrackedImagesChanged_m7D340AF3DC1C788A581C43A4AA47C4D3286D0C01,
	TrackedImageInfoManager__ctor_mF3CC8344AE3000C2C0BD654B6853371F86B0972C,
	DynamicLibrary_get_images_m2FC396F22C83BBD54343040144A935ED3FBF547F,
	DynamicLibrary_set_images_m6C44825F8CBD0AB08CB033A3220B0937C8AAF035,
	DynamicLibrary_OnGUI_mEB7711C3A3BD11B1F4B5D168152A051C75E12506,
	DynamicLibrary_SetError_m3C91911DB9F4FF8EAFABD2C9821C936738426973,
	DynamicLibrary_Update_mD093DD71352B9198CEAD96F5E8D9F43EFC15AC78,
	DynamicLibrary__ctor_mD51A617C9BE65684051DF13361876A44AA57FD4D,
	ImageData_get_texture_m808D389F1C640BDDF41FA7BA62D33D7CBF688DDE,
	ImageData_set_texture_mE9BFF48AB9EF7DC01ABEB4DCACF0E2352C9E506B,
	ImageData_get_name_mEE3EE88AC17A5486CA757AB8551B54CE7991DB09,
	ImageData_set_name_m6DF1F29081F51AC73809C880EA6C1FCFDCFA7F9C,
	ImageData_get_width_mDA30009E63B828F4D8617FD37DC591989A5AD4C1,
	ImageData_set_width_mC1E5E51A65F3D52405F1FBE4246602413A0740E0,
	ImageData_get_jobState_mFA144AA269EBBE825C57B43F8F4850CB37D4F677,
	ImageData_set_jobState_m9EC3407D1C8FAFECC0A2F25EB924297F7C901A73,
	ImageData__ctor_mAD7C5F0696DAAFE916D1B5BBF9EE7A771972B789,
	DynamicPrefab_get_alternativePrefab_m4B48EB6B3CFE5493EDF32163F0A185A3E0B284F6,
	DynamicPrefab_set_alternativePrefab_mB496995B03DBC0C9F41FE31C6E09E6D711311204,
	DynamicPrefab_OnGUI_m7B42974745B7C610AC8BEE42B00461EB4F36FEC9,
	DynamicPrefab_SetError_mC4122FB4973F14E169E99825FA9A01DFA9A6B061,
	DynamicPrefab_Update_m34D182813AFDCF28D8C664D835F61912273079CF,
	DynamicPrefab__ctor_m7B0ABDA21E114912AB522C7DBB5413668AE9D425,
	PrefabImagePairManager_get_imageLibrary_mF75838258C00F909150CB75F5989903E7AF21286,
	PrefabImagePairManager_set_imageLibrary_m5598C2AC4459C6D5135CB5AC153B803874958BC9,
	PrefabImagePairManager_OnBeforeSerialize_m5B178C81BC1C1982D01E7CABB3DA6C8152F5F451,
	PrefabImagePairManager_OnAfterDeserialize_m10CAF6488BD05E15650CCAC2D8DDFDBEBE4C6D53,
	PrefabImagePairManager_Awake_m5D1BADF276ACEBE409796C5783389B4DDBC6A076,
	PrefabImagePairManager_OnEnable_mA5D8272C6A44CC8ED3E26144CC2424F0B3F48133,
	PrefabImagePairManager_OnDisable_mC339AF0AAEBDB53FEC7E643E1A69D9560A23890C,
	PrefabImagePairManager_OnTrackedImagesChanged_m8950068F3FE83FBE39C0D38F1EA06FA58F82DF5E,
	PrefabImagePairManager_AssignPrefab_m3A612DEED64B82D56C3FAACF8BF4E46A7729BA72,
	PrefabImagePairManager_GetPrefabForReferenceImage_m37CE15147B141D8ECAF155CDD7BBEAF86C109364,
	PrefabImagePairManager_SetPrefabForReferenceImage_m660F9185F078D8492942F596D6596986DCF68DC8,
	PrefabImagePairManager__ctor_m706F23D05C16A9911D7637808AE62800AFFE6760,
	NamedPrefab__ctor_m4C5A1EAC954851954E10ADF5F4ED646967BAB252,
	InputSystem_PlaceOnPlane_get_placedPrefab_m1294EBD3BD1D9BC33BC68AAF0E3B1C9563D17305,
	InputSystem_PlaceOnPlane_set_placedPrefab_mF3CFC1B744337A313EC32DD14754BA086AA47F0F,
	InputSystem_PlaceOnPlane_get_spawnedObject_m0AC5BD84B735508ED94FFE77CA3F83F510C07434,
	InputSystem_PlaceOnPlane_set_spawnedObject_m44AFB1A6AE9D71955BF4EE29DE4FE4D9AF757E98,
	InputSystem_PlaceOnPlane_Awake_mAECA5D93BDE511AB0D0FE41F829888DB0BF6B7C3,
	InputSystem_PlaceOnPlane_AddObject_m622FA4442A14D844FE4BB5C7C3C881D41D5ECA64,
	InputSystem_PlaceOnPlane__ctor_mDCD75DE0C26E8F3848762A19AD7A90F2C3FCEC46,
	InputSystem_PlaceOnPlane__cctor_mE9C17502BA947C08D1F3C307F0689AC2EBD40AAC,
	FacingDirectionManager_get_worldSpaceObject_m5A8EF128F606CA0B9A5DDEAFE1CEEDC8ADE73195,
	FacingDirectionManager_set_worldSpaceObject_mD7EB5526AC76B928B920368BAF7B0C0359AFC9ED,
	FacingDirectionManager_OnEnable_mCB4CA374C5FF2EA7E77C941D31A875D064923FE7,
	FacingDirectionManager_Update_mE07AC3E3A7F7D8359AD22A3272F1C0616357A84A,
	FacingDirectionManager_OnDisable_mE41E3232C5B4F735D43C861067AADFC3DD637B6B,
	FacingDirectionManager_OnBeforeRender_mE9B6589A255D251879736DB577EFCF56FE883418,
	FacingDirectionManager__ctor_m8E25BA21D4A091FA374E1B8971585ED06C4691D1,
	Rotator_Update_m4CC324BFC9A5FEA628BD67053EED1DC1BF2D5E14,
	Rotator__ctor_m5919872FB104ECD80FB34A18D4B8C16169B9523C,
	MeshClassificationFracking_Awake_mD86D06C56B8667774DA34D843F2C0E0A7A89BDA4,
	MeshClassificationFracking_OnEnable_m4B836A9A43C928520870E4F33730233CCD4D3B77,
	MeshClassificationFracking_OnDisable_mC71ECB3C6A8915C35BD05E5B21FDCFCE7236AAAA,
	MeshClassificationFracking_OnMeshesChanged_m0B02E7A7108C195CF1479FC64AF7FA0994484A5A,
	MeshClassificationFracking_ExtractTrackableId_m4007E88EC94D38E3E16626F1CC4AC074AB78D62D,
	MeshClassificationFracking_ExtractClassifiedMesh_mCEB94D57DEAF921272F7E66547622DF3773D5166,
	MeshClassificationFracking_BreakupMesh_mD722493A6C5C55C1135823F0405163D0AD3B5DBB,
	MeshClassificationFracking_UpdateMesh_m34745767B9BA1F79C3F98153E02A7EC8C8D286A9,
	MeshClassificationFracking_RemoveMesh_m27A3A7197CCC6ABBD72A31F6A09ED2BB030E4E03,
	MeshClassificationFracking__ctor_mA95E462A2CFBBF9E0AFD5CFDB3A57884C445459B,
	ProjectileLauncher_get_projectilePrefab_m4F05A1B02168D3215721555025FF4A4AEC06F6A8,
	ProjectileLauncher_set_projectilePrefab_m81569C053340C089D73241BDD1841989F52C55CE,
	ProjectileLauncher_get_initialSpeed_mCE0943F2C6E14E623A450DFE1A6D719A371000FD,
	ProjectileLauncher_set_initialSpeed_m6830FD039243F187A00399F6F88BEC0F75804453,
	ProjectileLauncher_Update_mFE282128D30F7581A4C8E6EFEBCDB55F43FC3516,
	ProjectileLauncher__ctor_m4B3DF202914A639E1F642243322722281046F85C,
	ToggleMeshClassification_get_meshManager_mBDA4125E3DFB9D601810935F3CC90E9171C9A18D,
	ToggleMeshClassification_set_meshManager_m7283B677E39AD9CDD22815514D9D17216B7EDE68,
	ToggleMeshClassification_get_classificationEnabled_mF4C6A5C1FABE3522D314EAF8607DF73B5886D5E9,
	ToggleMeshClassification_set_classificationEnabled_mF2725278262931FB74542064BE14F5D9DC45FBA2,
	ToggleMeshClassification_OnEnable_mBABEDFE0342AA7E7827329B7C69A5DC1A5A76357,
	ToggleMeshClassification_UpdateMeshSubsystem_m8370947D0DA98CBB6493161CF656B832121D7F0B,
	ToggleMeshClassification__ctor_m32F40C59A6530B5CC8A3751AFBC47F3257FEEFDD,
	PlaneClassificationLabeler_Awake_mA8E8CE7C74787E4C897E8A8C392A7E60B3D0E447,
	PlaneClassificationLabeler_Update_m4E8FC7D0E31D2BF7BBD8AE664B04111D62338883,
	PlaneClassificationLabeler_UpdateLabel_m9704258C3912DB6B8383AACF7D8155F7353D8A09,
	PlaneClassificationLabeler_UpdatePlaneColor_m337F617E6EED7737A9970797ABECFE6ADEEFF9F4,
	PlaneClassificationLabeler_OnDestroy_mBAFB13C3696BE286FAFC07A32F0322E435D238E2,
	PlaneClassificationLabeler__ctor_m863EB2A0ED6190ACFC36EC5B1159C9E409B9FC94,
	ARCoreFaceRegionManager_get_regionPrefab_m7884407D2CC7FC144F9B11CB140FA6416F03B5A5,
	ARCoreFaceRegionManager_set_regionPrefab_mA42D42267CE88E735EFDCB80316EF95EF3CF083E,
	ARCoreFaceRegionManager_Start_mDB7040A56177E80A0B0A73D988A21EDF81FE5A9E,
	ARCoreFaceRegionManager_Update_m0CBA5BD373C03DB42FCC6BBDD299786E42C8B79C,
	ARCoreFaceRegionManager_OnDestroy_mE4EBCB2E365AD7C91056D218C2BF65CE3701E6B2,
	ARCoreFaceRegionManager__ctor_m7201895468F37B815262B66BEC6EB6E56E066B2B,
	ARFeatheredPlaneMeshVisualizer_get_featheringWidth_mEC5F0FFB6CC81194431BA3A90EDFA779EC2C53E9,
	ARFeatheredPlaneMeshVisualizer_set_featheringWidth_mBCC1C2C9FC3E4A5499438F0E558C8CA8D38B2749,
	ARFeatheredPlaneMeshVisualizer_Awake_mACD2BF5B879D5F0B815711EDE6233060192B45F9,
	ARFeatheredPlaneMeshVisualizer_OnEnable_m1398A828B101DF022DD23DCB3EC52F1501DE49F4,
	ARFeatheredPlaneMeshVisualizer_OnDisable_m26C8AF9A5BFCBB372D8B2B7495182D1092A1FE63,
	ARFeatheredPlaneMeshVisualizer_ARPlane_boundaryUpdated_m2673DCA745FD934E5A2B961B4FAAC239A505B046,
	ARFeatheredPlaneMeshVisualizer_GenerateBoundaryUVs_mE1328246098580D09EC14266F0DB8A4AA1B1980F,
	ARFeatheredPlaneMeshVisualizer__ctor_mF21C6CBF25B44AD20AC601D9447BE849B9DAB207,
	ARFeatheredPlaneMeshVisualizer__cctor_m47BC2F741A3AC0DE3272F7E0291955102CBA9AA7,
	ARWorldMapController_get_arSession_m69026D4C29EF8DB2CD86257E251F84542725F52A,
	ARWorldMapController_set_arSession_mFFCCA36CC053F88636141338B7711D09BCF0F21F,
	ARWorldMapController_get_errorText_m4DADD78C2F6CBC20688B750C9DC1F23F560E75A4,
	ARWorldMapController_set_errorText_m6810095C64B844B2A1A0A79BB7762A1B98047DBA,
	ARWorldMapController_get_logText_m443EDE6108DC259B188BC877D8D1276D4C4C210E,
	ARWorldMapController_set_logText_m417B4D06634A39F0EBC4F4C6E7E6195D68DC69A6,
	ARWorldMapController_get_mappingStatusText_mB66A598071A4D7174A470D3290AFF7E58840256D,
	ARWorldMapController_set_mappingStatusText_m20F9526980AC71CE836F56BBBAE0FF73B11FEC51,
	ARWorldMapController_get_saveButton_m41771C16AC02935EE89284ED6AB38C681C30864D,
	ARWorldMapController_set_saveButton_mFDE9A27E0D719BEC85E2FC1FEBAA5A546B3DF28C,
	ARWorldMapController_get_loadButton_mCAC816CAB2264E326A28AEBC6A500F825E11A9FB,
	ARWorldMapController_set_loadButton_m425700EBEA7DCF8AFFF8F385A3837EE4F3F1667C,
	ARWorldMapController_OnSaveButton_m144B637F1D2B068DB4D9EDBBEB08856254742990,
	ARWorldMapController_OnLoadButton_m85CD45BC18EF99CA1ED15496C06A305F8367847C,
	ARWorldMapController_OnResetButton_m649547DFBAF7ED1A2709370AF9D6EBAED498323E,
	ARWorldMapController_OnDisable_mC991427317B4ED97FE6CF2452A9CE5770E30E959,
	ARWorldMapController_OnEnable_m65C4AA9EF65E1A01518DC7BB1C97136F375D7F53,
	ARWorldMapController_Save_mEBC0CB6E12015C5F906CC072AC3241FF35E953DB,
	ARWorldMapController_Load_mE20F01573B21065DFC8C79EA84882FFCC0971D56,
	ARWorldMapController_SaveAndDisposeWorldMap_mE23C4AC42725CE7B1808BF13CDD8F17D0E58FFB3,
	ARWorldMapController_get_path_mBE58B6C7DBC04BC95F797B1E19AB816937A954C9,
	ARWorldMapController_get_supported_mCEB0F6C290AA5CB340952F48FAB0DFB41F102FEF,
	ARWorldMapController_Awake_m83374F221A1CE2A456B67575A38F44EB8946AEA8,
	ARWorldMapController_Log_m34249A60506298E07471366AACD5A3A152BEB590,
	ARWorldMapController_SetActive_mCF214EBBE83A213CCC9CB107EF410FAA5F82A7A4,
	ARWorldMapController_SetActive_mB774A9DB91E3F5F2D1640A829199E909EFC2990B,
	ARWorldMapController_SetText_m98C4EA68C5C0CDD840AC4586441AD140DE43591A,
	ARWorldMapController_Update_mE97C929A2D053A6008711CA0443BB90B94D5EA08,
	ARWorldMapController__ctor_mCBB740A0A2ECA65CCD3A2A2E4A737B180730AF5D,
	U3CSaveU3Ed__29__ctor_mDCE4891B2A5738FE78C7E83CEE5D565502AC9BEB,
	U3CSaveU3Ed__29_System_IDisposable_Dispose_m8FC5CF2CF627629A7F9A08B461B0995012D4EA3C,
	U3CSaveU3Ed__29_MoveNext_mF72BB112DBC436A2CCB551E53711C854117B16F2,
	U3CSaveU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m49BCAC304972E4E88E45CF4FD4466ECE9DC9A77F,
	U3CSaveU3Ed__29_System_Collections_IEnumerator_Reset_mB725B98E0DC2804591774BE458964E3316B09702,
	U3CSaveU3Ed__29_System_Collections_IEnumerator_get_Current_mEEF293954D0CA73EBA6E2A8EA21ABD2A904426A1,
	U3CLoadU3Ed__30__ctor_m7D5110F97548BAE0C00CF1B7B394C4C11270E81F,
	U3CLoadU3Ed__30_System_IDisposable_Dispose_m7F12A811CEBA553460C3971DD27A5B0C7A56EB47,
	U3CLoadU3Ed__30_MoveNext_m49BFE332AD9ED0AA590226B073EA5B783C3BAEA9,
	U3CLoadU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAB3EBB68B37A4E190E7BF896DD1C54C8B0748E54,
	U3CLoadU3Ed__30_System_Collections_IEnumerator_Reset_mFB86B0FB985083E6A0E6187D072CCB0674199ED5,
	U3CLoadU3Ed__30_System_Collections_IEnumerator_get_Current_mB48DD21F3BBDE4B899BE248FE6C979562F4B5FCE,
	AnchorCreator_get_prefab_m912AB3AC42E54CE22DB6D9B08E0371C16731EF6D,
	AnchorCreator_set_prefab_m1F14137F2AA4F3FE1D1887BCCEC706092B8B951A,
	AnchorCreator_RemoveAllAnchors_m703F0AF2F4BB316073A495CFE6C32096FD706158,
	AnchorCreator_Awake_mBADB4F2A6680A1958EE0100C9421FDFE09939583,
	AnchorCreator_SetAnchorText_mE41668A55201B063262EBD80B6B955DDAF08F82E,
	AnchorCreator_CreateAnchor_m9B70ECAACA6B5F5083011C5AF84390DF5B7E9A61,
	AnchorCreator_Update_m548D84FC0481BB0E5D1C6A12E79668F69C004770,
	AnchorCreator__ctor_m7B451467D22D68EC8A0C054F05E6C830C385D299,
	AnchorCreator__cctor_m4D61D040B4AA4609EF0B31991EF7D95B9467B9E9,
	BasicLightEstimation_get_cameraManager_m2FF46C0DCBA1AAA282A75819242E86E461994A63,
	BasicLightEstimation_set_cameraManager_m9834DEFCF429BC776121EC00C175CC5FC2D83511,
	BasicLightEstimation_get_brightness_m47023A76E5A5C0F4031EEFEFDF16992EE6C907ED,
	BasicLightEstimation_set_brightness_m69FE007316403054C8769132F9D9677F2BBCDB85,
	BasicLightEstimation_get_colorTemperature_mE1DF5ACF679FFA019ECF36BCC8CEE1ECAF2D627F,
	BasicLightEstimation_set_colorTemperature_mCCA65C52707512E9A69A4436AE21829E9E557690,
	BasicLightEstimation_get_colorCorrection_m4A7BE7EE9AA132D8BAE721F32AE1F7988D4BFB67,
	BasicLightEstimation_set_colorCorrection_mCA839E2B4EDFCABB3603E399B0266AC734EE03E1,
	BasicLightEstimation_Awake_m1082ECB1B7B816098B1CF6E623577B6E4BD97667,
	BasicLightEstimation_OnEnable_m7420DDF8C4C19430384B021B6CAA709B9669D773,
	BasicLightEstimation_OnDisable_m3F5488EC84F1541AA24CD6D493285B24591C310E,
	BasicLightEstimation_FrameChanged_m5921A950A4A78F1C7B010A1C59D41F5CB3D7E6DF,
	BasicLightEstimation__ctor_m4DB7B5CDE38B20A9A78C3FF153387217F055D174,
	BasicLightEstimationUI_get_ambientIntensityText_mF5DD9EC437B8EEA9CC3D88112C3CB709CA415758,
	BasicLightEstimationUI_set_ambientIntensityText_m73EBAF44DE08F1EF99681575453A6EAE4051BFC3,
	BasicLightEstimationUI_get_ambientColorText_m78683D6B9E19CF20CC0CA57EBBB86626DAAE69E2,
	BasicLightEstimationUI_set_ambientColorText_m65ED5BE1033D384547B5889897B586F990FD93C1,
	BasicLightEstimationUI_Awake_mD8481D54BD2FD529DBA49FAAAEA8484B72E54D26,
	BasicLightEstimationUI_Update_m7C6A4CB5E40734089F881B261E0BF449624576A0,
	NULL,
	BasicLightEstimationUI__ctor_m562829C03EC74BFA75B9510FEF1136DB91E0DDC3,
	BoneController_get_skeletonRoot_m697B5CE6F22C45E1B14D11D825CC1752246593C8,
	BoneController_set_skeletonRoot_m55AB83224C341EB9A485028D6B36DA0BAB87AA00,
	BoneController_InitializeSkeletonJoints_m7D3A632559B7682732BC315D41D5773075835908,
	BoneController_ApplyBodyPose_m56466BFEAEB2AAE2690B972D1D1183282B702B2A,
	BoneController_ProcessJoint_m30129C62C2813FDCA741CB988F479972B2B68FF0,
	BoneController_GetJointIndex_mCC196D3A8A3842E5BA8F9182A50F3A809F58DD9C,
	BoneController__ctor_m3F59B437317CD5417C12D28337377FC2CC39566B,
	CameraConfigController_get_cameraManager_m393112C1D6D75EC49E1F208B723ED8D9C4E36945,
	CameraConfigController_set_cameraManager_m20E7984E7798F2001B23AA53056C52E0C01BA5E9,
	CameraConfigController_OnDropdownValueChanged_m0D6E37DB8DB0B9D1980842ED425FE70522810A19,
	CameraConfigController_Awake_m0FC893876C79EE026661CA139E15D56E90E5B62B,
	CameraConfigController_PopulateDropdown_m63B6FD3782AC6F12727DCEF7FBF613978A4779F9,
	CameraConfigController_Update_m5B1792D6E2A38AB7B0481AE4774A389629128CEA,
	CameraConfigController__ctor_m8B0D7D7C40BF77774E6D98A531833A4487149AF1,
	CameraConfigController_U3CAwakeU3Eb__7_0_m7485FB75401600BD48EE6B0D723A604769A2FC84,
	CameraSwapper_get_cameraManager_m45680899758DB6717619A287F5C5F3255AE125A0,
	CameraSwapper_set_cameraManager_m697FF15D67E329D79298042416E37B5327F45D63,
	CameraSwapper_OnSwapCameraButtonPress_mDF7ACDCCDEC3594633234E496908DDF0732E2EF3,
	CameraSwapper__ctor_m5CAE77BEAF6CA736958740E7816CB34B7A27013A,
	CpuImageSample_get_cameraManager_m15DC9F90C267E95F1D327EED1C62AE9A32E5BF24,
	CpuImageSample_set_cameraManager_m94232E4E08912AD4D6BAB3093859E0C3CE217EBC,
	CpuImageSample_get_rawCameraImage_m6F2D78AFF0CE51931A6CD679FDCCEE3CBF066CDB,
	CpuImageSample_set_rawCameraImage_mF8CD35C518B2D27381ACFB40994C8BFA170611FB,
	CpuImageSample_get_occlusionManager_m860B08FBDF6109D1EBB6536551BB78607F601C8A,
	CpuImageSample_set_occlusionManager_mDE4BC9B126AA713084D2EED93DADC29128ECE236,
	CpuImageSample_get_rawHumanDepthImage_m9E8150358C97F4832AB296EEDCAFE1EF2226E8C9,
	CpuImageSample_set_rawHumanDepthImage_mB58AD845F964B70DF7D2D890F4EACC8067F9AC48,
	CpuImageSample_get_rawHumanStencilImage_m63A967551A83F182E6EE08A22A5F78A1005B2DFF,
	CpuImageSample_set_rawHumanStencilImage_m04AE7D841E9BB7D25563B5CA53945CA8BDC8B954,
	CpuImageSample_get_rawEnvironmentDepthImage_m857278B6770C5DF1282A28CB85DA0C1188BD456B,
	CpuImageSample_set_rawEnvironmentDepthImage_m45A4FC88B175348475BE5DD04980153223C047BF,
	CpuImageSample_get_rawEnvironmentDepthConfidenceImage_m7991CE32992B2F0A9A7B6B80578E97018F7AEAEE,
	CpuImageSample_set_rawEnvironmentDepthConfidenceImage_mCAE5C3BE528A1B8F889765604392045EEF06C4EE,
	CpuImageSample_get_imageInfo_m25E380710AB1DE2E0B31AF3F2256D8D5948D67F1,
	CpuImageSample_set_imageInfo_m33E658759CE129958F3E75E46D0D22AC1D348692,
	CpuImageSample_get_transformationButton_m577D64F70296935B6638D4235F1CE5F0A8CD4236,
	CpuImageSample_set_transformationButton_mAEA3EBBA554CA0DEB95094FC072E902977EE084F,
	CpuImageSample_CycleTransformation_m93E95F9CCD059F1BE0EBB4B10F57710B409B9715,
	CpuImageSample_OnEnable_m2105BFDB78E13309B81CEA650FDEC727BAAB012A,
	CpuImageSample_OnDisable_m2FA10B8069C0A3FA86561792AC18251575156F81,
	CpuImageSample_UpdateCameraImage_m3F11902F25F15184BD3A72115CA69E1797968850,
	CpuImageSample_UpdateHumanDepthImage_m58BBCBE881DBF069845FA097C0F73E4786078771,
	CpuImageSample_UpdateHumanStencilImage_mA9F27AAAA22113EA3230435EECC21023D66A6D08,
	CpuImageSample_UpdateEnvironmentDepthImage_mBDFBE3464DCA551FEA4D76683B61F88562C27CEB,
	CpuImageSample_UpdateEnvironmentDepthConfidenceImage_m11D88113BC0DEE8B92E62D2941174827B45C8827,
	CpuImageSample_UpdateRawImage_m0AEEF002454523157ADF5DD753A985C6AE4D2271,
	CpuImageSample_OnCameraFrameReceived_mEEF1868F0CA5A61920465C9990681C8842708DBF,
	CpuImageSample__ctor_m1CFB846E16C673B6723C106A6E4D35C597D3CD1F,
	CustomConfigurationChooser_get_session_m02CF779466E363657FF1F81FC83663BD161B9ED6,
	CustomConfigurationChooser_set_session_m845D42CB95D87B00DE0EED482E0C919D6FEE44EA,
	CustomConfigurationChooser_Start_m6261E4058AD28C3627E444A43E24CD7CDB3CEC4E,
	CustomConfigurationChooser_OnDepthModeDropdownValueChanged_m59AF7B6746B2145E512AE8DE3AB3287444E208EF,
	CustomConfigurationChooser_UpdateConfigurationChooser_m12CE92F7124DABFC5081F5C60F7AC2B2A7957315,
	CustomConfigurationChooser__ctor_mC49F3D4437FDCE4C96F4513804B55E7D1D2EF399,
	CustomConfigurationChooser__cctor_m9AE5CBBCC16D5DF81DE428E2D7A83E443064E4B5,
	PreferCameraConfigurationChooser_ChooseConfiguration_m78AF1466A38DC288F00EA299D86DA2BA8135BE65,
	PreferCameraConfigurationChooser__ctor_mFF508587CD4779A9B98A50CE1498859BE702DE92,
	DisableVerticalPlanes_get_logText_m4FC2821AB03D129738B7D97ABB66A8D522829ABB,
	DisableVerticalPlanes_set_logText_m2CF8BAF19AEEDEE9966FD2D11204EA217DEFA6C5,
	DisableVerticalPlanes_OnEnable_m6025E522FF3272391926FB389BDC44B8AAD91E23,
	DisableVerticalPlanes_OnDisable_m6B0DF33C6DCF0B12EEBB4C23B6DCF44F4244CFA7,
	DisableVerticalPlanes_OnPlaneAdded_m340CA415E7B4742A92E480107555C24570BA8E32,
	DisableVerticalPlanes_DisableIfVertical_mD91BD30FE87A833AF7E3E25078A24632BD4DBD49,
	DisableVerticalPlanes__ctor_mDF58BA984AAB54DB4C7619A90A777251CC202352,
	DisplayARSessionInformation_get_infoText_m288E28D2E54F488632E836623E845C70B480F9E8,
	DisplayARSessionInformation_set_infoText_mB7DD855E260347EC58B7AED5ABEA1ED3C09B88DC,
	DisplayARSessionInformation_get_cameraManager_mB0F37919CC75A6E945F99FF0039CE42032EAF478,
	DisplayARSessionInformation_set_cameraManager_mE5BAEF3345E95178447A5C5D20166D2E5FF130B8,
	DisplayARSessionInformation_get_planeManager_mB993D1332A5C7B1ED6098DB3E42E889D3FA1F2D1,
	DisplayARSessionInformation_set_planeManager_m59A893B36F0B903AC74B0964506A54001EB7CE7A,
	DisplayARSessionInformation_get_occlusionManager_mA4612967D9F128FA04223D9B15AE24D2A645DD43,
	DisplayARSessionInformation_set_occlusionManager_m6ED77883F5AB357BEF8C7432663B5F226B4ED3A1,
	DisplayARSessionInformation_Update_m22C72883BEABD571D84995070F9A8D85CCAC2353,
	DisplayARSessionInformation_BuildCameraMangerInfo_m373CF5EDA0E015E745E1F54166C347275E46DA79,
	DisplayARSessionInformation_BuildPlaneMangerInfo_m03F66BE39C78397C3E5D23E65F726D9FD58DCC6B,
	DisplayARSessionInformation_BuildOcclusionMangerInfo_m9EAD0DC2AA7F61824A9B12C156B9F8A7EC10D73C,
	DisplayARSessionInformation_LogText_mC2717993968CC3B668B0B3737B95FE24A1CD62AF,
	DisplayARSessionInformation__ctor_m5BF2029E9983E6492786D7215BF1C08F10EE0905,
	DisplayDepthImage_get_occlusionManager_m4B6F625E54247ECD7DE19B9B89B358CB8AF6A64D,
	DisplayDepthImage_set_occlusionManager_m4F84AA719DF0589955056F0561FE7945A1476078,
	DisplayDepthImage_get_cameraManager_mA930D2941683660701D8F8C7E59833660C5E9D3F,
	DisplayDepthImage_set_cameraManager_m89283B3ABCC6B111018CF4CA754E655736CB70F1,
	DisplayDepthImage_get_rawImage_m555BA4023EE84A74F3E48317908D29C1E7C89E01,
	DisplayDepthImage_set_rawImage_mF1EA809CB7FDD6DCFAF7B8294DC55A2A97D0C83D,
	DisplayDepthImage_get_imageInfo_m86E8F1F4E9A868043AC9D7331A11406C58082F55,
	DisplayDepthImage_set_imageInfo_mC979D02106E58FAEA201DB0EDC3BD487B675A4AF,
	DisplayDepthImage_get_depthMaterial_mDE033B005D94CE31A28C2E282C24A75C0A972D3F,
	DisplayDepthImage_set_depthMaterial_m49F8B096FC7F05F32265306103BC29C24323E128,
	DisplayDepthImage_get_stencilMaterial_m7D87A8A4341CB44DEBF57D525EE76249D8A9ACCC,
	DisplayDepthImage_set_stencilMaterial_m945286FDB49120653054DD26DD7CC39FB4F76D7F,
	DisplayDepthImage_get_maxEnvironmentDistance_m6E539E7BD828F8F5676AA560D9AA019866D738A0,
	DisplayDepthImage_set_maxEnvironmentDistance_mE5AD8BF6610AB450C153A91332D0FAD369E7A3C5,
	DisplayDepthImage_get_maxHumanDistance_mFADDABF942055F7F0E0722A399104DC1D9FB222C,
	DisplayDepthImage_set_maxHumanDistance_m830E41789F47D11B1DE10F2A5202D047AA33D40C,
	DisplayDepthImage_Awake_m6FAB11844F77EE3C9E4A5F1B549CA9C7454A9BEB,
	DisplayDepthImage_OnEnable_m41A7394A88F1D15900B1525FA56633AAE60FF5C8,
	DisplayDepthImage_OnDisable_mA4C930A96939F5824635241CFF314D239F89FF1D,
	DisplayDepthImage_Update_mA5ADB24067F51144AF22E93BB52BBF5B72072548,
	DisplayDepthImage_OnCameraFrameEventReceived_mBF6AFD20DD4A4BE08DC22AF62D452961250879C3,
	DisplayDepthImage_BuildTextureInfo_m1C378251232FD154A1D572697B870F7F0512D950,
	DisplayDepthImage_LogText_mF47707F237D9B84714B240F3AA47921BC74A7A5A,
	DisplayDepthImage_UpdateRawImage_m79B0E475C16EA6C8BA70D8809C594FD86B508763,
	DisplayDepthImage_OnDepthModeDropdownValueChanged_mA9702D1DB4784BC61EB08EFFD4BB3A00E26A2E09,
	DisplayDepthImage__ctor_mCD4ECA7AE1314093C83BB107869971BCDFDB5B51,
	DisplayDepthImage__cctor_m14CFB48C8FB7B4DB6BDE4A108024844F3DD59736,
	EnvironmentProbeVisualizer_get_reflectionProbe_m61ADE1ED312E60EADCFACD21D7A0FE125848CB4B,
	EnvironmentProbeVisualizer_set_reflectionProbe_mD70DC1DDD225326BDEDF51D1B21A44B9F5456C1E,
	EnvironmentProbeVisualizer_Update_m677D36029326CD0EC1EB87C7FF711645B35AFC97,
	EnvironmentProbeVisualizer__ctor_m5F51424429041401691F83C82FAD8CF4D5BDA636,
	EyePoseVisualizer_get_eyePrefab_m74D5C1314105BF8202A27531428429E297314B6F,
	EyePoseVisualizer_set_eyePrefab_mDC3549A5C860D76CB5F9D3CFB62257A2FA45BCDC,
	EyePoseVisualizer_Awake_m0610E20376FC9A93E122D9943153AF9067A25DF1,
	EyePoseVisualizer_CreateEyeGameObjectsIfNecessary_mDE18B2E9AADB86A108A1C2F2663E19CAC766A7F8,
	EyePoseVisualizer_SetVisible_m9BAA4E2E9FA0F367A1C1BD82548AFD27B70A297F,
	EyePoseVisualizer_OnEnable_m22DFFE533C432D6C77A65943F63825442A1C0A34,
	EyePoseVisualizer_OnDisable_m5E622EA50288E0FDD02CFEEA7A02CAAA1704FD24,
	EyePoseVisualizer_OnUpdated_mC63CD584AB5AB242D3FBF355CCB1D6C113C237B4,
	EyePoseVisualizer__ctor_m559A9515B10C94B45336F660BA20258BD5AE3AE0,
	EyeTrackingUI_OnEnable_m93171CFDBE6A15584E3EAE557D17CAE682C5E787,
	EyeTrackingUI__ctor_m56E74AD4A03C5A4F72D57C0A1DFFC3473F3C4BA4,
	FaceMaterialSwitcher_get_faceMaterials_mDB48B48C5AA4BE6467FD42BC1B391A16F4C13C69,
	FaceMaterialSwitcher_set_faceMaterials_m8497777F759682093C8B7C77D6FD063B7C0A3D92,
	FaceMaterialSwitcher_Start_mDA3CDC9F077770F4ED3FE67314F3C50B14D25412,
	FaceMaterialSwitcher__ctor_m3552FA11C9908431E2FCD579CA07A48B05183880,
	FaceMaterialSwitcher__cctor_mE5A6A6016E634C9FBBE9873E21E69C0348D3FBE2,
	FixationPoint2DVisualizer_get_fixationReticlePrefab_m0AC88784C743616CF8BE1DE9E59F416DD01C549E,
	FixationPoint2DVisualizer_set_fixationReticlePrefab_m6612A3DF1FF5FCF7B97D1A50A6F8828F2A3CCD8E,
	FixationPoint2DVisualizer_Awake_mB70B69DDC32EA887DD982D653F3CAA206780C116,
	FixationPoint2DVisualizer_CreateEyeGameObjectsIfNecessary_m0725F95191DDC8E56C034455E3CDB1A6E6470013,
	FixationPoint2DVisualizer_SetVisible_m6E379224EB59A0F4F9AF05CF292CDFEDB7162E8D,
	FixationPoint2DVisualizer_OnEnable_mC9C9B32F2BC0D095AACAD5CD2D5E971C087308A3,
	FixationPoint2DVisualizer_OnDisable_m67E6CED611148BB2099C41794D5BEA61FEDD9B8B,
	FixationPoint2DVisualizer_OnUpdated_m4D5CB9273DDBE8709F7EFDB9A7C997183CC83C92,
	FixationPoint2DVisualizer_UpdateScreenReticle_mC64AFC5718C4D4CB3D4A86E8D000D4177199C2E5,
	FixationPoint2DVisualizer__ctor_mE3275F757592205E593A9C7E1894F19B16A0A16F,
	FixationPoint3DVisualizer_get_fixationRayPrefab_mDD47B9E5145B54181E687BA651B079E537A18CA1,
	FixationPoint3DVisualizer_set_fixationRayPrefab_mADB921730CB68A8DC5432D1FF555C76805B23292,
	FixationPoint3DVisualizer_Awake_m00E7437FA6254A80DE07F60FE5131A30ADB73A73,
	FixationPoint3DVisualizer_CreateEyeGameObjectsIfNecessary_m4894BCD862920D2E748FF47CEA7B3F82DA2DE1D8,
	FixationPoint3DVisualizer_SetVisible_m8644E24B74D10D6145E8A719F766661EA9CA110A,
	FixationPoint3DVisualizer_OnEnable_m74453225F5848B489BFA3F47FD0DEFDA3F9167D8,
	FixationPoint3DVisualizer_OnDisable_mC5E8C4E168F22EB4E2A9A10113C0DDA44D81C897,
	FixationPoint3DVisualizer_OnUpdated_m669960CF84D281A810B61D87B2E02A72A0E2FE39,
	FixationPoint3DVisualizer_UpdateFixationPoint_m3FAB46BD53F9AD16E78A3F7C4489A34805AE9F65,
	FixationPoint3DVisualizer__ctor_m0986C5F6392CFED3D66D9F273B75ADE77F817794,
	HDRLightEstimation_get_arrow_m13863AA62B103000FC1B11480939D91DBFF0F9B1,
	HDRLightEstimation_set_arrow_m2C0873884E747E47551B1504283AD00686A31A0C,
	HDRLightEstimation_get_cameraManager_m812D48793AC05371A33E1BDDD1B64581353DC6EB,
	HDRLightEstimation_set_cameraManager_mE2F1E770C5E2381A53110D41FFCC0F1A9E01D050,
	HDRLightEstimation_get_brightness_mAC9A8A666266491EB7548AE78200B3D078BB27A0,
	HDRLightEstimation_set_brightness_m75A6F53B06B1774309C54BCDE08189DEF25B5D7D,
	HDRLightEstimation_get_colorTemperature_m37FC4B5A3D359B5543FF27975B84A4160674ACE3,
	HDRLightEstimation_set_colorTemperature_mD6CEE6C0EB848772B794870BAD31FA934B38265D,
	HDRLightEstimation_get_colorCorrection_mABD61C33CF8C51594822CED7C9B32A47595647DC,
	HDRLightEstimation_set_colorCorrection_mEEE316804EECE09BC5D2F7AD1497FCA4300DA6CC,
	HDRLightEstimation_get_mainLightDirection_m2B3E896A43CFC5AD43A77B8F082BD1E2DA86FAD6,
	HDRLightEstimation_set_mainLightDirection_m0184B2A629BFA8DC8283D6A7B33D7E19EEE2B191,
	HDRLightEstimation_get_mainLightColor_m5305DF10F6F7C734959DA7F90F0BE718D90CACAA,
	HDRLightEstimation_set_mainLightColor_m8B42F93CA5A24B47D95DA30AFDAFE13EAE3410E0,
	HDRLightEstimation_get_mainLightIntensityLumens_m9B63B9BD7A8409CFF8E1FBB47F8F93EBF10F6326,
	HDRLightEstimation_set_mainLightIntensityLumens_m24A7EF4F8F32DED0A3D5DBFB2CE623D661B8B853,
	HDRLightEstimation_get_sphericalHarmonics_m2B3F3AFC87ED7EB1ED81D49506C9B61FF03E3F92,
	HDRLightEstimation_set_sphericalHarmonics_m4F08104D790EF9E052E4EA491E5CA447533FFA9B,
	HDRLightEstimation_Awake_m9CDD40A1EF21B9172AA39C104793241F098967FB,
	HDRLightEstimation_OnEnable_m383309DF85AA9329CE1D3966851433FB244ED138,
	HDRLightEstimation_OnDisable_m0A56ABEB300E59FC7F05FA11CA037C8F344AB558,
	HDRLightEstimation_OnBeforeRender_m5FF4F08EAFA9F57AA9A0B0E05ED0ACC83E0263CF,
	HDRLightEstimation_FrameChanged_m5227B42F4E8A32FC08AAFF14C23D5849F739E6EA,
	HDRLightEstimation__ctor_mA6FA8FE8A8DE001B4765024E9536912E328EF584,
	HDRLightEstimationUI_get_ambientIntensityText_mF860111603CCAF7758DB4D66F6A5A0BF07694C83,
	HDRLightEstimationUI_set_ambientIntensityText_m225883E2BF39C8D6099321ED35E1DFE3B7A5AEE1,
	HDRLightEstimationUI_get_ambientColorText_mC7E352E37195E0828E601BC1C8A63C5FEB2196E0,
	HDRLightEstimationUI_set_ambientColorText_m4769CE4BFA8C7D94223FD6C1496F6FAB1A9040DE,
	HDRLightEstimationUI_get_mainLightDirectionText_mD3CEAC0CE3169ACE5BFC029FC6E5C041CFD5F3FD,
	HDRLightEstimationUI_set_mainLightDirectionText_m1636273FAC859F042B76D0DC3B297A5326F4B0B6,
	HDRLightEstimationUI_get_mainLightIntensityLumens_m6198E8087B6594AA1A7C2BC95BB57963F4993454,
	HDRLightEstimationUI_set_mainLightIntensityLumens_mDC48EED111445A44B0DB129B9B28F6E7DD7B5261,
	HDRLightEstimationUI_get_mainLightColorText_m90B05C3D83AF798FA2B2711ACE48488B69540F96,
	HDRLightEstimationUI_set_mainLightColorText_mD8B41FDF63A8282658365FD7B43B82F197F4DA93,
	HDRLightEstimationUI_get_ambientSphericalHarmonicsText_m222ACCC5E72E1B5BB2C2C2601083BD292DF212D2,
	HDRLightEstimationUI_set_ambientSphericalHarmonicsText_mB4D235352CAC99891E9195AA7D01154AEFAB9950,
	HDRLightEstimationUI_Awake_m643AA0B06E1363BA733147B417D6A86D3711EC79,
	HDRLightEstimationUI_Update_m60C3A05958F002B6430EB6E73CD6FB2F006A03AE,
	HDRLightEstimationUI_SetSphericalHarmonicsUIValue_m72C7EF297802E4C09D98F0F8F9ECB2ECB7D31AA8,
	NULL,
	HDRLightEstimationUI__ctor_mC109515C913718446D5AA703586414B3BA501071,
	HumanBodyTracker_get_humanBodyManager_mC4B8945E39D5320F9979435792D262AC93C4D073,
	HumanBodyTracker_set_humanBodyManager_mD698CD4F92BECC8322CA17ED80CF62114B690FB3,
	HumanBodyTracker_get_skeletonPrefab_m682A136691745238FA4425FBBD3B777E7AD42FFB,
	HumanBodyTracker_set_skeletonPrefab_m9A137EC0A4552DEA901F4EBFF84851E4EC7EA881,
	HumanBodyTracker_OnEnable_mB8955E62494A0CDD4402ACFC172A62496CA26394,
	HumanBodyTracker_OnDisable_m7E3C3D6DEAF82B58346242D91653ECB81487823A,
	HumanBodyTracker_OnHumanBodiesChanged_mCA0BA7F2AAC9D2A12EB37B819B86A2D793EDE86F,
	HumanBodyTracker__ctor_m5BE66EF9DEF2A47385D9E29D6C693C97F910392A,
	Logger_get_logText_m600AC307038B8EC747490883DCF4665FEAFD875F,
	Logger_set_logText_mF8B44C562FEE09DC292F556C9F36A7E9C6993BFA,
	Logger_get_visibleMessageCount_m285BE29324B7B59765055ADCEDCBC3D2BB5D89B4,
	Logger_set_visibleMessageCount_m1F814B57BBFF8D248C7A5A938D82818CA4123C1C,
	Logger_Awake_mD1016F1251DB6CA3FBD2ED28DE93A9B4B11DC98C,
	Logger_Update_m4721D90DF76645FF144C8C9AA3C97087BBF174A0,
	Logger_Log_m85A7E39D61A1961CEFC7B93FDAE300E26107B3FC,
	Logger__ctor_m6E4DB291CACE70BD296CC49008DD47D7C9E660AA,
	Logger__cctor_mA9B645798BC042347C3FBC3D1182B442D23C0D6A,
	MakeAppearOnPlane_get_content_m467D1D9468F11D8A7F8384C8F7529D94EDF04493,
	MakeAppearOnPlane_set_content_mF2774D958C87E7A4ADB68771DD2D2A658B3C52E6,
	MakeAppearOnPlane_get_rotation_m883F2C51687D1856ED637E74E88C9C8B51237447,
	MakeAppearOnPlane_set_rotation_m409C3FEC4330F90717156C433B1CD4CAA536843B,
	MakeAppearOnPlane_Awake_mA31F7C1E85C5C25B755CEAF4790AD37A418F3DFF,
	MakeAppearOnPlane_Update_m10DF99B497DB464A96AF4BC6ED003736FE8A6E1B,
	MakeAppearOnPlane__ctor_m525EF5D6F93E1E740946A1E75977CA5341582B49,
	MakeAppearOnPlane__cctor_m3DAA4262BF27E0B9BE69E72EDA89EEBFD0375842,
	PlaceMultipleObjectsOnPlane_get_placedPrefab_mD60B8AC0A8C0C41F8C1E766370FF9F898CCB917A,
	PlaceMultipleObjectsOnPlane_set_placedPrefab_mEC44306B88B9D34593B6A3DFE0DCB97ED4F679EE,
	PlaceMultipleObjectsOnPlane_get_spawnedObject_mD86A3A7110223667DA31A226CEC429FB88E24EF7,
	PlaceMultipleObjectsOnPlane_set_spawnedObject_m5407E287877625F501DC82BEA022FF6E8C183014,
	PlaceMultipleObjectsOnPlane_add_onPlacedObject_m28FBD07B2F6D8D6B3431FEE5E754F6BAECBBDFE5,
	PlaceMultipleObjectsOnPlane_remove_onPlacedObject_mECDF0B3E90B57303F9D2A7D539E4067838C81CFE,
	PlaceMultipleObjectsOnPlane_Awake_mA7E09D89EC480E9D00045922ED93CCACD03CF3C6,
	PlaceMultipleObjectsOnPlane_Update_m42D86D85FD1E2D2C5E3C0AE64ED213608F37FE62,
	PlaceMultipleObjectsOnPlane__ctor_m04EA4054A36090BF7C4B3F0813232094873745B8,
	PlaceMultipleObjectsOnPlane__cctor_m3A901E424B21C70F413709C9BF0AEF28C7FC68FA,
	PlaceOnPlane_get_placedPrefab_m763D6F073D3EDF7415E1E32E10AA426967DCDA18,
	PlaceOnPlane_set_placedPrefab_mB1D74F674553C7DD3E35919E9B3510A2A72300D9,
	PlaceOnPlane_get_spawnedObject_m35FA877FA58C9C21BD450CDA01EF4BF5F6189C09,
	PlaceOnPlane_set_spawnedObject_m4831A436BFFF40945FCE6376CF88870D4E0DFA87,
	PlaceOnPlane_Awake_mE68544675C14502921A783F67B2D7B80E00BDBB9,
	PlaceOnPlane_TryGetTouchPosition_mE940E600E9FAB1C3D9F182B04A920CAE6F39C61C,
	PlaceOnPlane_Update_mDAFDB8C7CABA3CFA3E6C422A51585A3159C5B807,
	PlaceOnPlane__ctor_m6DD8D183AC8B0486B5791C6AF332B305E3CAA1AE,
	PlaceOnPlane__cctor_mDE3040EFFFB02FA683F04061F2ACC23E26010E76,
	PlaneDetectionController_get_togglePlaneDetectionText_m914781015127293A67ABFECF438A1CF2ACBF9E31,
	PlaneDetectionController_set_togglePlaneDetectionText_mDE2BA925E54F24A9AF128166DDBCF346E0FFCF94,
	PlaneDetectionController_TogglePlaneDetection_mDC082A6B018A93A31F9D3503447FB3B69BF7E17E,
	PlaneDetectionController_SetAllPlanesActive_m30933F95A82C187D3EF7B8122BD1A15180508B7F,
	PlaneDetectionController_Awake_m0248674C37BEB34AF39227EA8937309089340149,
	PlaneDetectionController__ctor_m70E75C6E3216E94E9600F78E7B2B8CD286D9FD5D,
	RotationController_get_slider_m688A2516939785224E2135387DFBEFCFB521B5E8,
	RotationController_set_slider_m032C8F2D03C60D88BF3C51E9E69BC839A058303A,
	RotationController_get_text_mAEF0FDDADD3A641257FF3C29D1681E9CD17C8839,
	RotationController_set_text_mBA49076027FE64EFAA7C45A331B0B19E4DFF27CF,
	RotationController_get_min_mB3746DD093A77C530E0CB36F6F673B321F83822C,
	RotationController_set_min_mCFDF217E94A470520148859597955A42C84020EE,
	RotationController_get_max_mDBB0C27A2AF8529AE647BCA1F48ACD2A6D3A1369,
	RotationController_set_max_m0184CBFE1E139F74BF2267C5D26F873255C4502F,
	RotationController_OnSliderValueChanged_m60BC86CF728B683B9F5E2DCB25481A021862B3ED,
	RotationController_get_angle_m6E3BB762FFD942D9D0EF2FF6E2BA87B8E26E75EB,
	RotationController_set_angle_mFC6803991BF2B19CE3E092E18CD2AECC9C0C28B3,
	RotationController_Awake_mACC2B7EF2D1375C9C0B3953AAC71F00BFEBF4CBA,
	RotationController_OnEnable_m1652900F3F65365A46C8168FA80A3F90F1BF478F,
	RotationController_UpdateText_m3786EFAF0145BA714F41DD135E2DE2E7D9B82C88,
	RotationController__ctor_m26E8F9A4C98DDEF771DF98A4B955A12F6EADD064,
	ScaleController_get_slider_m4759BCE9036C2F489DB791862A8F651EC4879762,
	ScaleController_set_slider_m3EA04E532D3A83F4DDEB30058B9A03265FC9ECEF,
	ScaleController_get_text_mEC690CB74411DF88C67C8A5442AF28DF11C29D29,
	ScaleController_set_text_mE1EA21CABB6941E297E26B9456E6080124C26465,
	ScaleController_get_min_m93ACE40734850E95EBC2FEDC1C86CE11F4967D87,
	ScaleController_set_min_m5B00A4A654467E0C9720AA4D9FC05302A5D49729,
	ScaleController_get_max_mC82966DEACB852B73D101156CDEB1B786037F0D5,
	ScaleController_set_max_mAB7B381C689D4FAF758A264258A93DC788D409B5,
	ScaleController_OnSliderValueChanged_m523EF1DF1293E767D9EAFA7359F8C53E0445D918,
	ScaleController_get_scale_m70A80D5C1AE10DC47849CDF68A8A349FA98A40BA,
	ScaleController_set_scale_m5C5040F5D3D019542F9A59496300643A7B2DCB11,
	ScaleController_Awake_m07CE359676D88A307EBB1F6EAE45A28E1C544195,
	ScaleController_OnEnable_m32ADA2F5C752C902FD37EF4E365F3AA0506EAFA5,
	ScaleController_UpdateText_m8BF6016F76B0DD20598237FDC35A8D3F55F944B8,
	ScaleController__ctor_mDB262AF1ABE1815460CAEB62A47BC996A62754AA,
	ScreenSpaceJointVisualizer_get_arCamera_m82AFEE273B58ECD1161F08092482BF4490D2A47A,
	ScreenSpaceJointVisualizer_set_arCamera_m54AC785E901584A0FA22633581C8E6FDADBEF81E,
	ScreenSpaceJointVisualizer_get_humanBodyManager_mC7CD80E44640BD62F49F534E5CA1F319564C63C1,
	ScreenSpaceJointVisualizer_set_humanBodyManager_mB1FAC9EE439BB945CB09F2F9E3F5FEC74EDD8A8E,
	ScreenSpaceJointVisualizer_get_lineRendererPrefab_m61EEB13EF427976C8DB5E1FB1A9AAE666F1679EE,
	ScreenSpaceJointVisualizer_set_lineRendererPrefab_m85C6B1BBC286B0BEEE8EB729199F26BB6545F4C9,
	ScreenSpaceJointVisualizer_Awake_mBA10AB7D06AC6C582459127F631DE8D10F05B5A4,
	ScreenSpaceJointVisualizer_UpdateRenderer_m7579F800D27BA512ECBDCC5BE3385305FEF8AB1B,
	ScreenSpaceJointVisualizer_Update_m9D138197FF36EA87CAB234FBB728FD42EBC62FBB,
	ScreenSpaceJointVisualizer_HideJointLines_m38A24B97D80276D5E89A4FA9975C83E43AC2DA65,
	ScreenSpaceJointVisualizer__ctor_m44D5CB68146DDA2E672736563FE99DF773C88B57,
	ScreenSpaceJointVisualizer__cctor_m282B7FF2145B51125F1B4260F8C18182ABC741C1,
	SupportChecker_get_session_mEE0E9882292D6996B415E9C4C6B6FA34AF0A3E40,
	SupportChecker_set_session_m3C19705CEB93F7E223885891A9F620DAC56DEAB6,
	SupportChecker_get_logText_m4218723C52FD34A5C56C915CA8CB1DB7445400DC,
	SupportChecker_set_logText_mCA32B9D279BECACE0B57965D6361E718537B6407,
	SupportChecker_get_installButton_mABEB073536D29DD2141F6B87A02F6F6F1BEEA9F3,
	SupportChecker_set_installButton_mA7649BD06BCA3F7E97FF1F2EC579BFD12A2D0A01,
	SupportChecker_Log_m46AD9890AA7900830EC90CD845F5EE2375BB7E17,
	SupportChecker_CheckSupport_m975FB3F2C90FE4E4EFD391E6747162496ABA7C27,
	SupportChecker_SetInstallButtonActive_m3FAAA2F30D77E269B7C169F6FAB5A0419C16350A,
	SupportChecker_Install_mD735E330C220AEC1266E88C73DB1BC8048FECCEF,
	SupportChecker_OnInstallButtonPressed_m36E562592D0D90C35EA48F96B9A4D7F6EAF6B7A8,
	SupportChecker_OnEnable_m0C7462B91FECE976CC2720F35657DA327C1639D1,
	SupportChecker__ctor_mD2784A992FDF0C6405AD1C43DBB58686F0E81712,
	U3CCheckSupportU3Ed__13__ctor_m6BC6258C4F1ABA808914DD53128A7D30D4A48EBE,
	U3CCheckSupportU3Ed__13_System_IDisposable_Dispose_m396334D7FCB3267C034E19C3CC2F3CDB5EAA31C1,
	U3CCheckSupportU3Ed__13_MoveNext_mD885F827DD821A2AA2CDC0A64A5581E72BD8093F,
	U3CCheckSupportU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA74258C01070CABFB04F4BA5784E6505009EDFB8,
	U3CCheckSupportU3Ed__13_System_Collections_IEnumerator_Reset_m4CE9E2693E35645009E13AEDD9C12B69D7731868,
	U3CCheckSupportU3Ed__13_System_Collections_IEnumerator_get_Current_m6E12FC445F61FF2A359FD44245513590E756C0E4,
	U3CInstallU3Ed__15__ctor_m7779EEDD7771D621B16FCC240A3FB9EB02E8B671,
	U3CInstallU3Ed__15_System_IDisposable_Dispose_m4C7497608DCC1C85B0A011B6A8944C0FCD27D8BD,
	U3CInstallU3Ed__15_MoveNext_mA1A230F1CD888AD0A52BD44712194C54A037EEA1,
	U3CInstallU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14C586453D364A731DEF5637A847E37B76BCDD6A,
	U3CInstallU3Ed__15_System_Collections_IEnumerator_Reset_m39BD04A5F9C8864E386EF7AD0FA2FD75CC2F44CA,
	U3CInstallU3Ed__15_System_Collections_IEnumerator_get_Current_mA1C54C035E6319C42E5B1B88BF7C71631EB559ED,
	TestBodyAnchorScale_get_humanBodyManager_m4B37BF9F3ADF4BC6B89BED199E611E580EBB0526,
	TestBodyAnchorScale_set_humanBodyManager_m43FF3C3D743059075AEE92F6C96AA0A066736DC0,
	TestBodyAnchorScale_get_imageInfo_m1A99F301BC287BB82EA21ECF33E947C1382A1E3F,
	TestBodyAnchorScale_set_imageInfo_mCAFB5D7DCF072A4D58D0B975B13B46FB14124F3C,
	TestBodyAnchorScale_OnEnable_m468B3225428A1BB05C8467977AD9923EB63C7C98,
	TestBodyAnchorScale_OnDisable_m2EC04A2C49D4DB6174622B949E058B583AB5CC5A,
	TestBodyAnchorScale_OnHumanBodiesChanged_m023FA84984B3ED88DBD663D3EC9CC87DD0944628,
	TestBodyAnchorScale__ctor_mB4D6913DBC970760892584FBEFA3FC899C3BFD29,
	ARSceneSelectUI_get_horizontalScrollBar_m5FC064E170727EC1341826098767F5E30E55D7A7,
	ARSceneSelectUI_set_horizontalScrollBar_mFCC0AC1D8001AC12A8A802CB24C5ACBD7DED53A9,
	ARSceneSelectUI_get_verticalScrollBar_mB7B43395D026639213E2C6C97D00E0317AECA8DA,
	ARSceneSelectUI_set_verticalScrollBar_m63AC2199A6318700CC9B4422EE6CC451AAE6BF7F,
	ARSceneSelectUI_get_allMenu_m6B813F9EFA127A805767249833689BED9DAA5B98,
	ARSceneSelectUI_set_allMenu_mC13C9AFCC9A6C8967B254542AECB4FC3DE5524F2,
	ARSceneSelectUI_get_imageTrackingMenu_m618C928E056904FE0305E8A343BC5BE8E62C20E1,
	ARSceneSelectUI_set_imageTrackingMenu_m1EA3088540273AE638DABBDB4F507ABFA38BAFF8,
	ARSceneSelectUI_get_faceTrackingMenu_m0FD1C96BCD7122107A90377CD90C9FE3A392A428,
	ARSceneSelectUI_set_faceTrackingMenu_mB3A6D2DAEC230A0D4895F8661390B6622E04B759,
	ARSceneSelectUI_get_bodyTrackingMenu_m1753C092DB4B2DCB0443871AD98E7F87824E7A43,
	ARSceneSelectUI_set_bodyTrackingMenu_m905D2F689496EFB15DF17A0800B968AE1EFBC39E,
	ARSceneSelectUI_get_planeDetectionMenu_m49174CF9A1CF511271213D2F5DAEF61AC6569549,
	ARSceneSelectUI_set_planeDetectionMenu_mF694A86F40ADBA7470108B6983906AD52E25623B,
	ARSceneSelectUI_get_meshingMenu_mCBA6BE45CF69596192BDE7366BBA21C3CD22268A,
	ARSceneSelectUI_set_meshingMenu_m373DF2BE32E1EB0C8256DAF7AA1F91FDCFDB2F0F,
	ARSceneSelectUI_get_depthMenu_mECB6444247E9C8365B001FE778BA383345372876,
	ARSceneSelectUI_set_depthMenu_mA1345C822EF8C625C1C96401CCA962CB5A8F813A,
	ARSceneSelectUI_get_lightEstimationMenu_mEFCD70188B0EF109BF3CFE831BBD37B7AB28910D,
	ARSceneSelectUI_set_lightEstimationMenu_m711C48D1E2B77ADF72BBADA4F4CEB9F39512FA03,
	ARSceneSelectUI_Start_mF4DA86AE4A4205E475EFDDF5E6AE06BC4313F338,
	ARSceneSelectUI_LoadScene_m05B38B643DF5DD35A8A6AF039653DC7ED81E4B4F,
	ARSceneSelectUI_SimpleARButtonPressed_mFECE0F3533419FED607D960BE741857AF071C7AF,
	ARSceneSelectUI_ImageTrackableButtonPressed_mFF06A0F6174405F9A28DA2AA944BAFAB7CD5B026,
	ARSceneSelectUI_BasicImageTrackingButtonPressed_mB69AA1FF3DAAF06030A616B27425D63E797D4816,
	ARSceneSelectUI_MultiImagesTrackingButtonPressed_mC7B68980F39FC224040BF1DC8927881AA30F9997,
	ARSceneSelectUI_AnchorsButtonPressed_m4D74007E998FFE09820E185E8B1B29748A551961,
	ARSceneSelectUI_ARCollaborationDataButtonPressed_mB64483D9440DAF442667BAE3BC3D5506A54EDB6A,
	ARSceneSelectUI_ARKitCoachingOverlayButtonPressed_mBC1FBF527FCA56EDCFAFD2C2420A9B30040AFF4C,
	ARSceneSelectUI_ARWorldMapButtonPressed_m685077D2BC15B90CFDCAFF677F72EF5F73C22212,
	ARSceneSelectUI_ARKitGeoAnchorsButtonPressed_mED96251EBE27AED533BBBEC9236FDAE94CB99BF2,
	ARSceneSelectUI_CpuImagesButtonPressed_mC92A3754DCD430A77C0676E9023E1124BECD5A47,
	ARSceneSelectUI_CheckSupportButtonPressed_m7DD5279F6C0B54D8DD6FA197AD0E48673AD29BA7,
	ARSceneSelectUI_EnvironmentProbesButtonPressed_m7EA06FF68E7294F5AC217B24DF35D1343BCDD397,
	ARSceneSelectUI_ObjectTrackingButtonPressed_mB58E92CB43E182FF064CE5F54815BA4ED70BA60F,
	ARSceneSelectUI_PlaneOcclusionButtonPressed_mB8B43C5B912F32574C3B3A287F2CAE8491DB97E3,
	ARSceneSelectUI_PointCloudButtonPressed_m56DC266199DDCB760381C7398626E23863EF72B4,
	ARSceneSelectUI_ScaleButtonPressed_m0E68CF5BA71FF162E501F57F4661BE819ECAF1E1,
	ARSceneSelectUI_ConfigChooserButtonPressed_mA72D45CB8BA104BDEBCCEA19ABDA3E08BA44E4E6,
	ARSceneSelectUI_FaceTrackingMenuButtonPressed_mEB63B5232054262E0F24AA74F21C8842EC0AF3D9,
	ARSceneSelectUI_ARCoreFaceRegionsButtonPressed_m1164E97990A1B64B761E83E17EEEF37F2157A8D1,
	ARSceneSelectUI_ARKitFaceBlendShapesButtonPressed_m29D1FC848969A13499EEB2A8D1D6BBCFF0A4FCC9,
	ARSceneSelectUI_EyeLasersButtonPressed_mA7157DC0B90604C8AC1D70E5D6E0B74F387B9419,
	ARSceneSelectUI_EyePosesButtonPressed_m4FAB1813DC0EF0B8447B5A8965CBB6E23643A7BE,
	ARSceneSelectUI_FaceMeshButtonPressed_m38A33707BED022E6462BFC93DD31A3FBF93873DD,
	ARSceneSelectUI_FacePoseButtonPressed_mE3F19B1E6E6A21D479DC9EDC256E9624218F586D,
	ARSceneSelectUI_FixationPointButtonPressed_mC3C88F6282B3B1271FE4D4DF05F0BAB965E8A461,
	ARSceneSelectUI_RearCameraWithFrontCameraFaceMeshButtonPressed_m48454B9187555F258A632F4010CE7B55830AC1FE,
	ARSceneSelectUI_BodyTrackingMenuButtonPressed_m0514F115B07CBF754390067B536F5E37DD5E2D94,
	ARSceneSelectUI_BodyTracking2DButtonPressed_m515F3BA1B85207D2E3EB41110159585053F1A476,
	ARSceneSelectUI_BodyTracking3DButtonPressed_mED675FBE8C46565FEF6AC19F9A2CB4B263D073DF,
	ARSceneSelectUI_LightEstimationMenuButtonPressed_mF43ECC02071AA319B73C919FAE89FB574C7EE41C,
	ARSceneSelectUI_BasicLightEstimationButtonPressed_m3CD7677C680229E141F2C081E2BF127A8F72636B,
	ARSceneSelectUI_HDRLightEstimationButtonPressed_m449963DD0B0A23925074C37CD9C993F0F1CC6F0B,
	ARSceneSelectUI_PlaneDetectionMenuButtonPressed_m215F88FAA9FDFD4EB6A9729FCA4AC4F8ACAE09FB,
	ARSceneSelectUI_FeatheredPlanesButtonPressed_m42C04E26051E1B85CC1D21D944A41DCD60BA31C0,
	ARSceneSelectUI_PlaneClassificationButtonPressed_m745BEAEAECD1CCF65B5BC353754658F6BFE93CA3,
	ARSceneSelectUI_TogglePlaneDetectionButtonPressed_m3A675640C489652D7809DB619F05F9C1C4674285,
	ARSceneSelectUI_BackButtonPressed_m44C70E5DCA188954A09D863BAD67E75817F0FF89,
	ARSceneSelectUI_MeshingMenuButtonPressed_m142F06B7B0603065037E91537CE104B2F993486C,
	ARSceneSelectUI_DepthMenuButtonPressed_mA3526C518E164667DA0EAB537BF6B1E5582E8CB9,
	ARSceneSelectUI_ClassificationMeshesButtonPressed_mD64FEFCB3EAC9A4C65BD6F11D4E6BBF6450CB83B,
	ARSceneSelectUI_NormalMeshesButtonPressed_m10DB0F01EEF975758AD042279F719AA5BA1FC714,
	ARSceneSelectUI_OcclusionMeshesButtonPressed_m0C13630ABB67F752B851312DADE9D589BE66E53C,
	ARSceneSelectUI_InteractionButtonPressed_mFBFA1BE40D0E00A6192A2D8D5640FD84E8C2CA7D,
	ARSceneSelectUI_SimpleOcclusionButtonPressed_mF54C9E9CDD6D84F3002527A4655834D22D66B95E,
	ARSceneSelectUI_DepthImagesButtonPressed_m59798E7B3CCCB943E3E4089C395C3ED0D8F9D000,
	ARSceneSelectUI_InputSystemButtonPressed_mACF7AF4DF8AD532DB4D57D0DAD8F3ED8AA4147DE,
	ARSceneSelectUI_CameraGrainButtonPressed_m725EBFA471DF073EA02007E78AD3783F586004ED,
	ARSceneSelectUI_ThermalStateButtonPressed_mA86AD006F0FFDA3CB0B498C02698974B78A7AAB4,
	ARSceneSelectUI_ScrollToStartPosition_m77D718FFC3D98A3BDD81909557E46948B25B48D4,
	ARSceneSelectUI__ctor_m3A439E23B65D0C0802CD21DAD6635627CF9F69F9,
	ActiveMenu_get_currentMenu_m57A139E0FF21CEEB6416C25D94832832496EB1F7,
	ActiveMenu_set_currentMenu_m1D5E74394A21ADC83B2694047532CDEE983F35FE,
	BackButton_get_backButton_m6C42C5B8C3C5D726A2A638614C3C1004F8FF6B9D,
	BackButton_set_backButton_mA20F4E8B6A0CAA1FCE697861488007DEB11588E7,
	BackButton_Start_m91D83EF34477C02DEDEACF640208D15735EA3526,
	BackButton_Update_m59F6AD5F9B15A73F885351A5521812C3E4253B8B,
	BackButton_BackButtonPressed_m3AE314F7F35880CCCC2D7C1D37075DF582A7CEEB,
	BackButton__ctor_m99B8ED800DE45430146458136205319B7DE77498,
	CheckAvailableFeatures_get_simpleAR_m9BDD412478DBFED9EFAD94FA990B4E4FD87E5D71,
	CheckAvailableFeatures_set_simpleAR_mA82545C6F82001D54265892D1628D58B44A7FEA2,
	CheckAvailableFeatures_get_imageTracking_m8B97E8F482FF872EC425C68B7436BA0750B4757E,
	CheckAvailableFeatures_set_imageTracking_mDDFBF25047147596820CDA7CC2B8C9DB31A70443,
	CheckAvailableFeatures_get_anchors_m0C777536139265A2F5EF3A9AD13A56BC1CEAAF3D,
	CheckAvailableFeatures_set_anchors_mB69251FB41E9511CA05662BEFB7A096025046414,
	CheckAvailableFeatures_get_ARWorldMap_m61D2685E3133DA2A1C54FD9524916A460859A12A,
	CheckAvailableFeatures_set_ARWorldMap_mE6F68F17EB263A7F83321E701200DBCC22CE3D86,
	CheckAvailableFeatures_get_ARKitGeoAnchors_mBD85BAD41538EE9C2DFCAE714A79566A1283E448,
	CheckAvailableFeatures_set_ARKitGeoAnchors_mD56CC44D8BFBC2DF442D109ED41427B079CDCF8F,
	CheckAvailableFeatures_get_cpuImages_m2B9E5E75184DE071E0327E4E7E369B0D7F2200F3,
	CheckAvailableFeatures_set_cpuImages_m834981832D3B834AA1C4E8FE6FDCC6775A0F27C1,
	CheckAvailableFeatures_get_environmentProbes_m7AA73DCDAAEE8A037168E746A0CD5F4BDDA43544,
	CheckAvailableFeatures_set_environmentProbes_m87ABD4E1275CA502BFDDBE2A164CD0E33F6A190D,
	CheckAvailableFeatures_get_ARCollaborationData_m9EA54245F78986923CB90F13844A58FBC426BC8C,
	CheckAvailableFeatures_set_ARCollaborationData_m00F15E38EC6FA7DFC95EFFC1FE2D9ACAEC62D96B,
	CheckAvailableFeatures_get_ARKitCoachingOverlay_mE755BA45EB7B7BA09CF48135E6D8BDCED1394EA3,
	CheckAvailableFeatures_set_ARKitCoachingOverlay_m5A209F73A11D8948A1A5C7ECFEBA68DC215EECCE,
	CheckAvailableFeatures_get_scale_m682AAABD71F99CA664528D6D87F6B80B2B091FEC,
	CheckAvailableFeatures_set_scale_m265178438AFB21F54B2B60FA5A4E940FA5828216,
	CheckAvailableFeatures_get_objectTracking_mFABD15AA61633BEFDB6C0075FA7796C01D58B39C,
	CheckAvailableFeatures_set_objectTracking_mBE263863F1C9CC3BF722567C0A7F7E895B53A610,
	CheckAvailableFeatures_get_planeOcclusion_m2567FD5A29E502A7E50428CA306C681D3C134FB3,
	CheckAvailableFeatures_set_planeOcclusion_m12ED16B83C7E453A76AC6E0A20DB41C002343DD4,
	CheckAvailableFeatures_get_pointCloud_mAAEFE28828216AE4E808874F59B96C6050091AFD,
	CheckAvailableFeatures_set_pointCloud_m74B0561B66D525AD8874D0F9F1BDF0D6F6F5938A,
	CheckAvailableFeatures_get_faceTracking_mD245135F71D1B439B1A39AF487E7FC03FC5CA518,
	CheckAvailableFeatures_set_faceTracking_mDCFD516DC78297DFA6DA2304BAA2D3527AAF24F7,
	CheckAvailableFeatures_get_faceBlendShapes_m30E405B68EB1C38CB90383883F88F82687E2A1F9,
	CheckAvailableFeatures_set_faceBlendShapes_mFFCC1814463E8EDDCA22BC1A14AD1428C5E6BBB2,
	CheckAvailableFeatures_get_faceRegions_mAA657FD6E5F354E8B2C61AA5233EB215FD9FF7F7,
	CheckAvailableFeatures_set_faceRegions_mF34A67C064754AD3901A7BE618A7A97596F1B983,
	CheckAvailableFeatures_get_bodyTracking_m20527AEEA4478E0E7F6BE34A8A5417872CAAC877,
	CheckAvailableFeatures_set_bodyTracking_m24DD1C3A24BEE5A90032093CC06F1998C307CCED,
	CheckAvailableFeatures_get_lightEstimation_m3F63CBDB379F89CC8236BD15BAA34C548EBD877F,
	CheckAvailableFeatures_set_lightEstimation_m495A8BDD20A58DF2F205E4F7E9B4B0168A0FCDCA,
	CheckAvailableFeatures_get_basicLightEstimation_mE275B32E67BAC14411E54CBDD84736B7EB5F63B9,
	CheckAvailableFeatures_set_basicLightEstimation_mD5000FC7736E58018808A92F5D552D9976316727,
	CheckAvailableFeatures_get_HDRLightEstimation_m8567404E4FB6522CC0BCE7D23BE99021B8003165,
	CheckAvailableFeatures_set_HDRLightEstimation_m88D9DFD515201B0E0729F98902913C807C098BFC,
	CheckAvailableFeatures_get_planeDetection_mA11D23A722E63E4153E3DA9783EBC9DED9BDD102,
	CheckAvailableFeatures_set_planeDetection_m6EDE773076FA2B5908E85D651C80124A9D488E02,
	CheckAvailableFeatures_get_planeClassification_m01CABC528B33EACD74653427D80DF2AE68ED42B5,
	CheckAvailableFeatures_set_planeClassification_m87C63BC487908C5A824D430E12C717CC59713218,
	CheckAvailableFeatures_get_meshing_m920941D5C3AD748F79C5561FB6E3312F14F4C4B3,
	CheckAvailableFeatures_set_meshing_mD958293C0A82B4765B5148003DFB2DD6D96F5654,
	CheckAvailableFeatures_get_interaction_m2E0EE4751E498795586C487D313253AF7C96FE9B,
	CheckAvailableFeatures_set_interaction_m7209AE06A6B3CEC26A8829F6F2773EBECD739A9D,
	CheckAvailableFeatures_get_fixationPoint_mD048092333181E71778D5FFA9BA026927FB9B74F,
	CheckAvailableFeatures_set_fixationPoint_mF87552591910BF75A0EACAEE6693FC8AE56623CA,
	CheckAvailableFeatures_get_eyePoses_mB28690DF1EE2EFFF4F93B071F2901C7F7CEABBAE,
	CheckAvailableFeatures_set_eyePoses_m5D45A4CED8962396FCC83D865C58F4CB5867CE7F,
	CheckAvailableFeatures_get_eyeLasers_m25F0044D1B80B19D1D6BC4A080F87DFA01535FA8,
	CheckAvailableFeatures_set_eyeLasers_m5F1030B67FEF1DA79522E30943E1AE628E749281,
	CheckAvailableFeatures_get_checkSupport_m5063595EAED8A5EBC4C1560DA14565E20C98A9BC,
	CheckAvailableFeatures_set_checkSupport_m707494422F72A4D121D91817C2B9AF8CE70A0E47,
	CheckAvailableFeatures_get_depth_mEF572EA560DE0091A7FC409F0EEEDEBBA03CB910,
	CheckAvailableFeatures_set_depth_mB7C3C8EC50F774273C94700D179BB79997C14B58,
	CheckAvailableFeatures_get_configChooser_m2394190E1FDB13C935403B9AEE77829CBE9CDDEC,
	CheckAvailableFeatures_set_configChooser_m716AFBFDC49F7E99DCC8C1D7F1A4FAD092A7F380,
	CheckAvailableFeatures_get_inputSystem_m060A8016F592EEDD9EF0F32E6782EB2866B92AF9,
	CheckAvailableFeatures_set_inputSystem_m37C37232C55AE0DA337CE248820C3C4D72EF094F,
	CheckAvailableFeatures_get_cameraGrain_m4555FDC4B2C25B1261CEFB1D675C423D247C1E78,
	CheckAvailableFeatures_set_cameraGrain_m10E9572EAA79933A22B75FDB0B30CAA040B7BA2A,
	CheckAvailableFeatures_get_thermalStateButton_m1ADC7BC2AD411824A1BAA8A2F9CC7BA91858CBB1,
	CheckAvailableFeatures_set_thermalStateButton_m2485BDDDA6C4C0DB205377D268ABE6F14AD21767,
	CheckAvailableFeatures_Start_m7A61A7DADD22FB81C3C0DD7255A0365900FD0ED3,
	CheckAvailableFeatures__ctor_mDE69B473063EE750F6E37D68692EE0C61573AA84,
	CheckRuntimeDepth_get_occlusionManager_m0E6BEAF0FE86814FFC75A505B3E4AEA1C14367A0,
	CheckRuntimeDepth_set_occlusionManager_mCF2E7E43C764A54EE8456D1F66AB2C0C567DC166,
	CheckRuntimeDepth_get_depthAvailabilityInfo_mA5333955942DDFD90E1F32C4598CC8AC8837D42C,
	CheckRuntimeDepth_set_depthAvailabilityInfo_mFC2E36C76071C838D12E8747DA106D4EA4B88226,
	CheckRuntimeDepth_Update_m11001470F189AFFD2A327F98C142C630BDDB3CAC,
	CheckRuntimeDepth__ctor_m5F249FF11545555777E3966567BF019EC8CBDA8A,
	Tooltip_get_toolTip_mFAEB13D6C6F59968E15515E7562590F2D679538F,
	Tooltip_set_toolTip_m563A8CA229F2D52047B281366C2ED796667923BE,
	Tooltip_Start_m09784ADCC06FE4C3A3EC00DA97BA1E37A4C94935,
	Tooltip_Update_mEFEFDB9EF2106B5C65B5F9D6C1EF08506A25EB0B,
	Tooltip_OnPointerEnter_m29F0B4FADC02BE76CEBB83F5903E87A39FB2C6CC,
	Tooltip_OnPointerExit_m542FD03066EABF976D9958F05CC4A6D0B8763FE6,
	Tooltip__ctor_mC6DACD76A8238B0F3D879EFB3581E2A4752401D0,
	Geometry_get_type_m1887A492B188BFB1F9F83DFA9D858EDC7C0B5CB3,
	Geometry_set_type_m0AC6BEF9990A280BD29326C644485AAA20338057,
	Geometry_get_coordinates_m65BB75C86E83D46BB5EE1275C85B32A0111AB375,
	Geometry_set_coordinates_m71CC1DDAEB1DB65565E80B34B4FD40621C3E4D87,
	Geometry__ctor_m3FFD17C9CDF524B05385A70CCAECD65CD82A0121,
	Feature_get_type_mF2E77FE6940C11B184B8DBCC79DBF03C0295D183,
	Feature_set_type_m31AD9B5EBFC2E508969A04CBF14EA639EF0609B9,
	Feature_get_properties_m2D29DD047BD28E7FE343E430CA66CDA093D4B72F,
	Feature_set_properties_m9E28FE4C74CD556CA38B1DBF4023661FCB0AA98E,
	Feature_get_geometry_mAF48ABD0CBD61DAC2CB073868EFAE6EAD00D5C2E,
	Feature_set_geometry_m00F69594E38D4DBF01CC1A42A53B6ED4D3DD4CDA,
	Feature_get_id_m8F409898D966264752DDBAF62473FA0ACF79EF07,
	Feature_set_id_m7B4E7DAA1B73A0AAAA5DFECCC74514A17FF5FE80,
	Feature__ctor_m0D546A98532F40794E22C4A0E9679B56F6ED0D6E,
	Root_get_type_m5F0226A3F03F1CD9A912726008E49735966BA95F,
	Root_set_type_mECC5E5A23C4B9A4EF1FEC8156C99F5CB07502F88,
	Root_get_features_m8CEF9B138BE459A7414671B9D5E2DC21B91633A3,
	Root_set_features_mFEB1BA3E6B7D46DDCA47BEE4B0FA13C78C1A2AE7,
	Root__ctor_m0E0B7CED7B239A774BF98F310E5E14899979B087,
	Properties_get_icon_m0ED2EFC338132D7F5E1C06E4437A5326C48019A6,
	Properties_set_icon_mA3422628CD62248FD408232125FBA72239ABD587,
	Properties_get_id_m38C16F2FFD0D5EE18D3D48F3E4C2BB982ED76326,
	Properties_set_id_m1A7BB9D5936286D8CA094B181D08BA35664698E7,
	Properties_get_name_mC39F85DF9902CEDD867EF30FDEF4867147776F58,
	Properties_set_name_mCA53F5665A286632EB169B44B8B74A092AB1A27C,
	Properties_get_description_mDC4B9BB91CC94F5693FC36D5FC185B0DC8FF1D0F,
	Properties_set_description_m43B6DAB50FEAF05E01FEE48741A09656585E75CC,
	Properties_get_info_type_mA5B588015A3CF2B2F721769E1F25C7A0CE634363,
	Properties_set_info_type_m3407628F5EE1B0F4069CC7B78A1CCB7EBCE4116F,
	Properties_get_Name_m8B9089BD4B41621ACE464AA295CDFA34CE531DA0,
	Properties_set_Name_mEADD5273FD508CD164BABE98C32C0B78241353CA,
	Properties_get_range_mE5184ED0FD15844A94E5496B8190195D79BC7452,
	Properties_set_range_mD71067C1F9A4D8164B90A76110C793BF5895548E,
	Properties_get_start_m84CBDBCC6432A5E278C8CFCC71034D716BE16CBF,
	Properties_set_start_mA53934C23868FF9821F6FD05195D41E1EEBDECC6,
	Properties_get_stop_m3EF3602B3555AEED1BD8C88B3E84776FCCC6A353,
	Properties_set_stop_m46BC4581D1A9FAAE92A09ECA677E9DA424836F17,
	Properties_get_message1_mC3790FE29009909102057A90496AD8C062E99304,
	Properties_set_message1_mE539BD051D38F76DDE742EB92FE64BBB648435CF,
	Properties_get_message2_mD2784E85D92A4B6C2653F47E80B720C5DFCE6842,
	Properties_set_message2_mE0DB7C92F0D420B92FDABAB8B74AAFED25CF6957,
	Properties_get_risk_type_mBB5FB85D1A3665AD8A83075050C5964DBA074BC2,
	Properties_set_risk_type_m799A4701C2A79877468BA4D0CEE91D1C9B58A17D,
	Properties_get_movie_m8A23142B10AECCF9B51E9966F6D8F3880CFF4FFB,
	Properties_set_movie_mF0948C281C93C9D631910AFF2F84CD3B5F32A0E6,
	Properties_get_pic_type_mF6B0300ABDFB652656BEF2D9399FB8AA1686FCA2,
	Properties_set_pic_type_mEE0CE33FF1BA7CEE91DDDA9BB5CA0EA86809DC60,
	Properties_get_photo_mE89456BC3ADFFAD1676EC82ECEDA3E48F2A30972,
	Properties_set_photo_mAE38814DDAEDF4DFD5C0F9CB3FC59ED8F0339F14,
	Properties_get_water_level_mA2DCF204B67965D93B7AACD6BFDD29A50071C08F,
	Properties_set_water_level_mA5543B61DE90A04559CC17E2891543D6112B03C7,
	Properties__ctor_m76BDFD561290BCEF4A9FCC700412328A1BEC3803,
};
extern void ThermalStateChange_get_previousThermalState_mC8EA5EEB4C6D830DC54841A68E19E8D96D4C3B97_AdjustorThunk (void);
extern void ThermalStateChange_get_currentThermalState_mEC7E8C8DC348064CE3DB492C6C4207656A7F6FB8_AdjustorThunk (void);
extern void ThermalStateChange__ctor_m2F03DE32F10B12C5168435245D47607951A49515_AdjustorThunk (void);
extern void NamedPrefab__ctor_m4C5A1EAC954851954E10ADF5F4ED646967BAB252_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[4] = 
{
	{ 0x0600016C, ThermalStateChange_get_previousThermalState_mC8EA5EEB4C6D830DC54841A68E19E8D96D4C3B97_AdjustorThunk },
	{ 0x0600016D, ThermalStateChange_get_currentThermalState_mEC7E8C8DC348064CE3DB492C6C4207656A7F6FB8_AdjustorThunk },
	{ 0x0600016E, ThermalStateChange__ctor_m2F03DE32F10B12C5168435245D47607951A49515_AdjustorThunk },
	{ 0x060001C5, NamedPrefab__ctor_m4C5A1EAC954851954E10ADF5F4ED646967BAB252_AdjustorThunk },
};
static const int32_t s_InvokerIndices[1109] = 
{
	5515,
	5515,
	4321,
	4418,
	4321,
	5515,
	5515,
	5515,
	4418,
	5428,
	5473,
	5473,
	5515,
	4386,
	5515,
	5332,
	5428,
	5515,
	5428,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	4418,
	5515,
	5515,
	5428,
	5515,
	4386,
	5515,
	5332,
	5428,
	5515,
	5428,
	5515,
	5515,
	5515,
	5473,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	4067,
	4418,
	5515,
	5515,
	4418,
	5515,
	3171,
	3171,
	3171,
	3171,
	5515,
	5515,
	5515,
	5515,
	5515,
	5428,
	5428,
	4418,
	4418,
	5515,
	5515,
	4418,
	4067,
	4495,
	4432,
	5515,
	5515,
	5515,
	4386,
	5515,
	5332,
	5428,
	5515,
	5428,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	8766,
	4418,
	5515,
	5515,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5354,
	4349,
	5354,
	4349,
	5354,
	4349,
	5392,
	4386,
	5354,
	4349,
	5354,
	4349,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5392,
	4386,
	5349,
	4343,
	5349,
	4343,
	5428,
	4418,
	5428,
	4418,
	5392,
	4386,
	5332,
	4321,
	5515,
	6553,
	6930,
	6931,
	7446,
	8184,
	8184,
	5515,
	5354,
	4349,
	5354,
	4349,
	5332,
	5515,
	5515,
	5515,
	5428,
	4418,
	5428,
	4418,
	5392,
	4386,
	5515,
	5515,
	4301,
	4386,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5332,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5510,
	5515,
	5515,
	4418,
	4495,
	1451,
	4418,
	5515,
	5515,
	5510,
	5515,
	5515,
	4418,
	4495,
	1451,
	4418,
	5515,
	5392,
	4386,
	5392,
	4302,
	2315,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	4321,
	5515,
	5392,
	4386,
	4418,
	5428,
	5515,
	5515,
	8716,
	4418,
	8427,
	5515,
	5515,
	5428,
	4418,
	5515,
	5515,
	4288,
	4418,
	5515,
	5428,
	4418,
	5428,
	4418,
	5515,
	5515,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5515,
	8766,
	8766,
	8766,
	5515,
	5428,
	4418,
	4418,
	5515,
	5428,
	5515,
	5515,
	5515,
	5515,
	5515,
	5428,
	4418,
	5336,
	4326,
	5515,
	5428,
	4418,
	5515,
	5515,
	5515,
	5392,
	4386,
	5332,
	4321,
	5332,
	5515,
	4321,
	4321,
	5515,
	4418,
	4418,
	5515,
	1711,
	8718,
	5515,
	8766,
	8694,
	5515,
	5515,
	5515,
	8609,
	7122,
	5515,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5515,
	5515,
	2460,
	5515,
	5392,
	5515,
	5515,
	5515,
	5392,
	5392,
	2245,
	8716,
	5428,
	4418,
	5428,
	4418,
	5515,
	5515,
	5515,
	4606,
	4386,
	4321,
	4321,
	4321,
	4321,
	4321,
	4321,
	5515,
	5428,
	4418,
	5515,
	5515,
	4289,
	5515,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5428,
	4418,
	5428,
	4418,
	5515,
	5515,
	5428,
	4418,
	5428,
	4418,
	5515,
	5515,
	5515,
	4418,
	4312,
	5515,
	5428,
	4418,
	5515,
	4418,
	5515,
	5515,
	5428,
	4418,
	5428,
	4418,
	5473,
	4458,
	5325,
	4315,
	5515,
	5428,
	4418,
	5515,
	4418,
	5515,
	5515,
	5428,
	4418,
	5515,
	5515,
	5515,
	5515,
	5515,
	4312,
	4418,
	3929,
	2568,
	5515,
	2101,
	5428,
	4418,
	5428,
	4418,
	5515,
	4553,
	5515,
	8766,
	5428,
	4418,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	4296,
	4036,
	925,
	4418,
	4418,
	4418,
	5515,
	5428,
	4418,
	5473,
	4458,
	5515,
	5515,
	5428,
	4418,
	5332,
	4321,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5428,
	4418,
	5515,
	5515,
	5515,
	5515,
	5473,
	4458,
	5515,
	5515,
	5515,
	4299,
	4418,
	5515,
	8766,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5515,
	5515,
	5515,
	5515,
	5515,
	5428,
	5428,
	4314,
	5428,
	5332,
	5515,
	4418,
	7855,
	7855,
	7867,
	5515,
	5515,
	4386,
	5515,
	5332,
	5428,
	5515,
	5428,
	4386,
	5515,
	5332,
	5428,
	5515,
	5428,
	5428,
	4418,
	5515,
	5515,
	2478,
	3886,
	5515,
	5515,
	8766,
	5428,
	4418,
	5257,
	4247,
	5257,
	4247,
	5240,
	4229,
	5515,
	5515,
	5515,
	4289,
	5515,
	5428,
	4418,
	5428,
	4418,
	5515,
	5515,
	-1,
	5515,
	5428,
	4418,
	5515,
	4418,
	4418,
	3654,
	5515,
	5428,
	4418,
	4418,
	5515,
	5515,
	5515,
	5515,
	4386,
	5428,
	4418,
	5515,
	5515,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	7171,
	4289,
	5515,
	5428,
	4418,
	5515,
	4418,
	5515,
	5515,
	8766,
	1711,
	5515,
	5428,
	4418,
	5515,
	5515,
	4300,
	4418,
	5515,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5515,
	4418,
	4418,
	4418,
	4418,
	5515,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5473,
	4458,
	5473,
	4458,
	5515,
	5515,
	5515,
	5515,
	4289,
	1440,
	4418,
	5515,
	4418,
	5515,
	8766,
	5428,
	4418,
	5515,
	5515,
	5428,
	4418,
	5515,
	5515,
	4321,
	5515,
	5515,
	4292,
	5515,
	5515,
	5515,
	5428,
	4418,
	5515,
	5515,
	8766,
	5428,
	4418,
	5515,
	5515,
	4321,
	5515,
	5515,
	4292,
	5515,
	5515,
	5428,
	4418,
	5515,
	5515,
	4321,
	5515,
	5515,
	4292,
	5515,
	5515,
	5428,
	4418,
	5428,
	4418,
	5257,
	4247,
	5257,
	4247,
	5240,
	4229,
	5261,
	4255,
	5240,
	4229,
	5257,
	4247,
	5258,
	4248,
	5515,
	5515,
	5515,
	5515,
	4289,
	5515,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5515,
	5515,
	2060,
	-1,
	5515,
	5428,
	4418,
	5428,
	4418,
	5515,
	5515,
	4294,
	5515,
	5428,
	4418,
	5392,
	4386,
	5515,
	5515,
	8612,
	5515,
	8766,
	5428,
	4418,
	5441,
	4432,
	5515,
	5515,
	5515,
	8766,
	5428,
	4418,
	5428,
	4418,
	8612,
	8612,
	5515,
	5515,
	5515,
	8766,
	5428,
	4418,
	5428,
	4418,
	5515,
	3025,
	5515,
	5515,
	8766,
	5428,
	4418,
	5515,
	4321,
	5515,
	5515,
	5428,
	4418,
	5428,
	4418,
	5473,
	4458,
	5473,
	4458,
	5515,
	5473,
	4458,
	5515,
	5515,
	5515,
	5515,
	5428,
	4418,
	5428,
	4418,
	5473,
	4458,
	5473,
	4458,
	5515,
	5473,
	4458,
	5515,
	5515,
	5515,
	5515,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5515,
	2039,
	5515,
	5515,
	5515,
	8766,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	4418,
	5428,
	4321,
	5428,
	5515,
	5515,
	5515,
	4386,
	5515,
	5332,
	5428,
	5515,
	5428,
	4386,
	5515,
	5332,
	5428,
	5515,
	5428,
	5428,
	4418,
	5428,
	4418,
	5515,
	5515,
	4294,
	5515,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5515,
	8612,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	8716,
	8607,
	5428,
	4418,
	5515,
	5515,
	5515,
	5515,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5515,
	5515,
	5428,
	4418,
	5428,
	4418,
	5515,
	5515,
	5428,
	4418,
	5515,
	5515,
	4418,
	4418,
	5515,
	5428,
	4418,
	5428,
	4418,
	5515,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5515,
	5428,
	4418,
	5428,
	4418,
	5515,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5250,
	4239,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5250,
	4239,
	5428,
	4418,
	5428,
	4418,
	5428,
	4418,
	5246,
	4235,
	5515,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x06000248, { 0, 3 } },
	{ 0x06000302, { 3, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[6] = 
{
	{ (Il2CppRGCTXDataType)3, 26231 },
	{ (Il2CppRGCTXDataType)3, 26232 },
	{ (Il2CppRGCTXDataType)2, 110 },
	{ (Il2CppRGCTXDataType)3, 26233 },
	{ (Il2CppRGCTXDataType)3, 26234 },
	{ (Il2CppRGCTXDataType)2, 215 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	1109,
	s_methodPointers,
	4,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	6,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

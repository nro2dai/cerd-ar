﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct VirtFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};

// System.Collections.Generic.Dictionary`2<System.Int32,System.Globalization.CultureInfo>
struct Dictionary_2_t5B8303F2C9869A39ED3E03C0FBB09F817E479402;
// System.Collections.Generic.Dictionary`2<System.String,System.Globalization.CultureInfo>
struct Dictionary_2_t0015CBF964B0687CBB5ECFDDE06671A8F3DDE4BC;
// System.Collections.Generic.Dictionary`2<System.String,System.Resources.ResourceSet>
struct Dictionary_2_tF591ED968D904B93A92B04B711C65E797B9D6E5E;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.ArgumentException
struct ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00;
// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8;
// System.Reflection.Assembly
struct Assembly_t;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Globalization.Calendar
struct Calendar_t3D638AEAB45F029DF47138EDA4CF9A7CBBB1C32A;
// System.Device.Location.CivicAddress
struct CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5;
// System.Globalization.CompareInfo
struct CompareInfo_t4AB62EC32E8AF1E469E315620C7E3FB8B0CAE0C9;
// System.Globalization.CultureData
struct CultureData_t53CDF1C5F789A28897415891667799420D3C5529;
// System.Globalization.CultureInfo
struct CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98;
// System.Globalization.DateTimeFormatInfo
struct DateTimeFormatInfo_t0B9F6CA631A51CFC98A3C6031CF8069843137C90;
// System.Device.Location.GeoCoordinate
struct GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239;
// System.Collections.Hashtable
struct Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// System.IFormatProvider
struct IFormatProvider_tF2AECC4B14F41D36718920D67F930CED940412DF;
// System.Resources.IResourceGroveler
struct IResourceGroveler_tD738FE6B83F63AC66FDD73BCD3193016FDEBFAB0;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D;
// System.Resources.ResourceManager
struct ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A;
// System.Reflection.RuntimeAssembly
struct RuntimeAssembly_t799877C849878A70E10D25C690D7B0476DAF0B56;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// System.String
struct String_t;
// System.Globalization.TextInfo
struct TextInfo_tE823D0684BFE8B203501C9B2B38585E8F06E872C;
// System.Type
struct Type_t;
// System.Reflection.TypeInfo
struct TypeInfo_tFFBAC0D7187BFD2D25CC801679BC9645020EC04F;
// System.Version
struct Version_tBDAEDED25425A1D09910468B8BD1759115646E3C;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// System.Reflection.Assembly/ResolveEventHolder
struct ResolveEventHolder_tA37081FAEBE21D83D216225B4489BA8A37B4E13C;
// System.Resources.ResourceManager/CultureNameResourceSetPair
struct CultureNameResourceSetPair_t7DF2947B0015A29C8148DB0F32695ECB59369A84;

IL2CPP_EXTERN_C RuntimeClass* ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Resources_tBF455E9A126C8E65140A86AAC63C951FAFA7F4BA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* String_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Type_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral0F278FC1CBF98DA834E8A4EFCFD237AD661C1296;
IL2CPP_EXTERN_C String_t* _stringLiteral163B0FC24A83A793B3FCA4691A1D7CADC20A580B;
IL2CPP_EXTERN_C String_t* _stringLiteral25924FC7B1F14B4B0A771EB347F431419F4974AF;
IL2CPP_EXTERN_C String_t* _stringLiteral2D61429A77614470501E536C4B7A7CB204B9125B;
IL2CPP_EXTERN_C String_t* _stringLiteral3DCC6243286938BE75C3FA773B9BA71160A2E869;
IL2CPP_EXTERN_C String_t* _stringLiteral550F2AF0287E2D987375F2915E7039E01605DBB3;
IL2CPP_EXTERN_C String_t* _stringLiteral5D54E959817188DBAD9E65FA3DB55F06B70F5E3C;
IL2CPP_EXTERN_C String_t* _stringLiteral68E20F5AB9D9A2A1478AEBAC7130BC321E58EC43;
IL2CPP_EXTERN_C String_t* _stringLiteral6C7AA5B2326C47E24C9195E0B3482525705D5FFB;
IL2CPP_EXTERN_C String_t* _stringLiteral758733BDBED83CBFF4F635AC26CA92AAE477F75D;
IL2CPP_EXTERN_C String_t* _stringLiteral7E9C99BC533135F65AF9BE0E25DCB5A661F1347E;
IL2CPP_EXTERN_C String_t* _stringLiteral86BA2E873128DBB921A90301586F310302569533;
IL2CPP_EXTERN_C String_t* _stringLiteral89649263D26EA274EA8BFDD69ADFA4816B486F5A;
IL2CPP_EXTERN_C String_t* _stringLiteral9D71DE2C80BE06DE001DE3E9D7AC51E415CA1B9D;
IL2CPP_EXTERN_C String_t* _stringLiteralEA197A98747F049B4D33EBC845749AF3B8429912;
IL2CPP_EXTERN_C const RuntimeMethod* GeoCoordinate_GetDistanceTo_m08A40DE4071016346FC25592DC09337FB16348D7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeoCoordinate_set_Course_mFBEF26FB5685F62D306A039A19215BDA720A5491_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeoCoordinate_set_HorizontalAccuracy_m463649A918284904B8B464B36A805A0C41DD2037_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeoCoordinate_set_Latitude_m6C75A7DEA0B9C1AB51EC5E592B8649F2DA63D385_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeoCoordinate_set_Longitude_m78F2D97804483F51BD85805EDD5DB5C2575A7BE2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeoCoordinate_set_Speed_m4BC694CFDFF6339660846EC8C2205A21D69F1D19_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GeoCoordinate_set_VerticalAccuracy_m8D9AFF4893323688B2247834CD0680E5D751EFB1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeType* Resources_tBF455E9A126C8E65140A86AAC63C951FAFA7F4BA_0_0_0_var;
struct CultureData_t53CDF1C5F789A28897415891667799420D3C5529_marshaled_com;
struct CultureData_t53CDF1C5F789A28897415891667799420D3C5529_marshaled_pinvoke;
struct CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_marshaled_com;
struct CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_t6276D2354F70B053A80C2F37FB298E6D799A0BC6 
{
public:

public:
};


// System.Object

struct Il2CppArrayBounds;

// System.Array


// System.Device.Location.CivicAddress
struct CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5  : public RuntimeObject
{
public:
	// System.String System.Device.Location.CivicAddress::<AddressLine1>k__BackingField
	String_t* ___U3CAddressLine1U3Ek__BackingField_1;
	// System.String System.Device.Location.CivicAddress::<AddressLine2>k__BackingField
	String_t* ___U3CAddressLine2U3Ek__BackingField_2;
	// System.String System.Device.Location.CivicAddress::<Building>k__BackingField
	String_t* ___U3CBuildingU3Ek__BackingField_3;
	// System.String System.Device.Location.CivicAddress::<City>k__BackingField
	String_t* ___U3CCityU3Ek__BackingField_4;
	// System.String System.Device.Location.CivicAddress::<CountryRegion>k__BackingField
	String_t* ___U3CCountryRegionU3Ek__BackingField_5;
	// System.String System.Device.Location.CivicAddress::<FloorLevel>k__BackingField
	String_t* ___U3CFloorLevelU3Ek__BackingField_6;
	// System.String System.Device.Location.CivicAddress::<PostalCode>k__BackingField
	String_t* ___U3CPostalCodeU3Ek__BackingField_7;
	// System.String System.Device.Location.CivicAddress::<StateProvince>k__BackingField
	String_t* ___U3CStateProvinceU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CAddressLine1U3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5, ___U3CAddressLine1U3Ek__BackingField_1)); }
	inline String_t* get_U3CAddressLine1U3Ek__BackingField_1() const { return ___U3CAddressLine1U3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CAddressLine1U3Ek__BackingField_1() { return &___U3CAddressLine1U3Ek__BackingField_1; }
	inline void set_U3CAddressLine1U3Ek__BackingField_1(String_t* value)
	{
		___U3CAddressLine1U3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CAddressLine1U3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CAddressLine2U3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5, ___U3CAddressLine2U3Ek__BackingField_2)); }
	inline String_t* get_U3CAddressLine2U3Ek__BackingField_2() const { return ___U3CAddressLine2U3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CAddressLine2U3Ek__BackingField_2() { return &___U3CAddressLine2U3Ek__BackingField_2; }
	inline void set_U3CAddressLine2U3Ek__BackingField_2(String_t* value)
	{
		___U3CAddressLine2U3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CAddressLine2U3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CBuildingU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5, ___U3CBuildingU3Ek__BackingField_3)); }
	inline String_t* get_U3CBuildingU3Ek__BackingField_3() const { return ___U3CBuildingU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CBuildingU3Ek__BackingField_3() { return &___U3CBuildingU3Ek__BackingField_3; }
	inline void set_U3CBuildingU3Ek__BackingField_3(String_t* value)
	{
		___U3CBuildingU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CBuildingU3Ek__BackingField_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCityU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5, ___U3CCityU3Ek__BackingField_4)); }
	inline String_t* get_U3CCityU3Ek__BackingField_4() const { return ___U3CCityU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CCityU3Ek__BackingField_4() { return &___U3CCityU3Ek__BackingField_4; }
	inline void set_U3CCityU3Ek__BackingField_4(String_t* value)
	{
		___U3CCityU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CCityU3Ek__BackingField_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CCountryRegionU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5, ___U3CCountryRegionU3Ek__BackingField_5)); }
	inline String_t* get_U3CCountryRegionU3Ek__BackingField_5() const { return ___U3CCountryRegionU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CCountryRegionU3Ek__BackingField_5() { return &___U3CCountryRegionU3Ek__BackingField_5; }
	inline void set_U3CCountryRegionU3Ek__BackingField_5(String_t* value)
	{
		___U3CCountryRegionU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CCountryRegionU3Ek__BackingField_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CFloorLevelU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5, ___U3CFloorLevelU3Ek__BackingField_6)); }
	inline String_t* get_U3CFloorLevelU3Ek__BackingField_6() const { return ___U3CFloorLevelU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CFloorLevelU3Ek__BackingField_6() { return &___U3CFloorLevelU3Ek__BackingField_6; }
	inline void set_U3CFloorLevelU3Ek__BackingField_6(String_t* value)
	{
		___U3CFloorLevelU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CFloorLevelU3Ek__BackingField_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CPostalCodeU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5, ___U3CPostalCodeU3Ek__BackingField_7)); }
	inline String_t* get_U3CPostalCodeU3Ek__BackingField_7() const { return ___U3CPostalCodeU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CPostalCodeU3Ek__BackingField_7() { return &___U3CPostalCodeU3Ek__BackingField_7; }
	inline void set_U3CPostalCodeU3Ek__BackingField_7(String_t* value)
	{
		___U3CPostalCodeU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CPostalCodeU3Ek__BackingField_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3CStateProvinceU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5, ___U3CStateProvinceU3Ek__BackingField_8)); }
	inline String_t* get_U3CStateProvinceU3Ek__BackingField_8() const { return ___U3CStateProvinceU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CStateProvinceU3Ek__BackingField_8() { return &___U3CStateProvinceU3Ek__BackingField_8; }
	inline void set_U3CStateProvinceU3Ek__BackingField_8(String_t* value)
	{
		___U3CStateProvinceU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateProvinceU3Ek__BackingField_8), (void*)value);
	}
};

struct CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5_StaticFields
{
public:
	// System.Device.Location.CivicAddress System.Device.Location.CivicAddress::Unknown
	CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 * ___Unknown_0;

public:
	inline static int32_t get_offset_of_Unknown_0() { return static_cast<int32_t>(offsetof(CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5_StaticFields, ___Unknown_0)); }
	inline CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 * get_Unknown_0() const { return ___Unknown_0; }
	inline CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 ** get_address_of_Unknown_0() { return &___Unknown_0; }
	inline void set_Unknown_0(CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 * value)
	{
		___Unknown_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Unknown_0), (void*)value);
	}
};


// System.Globalization.CultureInfo
struct CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98  : public RuntimeObject
{
public:
	// System.Boolean System.Globalization.CultureInfo::m_isReadOnly
	bool ___m_isReadOnly_3;
	// System.Int32 System.Globalization.CultureInfo::cultureID
	int32_t ___cultureID_4;
	// System.Int32 System.Globalization.CultureInfo::parent_lcid
	int32_t ___parent_lcid_5;
	// System.Int32 System.Globalization.CultureInfo::datetime_index
	int32_t ___datetime_index_6;
	// System.Int32 System.Globalization.CultureInfo::number_index
	int32_t ___number_index_7;
	// System.Int32 System.Globalization.CultureInfo::default_calendar_type
	int32_t ___default_calendar_type_8;
	// System.Boolean System.Globalization.CultureInfo::m_useUserOverride
	bool ___m_useUserOverride_9;
	// System.Globalization.NumberFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::numInfo
	NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D * ___numInfo_10;
	// System.Globalization.DateTimeFormatInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::dateTimeInfo
	DateTimeFormatInfo_t0B9F6CA631A51CFC98A3C6031CF8069843137C90 * ___dateTimeInfo_11;
	// System.Globalization.TextInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::textInfo
	TextInfo_tE823D0684BFE8B203501C9B2B38585E8F06E872C * ___textInfo_12;
	// System.String System.Globalization.CultureInfo::m_name
	String_t* ___m_name_13;
	// System.String System.Globalization.CultureInfo::englishname
	String_t* ___englishname_14;
	// System.String System.Globalization.CultureInfo::nativename
	String_t* ___nativename_15;
	// System.String System.Globalization.CultureInfo::iso3lang
	String_t* ___iso3lang_16;
	// System.String System.Globalization.CultureInfo::iso2lang
	String_t* ___iso2lang_17;
	// System.String System.Globalization.CultureInfo::win3lang
	String_t* ___win3lang_18;
	// System.String System.Globalization.CultureInfo::territory
	String_t* ___territory_19;
	// System.String[] System.Globalization.CultureInfo::native_calendar_names
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___native_calendar_names_20;
	// System.Globalization.CompareInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::compareInfo
	CompareInfo_t4AB62EC32E8AF1E469E315620C7E3FB8B0CAE0C9 * ___compareInfo_21;
	// System.Void* System.Globalization.CultureInfo::textinfo_data
	void* ___textinfo_data_22;
	// System.Int32 System.Globalization.CultureInfo::m_dataItem
	int32_t ___m_dataItem_23;
	// System.Globalization.Calendar System.Globalization.CultureInfo::calendar
	Calendar_t3D638AEAB45F029DF47138EDA4CF9A7CBBB1C32A * ___calendar_24;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::parent_culture
	CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * ___parent_culture_25;
	// System.Boolean System.Globalization.CultureInfo::constructed
	bool ___constructed_26;
	// System.Byte[] System.Globalization.CultureInfo::cached_serialized_form
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___cached_serialized_form_27;
	// System.Globalization.CultureData System.Globalization.CultureInfo::m_cultureData
	CultureData_t53CDF1C5F789A28897415891667799420D3C5529 * ___m_cultureData_28;
	// System.Boolean System.Globalization.CultureInfo::m_isInherited
	bool ___m_isInherited_29;

public:
	inline static int32_t get_offset_of_m_isReadOnly_3() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___m_isReadOnly_3)); }
	inline bool get_m_isReadOnly_3() const { return ___m_isReadOnly_3; }
	inline bool* get_address_of_m_isReadOnly_3() { return &___m_isReadOnly_3; }
	inline void set_m_isReadOnly_3(bool value)
	{
		___m_isReadOnly_3 = value;
	}

	inline static int32_t get_offset_of_cultureID_4() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___cultureID_4)); }
	inline int32_t get_cultureID_4() const { return ___cultureID_4; }
	inline int32_t* get_address_of_cultureID_4() { return &___cultureID_4; }
	inline void set_cultureID_4(int32_t value)
	{
		___cultureID_4 = value;
	}

	inline static int32_t get_offset_of_parent_lcid_5() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___parent_lcid_5)); }
	inline int32_t get_parent_lcid_5() const { return ___parent_lcid_5; }
	inline int32_t* get_address_of_parent_lcid_5() { return &___parent_lcid_5; }
	inline void set_parent_lcid_5(int32_t value)
	{
		___parent_lcid_5 = value;
	}

	inline static int32_t get_offset_of_datetime_index_6() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___datetime_index_6)); }
	inline int32_t get_datetime_index_6() const { return ___datetime_index_6; }
	inline int32_t* get_address_of_datetime_index_6() { return &___datetime_index_6; }
	inline void set_datetime_index_6(int32_t value)
	{
		___datetime_index_6 = value;
	}

	inline static int32_t get_offset_of_number_index_7() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___number_index_7)); }
	inline int32_t get_number_index_7() const { return ___number_index_7; }
	inline int32_t* get_address_of_number_index_7() { return &___number_index_7; }
	inline void set_number_index_7(int32_t value)
	{
		___number_index_7 = value;
	}

	inline static int32_t get_offset_of_default_calendar_type_8() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___default_calendar_type_8)); }
	inline int32_t get_default_calendar_type_8() const { return ___default_calendar_type_8; }
	inline int32_t* get_address_of_default_calendar_type_8() { return &___default_calendar_type_8; }
	inline void set_default_calendar_type_8(int32_t value)
	{
		___default_calendar_type_8 = value;
	}

	inline static int32_t get_offset_of_m_useUserOverride_9() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___m_useUserOverride_9)); }
	inline bool get_m_useUserOverride_9() const { return ___m_useUserOverride_9; }
	inline bool* get_address_of_m_useUserOverride_9() { return &___m_useUserOverride_9; }
	inline void set_m_useUserOverride_9(bool value)
	{
		___m_useUserOverride_9 = value;
	}

	inline static int32_t get_offset_of_numInfo_10() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___numInfo_10)); }
	inline NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D * get_numInfo_10() const { return ___numInfo_10; }
	inline NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D ** get_address_of_numInfo_10() { return &___numInfo_10; }
	inline void set_numInfo_10(NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D * value)
	{
		___numInfo_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___numInfo_10), (void*)value);
	}

	inline static int32_t get_offset_of_dateTimeInfo_11() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___dateTimeInfo_11)); }
	inline DateTimeFormatInfo_t0B9F6CA631A51CFC98A3C6031CF8069843137C90 * get_dateTimeInfo_11() const { return ___dateTimeInfo_11; }
	inline DateTimeFormatInfo_t0B9F6CA631A51CFC98A3C6031CF8069843137C90 ** get_address_of_dateTimeInfo_11() { return &___dateTimeInfo_11; }
	inline void set_dateTimeInfo_11(DateTimeFormatInfo_t0B9F6CA631A51CFC98A3C6031CF8069843137C90 * value)
	{
		___dateTimeInfo_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dateTimeInfo_11), (void*)value);
	}

	inline static int32_t get_offset_of_textInfo_12() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___textInfo_12)); }
	inline TextInfo_tE823D0684BFE8B203501C9B2B38585E8F06E872C * get_textInfo_12() const { return ___textInfo_12; }
	inline TextInfo_tE823D0684BFE8B203501C9B2B38585E8F06E872C ** get_address_of_textInfo_12() { return &___textInfo_12; }
	inline void set_textInfo_12(TextInfo_tE823D0684BFE8B203501C9B2B38585E8F06E872C * value)
	{
		___textInfo_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textInfo_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_name_13() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___m_name_13)); }
	inline String_t* get_m_name_13() const { return ___m_name_13; }
	inline String_t** get_address_of_m_name_13() { return &___m_name_13; }
	inline void set_m_name_13(String_t* value)
	{
		___m_name_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_name_13), (void*)value);
	}

	inline static int32_t get_offset_of_englishname_14() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___englishname_14)); }
	inline String_t* get_englishname_14() const { return ___englishname_14; }
	inline String_t** get_address_of_englishname_14() { return &___englishname_14; }
	inline void set_englishname_14(String_t* value)
	{
		___englishname_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___englishname_14), (void*)value);
	}

	inline static int32_t get_offset_of_nativename_15() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___nativename_15)); }
	inline String_t* get_nativename_15() const { return ___nativename_15; }
	inline String_t** get_address_of_nativename_15() { return &___nativename_15; }
	inline void set_nativename_15(String_t* value)
	{
		___nativename_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___nativename_15), (void*)value);
	}

	inline static int32_t get_offset_of_iso3lang_16() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___iso3lang_16)); }
	inline String_t* get_iso3lang_16() const { return ___iso3lang_16; }
	inline String_t** get_address_of_iso3lang_16() { return &___iso3lang_16; }
	inline void set_iso3lang_16(String_t* value)
	{
		___iso3lang_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___iso3lang_16), (void*)value);
	}

	inline static int32_t get_offset_of_iso2lang_17() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___iso2lang_17)); }
	inline String_t* get_iso2lang_17() const { return ___iso2lang_17; }
	inline String_t** get_address_of_iso2lang_17() { return &___iso2lang_17; }
	inline void set_iso2lang_17(String_t* value)
	{
		___iso2lang_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___iso2lang_17), (void*)value);
	}

	inline static int32_t get_offset_of_win3lang_18() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___win3lang_18)); }
	inline String_t* get_win3lang_18() const { return ___win3lang_18; }
	inline String_t** get_address_of_win3lang_18() { return &___win3lang_18; }
	inline void set_win3lang_18(String_t* value)
	{
		___win3lang_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___win3lang_18), (void*)value);
	}

	inline static int32_t get_offset_of_territory_19() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___territory_19)); }
	inline String_t* get_territory_19() const { return ___territory_19; }
	inline String_t** get_address_of_territory_19() { return &___territory_19; }
	inline void set_territory_19(String_t* value)
	{
		___territory_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___territory_19), (void*)value);
	}

	inline static int32_t get_offset_of_native_calendar_names_20() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___native_calendar_names_20)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_native_calendar_names_20() const { return ___native_calendar_names_20; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_native_calendar_names_20() { return &___native_calendar_names_20; }
	inline void set_native_calendar_names_20(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___native_calendar_names_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_calendar_names_20), (void*)value);
	}

	inline static int32_t get_offset_of_compareInfo_21() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___compareInfo_21)); }
	inline CompareInfo_t4AB62EC32E8AF1E469E315620C7E3FB8B0CAE0C9 * get_compareInfo_21() const { return ___compareInfo_21; }
	inline CompareInfo_t4AB62EC32E8AF1E469E315620C7E3FB8B0CAE0C9 ** get_address_of_compareInfo_21() { return &___compareInfo_21; }
	inline void set_compareInfo_21(CompareInfo_t4AB62EC32E8AF1E469E315620C7E3FB8B0CAE0C9 * value)
	{
		___compareInfo_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___compareInfo_21), (void*)value);
	}

	inline static int32_t get_offset_of_textinfo_data_22() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___textinfo_data_22)); }
	inline void* get_textinfo_data_22() const { return ___textinfo_data_22; }
	inline void** get_address_of_textinfo_data_22() { return &___textinfo_data_22; }
	inline void set_textinfo_data_22(void* value)
	{
		___textinfo_data_22 = value;
	}

	inline static int32_t get_offset_of_m_dataItem_23() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___m_dataItem_23)); }
	inline int32_t get_m_dataItem_23() const { return ___m_dataItem_23; }
	inline int32_t* get_address_of_m_dataItem_23() { return &___m_dataItem_23; }
	inline void set_m_dataItem_23(int32_t value)
	{
		___m_dataItem_23 = value;
	}

	inline static int32_t get_offset_of_calendar_24() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___calendar_24)); }
	inline Calendar_t3D638AEAB45F029DF47138EDA4CF9A7CBBB1C32A * get_calendar_24() const { return ___calendar_24; }
	inline Calendar_t3D638AEAB45F029DF47138EDA4CF9A7CBBB1C32A ** get_address_of_calendar_24() { return &___calendar_24; }
	inline void set_calendar_24(Calendar_t3D638AEAB45F029DF47138EDA4CF9A7CBBB1C32A * value)
	{
		___calendar_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___calendar_24), (void*)value);
	}

	inline static int32_t get_offset_of_parent_culture_25() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___parent_culture_25)); }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * get_parent_culture_25() const { return ___parent_culture_25; }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 ** get_address_of_parent_culture_25() { return &___parent_culture_25; }
	inline void set_parent_culture_25(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * value)
	{
		___parent_culture_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_culture_25), (void*)value);
	}

	inline static int32_t get_offset_of_constructed_26() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___constructed_26)); }
	inline bool get_constructed_26() const { return ___constructed_26; }
	inline bool* get_address_of_constructed_26() { return &___constructed_26; }
	inline void set_constructed_26(bool value)
	{
		___constructed_26 = value;
	}

	inline static int32_t get_offset_of_cached_serialized_form_27() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___cached_serialized_form_27)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_cached_serialized_form_27() const { return ___cached_serialized_form_27; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_cached_serialized_form_27() { return &___cached_serialized_form_27; }
	inline void set_cached_serialized_form_27(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___cached_serialized_form_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cached_serialized_form_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_cultureData_28() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___m_cultureData_28)); }
	inline CultureData_t53CDF1C5F789A28897415891667799420D3C5529 * get_m_cultureData_28() const { return ___m_cultureData_28; }
	inline CultureData_t53CDF1C5F789A28897415891667799420D3C5529 ** get_address_of_m_cultureData_28() { return &___m_cultureData_28; }
	inline void set_m_cultureData_28(CultureData_t53CDF1C5F789A28897415891667799420D3C5529 * value)
	{
		___m_cultureData_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_cultureData_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_isInherited_29() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98, ___m_isInherited_29)); }
	inline bool get_m_isInherited_29() const { return ___m_isInherited_29; }
	inline bool* get_address_of_m_isInherited_29() { return &___m_isInherited_29; }
	inline void set_m_isInherited_29(bool value)
	{
		___m_isInherited_29 = value;
	}
};

struct CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields
{
public:
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::invariant_culture_info
	CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * ___invariant_culture_info_0;
	// System.Object System.Globalization.CultureInfo::shared_table_lock
	RuntimeObject * ___shared_table_lock_1;
	// System.Globalization.CultureInfo System.Globalization.CultureInfo::default_current_culture
	CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * ___default_current_culture_2;
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::s_DefaultThreadCurrentUICulture
	CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * ___s_DefaultThreadCurrentUICulture_33;
	// System.Globalization.CultureInfo modreq(System.Runtime.CompilerServices.IsVolatile) System.Globalization.CultureInfo::s_DefaultThreadCurrentCulture
	CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * ___s_DefaultThreadCurrentCulture_34;
	// System.Collections.Generic.Dictionary`2<System.Int32,System.Globalization.CultureInfo> System.Globalization.CultureInfo::shared_by_number
	Dictionary_2_t5B8303F2C9869A39ED3E03C0FBB09F817E479402 * ___shared_by_number_35;
	// System.Collections.Generic.Dictionary`2<System.String,System.Globalization.CultureInfo> System.Globalization.CultureInfo::shared_by_name
	Dictionary_2_t0015CBF964B0687CBB5ECFDDE06671A8F3DDE4BC * ___shared_by_name_36;
	// System.Boolean System.Globalization.CultureInfo::IsTaiwanSku
	bool ___IsTaiwanSku_37;

public:
	inline static int32_t get_offset_of_invariant_culture_info_0() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields, ___invariant_culture_info_0)); }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * get_invariant_culture_info_0() const { return ___invariant_culture_info_0; }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 ** get_address_of_invariant_culture_info_0() { return &___invariant_culture_info_0; }
	inline void set_invariant_culture_info_0(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * value)
	{
		___invariant_culture_info_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___invariant_culture_info_0), (void*)value);
	}

	inline static int32_t get_offset_of_shared_table_lock_1() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields, ___shared_table_lock_1)); }
	inline RuntimeObject * get_shared_table_lock_1() const { return ___shared_table_lock_1; }
	inline RuntimeObject ** get_address_of_shared_table_lock_1() { return &___shared_table_lock_1; }
	inline void set_shared_table_lock_1(RuntimeObject * value)
	{
		___shared_table_lock_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___shared_table_lock_1), (void*)value);
	}

	inline static int32_t get_offset_of_default_current_culture_2() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields, ___default_current_culture_2)); }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * get_default_current_culture_2() const { return ___default_current_culture_2; }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 ** get_address_of_default_current_culture_2() { return &___default_current_culture_2; }
	inline void set_default_current_culture_2(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * value)
	{
		___default_current_culture_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___default_current_culture_2), (void*)value);
	}

	inline static int32_t get_offset_of_s_DefaultThreadCurrentUICulture_33() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields, ___s_DefaultThreadCurrentUICulture_33)); }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * get_s_DefaultThreadCurrentUICulture_33() const { return ___s_DefaultThreadCurrentUICulture_33; }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 ** get_address_of_s_DefaultThreadCurrentUICulture_33() { return &___s_DefaultThreadCurrentUICulture_33; }
	inline void set_s_DefaultThreadCurrentUICulture_33(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * value)
	{
		___s_DefaultThreadCurrentUICulture_33 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultThreadCurrentUICulture_33), (void*)value);
	}

	inline static int32_t get_offset_of_s_DefaultThreadCurrentCulture_34() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields, ___s_DefaultThreadCurrentCulture_34)); }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * get_s_DefaultThreadCurrentCulture_34() const { return ___s_DefaultThreadCurrentCulture_34; }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 ** get_address_of_s_DefaultThreadCurrentCulture_34() { return &___s_DefaultThreadCurrentCulture_34; }
	inline void set_s_DefaultThreadCurrentCulture_34(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * value)
	{
		___s_DefaultThreadCurrentCulture_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultThreadCurrentCulture_34), (void*)value);
	}

	inline static int32_t get_offset_of_shared_by_number_35() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields, ___shared_by_number_35)); }
	inline Dictionary_2_t5B8303F2C9869A39ED3E03C0FBB09F817E479402 * get_shared_by_number_35() const { return ___shared_by_number_35; }
	inline Dictionary_2_t5B8303F2C9869A39ED3E03C0FBB09F817E479402 ** get_address_of_shared_by_number_35() { return &___shared_by_number_35; }
	inline void set_shared_by_number_35(Dictionary_2_t5B8303F2C9869A39ED3E03C0FBB09F817E479402 * value)
	{
		___shared_by_number_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___shared_by_number_35), (void*)value);
	}

	inline static int32_t get_offset_of_shared_by_name_36() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields, ___shared_by_name_36)); }
	inline Dictionary_2_t0015CBF964B0687CBB5ECFDDE06671A8F3DDE4BC * get_shared_by_name_36() const { return ___shared_by_name_36; }
	inline Dictionary_2_t0015CBF964B0687CBB5ECFDDE06671A8F3DDE4BC ** get_address_of_shared_by_name_36() { return &___shared_by_name_36; }
	inline void set_shared_by_name_36(Dictionary_2_t0015CBF964B0687CBB5ECFDDE06671A8F3DDE4BC * value)
	{
		___shared_by_name_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___shared_by_name_36), (void*)value);
	}

	inline static int32_t get_offset_of_IsTaiwanSku_37() { return static_cast<int32_t>(offsetof(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_StaticFields, ___IsTaiwanSku_37)); }
	inline bool get_IsTaiwanSku_37() const { return ___IsTaiwanSku_37; }
	inline bool* get_address_of_IsTaiwanSku_37() { return &___IsTaiwanSku_37; }
	inline void set_IsTaiwanSku_37(bool value)
	{
		___IsTaiwanSku_37 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Globalization.CultureInfo
struct CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_marshaled_pinvoke
{
	int32_t ___m_isReadOnly_3;
	int32_t ___cultureID_4;
	int32_t ___parent_lcid_5;
	int32_t ___datetime_index_6;
	int32_t ___number_index_7;
	int32_t ___default_calendar_type_8;
	int32_t ___m_useUserOverride_9;
	NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D * ___numInfo_10;
	DateTimeFormatInfo_t0B9F6CA631A51CFC98A3C6031CF8069843137C90 * ___dateTimeInfo_11;
	TextInfo_tE823D0684BFE8B203501C9B2B38585E8F06E872C * ___textInfo_12;
	char* ___m_name_13;
	char* ___englishname_14;
	char* ___nativename_15;
	char* ___iso3lang_16;
	char* ___iso2lang_17;
	char* ___win3lang_18;
	char* ___territory_19;
	char** ___native_calendar_names_20;
	CompareInfo_t4AB62EC32E8AF1E469E315620C7E3FB8B0CAE0C9 * ___compareInfo_21;
	void* ___textinfo_data_22;
	int32_t ___m_dataItem_23;
	Calendar_t3D638AEAB45F029DF47138EDA4CF9A7CBBB1C32A * ___calendar_24;
	CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_marshaled_pinvoke* ___parent_culture_25;
	int32_t ___constructed_26;
	Il2CppSafeArray/*NONE*/* ___cached_serialized_form_27;
	CultureData_t53CDF1C5F789A28897415891667799420D3C5529_marshaled_pinvoke* ___m_cultureData_28;
	int32_t ___m_isInherited_29;
};
// Native definition for COM marshalling of System.Globalization.CultureInfo
struct CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_marshaled_com
{
	int32_t ___m_isReadOnly_3;
	int32_t ___cultureID_4;
	int32_t ___parent_lcid_5;
	int32_t ___datetime_index_6;
	int32_t ___number_index_7;
	int32_t ___default_calendar_type_8;
	int32_t ___m_useUserOverride_9;
	NumberFormatInfo_t58780B43B6A840C38FD10C50CDFE2128884CAD1D * ___numInfo_10;
	DateTimeFormatInfo_t0B9F6CA631A51CFC98A3C6031CF8069843137C90 * ___dateTimeInfo_11;
	TextInfo_tE823D0684BFE8B203501C9B2B38585E8F06E872C * ___textInfo_12;
	Il2CppChar* ___m_name_13;
	Il2CppChar* ___englishname_14;
	Il2CppChar* ___nativename_15;
	Il2CppChar* ___iso3lang_16;
	Il2CppChar* ___iso2lang_17;
	Il2CppChar* ___win3lang_18;
	Il2CppChar* ___territory_19;
	Il2CppChar** ___native_calendar_names_20;
	CompareInfo_t4AB62EC32E8AF1E469E315620C7E3FB8B0CAE0C9 * ___compareInfo_21;
	void* ___textinfo_data_22;
	int32_t ___m_dataItem_23;
	Calendar_t3D638AEAB45F029DF47138EDA4CF9A7CBBB1C32A * ___calendar_24;
	CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_marshaled_com* ___parent_culture_25;
	int32_t ___constructed_26;
	Il2CppSafeArray/*NONE*/* ___cached_serialized_form_27;
	CultureData_t53CDF1C5F789A28897415891667799420D3C5529_marshaled_com* ___m_cultureData_28;
	int32_t ___m_isInherited_29;
};

// System.Device.Location.GeoCoordinate
struct GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239  : public RuntimeObject
{
public:
	// System.Device.Location.CivicAddress System.Device.Location.GeoCoordinate::m_address
	CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 * ___m_address_0;
	// System.Double System.Device.Location.GeoCoordinate::m_altitude
	double ___m_altitude_1;
	// System.Double System.Device.Location.GeoCoordinate::m_course
	double ___m_course_2;
	// System.Double System.Device.Location.GeoCoordinate::m_horizontalAccuracy
	double ___m_horizontalAccuracy_3;
	// System.Double System.Device.Location.GeoCoordinate::m_latitude
	double ___m_latitude_4;
	// System.Double System.Device.Location.GeoCoordinate::m_longitude
	double ___m_longitude_5;
	// System.Double System.Device.Location.GeoCoordinate::m_speed
	double ___m_speed_6;
	// System.Double System.Device.Location.GeoCoordinate::m_verticalAccuracy
	double ___m_verticalAccuracy_7;

public:
	inline static int32_t get_offset_of_m_address_0() { return static_cast<int32_t>(offsetof(GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239, ___m_address_0)); }
	inline CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 * get_m_address_0() const { return ___m_address_0; }
	inline CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 ** get_address_of_m_address_0() { return &___m_address_0; }
	inline void set_m_address_0(CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 * value)
	{
		___m_address_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_address_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_altitude_1() { return static_cast<int32_t>(offsetof(GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239, ___m_altitude_1)); }
	inline double get_m_altitude_1() const { return ___m_altitude_1; }
	inline double* get_address_of_m_altitude_1() { return &___m_altitude_1; }
	inline void set_m_altitude_1(double value)
	{
		___m_altitude_1 = value;
	}

	inline static int32_t get_offset_of_m_course_2() { return static_cast<int32_t>(offsetof(GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239, ___m_course_2)); }
	inline double get_m_course_2() const { return ___m_course_2; }
	inline double* get_address_of_m_course_2() { return &___m_course_2; }
	inline void set_m_course_2(double value)
	{
		___m_course_2 = value;
	}

	inline static int32_t get_offset_of_m_horizontalAccuracy_3() { return static_cast<int32_t>(offsetof(GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239, ___m_horizontalAccuracy_3)); }
	inline double get_m_horizontalAccuracy_3() const { return ___m_horizontalAccuracy_3; }
	inline double* get_address_of_m_horizontalAccuracy_3() { return &___m_horizontalAccuracy_3; }
	inline void set_m_horizontalAccuracy_3(double value)
	{
		___m_horizontalAccuracy_3 = value;
	}

	inline static int32_t get_offset_of_m_latitude_4() { return static_cast<int32_t>(offsetof(GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239, ___m_latitude_4)); }
	inline double get_m_latitude_4() const { return ___m_latitude_4; }
	inline double* get_address_of_m_latitude_4() { return &___m_latitude_4; }
	inline void set_m_latitude_4(double value)
	{
		___m_latitude_4 = value;
	}

	inline static int32_t get_offset_of_m_longitude_5() { return static_cast<int32_t>(offsetof(GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239, ___m_longitude_5)); }
	inline double get_m_longitude_5() const { return ___m_longitude_5; }
	inline double* get_address_of_m_longitude_5() { return &___m_longitude_5; }
	inline void set_m_longitude_5(double value)
	{
		___m_longitude_5 = value;
	}

	inline static int32_t get_offset_of_m_speed_6() { return static_cast<int32_t>(offsetof(GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239, ___m_speed_6)); }
	inline double get_m_speed_6() const { return ___m_speed_6; }
	inline double* get_address_of_m_speed_6() { return &___m_speed_6; }
	inline void set_m_speed_6(double value)
	{
		___m_speed_6 = value;
	}

	inline static int32_t get_offset_of_m_verticalAccuracy_7() { return static_cast<int32_t>(offsetof(GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239, ___m_verticalAccuracy_7)); }
	inline double get_m_verticalAccuracy_7() const { return ___m_verticalAccuracy_7; }
	inline double* get_address_of_m_verticalAccuracy_7() { return &___m_verticalAccuracy_7; }
	inline void set_m_verticalAccuracy_7(double value)
	{
		___m_verticalAccuracy_7 = value;
	}
};

struct GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239_StaticFields
{
public:
	// System.Device.Location.GeoCoordinate System.Device.Location.GeoCoordinate::Unknown
	GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * ___Unknown_8;

public:
	inline static int32_t get_offset_of_Unknown_8() { return static_cast<int32_t>(offsetof(GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239_StaticFields, ___Unknown_8)); }
	inline GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * get_Unknown_8() const { return ___Unknown_8; }
	inline GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 ** get_address_of_Unknown_8() { return &___Unknown_8; }
	inline void set_Unknown_8(GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * value)
	{
		___Unknown_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Unknown_8), (void*)value);
	}
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.Device.Properties.Resources
struct Resources_tBF455E9A126C8E65140A86AAC63C951FAFA7F4BA  : public RuntimeObject
{
public:

public:
};

struct Resources_tBF455E9A126C8E65140A86AAC63C951FAFA7F4BA_StaticFields
{
public:
	// System.Resources.ResourceManager System.Device.Properties.Resources::resourceMan
	ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A * ___resourceMan_0;
	// System.Globalization.CultureInfo System.Device.Properties.Resources::resourceCulture
	CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * ___resourceCulture_1;

public:
	inline static int32_t get_offset_of_resourceMan_0() { return static_cast<int32_t>(offsetof(Resources_tBF455E9A126C8E65140A86AAC63C951FAFA7F4BA_StaticFields, ___resourceMan_0)); }
	inline ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A * get_resourceMan_0() const { return ___resourceMan_0; }
	inline ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A ** get_address_of_resourceMan_0() { return &___resourceMan_0; }
	inline void set_resourceMan_0(ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A * value)
	{
		___resourceMan_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___resourceMan_0), (void*)value);
	}

	inline static int32_t get_offset_of_resourceCulture_1() { return static_cast<int32_t>(offsetof(Resources_tBF455E9A126C8E65140A86AAC63C951FAFA7F4BA_StaticFields, ___resourceCulture_1)); }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * get_resourceCulture_1() const { return ___resourceCulture_1; }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 ** get_address_of_resourceCulture_1() { return &___resourceCulture_1; }
	inline void set_resourceCulture_1(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * value)
	{
		___resourceCulture_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___resourceCulture_1), (void*)value);
	}
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Double
struct Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.Assembly
struct Assembly_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Reflection.Assembly::_mono_assembly
	intptr_t ____mono_assembly_0;
	// System.Reflection.Assembly/ResolveEventHolder System.Reflection.Assembly::resolve_event_holder
	ResolveEventHolder_tA37081FAEBE21D83D216225B4489BA8A37B4E13C * ___resolve_event_holder_1;
	// System.Object System.Reflection.Assembly::_evidence
	RuntimeObject * ____evidence_2;
	// System.Object System.Reflection.Assembly::_minimum
	RuntimeObject * ____minimum_3;
	// System.Object System.Reflection.Assembly::_optional
	RuntimeObject * ____optional_4;
	// System.Object System.Reflection.Assembly::_refuse
	RuntimeObject * ____refuse_5;
	// System.Object System.Reflection.Assembly::_granted
	RuntimeObject * ____granted_6;
	// System.Object System.Reflection.Assembly::_denied
	RuntimeObject * ____denied_7;
	// System.Boolean System.Reflection.Assembly::fromByteArray
	bool ___fromByteArray_8;
	// System.String System.Reflection.Assembly::assemblyName
	String_t* ___assemblyName_9;

public:
	inline static int32_t get_offset_of__mono_assembly_0() { return static_cast<int32_t>(offsetof(Assembly_t, ____mono_assembly_0)); }
	inline intptr_t get__mono_assembly_0() const { return ____mono_assembly_0; }
	inline intptr_t* get_address_of__mono_assembly_0() { return &____mono_assembly_0; }
	inline void set__mono_assembly_0(intptr_t value)
	{
		____mono_assembly_0 = value;
	}

	inline static int32_t get_offset_of_resolve_event_holder_1() { return static_cast<int32_t>(offsetof(Assembly_t, ___resolve_event_holder_1)); }
	inline ResolveEventHolder_tA37081FAEBE21D83D216225B4489BA8A37B4E13C * get_resolve_event_holder_1() const { return ___resolve_event_holder_1; }
	inline ResolveEventHolder_tA37081FAEBE21D83D216225B4489BA8A37B4E13C ** get_address_of_resolve_event_holder_1() { return &___resolve_event_holder_1; }
	inline void set_resolve_event_holder_1(ResolveEventHolder_tA37081FAEBE21D83D216225B4489BA8A37B4E13C * value)
	{
		___resolve_event_holder_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___resolve_event_holder_1), (void*)value);
	}

	inline static int32_t get_offset_of__evidence_2() { return static_cast<int32_t>(offsetof(Assembly_t, ____evidence_2)); }
	inline RuntimeObject * get__evidence_2() const { return ____evidence_2; }
	inline RuntimeObject ** get_address_of__evidence_2() { return &____evidence_2; }
	inline void set__evidence_2(RuntimeObject * value)
	{
		____evidence_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____evidence_2), (void*)value);
	}

	inline static int32_t get_offset_of__minimum_3() { return static_cast<int32_t>(offsetof(Assembly_t, ____minimum_3)); }
	inline RuntimeObject * get__minimum_3() const { return ____minimum_3; }
	inline RuntimeObject ** get_address_of__minimum_3() { return &____minimum_3; }
	inline void set__minimum_3(RuntimeObject * value)
	{
		____minimum_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____minimum_3), (void*)value);
	}

	inline static int32_t get_offset_of__optional_4() { return static_cast<int32_t>(offsetof(Assembly_t, ____optional_4)); }
	inline RuntimeObject * get__optional_4() const { return ____optional_4; }
	inline RuntimeObject ** get_address_of__optional_4() { return &____optional_4; }
	inline void set__optional_4(RuntimeObject * value)
	{
		____optional_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____optional_4), (void*)value);
	}

	inline static int32_t get_offset_of__refuse_5() { return static_cast<int32_t>(offsetof(Assembly_t, ____refuse_5)); }
	inline RuntimeObject * get__refuse_5() const { return ____refuse_5; }
	inline RuntimeObject ** get_address_of__refuse_5() { return &____refuse_5; }
	inline void set__refuse_5(RuntimeObject * value)
	{
		____refuse_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____refuse_5), (void*)value);
	}

	inline static int32_t get_offset_of__granted_6() { return static_cast<int32_t>(offsetof(Assembly_t, ____granted_6)); }
	inline RuntimeObject * get__granted_6() const { return ____granted_6; }
	inline RuntimeObject ** get_address_of__granted_6() { return &____granted_6; }
	inline void set__granted_6(RuntimeObject * value)
	{
		____granted_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____granted_6), (void*)value);
	}

	inline static int32_t get_offset_of__denied_7() { return static_cast<int32_t>(offsetof(Assembly_t, ____denied_7)); }
	inline RuntimeObject * get__denied_7() const { return ____denied_7; }
	inline RuntimeObject ** get_address_of__denied_7() { return &____denied_7; }
	inline void set__denied_7(RuntimeObject * value)
	{
		____denied_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____denied_7), (void*)value);
	}

	inline static int32_t get_offset_of_fromByteArray_8() { return static_cast<int32_t>(offsetof(Assembly_t, ___fromByteArray_8)); }
	inline bool get_fromByteArray_8() const { return ___fromByteArray_8; }
	inline bool* get_address_of_fromByteArray_8() { return &___fromByteArray_8; }
	inline void set_fromByteArray_8(bool value)
	{
		___fromByteArray_8 = value;
	}

	inline static int32_t get_offset_of_assemblyName_9() { return static_cast<int32_t>(offsetof(Assembly_t, ___assemblyName_9)); }
	inline String_t* get_assemblyName_9() const { return ___assemblyName_9; }
	inline String_t** get_address_of_assemblyName_9() { return &___assemblyName_9; }
	inline void set_assemblyName_9(String_t* value)
	{
		___assemblyName_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___assemblyName_9), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Reflection.Assembly
struct Assembly_t_marshaled_pinvoke
{
	intptr_t ____mono_assembly_0;
	ResolveEventHolder_tA37081FAEBE21D83D216225B4489BA8A37B4E13C * ___resolve_event_holder_1;
	Il2CppIUnknown* ____evidence_2;
	Il2CppIUnknown* ____minimum_3;
	Il2CppIUnknown* ____optional_4;
	Il2CppIUnknown* ____refuse_5;
	Il2CppIUnknown* ____granted_6;
	Il2CppIUnknown* ____denied_7;
	int32_t ___fromByteArray_8;
	char* ___assemblyName_9;
};
// Native definition for COM marshalling of System.Reflection.Assembly
struct Assembly_t_marshaled_com
{
	intptr_t ____mono_assembly_0;
	ResolveEventHolder_tA37081FAEBE21D83D216225B4489BA8A37B4E13C * ___resolve_event_holder_1;
	Il2CppIUnknown* ____evidence_2;
	Il2CppIUnknown* ____minimum_3;
	Il2CppIUnknown* ____optional_4;
	Il2CppIUnknown* ____refuse_5;
	Il2CppIUnknown* ____granted_6;
	Il2CppIUnknown* ____denied_7;
	int32_t ___fromByteArray_8;
	Il2CppChar* ___assemblyName_9;
};

// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Exception
struct Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Resources.UltimateResourceFallbackLocation
struct UltimateResourceFallbackLocation_tA4EBEA627CD0C386314EBB60D7A4225C435D0F0B 
{
public:
	// System.Int32 System.Resources.UltimateResourceFallbackLocation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UltimateResourceFallbackLocation_tA4EBEA627CD0C386314EBB60D7A4225C435D0F0B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Resources.ResourceManager
struct ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A  : public RuntimeObject
{
public:
	// System.String System.Resources.ResourceManager::BaseNameField
	String_t* ___BaseNameField_0;
	// System.Collections.Hashtable System.Resources.ResourceManager::ResourceSets
	Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * ___ResourceSets_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.Resources.ResourceSet> System.Resources.ResourceManager::_resourceSets
	Dictionary_2_tF591ED968D904B93A92B04B711C65E797B9D6E5E * ____resourceSets_2;
	// System.String System.Resources.ResourceManager::moduleDir
	String_t* ___moduleDir_3;
	// System.Reflection.Assembly System.Resources.ResourceManager::MainAssembly
	Assembly_t * ___MainAssembly_4;
	// System.Type System.Resources.ResourceManager::_locationInfo
	Type_t * ____locationInfo_5;
	// System.Type System.Resources.ResourceManager::_userResourceSet
	Type_t * ____userResourceSet_6;
	// System.Globalization.CultureInfo System.Resources.ResourceManager::_neutralResourcesCulture
	CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * ____neutralResourcesCulture_7;
	// System.Resources.ResourceManager/CultureNameResourceSetPair System.Resources.ResourceManager::_lastUsedResourceCache
	CultureNameResourceSetPair_t7DF2947B0015A29C8148DB0F32695ECB59369A84 * ____lastUsedResourceCache_8;
	// System.Boolean System.Resources.ResourceManager::_ignoreCase
	bool ____ignoreCase_9;
	// System.Boolean System.Resources.ResourceManager::UseManifest
	bool ___UseManifest_10;
	// System.Boolean System.Resources.ResourceManager::UseSatelliteAssem
	bool ___UseSatelliteAssem_11;
	// System.Resources.UltimateResourceFallbackLocation System.Resources.ResourceManager::_fallbackLoc
	int32_t ____fallbackLoc_12;
	// System.Version System.Resources.ResourceManager::_satelliteContractVersion
	Version_tBDAEDED25425A1D09910468B8BD1759115646E3C * ____satelliteContractVersion_13;
	// System.Boolean System.Resources.ResourceManager::_lookedForSatelliteContractVersion
	bool ____lookedForSatelliteContractVersion_14;
	// System.Reflection.Assembly System.Resources.ResourceManager::_callingAssembly
	Assembly_t * ____callingAssembly_15;
	// System.Reflection.RuntimeAssembly System.Resources.ResourceManager::m_callingAssembly
	RuntimeAssembly_t799877C849878A70E10D25C690D7B0476DAF0B56 * ___m_callingAssembly_16;
	// System.Resources.IResourceGroveler System.Resources.ResourceManager::resourceGroveler
	RuntimeObject* ___resourceGroveler_17;

public:
	inline static int32_t get_offset_of_BaseNameField_0() { return static_cast<int32_t>(offsetof(ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A, ___BaseNameField_0)); }
	inline String_t* get_BaseNameField_0() const { return ___BaseNameField_0; }
	inline String_t** get_address_of_BaseNameField_0() { return &___BaseNameField_0; }
	inline void set_BaseNameField_0(String_t* value)
	{
		___BaseNameField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BaseNameField_0), (void*)value);
	}

	inline static int32_t get_offset_of_ResourceSets_1() { return static_cast<int32_t>(offsetof(ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A, ___ResourceSets_1)); }
	inline Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * get_ResourceSets_1() const { return ___ResourceSets_1; }
	inline Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC ** get_address_of_ResourceSets_1() { return &___ResourceSets_1; }
	inline void set_ResourceSets_1(Hashtable_t7565AB92A12227AD5BADD6911F10D87EE52509AC * value)
	{
		___ResourceSets_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ResourceSets_1), (void*)value);
	}

	inline static int32_t get_offset_of__resourceSets_2() { return static_cast<int32_t>(offsetof(ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A, ____resourceSets_2)); }
	inline Dictionary_2_tF591ED968D904B93A92B04B711C65E797B9D6E5E * get__resourceSets_2() const { return ____resourceSets_2; }
	inline Dictionary_2_tF591ED968D904B93A92B04B711C65E797B9D6E5E ** get_address_of__resourceSets_2() { return &____resourceSets_2; }
	inline void set__resourceSets_2(Dictionary_2_tF591ED968D904B93A92B04B711C65E797B9D6E5E * value)
	{
		____resourceSets_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____resourceSets_2), (void*)value);
	}

	inline static int32_t get_offset_of_moduleDir_3() { return static_cast<int32_t>(offsetof(ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A, ___moduleDir_3)); }
	inline String_t* get_moduleDir_3() const { return ___moduleDir_3; }
	inline String_t** get_address_of_moduleDir_3() { return &___moduleDir_3; }
	inline void set_moduleDir_3(String_t* value)
	{
		___moduleDir_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___moduleDir_3), (void*)value);
	}

	inline static int32_t get_offset_of_MainAssembly_4() { return static_cast<int32_t>(offsetof(ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A, ___MainAssembly_4)); }
	inline Assembly_t * get_MainAssembly_4() const { return ___MainAssembly_4; }
	inline Assembly_t ** get_address_of_MainAssembly_4() { return &___MainAssembly_4; }
	inline void set_MainAssembly_4(Assembly_t * value)
	{
		___MainAssembly_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MainAssembly_4), (void*)value);
	}

	inline static int32_t get_offset_of__locationInfo_5() { return static_cast<int32_t>(offsetof(ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A, ____locationInfo_5)); }
	inline Type_t * get__locationInfo_5() const { return ____locationInfo_5; }
	inline Type_t ** get_address_of__locationInfo_5() { return &____locationInfo_5; }
	inline void set__locationInfo_5(Type_t * value)
	{
		____locationInfo_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____locationInfo_5), (void*)value);
	}

	inline static int32_t get_offset_of__userResourceSet_6() { return static_cast<int32_t>(offsetof(ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A, ____userResourceSet_6)); }
	inline Type_t * get__userResourceSet_6() const { return ____userResourceSet_6; }
	inline Type_t ** get_address_of__userResourceSet_6() { return &____userResourceSet_6; }
	inline void set__userResourceSet_6(Type_t * value)
	{
		____userResourceSet_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____userResourceSet_6), (void*)value);
	}

	inline static int32_t get_offset_of__neutralResourcesCulture_7() { return static_cast<int32_t>(offsetof(ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A, ____neutralResourcesCulture_7)); }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * get__neutralResourcesCulture_7() const { return ____neutralResourcesCulture_7; }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 ** get_address_of__neutralResourcesCulture_7() { return &____neutralResourcesCulture_7; }
	inline void set__neutralResourcesCulture_7(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * value)
	{
		____neutralResourcesCulture_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____neutralResourcesCulture_7), (void*)value);
	}

	inline static int32_t get_offset_of__lastUsedResourceCache_8() { return static_cast<int32_t>(offsetof(ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A, ____lastUsedResourceCache_8)); }
	inline CultureNameResourceSetPair_t7DF2947B0015A29C8148DB0F32695ECB59369A84 * get__lastUsedResourceCache_8() const { return ____lastUsedResourceCache_8; }
	inline CultureNameResourceSetPair_t7DF2947B0015A29C8148DB0F32695ECB59369A84 ** get_address_of__lastUsedResourceCache_8() { return &____lastUsedResourceCache_8; }
	inline void set__lastUsedResourceCache_8(CultureNameResourceSetPair_t7DF2947B0015A29C8148DB0F32695ECB59369A84 * value)
	{
		____lastUsedResourceCache_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____lastUsedResourceCache_8), (void*)value);
	}

	inline static int32_t get_offset_of__ignoreCase_9() { return static_cast<int32_t>(offsetof(ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A, ____ignoreCase_9)); }
	inline bool get__ignoreCase_9() const { return ____ignoreCase_9; }
	inline bool* get_address_of__ignoreCase_9() { return &____ignoreCase_9; }
	inline void set__ignoreCase_9(bool value)
	{
		____ignoreCase_9 = value;
	}

	inline static int32_t get_offset_of_UseManifest_10() { return static_cast<int32_t>(offsetof(ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A, ___UseManifest_10)); }
	inline bool get_UseManifest_10() const { return ___UseManifest_10; }
	inline bool* get_address_of_UseManifest_10() { return &___UseManifest_10; }
	inline void set_UseManifest_10(bool value)
	{
		___UseManifest_10 = value;
	}

	inline static int32_t get_offset_of_UseSatelliteAssem_11() { return static_cast<int32_t>(offsetof(ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A, ___UseSatelliteAssem_11)); }
	inline bool get_UseSatelliteAssem_11() const { return ___UseSatelliteAssem_11; }
	inline bool* get_address_of_UseSatelliteAssem_11() { return &___UseSatelliteAssem_11; }
	inline void set_UseSatelliteAssem_11(bool value)
	{
		___UseSatelliteAssem_11 = value;
	}

	inline static int32_t get_offset_of__fallbackLoc_12() { return static_cast<int32_t>(offsetof(ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A, ____fallbackLoc_12)); }
	inline int32_t get__fallbackLoc_12() const { return ____fallbackLoc_12; }
	inline int32_t* get_address_of__fallbackLoc_12() { return &____fallbackLoc_12; }
	inline void set__fallbackLoc_12(int32_t value)
	{
		____fallbackLoc_12 = value;
	}

	inline static int32_t get_offset_of__satelliteContractVersion_13() { return static_cast<int32_t>(offsetof(ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A, ____satelliteContractVersion_13)); }
	inline Version_tBDAEDED25425A1D09910468B8BD1759115646E3C * get__satelliteContractVersion_13() const { return ____satelliteContractVersion_13; }
	inline Version_tBDAEDED25425A1D09910468B8BD1759115646E3C ** get_address_of__satelliteContractVersion_13() { return &____satelliteContractVersion_13; }
	inline void set__satelliteContractVersion_13(Version_tBDAEDED25425A1D09910468B8BD1759115646E3C * value)
	{
		____satelliteContractVersion_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____satelliteContractVersion_13), (void*)value);
	}

	inline static int32_t get_offset_of__lookedForSatelliteContractVersion_14() { return static_cast<int32_t>(offsetof(ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A, ____lookedForSatelliteContractVersion_14)); }
	inline bool get__lookedForSatelliteContractVersion_14() const { return ____lookedForSatelliteContractVersion_14; }
	inline bool* get_address_of__lookedForSatelliteContractVersion_14() { return &____lookedForSatelliteContractVersion_14; }
	inline void set__lookedForSatelliteContractVersion_14(bool value)
	{
		____lookedForSatelliteContractVersion_14 = value;
	}

	inline static int32_t get_offset_of__callingAssembly_15() { return static_cast<int32_t>(offsetof(ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A, ____callingAssembly_15)); }
	inline Assembly_t * get__callingAssembly_15() const { return ____callingAssembly_15; }
	inline Assembly_t ** get_address_of__callingAssembly_15() { return &____callingAssembly_15; }
	inline void set__callingAssembly_15(Assembly_t * value)
	{
		____callingAssembly_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____callingAssembly_15), (void*)value);
	}

	inline static int32_t get_offset_of_m_callingAssembly_16() { return static_cast<int32_t>(offsetof(ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A, ___m_callingAssembly_16)); }
	inline RuntimeAssembly_t799877C849878A70E10D25C690D7B0476DAF0B56 * get_m_callingAssembly_16() const { return ___m_callingAssembly_16; }
	inline RuntimeAssembly_t799877C849878A70E10D25C690D7B0476DAF0B56 ** get_address_of_m_callingAssembly_16() { return &___m_callingAssembly_16; }
	inline void set_m_callingAssembly_16(RuntimeAssembly_t799877C849878A70E10D25C690D7B0476DAF0B56 * value)
	{
		___m_callingAssembly_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_callingAssembly_16), (void*)value);
	}

	inline static int32_t get_offset_of_resourceGroveler_17() { return static_cast<int32_t>(offsetof(ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A, ___resourceGroveler_17)); }
	inline RuntimeObject* get_resourceGroveler_17() const { return ___resourceGroveler_17; }
	inline RuntimeObject** get_address_of_resourceGroveler_17() { return &___resourceGroveler_17; }
	inline void set_resourceGroveler_17(RuntimeObject* value)
	{
		___resourceGroveler_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___resourceGroveler_17), (void*)value);
	}
};

struct ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A_StaticFields
{
public:
	// System.Int32 System.Resources.ResourceManager::MagicNumber
	int32_t ___MagicNumber_18;
	// System.Int32 System.Resources.ResourceManager::HeaderVersionNumber
	int32_t ___HeaderVersionNumber_19;
	// System.Type System.Resources.ResourceManager::_minResourceSet
	Type_t * ____minResourceSet_20;
	// System.String System.Resources.ResourceManager::ResReaderTypeName
	String_t* ___ResReaderTypeName_21;
	// System.String System.Resources.ResourceManager::ResSetTypeName
	String_t* ___ResSetTypeName_22;
	// System.String System.Resources.ResourceManager::MscorlibName
	String_t* ___MscorlibName_23;
	// System.Int32 System.Resources.ResourceManager::DEBUG
	int32_t ___DEBUG_24;

public:
	inline static int32_t get_offset_of_MagicNumber_18() { return static_cast<int32_t>(offsetof(ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A_StaticFields, ___MagicNumber_18)); }
	inline int32_t get_MagicNumber_18() const { return ___MagicNumber_18; }
	inline int32_t* get_address_of_MagicNumber_18() { return &___MagicNumber_18; }
	inline void set_MagicNumber_18(int32_t value)
	{
		___MagicNumber_18 = value;
	}

	inline static int32_t get_offset_of_HeaderVersionNumber_19() { return static_cast<int32_t>(offsetof(ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A_StaticFields, ___HeaderVersionNumber_19)); }
	inline int32_t get_HeaderVersionNumber_19() const { return ___HeaderVersionNumber_19; }
	inline int32_t* get_address_of_HeaderVersionNumber_19() { return &___HeaderVersionNumber_19; }
	inline void set_HeaderVersionNumber_19(int32_t value)
	{
		___HeaderVersionNumber_19 = value;
	}

	inline static int32_t get_offset_of__minResourceSet_20() { return static_cast<int32_t>(offsetof(ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A_StaticFields, ____minResourceSet_20)); }
	inline Type_t * get__minResourceSet_20() const { return ____minResourceSet_20; }
	inline Type_t ** get_address_of__minResourceSet_20() { return &____minResourceSet_20; }
	inline void set__minResourceSet_20(Type_t * value)
	{
		____minResourceSet_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____minResourceSet_20), (void*)value);
	}

	inline static int32_t get_offset_of_ResReaderTypeName_21() { return static_cast<int32_t>(offsetof(ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A_StaticFields, ___ResReaderTypeName_21)); }
	inline String_t* get_ResReaderTypeName_21() const { return ___ResReaderTypeName_21; }
	inline String_t** get_address_of_ResReaderTypeName_21() { return &___ResReaderTypeName_21; }
	inline void set_ResReaderTypeName_21(String_t* value)
	{
		___ResReaderTypeName_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ResReaderTypeName_21), (void*)value);
	}

	inline static int32_t get_offset_of_ResSetTypeName_22() { return static_cast<int32_t>(offsetof(ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A_StaticFields, ___ResSetTypeName_22)); }
	inline String_t* get_ResSetTypeName_22() const { return ___ResSetTypeName_22; }
	inline String_t** get_address_of_ResSetTypeName_22() { return &___ResSetTypeName_22; }
	inline void set_ResSetTypeName_22(String_t* value)
	{
		___ResSetTypeName_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ResSetTypeName_22), (void*)value);
	}

	inline static int32_t get_offset_of_MscorlibName_23() { return static_cast<int32_t>(offsetof(ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A_StaticFields, ___MscorlibName_23)); }
	inline String_t* get_MscorlibName_23() const { return ___MscorlibName_23; }
	inline String_t** get_address_of_MscorlibName_23() { return &___MscorlibName_23; }
	inline void set_MscorlibName_23(String_t* value)
	{
		___MscorlibName_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MscorlibName_23), (void*)value);
	}

	inline static int32_t get_offset_of_DEBUG_24() { return static_cast<int32_t>(offsetof(ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A_StaticFields, ___DEBUG_24)); }
	inline int32_t get_DEBUG_24() const { return ___DEBUG_24; }
	inline int32_t* get_address_of_DEBUG_24() { return &___DEBUG_24; }
	inline void set_DEBUG_24(int32_t value)
	{
		___DEBUG_24 = value;
	}
};


// System.SystemException
struct SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// System.ArgumentException
struct ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:
	// System.String System.ArgumentException::m_paramName
	String_t* ___m_paramName_17;

public:
	inline static int32_t get_offset_of_m_paramName_17() { return static_cast<int32_t>(offsetof(ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00, ___m_paramName_17)); }
	inline String_t* get_m_paramName_17() const { return ___m_paramName_17; }
	inline String_t** get_address_of_m_paramName_17() { return &___m_paramName_17; }
	inline void set_m_paramName_17(String_t* value)
	{
		___m_paramName_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_paramName_17), (void*)value);
	}
};


// System.Reflection.TypeInfo
struct TypeInfo_tFFBAC0D7187BFD2D25CC801679BC9645020EC04F  : public Type_t
{
public:

public:
};


// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8  : public ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00
{
public:
	// System.Object System.ArgumentOutOfRangeException::m_actualValue
	RuntimeObject * ___m_actualValue_19;

public:
	inline static int32_t get_offset_of_m_actualValue_19() { return static_cast<int32_t>(offsetof(ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8, ___m_actualValue_19)); }
	inline RuntimeObject * get_m_actualValue_19() const { return ___m_actualValue_19; }
	inline RuntimeObject ** get_address_of_m_actualValue_19() { return &___m_actualValue_19; }
	inline void set_m_actualValue_19(RuntimeObject * value)
	{
		___m_actualValue_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_actualValue_19), (void*)value);
	}
};

struct ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_StaticFields
{
public:
	// System.String modreq(System.Runtime.CompilerServices.IsVolatile) System.ArgumentOutOfRangeException::_rangeMessage
	String_t* ____rangeMessage_18;

public:
	inline static int32_t get_offset_of__rangeMessage_18() { return static_cast<int32_t>(offsetof(ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_StaticFields, ____rangeMessage_18)); }
	inline String_t* get__rangeMessage_18() const { return ____rangeMessage_18; }
	inline String_t** get_address_of__rangeMessage_18() { return &____rangeMessage_18; }
	inline void set__rangeMessage_18(String_t* value)
	{
		____rangeMessage_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rangeMessage_18), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void System.Device.Location.CivicAddress::set_AddressLine1(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CivicAddress_set_AddressLine1_m7332B7407FE83DDEAF587C582F543D3939BD052C_inline (CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void System.Device.Location.CivicAddress::set_AddressLine2(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CivicAddress_set_AddressLine2_m78C8EA756514DF24B11385575A65C6270E5CDF37_inline (CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void System.Device.Location.CivicAddress::set_Building(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CivicAddress_set_Building_m0CFF68430E65DBDD6EA41E2CE13775AF120D59CC_inline (CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void System.Device.Location.CivicAddress::set_City(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CivicAddress_set_City_m56F2D9F77FFE8779858128DC13144C0EFA307227_inline (CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void System.Device.Location.CivicAddress::set_CountryRegion(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CivicAddress_set_CountryRegion_m599882DD6A8BB8F1BFA8DB1F2B1E6E413CAA0248_inline (CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void System.Device.Location.CivicAddress::set_FloorLevel(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CivicAddress_set_FloorLevel_m2763C9EEA07862C276A52B72CC3F154D35115F76_inline (CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void System.Device.Location.CivicAddress::set_PostalCode(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CivicAddress_set_PostalCode_m96F6E5FB7575AD8EC32BFC3FAE070217BC8D3D9F_inline (CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void System.Device.Location.CivicAddress::set_StateProvince(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CivicAddress_set_StateProvince_m4616D216C2DC3CFC119662B46F799E642E6CF2CF_inline (CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void System.Device.Location.CivicAddress::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CivicAddress__ctor_m7F59ABD0BBD00DA425E9005597A21B6278CDB6D1 (CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 * __this, const RuntimeMethod* method);
// System.Void System.Device.Location.GeoCoordinate::.ctor(System.Double,System.Double,System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeoCoordinate__ctor_mCE00B122E0E2CEE90C1238BC9798EC215BACA029 (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * __this, double ___latitude0, double ___longitude1, double ___altitude2, const RuntimeMethod* method);
// System.Void System.Device.Location.GeoCoordinate::.ctor(System.Double,System.Double,System.Double,System.Double,System.Double,System.Double,System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeoCoordinate__ctor_mA1FC781769EC725D1314778E2BC45D0E3A237453 (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * __this, double ___latitude0, double ___longitude1, double ___altitude2, double ___horizontalAccuracy3, double ___verticalAccuracy4, double ___speed5, double ___course6, const RuntimeMethod* method);
// System.Void System.Device.Location.GeoCoordinate::set_Latitude(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeoCoordinate_set_Latitude_m6C75A7DEA0B9C1AB51EC5E592B8649F2DA63D385 (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * __this, double ___value0, const RuntimeMethod* method);
// System.Void System.Device.Location.GeoCoordinate::set_Longitude(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeoCoordinate_set_Longitude_m78F2D97804483F51BD85805EDD5DB5C2575A7BE2 (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * __this, double ___value0, const RuntimeMethod* method);
// System.Void System.Device.Location.GeoCoordinate::set_Altitude(System.Double)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void GeoCoordinate_set_Altitude_m19DDB5D204D693C21DF883A59A6223AE8DB14738_inline (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * __this, double ___value0, const RuntimeMethod* method);
// System.Void System.Device.Location.GeoCoordinate::set_HorizontalAccuracy(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeoCoordinate_set_HorizontalAccuracy_m463649A918284904B8B464B36A805A0C41DD2037 (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * __this, double ___value0, const RuntimeMethod* method);
// System.Void System.Device.Location.GeoCoordinate::set_VerticalAccuracy(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeoCoordinate_set_VerticalAccuracy_m8D9AFF4893323688B2247834CD0680E5D751EFB1 (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * __this, double ___value0, const RuntimeMethod* method);
// System.Void System.Device.Location.GeoCoordinate::set_Speed(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeoCoordinate_set_Speed_m4BC694CFDFF6339660846EC8C2205A21D69F1D19 (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * __this, double ___value0, const RuntimeMethod* method);
// System.Void System.Device.Location.GeoCoordinate::set_Course(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeoCoordinate_set_Course_mFBEF26FB5685F62D306A039A19215BDA720A5491 (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * __this, double ___value0, const RuntimeMethod* method);
// System.Double System.Device.Location.GeoCoordinate::get_Latitude()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double GeoCoordinate_get_Latitude_mD487CAE6C0383D9C4D0CCE2BE85B38C0E68B1888_inline (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * __this, const RuntimeMethod* method);
// System.Boolean System.Double::Equals(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Double_Equals_m8C171E8C7F556087E707D1396DB29D1D8B21A46B (double* __this, double ___obj0, const RuntimeMethod* method);
// System.Double System.Device.Location.GeoCoordinate::get_Longitude()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double GeoCoordinate_get_Longitude_m2EC0079450A74DB09F7F2B68A49D80390716711A_inline (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * __this, const RuntimeMethod* method);
// System.Boolean System.Device.Location.GeoCoordinate::Equals(System.Device.Location.GeoCoordinate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GeoCoordinate_Equals_m4A5B248447B89BD204A4A7777DE0137657B29A50 (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * __this, GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * ___other0, const RuntimeMethod* method);
// System.Boolean System.Object::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_Equals_m6E8965587E01BDEC1CDF25DE69A64253CD97EF85 (RuntimeObject * __this, RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.Boolean System.Double::IsNaN(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Double_IsNaN_m94415C98C2D7DCAA32A82E1911AC13958AAD4347 (double ___d0, const RuntimeMethod* method);
// System.String System.Device.Properties.Resources::get_Argument_LatitudeOrLongitudeIsNotANumber()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Resources_get_Argument_LatitudeOrLongitudeIsNotANumber_mEF075CA186C22FBFB71141EC851E0958EDC41214 (const RuntimeMethod* method);
// System.Void System.ArgumentException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentException__ctor_m2D35EAD113C2ADC99EB17B940A2097A93FD23EFC (ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Double System.Math::Pow(System.Double,System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double Math_Pow_mC2C8700DAAD1316AA457A1D271F78CDF0D61AC2F (double ___x0, double ___y1, const RuntimeMethod* method);
// System.Int32 System.Double::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Double_GetHashCode_m33CB20AA5674C6F4367B7B08340B33FB979F9F39 (double* __this, const RuntimeMethod* method);
// System.Boolean System.Device.Location.GeoCoordinate::op_Equality(System.Device.Location.GeoCoordinate,System.Device.Location.GeoCoordinate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GeoCoordinate_op_Equality_m96E1FD033870A064072EF0AFC6BCF7C8DF0AAA53 (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * ___left0, GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * ___right1, const RuntimeMethod* method);
// System.Globalization.CultureInfo System.Globalization.CultureInfo::get_InvariantCulture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * CultureInfo_get_InvariantCulture_m9FAAFAF8A00091EE1FCB7098AD3F163ECDF02164 (const RuntimeMethod* method);
// System.String System.Double::ToString(System.String,System.IFormatProvider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Double_ToString_mFF1DAF2003FC7096C54C5A2685F85082220E330B (double* __this, String_t* ___format0, RuntimeObject* ___provider1, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44 (String_t* ___str00, String_t* ___str11, String_t* ___str22, const RuntimeMethod* method);
// System.String System.Device.Properties.Resources::get_Argument_MustBeInRangeZeroTo360()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Resources_get_Argument_MustBeInRangeZeroTo360_m921B2506D02D201C26D9D02990B26312464F687C (const RuntimeMethod* method);
// System.Void System.ArgumentOutOfRangeException::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentOutOfRangeException__ctor_mE43AFC74F5F3932913C023A04B24905E093C5005 (ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 * __this, String_t* ___paramName0, String_t* ___message1, const RuntimeMethod* method);
// System.String System.Device.Properties.Resources::get_Argument_MustBeNonNegative()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Resources_get_Argument_MustBeNonNegative_m3ECFCC0A8407832F903B12FC0C891338C8D6E7FD (const RuntimeMethod* method);
// System.String System.Device.Properties.Resources::get_Argument_MustBeInRangeNegative90to90()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Resources_get_Argument_MustBeInRangeNegative90to90_m62EE743532EF0BDC91D4676912FBF9CC82880CF7 (const RuntimeMethod* method);
// System.String System.Device.Properties.Resources::get_Argument_MustBeInRangeNegative180To180()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Resources_get_Argument_MustBeInRangeNegative180To180_m6FE557A9143F65CA218C0E2500F29B2444D0C654 (const RuntimeMethod* method);
// System.Void System.Device.Location.GeoCoordinate::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeoCoordinate__ctor_mFF7956F6041D18D6CF94986C4501E12A40DF7CDB (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * __this, const RuntimeMethod* method);
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E (RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ___handle0, const RuntimeMethod* method);
// System.Reflection.TypeInfo System.Reflection.IntrospectionExtensions::GetTypeInfo(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TypeInfo_tFFBAC0D7187BFD2D25CC801679BC9645020EC04F * IntrospectionExtensions_GetTypeInfo_m77034F8576BE695819427C13103C591277C1B636 (Type_t * ___type0, const RuntimeMethod* method);
// System.Void System.Resources.ResourceManager::.ctor(System.String,System.Reflection.Assembly)
IL2CPP_EXTERN_C IL2CPP_NO_INLINE IL2CPP_METHOD_ATTR void ResourceManager__ctor_m71706EDDD65E34D84909C9C65591C6FD7C6D95A2 (ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A * __this, String_t* ___baseName0, Assembly_t * ___assembly1, const RuntimeMethod* method);
// System.Resources.ResourceManager System.Device.Properties.Resources::get_ResourceManager()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A * Resources_get_ResourceManager_m4392F8971FB189F0151B830C4E8641CBD6B63B1F (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Device.Location.CivicAddress::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CivicAddress__ctor_m7F59ABD0BBD00DA425E9005597A21B6278CDB6D1 (CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&String_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		CivicAddress_set_AddressLine1_m7332B7407FE83DDEAF587C582F543D3939BD052C_inline(__this, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		CivicAddress_set_AddressLine2_m78C8EA756514DF24B11385575A65C6270E5CDF37_inline(__this, L_1, /*hidden argument*/NULL);
		String_t* L_2 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		CivicAddress_set_Building_m0CFF68430E65DBDD6EA41E2CE13775AF120D59CC_inline(__this, L_2, /*hidden argument*/NULL);
		String_t* L_3 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		CivicAddress_set_City_m56F2D9F77FFE8779858128DC13144C0EFA307227_inline(__this, L_3, /*hidden argument*/NULL);
		String_t* L_4 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		CivicAddress_set_CountryRegion_m599882DD6A8BB8F1BFA8DB1F2B1E6E413CAA0248_inline(__this, L_4, /*hidden argument*/NULL);
		String_t* L_5 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		CivicAddress_set_FloorLevel_m2763C9EEA07862C276A52B72CC3F154D35115F76_inline(__this, L_5, /*hidden argument*/NULL);
		String_t* L_6 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		CivicAddress_set_PostalCode_m96F6E5FB7575AD8EC32BFC3FAE070217BC8D3D9F_inline(__this, L_6, /*hidden argument*/NULL);
		String_t* L_7 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		CivicAddress_set_StateProvince_m4616D216C2DC3CFC119662B46F799E642E6CF2CF_inline(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Device.Location.CivicAddress::set_AddressLine1(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CivicAddress_set_AddressLine1_m7332B7407FE83DDEAF587C582F543D3939BD052C (CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CAddressLine1U3Ek__BackingField_1(L_0);
		return;
	}
}
// System.Void System.Device.Location.CivicAddress::set_AddressLine2(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CivicAddress_set_AddressLine2_m78C8EA756514DF24B11385575A65C6270E5CDF37 (CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CAddressLine2U3Ek__BackingField_2(L_0);
		return;
	}
}
// System.Void System.Device.Location.CivicAddress::set_Building(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CivicAddress_set_Building_m0CFF68430E65DBDD6EA41E2CE13775AF120D59CC (CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CBuildingU3Ek__BackingField_3(L_0);
		return;
	}
}
// System.Void System.Device.Location.CivicAddress::set_City(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CivicAddress_set_City_m56F2D9F77FFE8779858128DC13144C0EFA307227 (CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCityU3Ek__BackingField_4(L_0);
		return;
	}
}
// System.Void System.Device.Location.CivicAddress::set_CountryRegion(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CivicAddress_set_CountryRegion_m599882DD6A8BB8F1BFA8DB1F2B1E6E413CAA0248 (CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCountryRegionU3Ek__BackingField_5(L_0);
		return;
	}
}
// System.Void System.Device.Location.CivicAddress::set_FloorLevel(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CivicAddress_set_FloorLevel_m2763C9EEA07862C276A52B72CC3F154D35115F76 (CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CFloorLevelU3Ek__BackingField_6(L_0);
		return;
	}
}
// System.Void System.Device.Location.CivicAddress::set_PostalCode(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CivicAddress_set_PostalCode_m96F6E5FB7575AD8EC32BFC3FAE070217BC8D3D9F (CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPostalCodeU3Ek__BackingField_7(L_0);
		return;
	}
}
// System.Void System.Device.Location.CivicAddress::set_StateProvince(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CivicAddress_set_StateProvince_m4616D216C2DC3CFC119662B46F799E642E6CF2CF (CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CStateProvinceU3Ek__BackingField_8(L_0);
		return;
	}
}
// System.Void System.Device.Location.CivicAddress::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CivicAddress__cctor_m875CE2561270EC9FCBAEFA674A0240A962F30199 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 * L_0 = (CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 *)il2cpp_codegen_object_new(CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5_il2cpp_TypeInfo_var);
		CivicAddress__ctor_m7F59ABD0BBD00DA425E9005597A21B6278CDB6D1(L_0, /*hidden argument*/NULL);
		((CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5_StaticFields*)il2cpp_codegen_static_fields_for(CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5_il2cpp_TypeInfo_var))->set_Unknown_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Device.Location.GeoCoordinate::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeoCoordinate__ctor_mFF7956F6041D18D6CF94986C4501E12A40DF7CDB (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		__this->set_m_latitude_4((std::numeric_limits<double>::quiet_NaN()));
		__this->set_m_longitude_5((std::numeric_limits<double>::quiet_NaN()));
		__this->set_m_altitude_1((std::numeric_limits<double>::quiet_NaN()));
		__this->set_m_verticalAccuracy_7((std::numeric_limits<double>::quiet_NaN()));
		__this->set_m_horizontalAccuracy_3((std::numeric_limits<double>::quiet_NaN()));
		__this->set_m_speed_6((std::numeric_limits<double>::quiet_NaN()));
		__this->set_m_course_2((std::numeric_limits<double>::quiet_NaN()));
		IL2CPP_RUNTIME_CLASS_INIT(CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5_il2cpp_TypeInfo_var);
		CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 * L_0 = ((CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5_StaticFields*)il2cpp_codegen_static_fields_for(CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5_il2cpp_TypeInfo_var))->get_Unknown_0();
		__this->set_m_address_0(L_0);
		return;
	}
}
// System.Void System.Device.Location.GeoCoordinate::.ctor(System.Double,System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeoCoordinate__ctor_mF5ECBAD1850E5C9468C77126EDD3B45F93526E14 (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * __this, double ___latitude0, double ___longitude1, const RuntimeMethod* method)
{
	{
		double L_0 = ___latitude0;
		double L_1 = ___longitude1;
		GeoCoordinate__ctor_mCE00B122E0E2CEE90C1238BC9798EC215BACA029(__this, L_0, L_1, (std::numeric_limits<double>::quiet_NaN()), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Device.Location.GeoCoordinate::.ctor(System.Double,System.Double,System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeoCoordinate__ctor_mCE00B122E0E2CEE90C1238BC9798EC215BACA029 (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * __this, double ___latitude0, double ___longitude1, double ___altitude2, const RuntimeMethod* method)
{
	{
		double L_0 = ___latitude0;
		double L_1 = ___longitude1;
		double L_2 = ___altitude2;
		GeoCoordinate__ctor_mA1FC781769EC725D1314778E2BC45D0E3A237453(__this, L_0, L_1, L_2, (std::numeric_limits<double>::quiet_NaN()), (std::numeric_limits<double>::quiet_NaN()), (std::numeric_limits<double>::quiet_NaN()), (std::numeric_limits<double>::quiet_NaN()), /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Device.Location.GeoCoordinate::.ctor(System.Double,System.Double,System.Double,System.Double,System.Double,System.Double,System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeoCoordinate__ctor_mA1FC781769EC725D1314778E2BC45D0E3A237453 (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * __this, double ___latitude0, double ___longitude1, double ___altitude2, double ___horizontalAccuracy3, double ___verticalAccuracy4, double ___speed5, double ___course6, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		__this->set_m_latitude_4((std::numeric_limits<double>::quiet_NaN()));
		__this->set_m_longitude_5((std::numeric_limits<double>::quiet_NaN()));
		__this->set_m_altitude_1((std::numeric_limits<double>::quiet_NaN()));
		__this->set_m_verticalAccuracy_7((std::numeric_limits<double>::quiet_NaN()));
		__this->set_m_horizontalAccuracy_3((std::numeric_limits<double>::quiet_NaN()));
		__this->set_m_speed_6((std::numeric_limits<double>::quiet_NaN()));
		__this->set_m_course_2((std::numeric_limits<double>::quiet_NaN()));
		IL2CPP_RUNTIME_CLASS_INIT(CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5_il2cpp_TypeInfo_var);
		CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 * L_0 = ((CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5_StaticFields*)il2cpp_codegen_static_fields_for(CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5_il2cpp_TypeInfo_var))->get_Unknown_0();
		__this->set_m_address_0(L_0);
		double L_1 = ___latitude0;
		GeoCoordinate_set_Latitude_m6C75A7DEA0B9C1AB51EC5E592B8649F2DA63D385(__this, L_1, /*hidden argument*/NULL);
		double L_2 = ___longitude1;
		GeoCoordinate_set_Longitude_m78F2D97804483F51BD85805EDD5DB5C2575A7BE2(__this, L_2, /*hidden argument*/NULL);
		double L_3 = ___altitude2;
		GeoCoordinate_set_Altitude_m19DDB5D204D693C21DF883A59A6223AE8DB14738_inline(__this, L_3, /*hidden argument*/NULL);
		double L_4 = ___horizontalAccuracy3;
		GeoCoordinate_set_HorizontalAccuracy_m463649A918284904B8B464B36A805A0C41DD2037(__this, L_4, /*hidden argument*/NULL);
		double L_5 = ___verticalAccuracy4;
		GeoCoordinate_set_VerticalAccuracy_m8D9AFF4893323688B2247834CD0680E5D751EFB1(__this, L_5, /*hidden argument*/NULL);
		double L_6 = ___speed5;
		GeoCoordinate_set_Speed_m4BC694CFDFF6339660846EC8C2205A21D69F1D19(__this, L_6, /*hidden argument*/NULL);
		double L_7 = ___course6;
		GeoCoordinate_set_Course_mFBEF26FB5685F62D306A039A19215BDA720A5491(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean System.Device.Location.GeoCoordinate::Equals(System.Device.Location.GeoCoordinate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GeoCoordinate_Equals_m4A5B248447B89BD204A4A7777DE0137657B29A50 (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * __this, GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * ___other0, const RuntimeMethod* method)
{
	double V_0 = 0.0;
	double V_1 = 0.0;
	{
		GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * L_0 = ___other0;
		bool L_1;
		L_1 = il2cpp_codegen_object_reference_equals(L_0, NULL);
		if (!L_1)
		{
			goto IL_000b;
		}
	}
	{
		return (bool)0;
	}

IL_000b:
	{
		double L_2;
		L_2 = GeoCoordinate_get_Latitude_mD487CAE6C0383D9C4D0CCE2BE85B38C0E68B1888_inline(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * L_3 = ___other0;
		NullCheck(L_3);
		double L_4;
		L_4 = GeoCoordinate_get_Latitude_mD487CAE6C0383D9C4D0CCE2BE85B38C0E68B1888_inline(L_3, /*hidden argument*/NULL);
		bool L_5;
		L_5 = Double_Equals_m8C171E8C7F556087E707D1396DB29D1D8B21A46B((double*)(&V_0), L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0036;
		}
	}
	{
		double L_6;
		L_6 = GeoCoordinate_get_Longitude_m2EC0079450A74DB09F7F2B68A49D80390716711A_inline(__this, /*hidden argument*/NULL);
		V_1 = L_6;
		GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * L_7 = ___other0;
		NullCheck(L_7);
		double L_8;
		L_8 = GeoCoordinate_get_Longitude_m2EC0079450A74DB09F7F2B68A49D80390716711A_inline(L_7, /*hidden argument*/NULL);
		bool L_9;
		L_9 = Double_Equals_m8C171E8C7F556087E707D1396DB29D1D8B21A46B((double*)(&V_1), L_8, /*hidden argument*/NULL);
		return L_9;
	}

IL_0036:
	{
		return (bool)0;
	}
}
// System.Boolean System.Device.Location.GeoCoordinate::Equals(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GeoCoordinate_Equals_m51F02CE615151AF310DD004118B2D0487048B94B (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * __this, RuntimeObject * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = ___obj0;
		if (!((GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 *)IsInstClass((RuntimeObject*)L_0, GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239_il2cpp_TypeInfo_var)))
		{
			goto IL_0015;
		}
	}
	{
		RuntimeObject * L_1 = ___obj0;
		bool L_2;
		L_2 = GeoCoordinate_Equals_m4A5B248447B89BD204A4A7777DE0137657B29A50(__this, ((GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 *)IsInstClass((RuntimeObject*)L_1, GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_2;
	}

IL_0015:
	{
		RuntimeObject * L_3 = ___obj0;
		bool L_4;
		L_4 = Object_Equals_m6E8965587E01BDEC1CDF25DE69A64253CD97EF85(__this, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Double System.Device.Location.GeoCoordinate::GetDistanceTo(System.Device.Location.GeoCoordinate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double GeoCoordinate_GetDistanceTo_m08A40DE4071016346FC25592DC09337FB16348D7 (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * __this, GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	double V_0 = 0.0;
	double V_1 = 0.0;
	double V_2 = 0.0;
	double V_3 = 0.0;
	double V_4 = 0.0;
	double V_5 = 0.0;
	double V_6 = 0.0;
	double V_7 = 0.0;
	{
		double L_0;
		L_0 = GeoCoordinate_get_Latitude_mD487CAE6C0383D9C4D0CCE2BE85B38C0E68B1888_inline(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Double_IsNaN_m94415C98C2D7DCAA32A82E1911AC13958AAD4347(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0034;
		}
	}
	{
		double L_2;
		L_2 = GeoCoordinate_get_Longitude_m2EC0079450A74DB09F7F2B68A49D80390716711A_inline(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Double_IsNaN_m94415C98C2D7DCAA32A82E1911AC13958AAD4347(L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0034;
		}
	}
	{
		GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * L_4 = ___other0;
		NullCheck(L_4);
		double L_5;
		L_5 = GeoCoordinate_get_Latitude_mD487CAE6C0383D9C4D0CCE2BE85B38C0E68B1888_inline(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181_il2cpp_TypeInfo_var);
		bool L_6;
		L_6 = Double_IsNaN_m94415C98C2D7DCAA32A82E1911AC13958AAD4347(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0034;
		}
	}
	{
		GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * L_7 = ___other0;
		NullCheck(L_7);
		double L_8;
		L_8 = GeoCoordinate_get_Longitude_m2EC0079450A74DB09F7F2B68A49D80390716711A_inline(L_7, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181_il2cpp_TypeInfo_var);
		bool L_9;
		L_9 = Double_IsNaN_m94415C98C2D7DCAA32A82E1911AC13958AAD4347(L_8, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_003f;
		}
	}

IL_0034:
	{
		String_t* L_10;
		L_10 = Resources_get_Argument_LatitudeOrLongitudeIsNotANumber_mEF075CA186C22FBFB71141EC851E0958EDC41214(/*hidden argument*/NULL);
		ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 * L_11 = (ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00_il2cpp_TypeInfo_var)));
		ArgumentException__ctor_m2D35EAD113C2ADC99EB17B940A2097A93FD23EFC(L_11, L_10, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_11, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GeoCoordinate_GetDistanceTo_m08A40DE4071016346FC25592DC09337FB16348D7_RuntimeMethod_var)));
	}

IL_003f:
	{
		double L_12;
		L_12 = GeoCoordinate_get_Latitude_mD487CAE6C0383D9C4D0CCE2BE85B38C0E68B1888_inline(__this, /*hidden argument*/NULL);
		V_0 = ((double)il2cpp_codegen_multiply((double)L_12, (double)(0.017453292519943295)));
		double L_13;
		L_13 = GeoCoordinate_get_Longitude_m2EC0079450A74DB09F7F2B68A49D80390716711A_inline(__this, /*hidden argument*/NULL);
		V_1 = ((double)il2cpp_codegen_multiply((double)L_13, (double)(0.017453292519943295)));
		GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * L_14 = ___other0;
		NullCheck(L_14);
		double L_15;
		L_15 = GeoCoordinate_get_Latitude_mD487CAE6C0383D9C4D0CCE2BE85B38C0E68B1888_inline(L_14, /*hidden argument*/NULL);
		V_2 = ((double)il2cpp_codegen_multiply((double)L_15, (double)(0.017453292519943295)));
		GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * L_16 = ___other0;
		NullCheck(L_16);
		double L_17;
		L_17 = GeoCoordinate_get_Longitude_m2EC0079450A74DB09F7F2B68A49D80390716711A_inline(L_16, /*hidden argument*/NULL);
		V_3 = ((double)il2cpp_codegen_multiply((double)L_17, (double)(0.017453292519943295)));
		double L_18 = V_3;
		double L_19 = V_1;
		V_4 = ((double)il2cpp_codegen_subtract((double)L_18, (double)L_19));
		double L_20 = V_2;
		double L_21 = V_0;
		V_5 = ((double)il2cpp_codegen_subtract((double)L_20, (double)L_21));
		double L_22 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_23;
		L_23 = sin(((double)((double)L_22/(double)(2.0))));
		double L_24;
		L_24 = Math_Pow_mC2C8700DAAD1316AA457A1D271F78CDF0D61AC2F(L_23, (2.0), /*hidden argument*/NULL);
		double L_25 = V_0;
		double L_26;
		L_26 = cos(L_25);
		double L_27 = V_2;
		double L_28;
		L_28 = cos(L_27);
		double L_29 = V_4;
		double L_30;
		L_30 = sin(((double)((double)L_29/(double)(2.0))));
		double L_31;
		L_31 = Math_Pow_mC2C8700DAAD1316AA457A1D271F78CDF0D61AC2F(L_30, (2.0), /*hidden argument*/NULL);
		V_6 = ((double)il2cpp_codegen_add((double)L_24, (double)((double)il2cpp_codegen_multiply((double)((double)il2cpp_codegen_multiply((double)L_26, (double)L_28)), (double)L_31))));
		double L_32 = V_6;
		double L_33;
		L_33 = sqrt(L_32);
		double L_34 = V_6;
		double L_35;
		L_35 = sqrt(((double)il2cpp_codegen_subtract((double)(1.0), (double)L_34)));
		double L_36;
		L_36 = atan2(L_33, L_35);
		V_7 = ((double)il2cpp_codegen_multiply((double)(2.0), (double)L_36));
		double L_37 = V_7;
		return ((double)il2cpp_codegen_multiply((double)(6376500.0), (double)L_37));
	}
}
// System.Int32 System.Device.Location.GeoCoordinate::GetHashCode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t GeoCoordinate_GetHashCode_mE14FE09570E70DD73C816F1E0D0C33ABB5075AF5 (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * __this, const RuntimeMethod* method)
{
	double V_0 = 0.0;
	double V_1 = 0.0;
	{
		double L_0;
		L_0 = GeoCoordinate_get_Latitude_mD487CAE6C0383D9C4D0CCE2BE85B38C0E68B1888_inline(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1;
		L_1 = Double_GetHashCode_m33CB20AA5674C6F4367B7B08340B33FB979F9F39((double*)(&V_0), /*hidden argument*/NULL);
		double L_2;
		L_2 = GeoCoordinate_get_Longitude_m2EC0079450A74DB09F7F2B68A49D80390716711A_inline(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		int32_t L_3;
		L_3 = Double_GetHashCode_m33CB20AA5674C6F4367B7B08340B33FB979F9F39((double*)(&V_1), /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_1^(int32_t)L_3));
	}
}
// System.Boolean System.Device.Location.GeoCoordinate::op_Equality(System.Device.Location.GeoCoordinate,System.Device.Location.GeoCoordinate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GeoCoordinate_op_Equality_m96E1FD033870A064072EF0AFC6BCF7C8DF0AAA53 (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * ___left0, GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * ___right1, const RuntimeMethod* method)
{
	{
		GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * L_0 = ___left0;
		bool L_1;
		L_1 = il2cpp_codegen_object_reference_equals(L_0, NULL);
		if (!L_1)
		{
			goto IL_0011;
		}
	}
	{
		GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * L_2 = ___right1;
		bool L_3;
		L_3 = il2cpp_codegen_object_reference_equals(L_2, NULL);
		return L_3;
	}

IL_0011:
	{
		GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * L_4 = ___left0;
		GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * L_5 = ___right1;
		NullCheck(L_4);
		bool L_6;
		L_6 = GeoCoordinate_Equals_m4A5B248447B89BD204A4A7777DE0137657B29A50(L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.String System.Device.Location.GeoCoordinate::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* GeoCoordinate_ToString_m675154123B5EF9382BBDBC70A31954ADE2E155B8 (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3DCC6243286938BE75C3FA773B9BA71160A2E869);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral5D54E959817188DBAD9E65FA3DB55F06B70F5E3C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral758733BDBED83CBFF4F635AC26CA92AAE477F75D);
		s_Il2CppMethodInitialized = true;
	}
	double V_0 = 0.0;
	double V_1 = 0.0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239_il2cpp_TypeInfo_var);
		GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * L_0 = ((GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239_StaticFields*)il2cpp_codegen_static_fields_for(GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239_il2cpp_TypeInfo_var))->get_Unknown_8();
		bool L_1;
		L_1 = GeoCoordinate_op_Equality_m96E1FD033870A064072EF0AFC6BCF7C8DF0AAA53(__this, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		return _stringLiteral5D54E959817188DBAD9E65FA3DB55F06B70F5E3C;
	}

IL_0013:
	{
		double L_2;
		L_2 = GeoCoordinate_get_Latitude_mD487CAE6C0383D9C4D0CCE2BE85B38C0E68B1888_inline(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98_il2cpp_TypeInfo_var);
		CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * L_3;
		L_3 = CultureInfo_get_InvariantCulture_m9FAAFAF8A00091EE1FCB7098AD3F163ECDF02164(/*hidden argument*/NULL);
		String_t* L_4;
		L_4 = Double_ToString_mFF1DAF2003FC7096C54C5A2685F85082220E330B((double*)(&V_0), _stringLiteral3DCC6243286938BE75C3FA773B9BA71160A2E869, L_3, /*hidden argument*/NULL);
		double L_5;
		L_5 = GeoCoordinate_get_Longitude_m2EC0079450A74DB09F7F2B68A49D80390716711A_inline(__this, /*hidden argument*/NULL);
		V_1 = L_5;
		CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * L_6;
		L_6 = CultureInfo_get_InvariantCulture_m9FAAFAF8A00091EE1FCB7098AD3F163ECDF02164(/*hidden argument*/NULL);
		String_t* L_7;
		L_7 = Double_ToString_mFF1DAF2003FC7096C54C5A2685F85082220E330B((double*)(&V_1), _stringLiteral3DCC6243286938BE75C3FA773B9BA71160A2E869, L_6, /*hidden argument*/NULL);
		String_t* L_8;
		L_8 = String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44(L_4, _stringLiteral758733BDBED83CBFF4F635AC26CA92AAE477F75D, L_7, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void System.Device.Location.GeoCoordinate::set_Altitude(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeoCoordinate_set_Altitude_m19DDB5D204D693C21DF883A59A6223AE8DB14738 (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_m_altitude_1(L_0);
		return;
	}
}
// System.Void System.Device.Location.GeoCoordinate::set_Course(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeoCoordinate_set_Course_mFBEF26FB5685F62D306A039A19215BDA720A5491 (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		if ((((double)L_0) < ((double)(0.0))))
		{
			goto IL_0018;
		}
	}
	{
		double L_1 = ___value0;
		if ((!(((double)L_1) > ((double)(360.0)))))
		{
			goto IL_0028;
		}
	}

IL_0018:
	{
		String_t* L_2;
		L_2 = Resources_get_Argument_MustBeInRangeZeroTo360_m921B2506D02D201C26D9D02990B26312464F687C(/*hidden argument*/NULL);
		ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 * L_3 = (ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_il2cpp_TypeInfo_var)));
		ArgumentOutOfRangeException__ctor_mE43AFC74F5F3932913C023A04B24905E093C5005(L_3, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral550F2AF0287E2D987375F2915E7039E01605DBB3)), L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GeoCoordinate_set_Course_mFBEF26FB5685F62D306A039A19215BDA720A5491_RuntimeMethod_var)));
	}

IL_0028:
	{
		double L_4 = ___value0;
		__this->set_m_course_2(L_4);
		return;
	}
}
// System.Void System.Device.Location.GeoCoordinate::set_HorizontalAccuracy(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeoCoordinate_set_HorizontalAccuracy_m463649A918284904B8B464B36A805A0C41DD2037 (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * __this, double ___value0, const RuntimeMethod* method)
{
	GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * G_B4_0 = NULL;
	GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * G_B3_0 = NULL;
	double G_B5_0 = 0.0;
	GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * G_B5_1 = NULL;
	{
		double L_0 = ___value0;
		if ((!(((double)L_0) < ((double)(0.0)))))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1;
		L_1 = Resources_get_Argument_MustBeNonNegative_m3ECFCC0A8407832F903B12FC0C891338C8D6E7FD(/*hidden argument*/NULL);
		ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 * L_2 = (ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_il2cpp_TypeInfo_var)));
		ArgumentOutOfRangeException__ctor_mE43AFC74F5F3932913C023A04B24905E093C5005(L_2, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteralEA197A98747F049B4D33EBC845749AF3B8429912)), L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GeoCoordinate_set_HorizontalAccuracy_m463649A918284904B8B464B36A805A0C41DD2037_RuntimeMethod_var)));
	}

IL_001c:
	{
		double L_3 = ___value0;
		G_B3_0 = __this;
		if ((((double)L_3) == ((double)(0.0))))
		{
			G_B4_0 = __this;
			goto IL_002c;
		}
	}
	{
		double L_4 = ___value0;
		G_B5_0 = L_4;
		G_B5_1 = G_B3_0;
		goto IL_0035;
	}

IL_002c:
	{
		G_B5_0 = (std::numeric_limits<double>::quiet_NaN());
		G_B5_1 = G_B4_0;
	}

IL_0035:
	{
		NullCheck(G_B5_1);
		G_B5_1->set_m_horizontalAccuracy_3(G_B5_0);
		return;
	}
}
// System.Double System.Device.Location.GeoCoordinate::get_Latitude()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double GeoCoordinate_get_Latitude_mD487CAE6C0383D9C4D0CCE2BE85B38C0E68B1888 (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_m_latitude_4();
		return L_0;
	}
}
// System.Void System.Device.Location.GeoCoordinate::set_Latitude(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeoCoordinate_set_Latitude_m6C75A7DEA0B9C1AB51EC5E592B8649F2DA63D385 (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		if ((((double)L_0) > ((double)(90.0))))
		{
			goto IL_0018;
		}
	}
	{
		double L_1 = ___value0;
		if ((!(((double)L_1) < ((double)(-90.0)))))
		{
			goto IL_0028;
		}
	}

IL_0018:
	{
		String_t* L_2;
		L_2 = Resources_get_Argument_MustBeInRangeNegative90to90_m62EE743532EF0BDC91D4676912FBF9CC82880CF7(/*hidden argument*/NULL);
		ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 * L_3 = (ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_il2cpp_TypeInfo_var)));
		ArgumentOutOfRangeException__ctor_mE43AFC74F5F3932913C023A04B24905E093C5005(L_3, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral0F278FC1CBF98DA834E8A4EFCFD237AD661C1296)), L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GeoCoordinate_set_Latitude_m6C75A7DEA0B9C1AB51EC5E592B8649F2DA63D385_RuntimeMethod_var)));
	}

IL_0028:
	{
		double L_4 = ___value0;
		__this->set_m_latitude_4(L_4);
		return;
	}
}
// System.Double System.Device.Location.GeoCoordinate::get_Longitude()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double GeoCoordinate_get_Longitude_m2EC0079450A74DB09F7F2B68A49D80390716711A (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_m_longitude_5();
		return L_0;
	}
}
// System.Void System.Device.Location.GeoCoordinate::set_Longitude(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeoCoordinate_set_Longitude_m78F2D97804483F51BD85805EDD5DB5C2575A7BE2 (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		if ((((double)L_0) > ((double)(180.0))))
		{
			goto IL_0018;
		}
	}
	{
		double L_1 = ___value0;
		if ((!(((double)L_1) < ((double)(-180.0)))))
		{
			goto IL_0028;
		}
	}

IL_0018:
	{
		String_t* L_2;
		L_2 = Resources_get_Argument_MustBeInRangeNegative180To180_m6FE557A9143F65CA218C0E2500F29B2444D0C654(/*hidden argument*/NULL);
		ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 * L_3 = (ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_il2cpp_TypeInfo_var)));
		ArgumentOutOfRangeException__ctor_mE43AFC74F5F3932913C023A04B24905E093C5005(L_3, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral89649263D26EA274EA8BFDD69ADFA4816B486F5A)), L_2, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GeoCoordinate_set_Longitude_m78F2D97804483F51BD85805EDD5DB5C2575A7BE2_RuntimeMethod_var)));
	}

IL_0028:
	{
		double L_4 = ___value0;
		__this->set_m_longitude_5(L_4);
		return;
	}
}
// System.Void System.Device.Location.GeoCoordinate::set_Speed(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeoCoordinate_set_Speed_m4BC694CFDFF6339660846EC8C2205A21D69F1D19 (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		if ((!(((double)L_0) < ((double)(0.0)))))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1;
		L_1 = Resources_get_Argument_MustBeNonNegative_m3ECFCC0A8407832F903B12FC0C891338C8D6E7FD(/*hidden argument*/NULL);
		ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 * L_2 = (ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_il2cpp_TypeInfo_var)));
		ArgumentOutOfRangeException__ctor_mE43AFC74F5F3932913C023A04B24905E093C5005(L_2, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral2D61429A77614470501E536C4B7A7CB204B9125B)), L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GeoCoordinate_set_Speed_m4BC694CFDFF6339660846EC8C2205A21D69F1D19_RuntimeMethod_var)));
	}

IL_001c:
	{
		double L_3 = ___value0;
		__this->set_m_speed_6(L_3);
		return;
	}
}
// System.Void System.Device.Location.GeoCoordinate::set_VerticalAccuracy(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeoCoordinate_set_VerticalAccuracy_m8D9AFF4893323688B2247834CD0680E5D751EFB1 (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * __this, double ___value0, const RuntimeMethod* method)
{
	GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * G_B4_0 = NULL;
	GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * G_B3_0 = NULL;
	double G_B5_0 = 0.0;
	GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * G_B5_1 = NULL;
	{
		double L_0 = ___value0;
		if ((!(((double)L_0) < ((double)(0.0)))))
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_1;
		L_1 = Resources_get_Argument_MustBeNonNegative_m3ECFCC0A8407832F903B12FC0C891338C8D6E7FD(/*hidden argument*/NULL);
		ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 * L_2 = (ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&ArgumentOutOfRangeException_tFAF23713820951D4A09ABBFE5CC091E445A6F3D8_il2cpp_TypeInfo_var)));
		ArgumentOutOfRangeException__ctor_mE43AFC74F5F3932913C023A04B24905E093C5005(L_2, ((String_t*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&_stringLiteral7E9C99BC533135F65AF9BE0E25DCB5A661F1347E)), L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&GeoCoordinate_set_VerticalAccuracy_m8D9AFF4893323688B2247834CD0680E5D751EFB1_RuntimeMethod_var)));
	}

IL_001c:
	{
		double L_3 = ___value0;
		G_B3_0 = __this;
		if ((((double)L_3) == ((double)(0.0))))
		{
			G_B4_0 = __this;
			goto IL_002c;
		}
	}
	{
		double L_4 = ___value0;
		G_B5_0 = L_4;
		G_B5_1 = G_B3_0;
		goto IL_0035;
	}

IL_002c:
	{
		G_B5_0 = (std::numeric_limits<double>::quiet_NaN());
		G_B5_1 = G_B4_0;
	}

IL_0035:
	{
		NullCheck(G_B5_1);
		G_B5_1->set_m_verticalAccuracy_7(G_B5_0);
		return;
	}
}
// System.Void System.Device.Location.GeoCoordinate::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeoCoordinate__cctor_mB452811D6D6E8F903C2E8EEBE5B79E3676D198E8 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * L_0 = (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 *)il2cpp_codegen_object_new(GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239_il2cpp_TypeInfo_var);
		GeoCoordinate__ctor_mFF7956F6041D18D6CF94986C4501E12A40DF7CDB(L_0, /*hidden argument*/NULL);
		((GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239_StaticFields*)il2cpp_codegen_static_fields_for(GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239_il2cpp_TypeInfo_var))->set_Unknown_8(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Resources.ResourceManager System.Device.Properties.Resources::get_ResourceManager()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A * Resources_get_ResourceManager_m4392F8971FB189F0151B830C4E8641CBD6B63B1F (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Resources_tBF455E9A126C8E65140A86AAC63C951FAFA7F4BA_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Resources_tBF455E9A126C8E65140A86AAC63C951FAFA7F4BA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Type_t_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral25924FC7B1F14B4B0A771EB347F431419F4974AF);
		s_Il2CppMethodInitialized = true;
	}
	ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A * V_0 = NULL;
	{
		ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A * L_0 = ((Resources_tBF455E9A126C8E65140A86AAC63C951FAFA7F4BA_StaticFields*)il2cpp_codegen_static_fields_for(Resources_tBF455E9A126C8E65140A86AAC63C951FAFA7F4BA_il2cpp_TypeInfo_var))->get_resourceMan_0();
		bool L_1;
		L_1 = il2cpp_codegen_object_reference_equals(L_0, NULL);
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_2 = { reinterpret_cast<intptr_t> (Resources_tBF455E9A126C8E65140A86AAC63C951FAFA7F4BA_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_3;
		L_3 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_2, /*hidden argument*/NULL);
		TypeInfo_tFFBAC0D7187BFD2D25CC801679BC9645020EC04F * L_4;
		L_4 = IntrospectionExtensions_GetTypeInfo_m77034F8576BE695819427C13103C591277C1B636(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Assembly_t * L_5;
		L_5 = VirtFuncInvoker0< Assembly_t * >::Invoke(24 /* System.Reflection.Assembly System.Reflection.TypeInfo::get_Assembly() */, L_4);
		ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A * L_6 = (ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A *)il2cpp_codegen_object_new(ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A_il2cpp_TypeInfo_var);
		ResourceManager__ctor_m71706EDDD65E34D84909C9C65591C6FD7C6D95A2(L_6, _stringLiteral25924FC7B1F14B4B0A771EB347F431419F4974AF, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A * L_7 = V_0;
		((Resources_tBF455E9A126C8E65140A86AAC63C951FAFA7F4BA_StaticFields*)il2cpp_codegen_static_fields_for(Resources_tBF455E9A126C8E65140A86AAC63C951FAFA7F4BA_il2cpp_TypeInfo_var))->set_resourceMan_0(L_7);
	}

IL_0032:
	{
		ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A * L_8 = ((Resources_tBF455E9A126C8E65140A86AAC63C951FAFA7F4BA_StaticFields*)il2cpp_codegen_static_fields_for(Resources_tBF455E9A126C8E65140A86AAC63C951FAFA7F4BA_il2cpp_TypeInfo_var))->get_resourceMan_0();
		return L_8;
	}
}
// System.String System.Device.Properties.Resources::get_Argument_LatitudeOrLongitudeIsNotANumber()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Resources_get_Argument_LatitudeOrLongitudeIsNotANumber_mEF075CA186C22FBFB71141EC851E0958EDC41214 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Resources_tBF455E9A126C8E65140A86AAC63C951FAFA7F4BA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6C7AA5B2326C47E24C9195E0B3482525705D5FFB);
		s_Il2CppMethodInitialized = true;
	}
	{
		ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A * L_0;
		L_0 = Resources_get_ResourceManager_m4392F8971FB189F0151B830C4E8641CBD6B63B1F(/*hidden argument*/NULL);
		CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * L_1 = ((Resources_tBF455E9A126C8E65140A86AAC63C951FAFA7F4BA_StaticFields*)il2cpp_codegen_static_fields_for(Resources_tBF455E9A126C8E65140A86AAC63C951FAFA7F4BA_il2cpp_TypeInfo_var))->get_resourceCulture_1();
		NullCheck(L_0);
		String_t* L_2;
		L_2 = VirtFuncInvoker2< String_t*, String_t*, CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * >::Invoke(7 /* System.String System.Resources.ResourceManager::GetString(System.String,System.Globalization.CultureInfo) */, L_0, _stringLiteral6C7AA5B2326C47E24C9195E0B3482525705D5FFB, L_1);
		return L_2;
	}
}
// System.String System.Device.Properties.Resources::get_Argument_MustBeInRangeNegative180To180()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Resources_get_Argument_MustBeInRangeNegative180To180_m6FE557A9143F65CA218C0E2500F29B2444D0C654 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Resources_tBF455E9A126C8E65140A86AAC63C951FAFA7F4BA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral68E20F5AB9D9A2A1478AEBAC7130BC321E58EC43);
		s_Il2CppMethodInitialized = true;
	}
	{
		ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A * L_0;
		L_0 = Resources_get_ResourceManager_m4392F8971FB189F0151B830C4E8641CBD6B63B1F(/*hidden argument*/NULL);
		CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * L_1 = ((Resources_tBF455E9A126C8E65140A86AAC63C951FAFA7F4BA_StaticFields*)il2cpp_codegen_static_fields_for(Resources_tBF455E9A126C8E65140A86AAC63C951FAFA7F4BA_il2cpp_TypeInfo_var))->get_resourceCulture_1();
		NullCheck(L_0);
		String_t* L_2;
		L_2 = VirtFuncInvoker2< String_t*, String_t*, CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * >::Invoke(7 /* System.String System.Resources.ResourceManager::GetString(System.String,System.Globalization.CultureInfo) */, L_0, _stringLiteral68E20F5AB9D9A2A1478AEBAC7130BC321E58EC43, L_1);
		return L_2;
	}
}
// System.String System.Device.Properties.Resources::get_Argument_MustBeInRangeNegative90to90()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Resources_get_Argument_MustBeInRangeNegative90to90_m62EE743532EF0BDC91D4676912FBF9CC82880CF7 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Resources_tBF455E9A126C8E65140A86AAC63C951FAFA7F4BA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral163B0FC24A83A793B3FCA4691A1D7CADC20A580B);
		s_Il2CppMethodInitialized = true;
	}
	{
		ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A * L_0;
		L_0 = Resources_get_ResourceManager_m4392F8971FB189F0151B830C4E8641CBD6B63B1F(/*hidden argument*/NULL);
		CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * L_1 = ((Resources_tBF455E9A126C8E65140A86AAC63C951FAFA7F4BA_StaticFields*)il2cpp_codegen_static_fields_for(Resources_tBF455E9A126C8E65140A86AAC63C951FAFA7F4BA_il2cpp_TypeInfo_var))->get_resourceCulture_1();
		NullCheck(L_0);
		String_t* L_2;
		L_2 = VirtFuncInvoker2< String_t*, String_t*, CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * >::Invoke(7 /* System.String System.Resources.ResourceManager::GetString(System.String,System.Globalization.CultureInfo) */, L_0, _stringLiteral163B0FC24A83A793B3FCA4691A1D7CADC20A580B, L_1);
		return L_2;
	}
}
// System.String System.Device.Properties.Resources::get_Argument_MustBeInRangeZeroTo360()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Resources_get_Argument_MustBeInRangeZeroTo360_m921B2506D02D201C26D9D02990B26312464F687C (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Resources_tBF455E9A126C8E65140A86AAC63C951FAFA7F4BA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral86BA2E873128DBB921A90301586F310302569533);
		s_Il2CppMethodInitialized = true;
	}
	{
		ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A * L_0;
		L_0 = Resources_get_ResourceManager_m4392F8971FB189F0151B830C4E8641CBD6B63B1F(/*hidden argument*/NULL);
		CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * L_1 = ((Resources_tBF455E9A126C8E65140A86AAC63C951FAFA7F4BA_StaticFields*)il2cpp_codegen_static_fields_for(Resources_tBF455E9A126C8E65140A86AAC63C951FAFA7F4BA_il2cpp_TypeInfo_var))->get_resourceCulture_1();
		NullCheck(L_0);
		String_t* L_2;
		L_2 = VirtFuncInvoker2< String_t*, String_t*, CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * >::Invoke(7 /* System.String System.Resources.ResourceManager::GetString(System.String,System.Globalization.CultureInfo) */, L_0, _stringLiteral86BA2E873128DBB921A90301586F310302569533, L_1);
		return L_2;
	}
}
// System.String System.Device.Properties.Resources::get_Argument_MustBeNonNegative()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Resources_get_Argument_MustBeNonNegative_m3ECFCC0A8407832F903B12FC0C891338C8D6E7FD (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Resources_tBF455E9A126C8E65140A86AAC63C951FAFA7F4BA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9D71DE2C80BE06DE001DE3E9D7AC51E415CA1B9D);
		s_Il2CppMethodInitialized = true;
	}
	{
		ResourceManager_t015B887ECBCB2AEE41F774C390F25EB507B06B8A * L_0;
		L_0 = Resources_get_ResourceManager_m4392F8971FB189F0151B830C4E8641CBD6B63B1F(/*hidden argument*/NULL);
		CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * L_1 = ((Resources_tBF455E9A126C8E65140A86AAC63C951FAFA7F4BA_StaticFields*)il2cpp_codegen_static_fields_for(Resources_tBF455E9A126C8E65140A86AAC63C951FAFA7F4BA_il2cpp_TypeInfo_var))->get_resourceCulture_1();
		NullCheck(L_0);
		String_t* L_2;
		L_2 = VirtFuncInvoker2< String_t*, String_t*, CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * >::Invoke(7 /* System.String System.Resources.ResourceManager::GetString(System.String,System.Globalization.CultureInfo) */, L_0, _stringLiteral9D71DE2C80BE06DE001DE3E9D7AC51E415CA1B9D, L_1);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CivicAddress_set_AddressLine1_m7332B7407FE83DDEAF587C582F543D3939BD052C_inline (CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CAddressLine1U3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CivicAddress_set_AddressLine2_m78C8EA756514DF24B11385575A65C6270E5CDF37_inline (CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CAddressLine2U3Ek__BackingField_2(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CivicAddress_set_Building_m0CFF68430E65DBDD6EA41E2CE13775AF120D59CC_inline (CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CBuildingU3Ek__BackingField_3(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CivicAddress_set_City_m56F2D9F77FFE8779858128DC13144C0EFA307227_inline (CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCityU3Ek__BackingField_4(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CivicAddress_set_CountryRegion_m599882DD6A8BB8F1BFA8DB1F2B1E6E413CAA0248_inline (CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CCountryRegionU3Ek__BackingField_5(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CivicAddress_set_FloorLevel_m2763C9EEA07862C276A52B72CC3F154D35115F76_inline (CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CFloorLevelU3Ek__BackingField_6(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CivicAddress_set_PostalCode_m96F6E5FB7575AD8EC32BFC3FAE070217BC8D3D9F_inline (CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CPostalCodeU3Ek__BackingField_7(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CivicAddress_set_StateProvince_m4616D216C2DC3CFC119662B46F799E642E6CF2CF_inline (CivicAddress_t11DA2F642DC079C93D7911317B8F57D749FB1DE5 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CStateProvinceU3Ek__BackingField_8(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void GeoCoordinate_set_Altitude_m19DDB5D204D693C21DF883A59A6223AE8DB14738_inline (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * __this, double ___value0, const RuntimeMethod* method)
{
	{
		double L_0 = ___value0;
		__this->set_m_altitude_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double GeoCoordinate_get_Latitude_mD487CAE6C0383D9C4D0CCE2BE85B38C0E68B1888_inline (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_m_latitude_4();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR double GeoCoordinate_get_Longitude_m2EC0079450A74DB09F7F2B68A49D80390716711A_inline (GeoCoordinate_t29FB9150F6410AC74B9E34C29BA3EA22A3C7F239 * __this, const RuntimeMethod* method)
{
	{
		double L_0 = __this->get_m_longitude_5();
		return L_0;
	}
}

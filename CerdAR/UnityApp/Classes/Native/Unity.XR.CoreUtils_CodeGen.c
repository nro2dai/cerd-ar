﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
extern void EmbeddedAttribute__ctor_m397CF6D74070B39A844CEF4BC9846AF5DC8423B9 (void);
// 0x00000002 System.Void System.Runtime.CompilerServices.IsReadOnlyAttribute::.ctor()
extern void IsReadOnlyAttribute__ctor_m40764A40A4145B9996BC9AECBAD2C3BCE27FA65D (void);
// 0x00000003 Unity.XR.CoreUtils.XROrigin Unity.XR.CoreUtils.ARTrackablesParentTransformChangedEventArgs::get_Origin()
extern void ARTrackablesParentTransformChangedEventArgs_get_Origin_m635FF91C20A6D6C0514EABDDED14289D01BDC0A5 (void);
// 0x00000004 UnityEngine.Transform Unity.XR.CoreUtils.ARTrackablesParentTransformChangedEventArgs::get_TrackablesParent()
extern void ARTrackablesParentTransformChangedEventArgs_get_TrackablesParent_mC93ACA08A0A5E3F00A58F63EF932BD6AA768BA43 (void);
// 0x00000005 System.Void Unity.XR.CoreUtils.ARTrackablesParentTransformChangedEventArgs::.ctor(Unity.XR.CoreUtils.XROrigin,UnityEngine.Transform)
extern void ARTrackablesParentTransformChangedEventArgs__ctor_m90A574F56EAF0228BD1D7CBFDAF682B8E58F7F9D (void);
// 0x00000006 System.Boolean Unity.XR.CoreUtils.ARTrackablesParentTransformChangedEventArgs::Equals(Unity.XR.CoreUtils.ARTrackablesParentTransformChangedEventArgs)
extern void ARTrackablesParentTransformChangedEventArgs_Equals_mDEA555E578C8B4ED1C2EB86EC70E289D6A542DEE (void);
// 0x00000007 System.Boolean Unity.XR.CoreUtils.ARTrackablesParentTransformChangedEventArgs::Equals(System.Object)
extern void ARTrackablesParentTransformChangedEventArgs_Equals_mD1E41855005B1E2CF7834216ECA32621B6229D53 (void);
// 0x00000008 System.Int32 Unity.XR.CoreUtils.ARTrackablesParentTransformChangedEventArgs::GetHashCode()
extern void ARTrackablesParentTransformChangedEventArgs_GetHashCode_mDBE0D34111420DBF1A446062AD324FCDE522B51A (void);
// 0x00000009 UnityEngine.Pose Unity.XR.CoreUtils.TransformExtensions::GetWorldPose(UnityEngine.Transform)
extern void TransformExtensions_GetWorldPose_m33A1A882B998C9949D1C6424846E60D55B5AEF45 (void);
// 0x0000000A UnityEngine.Pose Unity.XR.CoreUtils.TransformExtensions::TransformPose(UnityEngine.Transform,UnityEngine.Pose)
extern void TransformExtensions_TransformPose_mEB3D81D8D439A0CB8C08B791A180B78542978AAA (void);
// 0x0000000B System.Void Unity.XR.CoreUtils.FloatUnityEvent::.ctor()
extern void FloatUnityEvent__ctor_m3C80AE317A469CC5F7C0920889BBB1F9D86195F5 (void);
// 0x0000000C System.Void Unity.XR.CoreUtils.Vector2UnityEvent::.ctor()
extern void Vector2UnityEvent__ctor_m5030F8916B653DFD26113971AC99AFC58C5D5C94 (void);
// 0x0000000D System.Void Unity.XR.CoreUtils.Vector3UnityEvent::.ctor()
extern void Vector3UnityEvent__ctor_m4C6C0F78526D7E9651F1BC2DE40F9A6B7795B3BB (void);
// 0x0000000E System.Void Unity.XR.CoreUtils.Vector4UnityEvent::.ctor()
extern void Vector4UnityEvent__ctor_m2C9E326EE573F0E05D04A57312872BD2C89CDF27 (void);
// 0x0000000F System.Void Unity.XR.CoreUtils.QuaternionUnityEvent::.ctor()
extern void QuaternionUnityEvent__ctor_m7B6198DF5266C35E7980BE275344236913C5D789 (void);
// 0x00000010 System.Void Unity.XR.CoreUtils.ColorUnityEvent::.ctor()
extern void ColorUnityEvent__ctor_m718169C7152A67F2CCC6954EDDE9599A722AC92A (void);
// 0x00000011 System.Single Unity.XR.CoreUtils.Vector3Extensions::MaxComponent(UnityEngine.Vector3)
extern void Vector3Extensions_MaxComponent_m19DF9C78F0799BD2D1F3E5E87565C600D40CFD4D (void);
// 0x00000012 UnityEngine.Vector3 Unity.XR.CoreUtils.Vector3Extensions::Multiply(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Vector3Extensions_Multiply_m7B8B8D6D08861C9401D99F12DA04FE1CB8438141 (void);
// 0x00000013 UnityEngine.Vector3 Unity.XR.CoreUtils.Vector3Extensions::Divide(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Vector3Extensions_Divide_m6BEABD7E540B8C17B3738253D908EC1C09D81DED (void);
// 0x00000014 System.Int32 Unity.XR.CoreUtils.HashCodeUtil::Combine(System.Int32,System.Int32)
extern void HashCodeUtil_Combine_m5EC3008FE1451272AED445DA4B1E7A65BAAF197E (void);
// 0x00000015 System.Int32 Unity.XR.CoreUtils.HashCodeUtil::ReferenceHash(System.Object)
extern void HashCodeUtil_ReferenceHash_m55B8BCB31F847BA215571F96A8FC190A53CE9D26 (void);
// 0x00000016 System.Void Unity.XR.CoreUtils.XRLoggingUtils::.cctor()
extern void XRLoggingUtils__cctor_m34C67D615FF5E41B0C2930A2B03E09F63E8F0EB1 (void);
// 0x00000017 System.Void Unity.XR.CoreUtils.XRLoggingUtils::LogWarning(System.String,UnityEngine.Object)
extern void XRLoggingUtils_LogWarning_m16C829B8740982FA4B0EBED606A2D518E492A5CC (void);
// 0x00000018 System.Void Unity.XR.CoreUtils.XRLoggingUtils::LogError(System.String,UnityEngine.Object)
extern void XRLoggingUtils_LogError_m65734BA6FECDC0C2A35DE1E6401886DFD6515402 (void);
// 0x00000019 UnityEngine.Camera Unity.XR.CoreUtils.XROrigin::get_Camera()
extern void XROrigin_get_Camera_mDEC1EA5E15968845DA812397BBA4506A88B0F9FF (void);
// 0x0000001A System.Void Unity.XR.CoreUtils.XROrigin::set_Camera(UnityEngine.Camera)
extern void XROrigin_set_Camera_m33ECB239BCDB92806AFD47EA8A4685E81EBDFE24 (void);
// 0x0000001B UnityEngine.Transform Unity.XR.CoreUtils.XROrigin::get_TrackablesParent()
extern void XROrigin_get_TrackablesParent_m1762CFBDDF007C80B97379835702D8607DB4F3E0 (void);
// 0x0000001C System.Void Unity.XR.CoreUtils.XROrigin::set_TrackablesParent(UnityEngine.Transform)
extern void XROrigin_set_TrackablesParent_mE069D24282C732C5164D03FEEDBA8C9FD8AF323E (void);
// 0x0000001D System.Void Unity.XR.CoreUtils.XROrigin::add_TrackablesParentTransformChanged(System.Action`1<Unity.XR.CoreUtils.ARTrackablesParentTransformChangedEventArgs>)
extern void XROrigin_add_TrackablesParentTransformChanged_mA320D83FF126A31F8139C86BC68BE280EE4BC280 (void);
// 0x0000001E System.Void Unity.XR.CoreUtils.XROrigin::remove_TrackablesParentTransformChanged(System.Action`1<Unity.XR.CoreUtils.ARTrackablesParentTransformChangedEventArgs>)
extern void XROrigin_remove_TrackablesParentTransformChanged_m4D7C48C781798E32FE484B40997BF6BFB5A8BBB0 (void);
// 0x0000001F UnityEngine.GameObject Unity.XR.CoreUtils.XROrigin::get_Origin()
extern void XROrigin_get_Origin_m791424808DC2F95B332CFCE34A260E5F53DEC2AD (void);
// 0x00000020 System.Void Unity.XR.CoreUtils.XROrigin::set_Origin(UnityEngine.GameObject)
extern void XROrigin_set_Origin_mA3665C3245F0B16C7C2AA41381FC8D57A80FD6D3 (void);
// 0x00000021 UnityEngine.GameObject Unity.XR.CoreUtils.XROrigin::get_CameraFloorOffsetObject()
extern void XROrigin_get_CameraFloorOffsetObject_mB06A006D2F1211B477E5AADF9DF6EBCB5EFCCF45 (void);
// 0x00000022 System.Void Unity.XR.CoreUtils.XROrigin::set_CameraFloorOffsetObject(UnityEngine.GameObject)
extern void XROrigin_set_CameraFloorOffsetObject_mE20DF102DDC9F456EE93F97D69A5EEF7306C1F5E (void);
// 0x00000023 Unity.XR.CoreUtils.XROrigin/TrackingOriginMode Unity.XR.CoreUtils.XROrigin::get_RequestedTrackingOriginMode()
extern void XROrigin_get_RequestedTrackingOriginMode_m4FF9E54CD679A9B3CC05B36314D66F3B1AD1688B (void);
// 0x00000024 System.Void Unity.XR.CoreUtils.XROrigin::set_RequestedTrackingOriginMode(Unity.XR.CoreUtils.XROrigin/TrackingOriginMode)
extern void XROrigin_set_RequestedTrackingOriginMode_m2D7D0AF1006FF55D8EC6D8BF6679F9867E5D496E (void);
// 0x00000025 System.Single Unity.XR.CoreUtils.XROrigin::get_CameraYOffset()
extern void XROrigin_get_CameraYOffset_mA396D70F0E27E80EA5177915433C5038BACDDF4E (void);
// 0x00000026 System.Void Unity.XR.CoreUtils.XROrigin::set_CameraYOffset(System.Single)
extern void XROrigin_set_CameraYOffset_mBA7E721A282BA9E414B1BCA15E82AE3D48C1CAA4 (void);
// 0x00000027 UnityEngine.XR.TrackingOriginModeFlags Unity.XR.CoreUtils.XROrigin::get_CurrentTrackingOriginMode()
extern void XROrigin_get_CurrentTrackingOriginMode_m90D206E17CF4EBD3DDD281F526E43AA3A1A84AFA (void);
// 0x00000028 System.Void Unity.XR.CoreUtils.XROrigin::set_CurrentTrackingOriginMode(UnityEngine.XR.TrackingOriginModeFlags)
extern void XROrigin_set_CurrentTrackingOriginMode_mE1F8C7BD93763B7503A9F1A66224B1E99AFB1680 (void);
// 0x00000029 UnityEngine.Vector3 Unity.XR.CoreUtils.XROrigin::get_OriginInCameraSpacePos()
extern void XROrigin_get_OriginInCameraSpacePos_mC934EF4E3E14C7DC72A88E058C39F7A40104174C (void);
// 0x0000002A UnityEngine.Vector3 Unity.XR.CoreUtils.XROrigin::get_CameraInOriginSpacePos()
extern void XROrigin_get_CameraInOriginSpacePos_mA25C4AECE774DCE217DF1AB838E9D82BB87BEE56 (void);
// 0x0000002B System.Single Unity.XR.CoreUtils.XROrigin::get_CameraInOriginSpaceHeight()
extern void XROrigin_get_CameraInOriginSpaceHeight_m59DC39ADD21E144299A2B7A283414591CC966FF7 (void);
// 0x0000002C System.Void Unity.XR.CoreUtils.XROrigin::MoveOffsetHeight()
extern void XROrigin_MoveOffsetHeight_mE85E434BC9ECF1D224C9A7506B814A3D37B2F3EC (void);
// 0x0000002D System.Void Unity.XR.CoreUtils.XROrigin::MoveOffsetHeight(System.Single)
extern void XROrigin_MoveOffsetHeight_m89E6241C5D67CCD0AC4A166AA4599AADF39E895F (void);
// 0x0000002E System.Void Unity.XR.CoreUtils.XROrigin::TryInitializeCamera()
extern void XROrigin_TryInitializeCamera_m8A470425D8369F6555D3C36C3335F9D8F82B40A4 (void);
// 0x0000002F System.Boolean Unity.XR.CoreUtils.XROrigin::SetupCamera()
extern void XROrigin_SetupCamera_m63ECB705B6D6398F5B8B7C2EF7B33CB37C796BC4 (void);
// 0x00000030 System.Boolean Unity.XR.CoreUtils.XROrigin::SetupCamera(UnityEngine.XR.XRInputSubsystem)
extern void XROrigin_SetupCamera_mEB39C60D1AE12C2A25647E578B353E127C406726 (void);
// 0x00000031 System.Void Unity.XR.CoreUtils.XROrigin::OnInputSubsystemTrackingOriginUpdated(UnityEngine.XR.XRInputSubsystem)
extern void XROrigin_OnInputSubsystemTrackingOriginUpdated_m132A24B0235B1E82289B45F7B5B9132A19A85719 (void);
// 0x00000032 System.Collections.IEnumerator Unity.XR.CoreUtils.XROrigin::RepeatInitializeCamera()
extern void XROrigin_RepeatInitializeCamera_mE77E233DAE27A9547E0BE478CC06ED6E51E8917A (void);
// 0x00000033 System.Boolean Unity.XR.CoreUtils.XROrigin::RotateAroundCameraUsingOriginUp(System.Single)
extern void XROrigin_RotateAroundCameraUsingOriginUp_mFBC31926C33A2A4BB0FB2CD37A5D6A2DB805C26D (void);
// 0x00000034 System.Boolean Unity.XR.CoreUtils.XROrigin::RotateAroundCameraPosition(UnityEngine.Vector3,System.Single)
extern void XROrigin_RotateAroundCameraPosition_mDFFA1FCA35ABE47B99085527C2DBF97141289EC3 (void);
// 0x00000035 System.Boolean Unity.XR.CoreUtils.XROrigin::MatchOriginUp(UnityEngine.Vector3)
extern void XROrigin_MatchOriginUp_mF2000A84EAFDFA87164B64B19BC93324AB030C26 (void);
// 0x00000036 System.Boolean Unity.XR.CoreUtils.XROrigin::MatchOriginUpCameraForward(UnityEngine.Vector3,UnityEngine.Vector3)
extern void XROrigin_MatchOriginUpCameraForward_mC4723BFAD3FF3C9ABA7659A74C4CB10800A2C50A (void);
// 0x00000037 System.Boolean Unity.XR.CoreUtils.XROrigin::MatchOriginUpOriginForward(UnityEngine.Vector3,UnityEngine.Vector3)
extern void XROrigin_MatchOriginUpOriginForward_m22ADB294CA783226D0C50DBDCEE0586A85AC9044 (void);
// 0x00000038 System.Boolean Unity.XR.CoreUtils.XROrigin::MoveCameraToWorldLocation(UnityEngine.Vector3)
extern void XROrigin_MoveCameraToWorldLocation_mBF588A0488903F9FEF0F70015E28772522794397 (void);
// 0x00000039 System.Void Unity.XR.CoreUtils.XROrigin::Awake()
extern void XROrigin_Awake_mB3295310096FFDA1017D3226E81748B301EB93FD (void);
// 0x0000003A UnityEngine.Pose Unity.XR.CoreUtils.XROrigin::GetCameraOriginPose()
extern void XROrigin_GetCameraOriginPose_m65F340AB82A12B79C1CE8A2239C49A4FCA34C3F5 (void);
// 0x0000003B System.Void Unity.XR.CoreUtils.XROrigin::OnEnable()
extern void XROrigin_OnEnable_m85878F36D9F87D746996935D8BB5A6B1335E6201 (void);
// 0x0000003C System.Void Unity.XR.CoreUtils.XROrigin::OnDisable()
extern void XROrigin_OnDisable_mCD6CC559152D9231118E30D97F253D6B5B0AE50F (void);
// 0x0000003D System.Void Unity.XR.CoreUtils.XROrigin::OnBeforeRender()
extern void XROrigin_OnBeforeRender_m20A25D128427DB95D8645B7B0C42CEC96547023E (void);
// 0x0000003E System.Void Unity.XR.CoreUtils.XROrigin::OnValidate()
extern void XROrigin_OnValidate_m107F310112D1458AD2F8E44CD064359B9476BEFE (void);
// 0x0000003F System.Void Unity.XR.CoreUtils.XROrigin::Start()
extern void XROrigin_Start_m3E862070874567EBC71CEDFDFCE96346D89872A9 (void);
// 0x00000040 System.Void Unity.XR.CoreUtils.XROrigin::OnDestroy()
extern void XROrigin_OnDestroy_m8F5C01F5CFB4BD23FCC83EA915398067189613B2 (void);
// 0x00000041 System.Void Unity.XR.CoreUtils.XROrigin::.ctor()
extern void XROrigin__ctor_mC64C36F5C94F69F20DDCC912292CB0E6614ED40B (void);
// 0x00000042 System.Void Unity.XR.CoreUtils.XROrigin::.cctor()
extern void XROrigin__cctor_mF110E2E7581DC702D61F2AAE789091C9755F244A (void);
// 0x00000043 System.Boolean Unity.XR.CoreUtils.XROrigin::<OnValidate>g__IsModeStale|60_0()
extern void XROrigin_U3COnValidateU3Eg__IsModeStaleU7C60_0_mFD6964C954EA4827D65DB27D63D003A16594CFB8 (void);
// 0x00000044 System.Void Unity.XR.CoreUtils.XROrigin/<RepeatInitializeCamera>d__48::.ctor(System.Int32)
extern void U3CRepeatInitializeCameraU3Ed__48__ctor_m38D16CB74A912FFAC7BDB663DAC73CCC66F353DB (void);
// 0x00000045 System.Void Unity.XR.CoreUtils.XROrigin/<RepeatInitializeCamera>d__48::System.IDisposable.Dispose()
extern void U3CRepeatInitializeCameraU3Ed__48_System_IDisposable_Dispose_m34D86CC2B00D547934EAB0050158EE4FC2316EB2 (void);
// 0x00000046 System.Boolean Unity.XR.CoreUtils.XROrigin/<RepeatInitializeCamera>d__48::MoveNext()
extern void U3CRepeatInitializeCameraU3Ed__48_MoveNext_m2A28BAD8A5F265425E6CED138B0ECD941C8C6D6A (void);
// 0x00000047 System.Object Unity.XR.CoreUtils.XROrigin/<RepeatInitializeCamera>d__48::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRepeatInitializeCameraU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m85F41DF9153BE60931348D9DA5A5A0B0F5CB24E0 (void);
// 0x00000048 System.Void Unity.XR.CoreUtils.XROrigin/<RepeatInitializeCamera>d__48::System.Collections.IEnumerator.Reset()
extern void U3CRepeatInitializeCameraU3Ed__48_System_Collections_IEnumerator_Reset_m034DF1C46AABE33936127AE4406DAB4F078F88D4 (void);
// 0x00000049 System.Object Unity.XR.CoreUtils.XROrigin/<RepeatInitializeCamera>d__48::System.Collections.IEnumerator.get_Current()
extern void U3CRepeatInitializeCameraU3Ed__48_System_Collections_IEnumerator_get_Current_m7C83C7C74A3582ABE2F428AB355A4C5B175B9E09 (void);
// 0x0000004A System.Void Unity.XR.CoreUtils.Datums.AnimationCurveDatum::.ctor()
extern void AnimationCurveDatum__ctor_mAC74C57A2E1FA217073BC2FA9F929B60B06C0A96 (void);
// 0x0000004B System.Void Unity.XR.CoreUtils.Datums.AnimationCurveDatumProperty::.ctor(UnityEngine.AnimationCurve)
extern void AnimationCurveDatumProperty__ctor_mC666A8B6268F99B4884A55E746C61B23B4D66A78 (void);
// 0x0000004C System.String Unity.XR.CoreUtils.Datums.Datum`1::get_Comments()
// 0x0000004D System.Void Unity.XR.CoreUtils.Datums.Datum`1::set_Comments(System.String)
// 0x0000004E System.Boolean Unity.XR.CoreUtils.Datums.Datum`1::get_ReadOnly()
// 0x0000004F System.Void Unity.XR.CoreUtils.Datums.Datum`1::set_ReadOnly(System.Boolean)
// 0x00000050 Unity.XR.CoreUtils.Bindings.Variables.IReadOnlyBindableVariable`1<T> Unity.XR.CoreUtils.Datums.Datum`1::get_BindableVariableReference()
// 0x00000051 T Unity.XR.CoreUtils.Datums.Datum`1::get_Value()
// 0x00000052 System.Void Unity.XR.CoreUtils.Datums.Datum`1::set_Value(T)
// 0x00000053 System.Void Unity.XR.CoreUtils.Datums.Datum`1::OnEnable()
// 0x00000054 System.Void Unity.XR.CoreUtils.Datums.Datum`1::.ctor()
// 0x00000055 System.Void Unity.XR.CoreUtils.Datums.DatumProperty`2::.ctor()
// 0x00000056 System.Void Unity.XR.CoreUtils.Datums.DatumProperty`2::.ctor(TValue)
// 0x00000057 System.Void Unity.XR.CoreUtils.Datums.DatumProperty`2::.ctor(TDatum)
// 0x00000058 TValue Unity.XR.CoreUtils.Datums.DatumProperty`2::get_Value()
// 0x00000059 System.Void Unity.XR.CoreUtils.Datums.DatumProperty`2::set_Value(TValue)
// 0x0000005A Unity.XR.CoreUtils.Datums.Datum`1<TValue> Unity.XR.CoreUtils.Datums.DatumProperty`2::get_Datum()
// 0x0000005B System.Int32 Unity.XR.CoreUtils.Collections.HashSetList`1::get_Count()
// 0x0000005C System.Boolean Unity.XR.CoreUtils.Collections.HashSetList`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x0000005D T Unity.XR.CoreUtils.Collections.HashSetList`1::get_Item(System.Int32)
// 0x0000005E System.Void Unity.XR.CoreUtils.Collections.HashSetList`1::.ctor(System.Int32)
// 0x0000005F System.Collections.Generic.List`1/Enumerator<T> Unity.XR.CoreUtils.Collections.HashSetList`1::GetEnumerator()
// 0x00000060 System.Collections.Generic.IEnumerator`1<T> Unity.XR.CoreUtils.Collections.HashSetList`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000061 System.Collections.IEnumerator Unity.XR.CoreUtils.Collections.HashSetList`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000062 System.Void Unity.XR.CoreUtils.Collections.HashSetList`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x00000063 System.Boolean Unity.XR.CoreUtils.Collections.HashSetList`1::Add(T)
// 0x00000064 System.Boolean Unity.XR.CoreUtils.Collections.HashSetList`1::Remove(T)
// 0x00000065 System.Void Unity.XR.CoreUtils.Collections.HashSetList`1::Clear()
// 0x00000066 System.Boolean Unity.XR.CoreUtils.Collections.HashSetList`1::Contains(T)
// 0x00000067 System.Void Unity.XR.CoreUtils.Collections.HashSetList`1::CopyTo(T[],System.Int32)
// 0x00000068 System.Void Unity.XR.CoreUtils.Collections.HashSetList`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000069 System.Void Unity.XR.CoreUtils.Collections.HashSetList`1::OnDeserialization(System.Object)
// 0x0000006A System.Void Unity.XR.CoreUtils.Collections.HashSetList`1::RefreshList()
// 0x0000006B System.Collections.Generic.IReadOnlyList`1<T> Unity.XR.CoreUtils.Collections.HashSetList`1::AsList()
// 0x0000006C System.Void Unity.XR.CoreUtils.Bindings.BindingsGroup::AddBinding(Unity.XR.CoreUtils.Bindings.IEventBinding)
extern void BindingsGroup_AddBinding_mB862CA624836F2F41878A83077484E5D4F51BBDC (void);
// 0x0000006D System.Void Unity.XR.CoreUtils.Bindings.BindingsGroup::Clear()
extern void BindingsGroup_Clear_mB28C44D8FC5CBEDCBE41D99AC52A06AD3D3C0024 (void);
// 0x0000006E System.Void Unity.XR.CoreUtils.Bindings.BindingsGroup::.ctor()
extern void BindingsGroup__ctor_m134F76F56E968839A28997A80A0B8F7272F0B745 (void);
// 0x0000006F System.Action Unity.XR.CoreUtils.Bindings.EventBinding::get_BindAction()
extern void EventBinding_get_BindAction_mC84020F3A9209984D2268B991678A608B21DBDE9 (void);
// 0x00000070 System.Void Unity.XR.CoreUtils.Bindings.EventBinding::set_BindAction(System.Action)
extern void EventBinding_set_BindAction_mBDF58F3C06D243CD7774B052BB5B0A90F5996D78 (void);
// 0x00000071 System.Action Unity.XR.CoreUtils.Bindings.EventBinding::get_UnbindAction()
extern void EventBinding_get_UnbindAction_m0F29A3DEC5CAC0716802A686F982DC6739C8EAA9 (void);
// 0x00000072 System.Void Unity.XR.CoreUtils.Bindings.EventBinding::set_UnbindAction(System.Action)
extern void EventBinding_set_UnbindAction_m0F0D30CC2B17576CA59618091D698F91C0FA28E9 (void);
// 0x00000073 System.Void Unity.XR.CoreUtils.Bindings.EventBinding::Bind()
extern void EventBinding_Bind_m2114894CB4EC2EC35EF0A4068F0CFAE4955C9385 (void);
// 0x00000074 System.Void Unity.XR.CoreUtils.Bindings.EventBinding::Unbind()
extern void EventBinding_Unbind_mA28A0372DAF5CF6757FF016EB2D9CA0E405A0A33 (void);
// 0x00000075 System.Void Unity.XR.CoreUtils.Bindings.EventBinding::ClearBinding()
extern void EventBinding_ClearBinding_m464FF9805BA626E2447B53C6FC273D9F79FE5BF1 (void);
// 0x00000076 System.Void Unity.XR.CoreUtils.Bindings.IEventBinding::ClearBinding()
// 0x00000077 System.Void Unity.XR.CoreUtils.Bindings.Variables.BindableVariable`1::.ctor(T,System.Boolean,System.Func`3<T,T,System.Boolean>,System.Boolean)
// 0x00000078 System.Boolean Unity.XR.CoreUtils.Bindings.Variables.BindableVariable`1::ValueEquals(T)
// 0x00000079 System.Void Unity.XR.CoreUtils.Bindings.Variables.BindableVariableAlloc`1::.ctor(T,System.Boolean,System.Func`3<T,T,System.Boolean>,System.Boolean)
// 0x0000007A System.Void Unity.XR.CoreUtils.Bindings.Variables.BindableVariableBase`1::add_valueUpdated(System.Action`1<T>)
// 0x0000007B System.Void Unity.XR.CoreUtils.Bindings.Variables.BindableVariableBase`1::remove_valueUpdated(System.Action`1<T>)
// 0x0000007C T Unity.XR.CoreUtils.Bindings.Variables.BindableVariableBase`1::get_Value()
// 0x0000007D System.Void Unity.XR.CoreUtils.Bindings.Variables.BindableVariableBase`1::set_Value(T)
// 0x0000007E System.Boolean Unity.XR.CoreUtils.Bindings.Variables.BindableVariableBase`1::SetValueWithoutNotify(T)
// 0x0000007F Unity.XR.CoreUtils.Bindings.IEventBinding Unity.XR.CoreUtils.Bindings.Variables.BindableVariableBase`1::Subscribe(System.Action`1<T>)
// 0x00000080 Unity.XR.CoreUtils.Bindings.IEventBinding Unity.XR.CoreUtils.Bindings.Variables.BindableVariableBase`1::SubscribeAndUpdate(System.Action`1<T>)
// 0x00000081 System.Void Unity.XR.CoreUtils.Bindings.Variables.BindableVariableBase`1::IncrementReferenceCount()
// 0x00000082 System.Void Unity.XR.CoreUtils.Bindings.Variables.BindableVariableBase`1::DecrementReferenceCount()
// 0x00000083 System.Void Unity.XR.CoreUtils.Bindings.Variables.BindableVariableBase`1::.ctor(T,System.Boolean,System.Func`3<T,T,System.Boolean>,System.Boolean)
// 0x00000084 System.Void Unity.XR.CoreUtils.Bindings.Variables.BindableVariableBase`1::BroadcastValue()
// 0x00000085 System.Boolean Unity.XR.CoreUtils.Bindings.Variables.BindableVariableBase`1::ValueEquals(T)
// 0x00000086 System.Void Unity.XR.CoreUtils.Bindings.Variables.BindableVariableBase`1/<>c__DisplayClass14_0::.ctor()
// 0x00000087 System.Void Unity.XR.CoreUtils.Bindings.Variables.BindableVariableBase`1/<>c__DisplayClass14_0::<Subscribe>b__0()
// 0x00000088 System.Void Unity.XR.CoreUtils.Bindings.Variables.BindableVariableBase`1/<>c__DisplayClass14_0::<Subscribe>b__1()
// 0x00000089 Unity.XR.CoreUtils.Bindings.IEventBinding Unity.XR.CoreUtils.Bindings.Variables.IReadOnlyBindableVariable`1::Subscribe(System.Action`1<T>)
// 0x0000008A T Unity.XR.CoreUtils.Bindings.Variables.IReadOnlyBindableVariable`1::get_Value()
static Il2CppMethodPointer s_methodPointers[138] = 
{
	EmbeddedAttribute__ctor_m397CF6D74070B39A844CEF4BC9846AF5DC8423B9,
	IsReadOnlyAttribute__ctor_m40764A40A4145B9996BC9AECBAD2C3BCE27FA65D,
	ARTrackablesParentTransformChangedEventArgs_get_Origin_m635FF91C20A6D6C0514EABDDED14289D01BDC0A5,
	ARTrackablesParentTransformChangedEventArgs_get_TrackablesParent_mC93ACA08A0A5E3F00A58F63EF932BD6AA768BA43,
	ARTrackablesParentTransformChangedEventArgs__ctor_m90A574F56EAF0228BD1D7CBFDAF682B8E58F7F9D,
	ARTrackablesParentTransformChangedEventArgs_Equals_mDEA555E578C8B4ED1C2EB86EC70E289D6A542DEE,
	ARTrackablesParentTransformChangedEventArgs_Equals_mD1E41855005B1E2CF7834216ECA32621B6229D53,
	ARTrackablesParentTransformChangedEventArgs_GetHashCode_mDBE0D34111420DBF1A446062AD324FCDE522B51A,
	TransformExtensions_GetWorldPose_m33A1A882B998C9949D1C6424846E60D55B5AEF45,
	TransformExtensions_TransformPose_mEB3D81D8D439A0CB8C08B791A180B78542978AAA,
	FloatUnityEvent__ctor_m3C80AE317A469CC5F7C0920889BBB1F9D86195F5,
	Vector2UnityEvent__ctor_m5030F8916B653DFD26113971AC99AFC58C5D5C94,
	Vector3UnityEvent__ctor_m4C6C0F78526D7E9651F1BC2DE40F9A6B7795B3BB,
	Vector4UnityEvent__ctor_m2C9E326EE573F0E05D04A57312872BD2C89CDF27,
	QuaternionUnityEvent__ctor_m7B6198DF5266C35E7980BE275344236913C5D789,
	ColorUnityEvent__ctor_m718169C7152A67F2CCC6954EDDE9599A722AC92A,
	Vector3Extensions_MaxComponent_m19DF9C78F0799BD2D1F3E5E87565C600D40CFD4D,
	Vector3Extensions_Multiply_m7B8B8D6D08861C9401D99F12DA04FE1CB8438141,
	Vector3Extensions_Divide_m6BEABD7E540B8C17B3738253D908EC1C09D81DED,
	HashCodeUtil_Combine_m5EC3008FE1451272AED445DA4B1E7A65BAAF197E,
	HashCodeUtil_ReferenceHash_m55B8BCB31F847BA215571F96A8FC190A53CE9D26,
	XRLoggingUtils__cctor_m34C67D615FF5E41B0C2930A2B03E09F63E8F0EB1,
	XRLoggingUtils_LogWarning_m16C829B8740982FA4B0EBED606A2D518E492A5CC,
	XRLoggingUtils_LogError_m65734BA6FECDC0C2A35DE1E6401886DFD6515402,
	XROrigin_get_Camera_mDEC1EA5E15968845DA812397BBA4506A88B0F9FF,
	XROrigin_set_Camera_m33ECB239BCDB92806AFD47EA8A4685E81EBDFE24,
	XROrigin_get_TrackablesParent_m1762CFBDDF007C80B97379835702D8607DB4F3E0,
	XROrigin_set_TrackablesParent_mE069D24282C732C5164D03FEEDBA8C9FD8AF323E,
	XROrigin_add_TrackablesParentTransformChanged_mA320D83FF126A31F8139C86BC68BE280EE4BC280,
	XROrigin_remove_TrackablesParentTransformChanged_m4D7C48C781798E32FE484B40997BF6BFB5A8BBB0,
	XROrigin_get_Origin_m791424808DC2F95B332CFCE34A260E5F53DEC2AD,
	XROrigin_set_Origin_mA3665C3245F0B16C7C2AA41381FC8D57A80FD6D3,
	XROrigin_get_CameraFloorOffsetObject_mB06A006D2F1211B477E5AADF9DF6EBCB5EFCCF45,
	XROrigin_set_CameraFloorOffsetObject_mE20DF102DDC9F456EE93F97D69A5EEF7306C1F5E,
	XROrigin_get_RequestedTrackingOriginMode_m4FF9E54CD679A9B3CC05B36314D66F3B1AD1688B,
	XROrigin_set_RequestedTrackingOriginMode_m2D7D0AF1006FF55D8EC6D8BF6679F9867E5D496E,
	XROrigin_get_CameraYOffset_mA396D70F0E27E80EA5177915433C5038BACDDF4E,
	XROrigin_set_CameraYOffset_mBA7E721A282BA9E414B1BCA15E82AE3D48C1CAA4,
	XROrigin_get_CurrentTrackingOriginMode_m90D206E17CF4EBD3DDD281F526E43AA3A1A84AFA,
	XROrigin_set_CurrentTrackingOriginMode_mE1F8C7BD93763B7503A9F1A66224B1E99AFB1680,
	XROrigin_get_OriginInCameraSpacePos_mC934EF4E3E14C7DC72A88E058C39F7A40104174C,
	XROrigin_get_CameraInOriginSpacePos_mA25C4AECE774DCE217DF1AB838E9D82BB87BEE56,
	XROrigin_get_CameraInOriginSpaceHeight_m59DC39ADD21E144299A2B7A283414591CC966FF7,
	XROrigin_MoveOffsetHeight_mE85E434BC9ECF1D224C9A7506B814A3D37B2F3EC,
	XROrigin_MoveOffsetHeight_m89E6241C5D67CCD0AC4A166AA4599AADF39E895F,
	XROrigin_TryInitializeCamera_m8A470425D8369F6555D3C36C3335F9D8F82B40A4,
	XROrigin_SetupCamera_m63ECB705B6D6398F5B8B7C2EF7B33CB37C796BC4,
	XROrigin_SetupCamera_mEB39C60D1AE12C2A25647E578B353E127C406726,
	XROrigin_OnInputSubsystemTrackingOriginUpdated_m132A24B0235B1E82289B45F7B5B9132A19A85719,
	XROrigin_RepeatInitializeCamera_mE77E233DAE27A9547E0BE478CC06ED6E51E8917A,
	XROrigin_RotateAroundCameraUsingOriginUp_mFBC31926C33A2A4BB0FB2CD37A5D6A2DB805C26D,
	XROrigin_RotateAroundCameraPosition_mDFFA1FCA35ABE47B99085527C2DBF97141289EC3,
	XROrigin_MatchOriginUp_mF2000A84EAFDFA87164B64B19BC93324AB030C26,
	XROrigin_MatchOriginUpCameraForward_mC4723BFAD3FF3C9ABA7659A74C4CB10800A2C50A,
	XROrigin_MatchOriginUpOriginForward_m22ADB294CA783226D0C50DBDCEE0586A85AC9044,
	XROrigin_MoveCameraToWorldLocation_mBF588A0488903F9FEF0F70015E28772522794397,
	XROrigin_Awake_mB3295310096FFDA1017D3226E81748B301EB93FD,
	XROrigin_GetCameraOriginPose_m65F340AB82A12B79C1CE8A2239C49A4FCA34C3F5,
	XROrigin_OnEnable_m85878F36D9F87D746996935D8BB5A6B1335E6201,
	XROrigin_OnDisable_mCD6CC559152D9231118E30D97F253D6B5B0AE50F,
	XROrigin_OnBeforeRender_m20A25D128427DB95D8645B7B0C42CEC96547023E,
	XROrigin_OnValidate_m107F310112D1458AD2F8E44CD064359B9476BEFE,
	XROrigin_Start_m3E862070874567EBC71CEDFDFCE96346D89872A9,
	XROrigin_OnDestroy_m8F5C01F5CFB4BD23FCC83EA915398067189613B2,
	XROrigin__ctor_mC64C36F5C94F69F20DDCC912292CB0E6614ED40B,
	XROrigin__cctor_mF110E2E7581DC702D61F2AAE789091C9755F244A,
	XROrigin_U3COnValidateU3Eg__IsModeStaleU7C60_0_mFD6964C954EA4827D65DB27D63D003A16594CFB8,
	U3CRepeatInitializeCameraU3Ed__48__ctor_m38D16CB74A912FFAC7BDB663DAC73CCC66F353DB,
	U3CRepeatInitializeCameraU3Ed__48_System_IDisposable_Dispose_m34D86CC2B00D547934EAB0050158EE4FC2316EB2,
	U3CRepeatInitializeCameraU3Ed__48_MoveNext_m2A28BAD8A5F265425E6CED138B0ECD941C8C6D6A,
	U3CRepeatInitializeCameraU3Ed__48_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m85F41DF9153BE60931348D9DA5A5A0B0F5CB24E0,
	U3CRepeatInitializeCameraU3Ed__48_System_Collections_IEnumerator_Reset_m034DF1C46AABE33936127AE4406DAB4F078F88D4,
	U3CRepeatInitializeCameraU3Ed__48_System_Collections_IEnumerator_get_Current_m7C83C7C74A3582ABE2F428AB355A4C5B175B9E09,
	AnimationCurveDatum__ctor_mAC74C57A2E1FA217073BC2FA9F929B60B06C0A96,
	AnimationCurveDatumProperty__ctor_mC666A8B6268F99B4884A55E746C61B23B4D66A78,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BindingsGroup_AddBinding_mB862CA624836F2F41878A83077484E5D4F51BBDC,
	BindingsGroup_Clear_mB28C44D8FC5CBEDCBE41D99AC52A06AD3D3C0024,
	BindingsGroup__ctor_m134F76F56E968839A28997A80A0B8F7272F0B745,
	EventBinding_get_BindAction_mC84020F3A9209984D2268B991678A608B21DBDE9,
	EventBinding_set_BindAction_mBDF58F3C06D243CD7774B052BB5B0A90F5996D78,
	EventBinding_get_UnbindAction_m0F29A3DEC5CAC0716802A686F982DC6739C8EAA9,
	EventBinding_set_UnbindAction_m0F0D30CC2B17576CA59618091D698F91C0FA28E9,
	EventBinding_Bind_m2114894CB4EC2EC35EF0A4068F0CFAE4955C9385,
	EventBinding_Unbind_mA28A0372DAF5CF6757FF016EB2D9CA0E405A0A33,
	EventBinding_ClearBinding_m464FF9805BA626E2447B53C6FC273D9F79FE5BF1,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
extern void ARTrackablesParentTransformChangedEventArgs_get_Origin_m635FF91C20A6D6C0514EABDDED14289D01BDC0A5_AdjustorThunk (void);
extern void ARTrackablesParentTransformChangedEventArgs_get_TrackablesParent_mC93ACA08A0A5E3F00A58F63EF932BD6AA768BA43_AdjustorThunk (void);
extern void ARTrackablesParentTransformChangedEventArgs__ctor_m90A574F56EAF0228BD1D7CBFDAF682B8E58F7F9D_AdjustorThunk (void);
extern void ARTrackablesParentTransformChangedEventArgs_Equals_mDEA555E578C8B4ED1C2EB86EC70E289D6A542DEE_AdjustorThunk (void);
extern void ARTrackablesParentTransformChangedEventArgs_Equals_mD1E41855005B1E2CF7834216ECA32621B6229D53_AdjustorThunk (void);
extern void ARTrackablesParentTransformChangedEventArgs_GetHashCode_mDBE0D34111420DBF1A446062AD324FCDE522B51A_AdjustorThunk (void);
extern void EventBinding_get_BindAction_mC84020F3A9209984D2268B991678A608B21DBDE9_AdjustorThunk (void);
extern void EventBinding_set_BindAction_mBDF58F3C06D243CD7774B052BB5B0A90F5996D78_AdjustorThunk (void);
extern void EventBinding_get_UnbindAction_m0F29A3DEC5CAC0716802A686F982DC6739C8EAA9_AdjustorThunk (void);
extern void EventBinding_set_UnbindAction_m0F0D30CC2B17576CA59618091D698F91C0FA28E9_AdjustorThunk (void);
extern void EventBinding_Bind_m2114894CB4EC2EC35EF0A4068F0CFAE4955C9385_AdjustorThunk (void);
extern void EventBinding_Unbind_mA28A0372DAF5CF6757FF016EB2D9CA0E405A0A33_AdjustorThunk (void);
extern void EventBinding_ClearBinding_m464FF9805BA626E2447B53C6FC273D9F79FE5BF1_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[13] = 
{
	{ 0x06000003, ARTrackablesParentTransformChangedEventArgs_get_Origin_m635FF91C20A6D6C0514EABDDED14289D01BDC0A5_AdjustorThunk },
	{ 0x06000004, ARTrackablesParentTransformChangedEventArgs_get_TrackablesParent_mC93ACA08A0A5E3F00A58F63EF932BD6AA768BA43_AdjustorThunk },
	{ 0x06000005, ARTrackablesParentTransformChangedEventArgs__ctor_m90A574F56EAF0228BD1D7CBFDAF682B8E58F7F9D_AdjustorThunk },
	{ 0x06000006, ARTrackablesParentTransformChangedEventArgs_Equals_mDEA555E578C8B4ED1C2EB86EC70E289D6A542DEE_AdjustorThunk },
	{ 0x06000007, ARTrackablesParentTransformChangedEventArgs_Equals_mD1E41855005B1E2CF7834216ECA32621B6229D53_AdjustorThunk },
	{ 0x06000008, ARTrackablesParentTransformChangedEventArgs_GetHashCode_mDBE0D34111420DBF1A446062AD324FCDE522B51A_AdjustorThunk },
	{ 0x0600006F, EventBinding_get_BindAction_mC84020F3A9209984D2268B991678A608B21DBDE9_AdjustorThunk },
	{ 0x06000070, EventBinding_set_BindAction_mBDF58F3C06D243CD7774B052BB5B0A90F5996D78_AdjustorThunk },
	{ 0x06000071, EventBinding_get_UnbindAction_m0F29A3DEC5CAC0716802A686F982DC6739C8EAA9_AdjustorThunk },
	{ 0x06000072, EventBinding_set_UnbindAction_m0F0D30CC2B17576CA59618091D698F91C0FA28E9_AdjustorThunk },
	{ 0x06000073, EventBinding_Bind_m2114894CB4EC2EC35EF0A4068F0CFAE4955C9385_AdjustorThunk },
	{ 0x06000074, EventBinding_Unbind_mA28A0372DAF5CF6757FF016EB2D9CA0E405A0A33_AdjustorThunk },
	{ 0x06000075, EventBinding_ClearBinding_m464FF9805BA626E2447B53C6FC273D9F79FE5BF1_AdjustorThunk },
};
static const int32_t s_InvokerIndices[138] = 
{
	5515,
	5515,
	5428,
	5428,
	2478,
	3049,
	3171,
	5392,
	8461,
	7608,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	8515,
	7697,
	7697,
	7481,
	8249,
	8766,
	7867,
	7867,
	5428,
	4418,
	5428,
	4418,
	4418,
	4418,
	5428,
	4418,
	5428,
	4418,
	5392,
	4386,
	5473,
	4458,
	5392,
	4386,
	5510,
	5510,
	5473,
	5515,
	4458,
	5515,
	5332,
	3171,
	4418,
	5428,
	3214,
	1654,
	3247,
	1655,
	1655,
	3247,
	5515,
	5437,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	5515,
	8766,
	5332,
	4386,
	5515,
	5332,
	5428,
	5515,
	5428,
	5515,
	4418,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4418,
	5515,
	5515,
	5428,
	4418,
	5428,
	4418,
	5515,
	5515,
	5515,
	5515,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[7] = 
{
	{ 0x02000014, { 0, 4 } },
	{ 0x02000015, { 4, 4 } },
	{ 0x02000016, { 8, 21 } },
	{ 0x0200001A, { 29, 6 } },
	{ 0x0200001B, { 35, 2 } },
	{ 0x0200001C, { 37, 13 } },
	{ 0x0200001D, { 50, 4 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[54] = 
{
	{ (Il2CppRGCTXDataType)3, 3653 },
	{ (Il2CppRGCTXDataType)3, 5112 },
	{ (Il2CppRGCTXDataType)2, 1776 },
	{ (Il2CppRGCTXDataType)3, 3636 },
	{ (Il2CppRGCTXDataType)3, 5079 },
	{ (Il2CppRGCTXDataType)3, 5110 },
	{ (Il2CppRGCTXDataType)3, 5111 },
	{ (Il2CppRGCTXDataType)2, 1089 },
	{ (Il2CppRGCTXDataType)3, 18992 },
	{ (Il2CppRGCTXDataType)3, 18993 },
	{ (Il2CppRGCTXDataType)2, 4533 },
	{ (Il2CppRGCTXDataType)3, 18985 },
	{ (Il2CppRGCTXDataType)2, 3035 },
	{ (Il2CppRGCTXDataType)3, 14448 },
	{ (Il2CppRGCTXDataType)3, 18990 },
	{ (Il2CppRGCTXDataType)2, 2356 },
	{ (Il2CppRGCTXDataType)3, 14373 },
	{ (Il2CppRGCTXDataType)3, 14449 },
	{ (Il2CppRGCTXDataType)3, 18986 },
	{ (Il2CppRGCTXDataType)3, 14454 },
	{ (Il2CppRGCTXDataType)3, 18991 },
	{ (Il2CppRGCTXDataType)3, 14450 },
	{ (Il2CppRGCTXDataType)3, 18988 },
	{ (Il2CppRGCTXDataType)3, 14451 },
	{ (Il2CppRGCTXDataType)3, 18989 },
	{ (Il2CppRGCTXDataType)3, 14452 },
	{ (Il2CppRGCTXDataType)3, 14374 },
	{ (Il2CppRGCTXDataType)3, 14453 },
	{ (Il2CppRGCTXDataType)3, 18987 },
	{ (Il2CppRGCTXDataType)3, 3649 },
	{ (Il2CppRGCTXDataType)2, 1779 },
	{ (Il2CppRGCTXDataType)3, 3652 },
	{ (Il2CppRGCTXDataType)2, 618 },
	{ (Il2CppRGCTXDataType)2, 3668 },
	{ (Il2CppRGCTXDataType)3, 14684 },
	{ (Il2CppRGCTXDataType)3, 3640 },
	{ (Il2CppRGCTXDataType)2, 1778 },
	{ (Il2CppRGCTXDataType)2, 1383 },
	{ (Il2CppRGCTXDataType)3, 40383 },
	{ (Il2CppRGCTXDataType)3, 3646 },
	{ (Il2CppRGCTXDataType)3, 3645 },
	{ (Il2CppRGCTXDataType)3, 3648 },
	{ (Il2CppRGCTXDataType)3, 13584 },
	{ (Il2CppRGCTXDataType)2, 1306 },
	{ (Il2CppRGCTXDataType)3, 120 },
	{ (Il2CppRGCTXDataType)3, 121 },
	{ (Il2CppRGCTXDataType)3, 122 },
	{ (Il2CppRGCTXDataType)3, 1184 },
	{ (Il2CppRGCTXDataType)3, 3647 },
	{ (Il2CppRGCTXDataType)2, 615 },
	{ (Il2CppRGCTXDataType)3, 3668 },
	{ (Il2CppRGCTXDataType)3, 3667 },
	{ (Il2CppRGCTXDataType)3, 3669 },
	{ (Il2CppRGCTXDataType)3, 3666 },
};
extern const CustomAttributesCacheGenerator g_Unity_XR_CoreUtils_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Unity_XR_CoreUtils_CodeGenModule;
const Il2CppCodeGenModule g_Unity_XR_CoreUtils_CodeGenModule = 
{
	"Unity.XR.CoreUtils.dll",
	138,
	s_methodPointers,
	13,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	7,
	s_rgctxIndices,
	54,
	s_rgctxValues,
	NULL,
	g_Unity_XR_CoreUtils_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};

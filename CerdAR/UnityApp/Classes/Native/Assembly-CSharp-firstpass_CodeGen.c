﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void WebViewObject::Awake()
extern void WebViewObject_Awake_m2479A80BF381F426557004B814EB5ADA9C4090B7 (void);
// 0x00000002 System.Boolean WebViewObject::get_IsKeyboardVisible()
extern void WebViewObject_get_IsKeyboardVisible_mBE163BC2CE093A03404D096FCE83868D415FEDE7 (void);
// 0x00000003 System.IntPtr WebViewObject::_CWebViewPlugin_Init(System.String,System.Boolean,System.Boolean,System.String,System.Boolean,System.Int32,System.Boolean)
extern void WebViewObject__CWebViewPlugin_Init_m0E80D3901024ABAF6930FBD79DE954C819113A16 (void);
// 0x00000004 System.Int32 WebViewObject::_CWebViewPlugin_Destroy(System.IntPtr)
extern void WebViewObject__CWebViewPlugin_Destroy_mA7C9DC6CD8D34F9CCCEBEB9B000B1918FF2594AD (void);
// 0x00000005 System.Void WebViewObject::_CWebViewPlugin_SetMargins(System.IntPtr,System.Single,System.Single,System.Single,System.Single,System.Boolean)
extern void WebViewObject__CWebViewPlugin_SetMargins_mB3B023742584BCF7E03863B124345569196EF268 (void);
// 0x00000006 System.Void WebViewObject::_CWebViewPlugin_SetVisibility(System.IntPtr,System.Boolean)
extern void WebViewObject__CWebViewPlugin_SetVisibility_mFF35C98810E952F7B57EF867F3B8E2163FEE2B81 (void);
// 0x00000007 System.Void WebViewObject::_CWebViewPlugin_SetScrollbarsVisibility(System.IntPtr,System.Boolean)
extern void WebViewObject__CWebViewPlugin_SetScrollbarsVisibility_mB5008BCF8B9190B286D416B61A75F6667F1A5BC0 (void);
// 0x00000008 System.Void WebViewObject::_CWebViewPlugin_SetAlertDialogEnabled(System.IntPtr,System.Boolean)
extern void WebViewObject__CWebViewPlugin_SetAlertDialogEnabled_mDFBDF3AF3208F661AE20E9C11A05BE5509408F06 (void);
// 0x00000009 System.Void WebViewObject::_CWebViewPlugin_SetScrollBounceEnabled(System.IntPtr,System.Boolean)
extern void WebViewObject__CWebViewPlugin_SetScrollBounceEnabled_m07AB6DE6CC0AB843C01C7689C2A015FA459EA456 (void);
// 0x0000000A System.Void WebViewObject::_CWebViewPlugin_SetInteractionEnabled(System.IntPtr,System.Boolean)
extern void WebViewObject__CWebViewPlugin_SetInteractionEnabled_m703F952DDB87DCAA6B73EE43E94EB37D1CD51CDE (void);
// 0x0000000B System.Boolean WebViewObject::_CWebViewPlugin_SetURLPattern(System.IntPtr,System.String,System.String,System.String)
extern void WebViewObject__CWebViewPlugin_SetURLPattern_m7C41F86D9177FE22FAA5AD75D552124E1B211A45 (void);
// 0x0000000C System.Void WebViewObject::_CWebViewPlugin_LoadURL(System.IntPtr,System.String)
extern void WebViewObject__CWebViewPlugin_LoadURL_mCBC755D8C818ACDD53EB106A06BA9326D997117D (void);
// 0x0000000D System.Void WebViewObject::_CWebViewPlugin_LoadHTML(System.IntPtr,System.String,System.String)
extern void WebViewObject__CWebViewPlugin_LoadHTML_mEB585F8C8EC855CD431251CF786D0EC01DFC330D (void);
// 0x0000000E System.Void WebViewObject::_CWebViewPlugin_EvaluateJS(System.IntPtr,System.String)
extern void WebViewObject__CWebViewPlugin_EvaluateJS_mF3409DDBEA43368DF7C111951C055680EB0411B3 (void);
// 0x0000000F System.Int32 WebViewObject::_CWebViewPlugin_Progress(System.IntPtr)
extern void WebViewObject__CWebViewPlugin_Progress_m1FE3D66B93E9FB3D55800533DA9D50693C40FF52 (void);
// 0x00000010 System.Boolean WebViewObject::_CWebViewPlugin_CanGoBack(System.IntPtr)
extern void WebViewObject__CWebViewPlugin_CanGoBack_m0792DAA7E383E9AE06E9ABEA01F2748653651CC2 (void);
// 0x00000011 System.Boolean WebViewObject::_CWebViewPlugin_CanGoForward(System.IntPtr)
extern void WebViewObject__CWebViewPlugin_CanGoForward_m03A744513E7A6F84336251DE48B71C4D96811D7D (void);
// 0x00000012 System.Void WebViewObject::_CWebViewPlugin_GoBack(System.IntPtr)
extern void WebViewObject__CWebViewPlugin_GoBack_m699637D677E6C4D66FE5821B7FAAC4C9720C0E94 (void);
// 0x00000013 System.Void WebViewObject::_CWebViewPlugin_GoForward(System.IntPtr)
extern void WebViewObject__CWebViewPlugin_GoForward_m6214084D60BE14AD8979B5A1F1737D4977CFE63D (void);
// 0x00000014 System.Void WebViewObject::_CWebViewPlugin_Reload(System.IntPtr)
extern void WebViewObject__CWebViewPlugin_Reload_mCF6C707FB0549018A3AA8C637F0FBDEFDB291FF4 (void);
// 0x00000015 System.Void WebViewObject::_CWebViewPlugin_AddCustomHeader(System.IntPtr,System.String,System.String)
extern void WebViewObject__CWebViewPlugin_AddCustomHeader_mF987F51AC2ADCD8580F2E8A798ED260C3256CBE5 (void);
// 0x00000016 System.String WebViewObject::_CWebViewPlugin_GetCustomHeaderValue(System.IntPtr,System.String)
extern void WebViewObject__CWebViewPlugin_GetCustomHeaderValue_m00B70B436EEBE5FD9A09912D13EC01C3493646D9 (void);
// 0x00000017 System.Void WebViewObject::_CWebViewPlugin_RemoveCustomHeader(System.IntPtr,System.String)
extern void WebViewObject__CWebViewPlugin_RemoveCustomHeader_mEFE86DB41B599CCD948456D7ECC2C38152D47F32 (void);
// 0x00000018 System.Void WebViewObject::_CWebViewPlugin_ClearCustomHeader(System.IntPtr)
extern void WebViewObject__CWebViewPlugin_ClearCustomHeader_m65D4C7B94B35A4C69319275A0296732608E4728C (void);
// 0x00000019 System.Void WebViewObject::_CWebViewPlugin_ClearCookies()
extern void WebViewObject__CWebViewPlugin_ClearCookies_m906D80E0C60733BD95F16B36F039940E533B704D (void);
// 0x0000001A System.Void WebViewObject::_CWebViewPlugin_SaveCookies()
extern void WebViewObject__CWebViewPlugin_SaveCookies_m1EC1592BE4241E9317F629EFD44726C7ADC93AB5 (void);
// 0x0000001B System.String WebViewObject::_CWebViewPlugin_GetCookies(System.String)
extern void WebViewObject__CWebViewPlugin_GetCookies_m444016E7EEB0B240DACECD4FD54A19E169C4F3D0 (void);
// 0x0000001C System.Void WebViewObject::_CWebViewPlugin_SetBasicAuthInfo(System.IntPtr,System.String,System.String)
extern void WebViewObject__CWebViewPlugin_SetBasicAuthInfo_m25864F2B1764FA42C3451A604EC50C01887B5DE0 (void);
// 0x0000001D System.Void WebViewObject::_CWebViewPlugin_ClearCache(System.IntPtr,System.Boolean)
extern void WebViewObject__CWebViewPlugin_ClearCache_mA9FE773F517FB9A78FFE3090333B77F08856552D (void);
// 0x0000001E System.Boolean WebViewObject::IsWebViewAvailable()
extern void WebViewObject_IsWebViewAvailable_m730482C758E0E6350B1A5CCF0B4D9438548CC17D (void);
// 0x0000001F System.Void WebViewObject::Init(System.Action`1<System.String>,System.Action`1<System.String>,System.Action`1<System.String>,System.Action`1<System.String>,System.Action`1<System.String>,System.Action`1<System.String>,System.Boolean,System.Boolean,System.String,System.Int32,System.Boolean,System.Int32,System.Boolean,System.Boolean)
extern void WebViewObject_Init_mE71A84E46BD4563E56B84BF06A56DC6160B97B30 (void);
// 0x00000020 System.Void WebViewObject::OnDestroy()
extern void WebViewObject_OnDestroy_m656ACF4F10BC03EC8E0884195978D2EF3768B6E9 (void);
// 0x00000021 System.Void WebViewObject::Pause()
extern void WebViewObject_Pause_mABAD2C7BDDACF1DA60A79BA507FF9E1ECC1F7592 (void);
// 0x00000022 System.Void WebViewObject::Resume()
extern void WebViewObject_Resume_m7F731886000E88566C93CB5F47CB981269E61DCD (void);
// 0x00000023 System.Void WebViewObject::SetCenterPositionWithScale(UnityEngine.Vector2,UnityEngine.Vector2)
extern void WebViewObject_SetCenterPositionWithScale_mD2E7477D0ABE98D562162D8CAB7541D084208729 (void);
// 0x00000024 System.Void WebViewObject::SetMargins(System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean)
extern void WebViewObject_SetMargins_m77E97BCD82A9E0A9B29BE83EE316149CEF863E1C (void);
// 0x00000025 System.Void WebViewObject::SetVisibility(System.Boolean)
extern void WebViewObject_SetVisibility_m8FF4BB72505A2632D450B23857EF7B2DF959BF72 (void);
// 0x00000026 System.Boolean WebViewObject::GetVisibility()
extern void WebViewObject_GetVisibility_m8811FDD7E75C8309FCC9F86649A8B8CDD59151B4 (void);
// 0x00000027 System.Void WebViewObject::SetScrollbarsVisibility(System.Boolean)
extern void WebViewObject_SetScrollbarsVisibility_m71D39E811BA9695300A5143DD935B44C2B75333C (void);
// 0x00000028 System.Void WebViewObject::SetInteractionEnabled(System.Boolean)
extern void WebViewObject_SetInteractionEnabled_mE08B4E19776522676C12193C4651B980C3DD66CE (void);
// 0x00000029 System.Void WebViewObject::SetAlertDialogEnabled(System.Boolean)
extern void WebViewObject_SetAlertDialogEnabled_mB0B9ABEFCC2CAAFBF74F2C9E3920C858CEF71C3E (void);
// 0x0000002A System.Boolean WebViewObject::GetAlertDialogEnabled()
extern void WebViewObject_GetAlertDialogEnabled_mBDD96D25CE495D22E4FBFCE0C038719706560C35 (void);
// 0x0000002B System.Void WebViewObject::SetScrollBounceEnabled(System.Boolean)
extern void WebViewObject_SetScrollBounceEnabled_mD54E5C8BB32524D4C175DC18B3190E62141337BC (void);
// 0x0000002C System.Boolean WebViewObject::GetScrollBounceEnabled()
extern void WebViewObject_GetScrollBounceEnabled_mEB6BBB64CA6ACD8F0B9C6288BAF9CF3B797762B8 (void);
// 0x0000002D System.Void WebViewObject::SetCameraAccess(System.Boolean)
extern void WebViewObject_SetCameraAccess_mCCF735816CDBA974A6BDD0277530C344FD0B3B22 (void);
// 0x0000002E System.Void WebViewObject::SetMicrophoneAccess(System.Boolean)
extern void WebViewObject_SetMicrophoneAccess_m117B976F1994698EB81D886EFF581A3621874070 (void);
// 0x0000002F System.Boolean WebViewObject::SetURLPattern(System.String,System.String,System.String)
extern void WebViewObject_SetURLPattern_mB5D70B5CE25822B7B3B5EC461158E385C3AF833C (void);
// 0x00000030 System.Void WebViewObject::LoadURL(System.String)
extern void WebViewObject_LoadURL_m5FF41A80EB02188056861E34E824077D77F7F4E1 (void);
// 0x00000031 System.Void WebViewObject::LoadHTML(System.String,System.String)
extern void WebViewObject_LoadHTML_m2CD61821B1DF575CD474E7988DC1031DF823D387 (void);
// 0x00000032 System.Void WebViewObject::EvaluateJS(System.String)
extern void WebViewObject_EvaluateJS_m220DC9C8C6A476A9917760D4A4E38BD382CFF30A (void);
// 0x00000033 System.Int32 WebViewObject::Progress()
extern void WebViewObject_Progress_mBEFEADC9225B52DFC808D25ED70FC027057F5AA1 (void);
// 0x00000034 System.Boolean WebViewObject::CanGoBack()
extern void WebViewObject_CanGoBack_mB4C735CB085B8015B480F8A28BDD50232A1385C7 (void);
// 0x00000035 System.Boolean WebViewObject::CanGoForward()
extern void WebViewObject_CanGoForward_m6DFD772C07CDB64E5D61BC55ECC50B8A8AF33DC6 (void);
// 0x00000036 System.Void WebViewObject::GoBack()
extern void WebViewObject_GoBack_m5947506758751DBED1F2229E5A16C7FA85A9C3FA (void);
// 0x00000037 System.Void WebViewObject::GoForward()
extern void WebViewObject_GoForward_m53FE4DE6920EBCF2C1F8841C975A4A938C9ADA3F (void);
// 0x00000038 System.Void WebViewObject::Reload()
extern void WebViewObject_Reload_m3C76F5E71CD39151C33EDF3C8D2A5D44414D458C (void);
// 0x00000039 System.Void WebViewObject::CallOnError(System.String)
extern void WebViewObject_CallOnError_mA0D59464A9DEFC616FB4AB4C8CE68595EE1B014C (void);
// 0x0000003A System.Void WebViewObject::CallOnHttpError(System.String)
extern void WebViewObject_CallOnHttpError_m7C685D4E301209DA45AC0C587F900D09B4174F3E (void);
// 0x0000003B System.Void WebViewObject::CallOnStarted(System.String)
extern void WebViewObject_CallOnStarted_m33E7CC35162E85E3AF1FF0906CDB6D02B8A2F9EB (void);
// 0x0000003C System.Void WebViewObject::CallOnLoaded(System.String)
extern void WebViewObject_CallOnLoaded_m4E2519EE9D1014784CF02AF00E0FCB473F0CD61D (void);
// 0x0000003D System.Void WebViewObject::CallFromJS(System.String)
extern void WebViewObject_CallFromJS_m1A086ADB71334EEE53DBC6E4C5A68955F8356E81 (void);
// 0x0000003E System.Void WebViewObject::CallOnHooked(System.String)
extern void WebViewObject_CallOnHooked_mBD21B01C8B3E27C5867D7221BEF6ACEB0C14EB5F (void);
// 0x0000003F System.Void WebViewObject::AddCustomHeader(System.String,System.String)
extern void WebViewObject_AddCustomHeader_mB9207F036B5B938D1606B062825ED1F2EFC4903D (void);
// 0x00000040 System.String WebViewObject::GetCustomHeaderValue(System.String)
extern void WebViewObject_GetCustomHeaderValue_mDBA6E91C38D5CD70395D29101529D0CF2BC9C918 (void);
// 0x00000041 System.Void WebViewObject::RemoveCustomHeader(System.String)
extern void WebViewObject_RemoveCustomHeader_m13B572E1B740C274EA0A7B30E8E0884AC203E0D3 (void);
// 0x00000042 System.Void WebViewObject::ClearCustomHeader()
extern void WebViewObject_ClearCustomHeader_m9D8C91F6C18628ABC0FAEEAFE2F25330EEE3369E (void);
// 0x00000043 System.Void WebViewObject::ClearCookies()
extern void WebViewObject_ClearCookies_mD08727CF5BCE0FF8F5FECE1CE090026E8BCCC7C5 (void);
// 0x00000044 System.Void WebViewObject::SaveCookies()
extern void WebViewObject_SaveCookies_m6081145D12FB3149742F2A158AAA259D62C74F89 (void);
// 0x00000045 System.String WebViewObject::GetCookies(System.String)
extern void WebViewObject_GetCookies_mA36C6AA27F87AFEB30E2EF8C32976E868F1943EF (void);
// 0x00000046 System.Void WebViewObject::SetBasicAuthInfo(System.String,System.String)
extern void WebViewObject_SetBasicAuthInfo_mA4AE183063615D3D69E02C1FB1B44D824B9B7FD2 (void);
// 0x00000047 System.Void WebViewObject::ClearCache(System.Boolean)
extern void WebViewObject_ClearCache_m685CFFA50D94672BB29EEBFA16354B732159523A (void);
// 0x00000048 System.Void WebViewObject::SetTextZoom(System.Int32)
extern void WebViewObject_SetTextZoom_m5A3A9F812A77504D8FCD5582C7AF44B26CAE61AB (void);
// 0x00000049 System.Void WebViewObject::.ctor()
extern void WebViewObject__ctor_m6BABBDBFF7B566CB90576F521C33FFD6FE103017 (void);
static Il2CppMethodPointer s_methodPointers[73] = 
{
	WebViewObject_Awake_m2479A80BF381F426557004B814EB5ADA9C4090B7,
	WebViewObject_get_IsKeyboardVisible_mBE163BC2CE093A03404D096FCE83868D415FEDE7,
	WebViewObject__CWebViewPlugin_Init_m0E80D3901024ABAF6930FBD79DE954C819113A16,
	WebViewObject__CWebViewPlugin_Destroy_mA7C9DC6CD8D34F9CCCEBEB9B000B1918FF2594AD,
	WebViewObject__CWebViewPlugin_SetMargins_mB3B023742584BCF7E03863B124345569196EF268,
	WebViewObject__CWebViewPlugin_SetVisibility_mFF35C98810E952F7B57EF867F3B8E2163FEE2B81,
	WebViewObject__CWebViewPlugin_SetScrollbarsVisibility_mB5008BCF8B9190B286D416B61A75F6667F1A5BC0,
	WebViewObject__CWebViewPlugin_SetAlertDialogEnabled_mDFBDF3AF3208F661AE20E9C11A05BE5509408F06,
	WebViewObject__CWebViewPlugin_SetScrollBounceEnabled_m07AB6DE6CC0AB843C01C7689C2A015FA459EA456,
	WebViewObject__CWebViewPlugin_SetInteractionEnabled_m703F952DDB87DCAA6B73EE43E94EB37D1CD51CDE,
	WebViewObject__CWebViewPlugin_SetURLPattern_m7C41F86D9177FE22FAA5AD75D552124E1B211A45,
	WebViewObject__CWebViewPlugin_LoadURL_mCBC755D8C818ACDD53EB106A06BA9326D997117D,
	WebViewObject__CWebViewPlugin_LoadHTML_mEB585F8C8EC855CD431251CF786D0EC01DFC330D,
	WebViewObject__CWebViewPlugin_EvaluateJS_mF3409DDBEA43368DF7C111951C055680EB0411B3,
	WebViewObject__CWebViewPlugin_Progress_m1FE3D66B93E9FB3D55800533DA9D50693C40FF52,
	WebViewObject__CWebViewPlugin_CanGoBack_m0792DAA7E383E9AE06E9ABEA01F2748653651CC2,
	WebViewObject__CWebViewPlugin_CanGoForward_m03A744513E7A6F84336251DE48B71C4D96811D7D,
	WebViewObject__CWebViewPlugin_GoBack_m699637D677E6C4D66FE5821B7FAAC4C9720C0E94,
	WebViewObject__CWebViewPlugin_GoForward_m6214084D60BE14AD8979B5A1F1737D4977CFE63D,
	WebViewObject__CWebViewPlugin_Reload_mCF6C707FB0549018A3AA8C637F0FBDEFDB291FF4,
	WebViewObject__CWebViewPlugin_AddCustomHeader_mF987F51AC2ADCD8580F2E8A798ED260C3256CBE5,
	WebViewObject__CWebViewPlugin_GetCustomHeaderValue_m00B70B436EEBE5FD9A09912D13EC01C3493646D9,
	WebViewObject__CWebViewPlugin_RemoveCustomHeader_mEFE86DB41B599CCD948456D7ECC2C38152D47F32,
	WebViewObject__CWebViewPlugin_ClearCustomHeader_m65D4C7B94B35A4C69319275A0296732608E4728C,
	WebViewObject__CWebViewPlugin_ClearCookies_m906D80E0C60733BD95F16B36F039940E533B704D,
	WebViewObject__CWebViewPlugin_SaveCookies_m1EC1592BE4241E9317F629EFD44726C7ADC93AB5,
	WebViewObject__CWebViewPlugin_GetCookies_m444016E7EEB0B240DACECD4FD54A19E169C4F3D0,
	WebViewObject__CWebViewPlugin_SetBasicAuthInfo_m25864F2B1764FA42C3451A604EC50C01887B5DE0,
	WebViewObject__CWebViewPlugin_ClearCache_mA9FE773F517FB9A78FFE3090333B77F08856552D,
	WebViewObject_IsWebViewAvailable_m730482C758E0E6350B1A5CCF0B4D9438548CC17D,
	WebViewObject_Init_mE71A84E46BD4563E56B84BF06A56DC6160B97B30,
	WebViewObject_OnDestroy_m656ACF4F10BC03EC8E0884195978D2EF3768B6E9,
	WebViewObject_Pause_mABAD2C7BDDACF1DA60A79BA507FF9E1ECC1F7592,
	WebViewObject_Resume_m7F731886000E88566C93CB5F47CB981269E61DCD,
	WebViewObject_SetCenterPositionWithScale_mD2E7477D0ABE98D562162D8CAB7541D084208729,
	WebViewObject_SetMargins_m77E97BCD82A9E0A9B29BE83EE316149CEF863E1C,
	WebViewObject_SetVisibility_m8FF4BB72505A2632D450B23857EF7B2DF959BF72,
	WebViewObject_GetVisibility_m8811FDD7E75C8309FCC9F86649A8B8CDD59151B4,
	WebViewObject_SetScrollbarsVisibility_m71D39E811BA9695300A5143DD935B44C2B75333C,
	WebViewObject_SetInteractionEnabled_mE08B4E19776522676C12193C4651B980C3DD66CE,
	WebViewObject_SetAlertDialogEnabled_mB0B9ABEFCC2CAAFBF74F2C9E3920C858CEF71C3E,
	WebViewObject_GetAlertDialogEnabled_mBDD96D25CE495D22E4FBFCE0C038719706560C35,
	WebViewObject_SetScrollBounceEnabled_mD54E5C8BB32524D4C175DC18B3190E62141337BC,
	WebViewObject_GetScrollBounceEnabled_mEB6BBB64CA6ACD8F0B9C6288BAF9CF3B797762B8,
	WebViewObject_SetCameraAccess_mCCF735816CDBA974A6BDD0277530C344FD0B3B22,
	WebViewObject_SetMicrophoneAccess_m117B976F1994698EB81D886EFF581A3621874070,
	WebViewObject_SetURLPattern_mB5D70B5CE25822B7B3B5EC461158E385C3AF833C,
	WebViewObject_LoadURL_m5FF41A80EB02188056861E34E824077D77F7F4E1,
	WebViewObject_LoadHTML_m2CD61821B1DF575CD474E7988DC1031DF823D387,
	WebViewObject_EvaluateJS_m220DC9C8C6A476A9917760D4A4E38BD382CFF30A,
	WebViewObject_Progress_mBEFEADC9225B52DFC808D25ED70FC027057F5AA1,
	WebViewObject_CanGoBack_mB4C735CB085B8015B480F8A28BDD50232A1385C7,
	WebViewObject_CanGoForward_m6DFD772C07CDB64E5D61BC55ECC50B8A8AF33DC6,
	WebViewObject_GoBack_m5947506758751DBED1F2229E5A16C7FA85A9C3FA,
	WebViewObject_GoForward_m53FE4DE6920EBCF2C1F8841C975A4A938C9ADA3F,
	WebViewObject_Reload_m3C76F5E71CD39151C33EDF3C8D2A5D44414D458C,
	WebViewObject_CallOnError_mA0D59464A9DEFC616FB4AB4C8CE68595EE1B014C,
	WebViewObject_CallOnHttpError_m7C685D4E301209DA45AC0C587F900D09B4174F3E,
	WebViewObject_CallOnStarted_m33E7CC35162E85E3AF1FF0906CDB6D02B8A2F9EB,
	WebViewObject_CallOnLoaded_m4E2519EE9D1014784CF02AF00E0FCB473F0CD61D,
	WebViewObject_CallFromJS_m1A086ADB71334EEE53DBC6E4C5A68955F8356E81,
	WebViewObject_CallOnHooked_mBD21B01C8B3E27C5867D7221BEF6ACEB0C14EB5F,
	WebViewObject_AddCustomHeader_mB9207F036B5B938D1606B062825ED1F2EFC4903D,
	WebViewObject_GetCustomHeaderValue_mDBA6E91C38D5CD70395D29101529D0CF2BC9C918,
	WebViewObject_RemoveCustomHeader_m13B572E1B740C274EA0A7B30E8E0884AC203E0D3,
	WebViewObject_ClearCustomHeader_m9D8C91F6C18628ABC0FAEEAFE2F25330EEE3369E,
	WebViewObject_ClearCookies_mD08727CF5BCE0FF8F5FECE1CE090026E8BCCC7C5,
	WebViewObject_SaveCookies_m6081145D12FB3149742F2A158AAA259D62C74F89,
	WebViewObject_GetCookies_mA36C6AA27F87AFEB30E2EF8C32976E868F1943EF,
	WebViewObject_SetBasicAuthInfo_mA4AE183063615D3D69E02C1FB1B44D824B9B7FD2,
	WebViewObject_ClearCache_m685CFFA50D94672BB29EEBFA16354B732159523A,
	WebViewObject_SetTextZoom_m5A3A9F812A77504D8FCD5582C7AF44B26CAE61AB,
	WebViewObject__ctor_m6BABBDBFF7B566CB90576F521C33FFD6FE103017,
};
static const int32_t s_InvokerIndices[73] = 
{
	5515,
	5332,
	5747,
	8236,
	5850,
	7794,
	7794,
	7794,
	7794,
	7794,
	6296,
	7798,
	7121,
	7798,
	8236,
	8133,
	8133,
	8609,
	8609,
	8609,
	7121,
	7571,
	7798,
	8609,
	8766,
	8766,
	8427,
	7121,
	7794,
	8694,
	7,
	5515,
	5515,
	5515,
	2558,
	370,
	4321,
	5332,
	4321,
	4321,
	4321,
	5332,
	4321,
	5332,
	4321,
	4321,
	1076,
	4418,
	2478,
	4418,
	5392,
	5332,
	5332,
	5515,
	5515,
	5515,
	4418,
	4418,
	4418,
	4418,
	4418,
	4418,
	2478,
	3907,
	4418,
	5515,
	5515,
	5515,
	3907,
	2478,
	4321,
	4386,
	5515,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharpU2Dfirstpass_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharpU2Dfirstpass_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpU2Dfirstpass_CodeGenModule = 
{
	"Assembly-CSharp-firstpass.dll",
	73,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharpU2Dfirstpass_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
